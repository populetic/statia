import { AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ListColumn } from './list-column.model';

@Component({
    // tslint:disable-next-line: component-selector
    selector: 'fury-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ListComponent implements AfterViewInit {
    @Input() name: string;
    @Input() columns: ListColumn[];

    @ViewChild('filter') filter: ElementRef;
    @Output() filterChange = new EventEmitter<string>();

    @ViewChild('clear') clearFilters: ElementRef;
    @Output() evtClearFilter = new EventEmitter<string>();

    @Input() hideHeader: boolean;
    @Input() hideAdvancedFilter: boolean;
    @Input() hasAdvancedFilter: boolean;
    @Input() searchFilterActivated: boolean;
    @Input() advancedFilterActivated: boolean;
    @Input() isDisabled: boolean;

    constructor() {
    }

    ngAfterViewInit() {
        if (!this.hideHeader) {
            fromEvent(this.filter.nativeElement, 'keyup').pipe(
                distinctUntilChanged(),
                debounceTime(150)
            ).subscribe(() => {
                this.filterChange.emit(this.filter.nativeElement.value);
            });

            fromEvent(this.clearFilters.nativeElement, 'click').subscribe(() => {
                this.filter.nativeElement.value = '';
                this.evtClearFilter.emit();
            });
        }
    }

    columnUsedToFilter(column: any, event: any) {
        event.stopPropagation();
        event.stopImmediatePropagation();
        column.isFilterSelected = !column.isFilterSelected;
    }

    showAdvancedFilter() {
        this.hideAdvancedFilter = !this.hideAdvancedFilter;
    }
}
