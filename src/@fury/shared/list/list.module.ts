import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { ListComponent } from './list.component';

import { SharedModule } from '@shared/modules/shared.module';

@NgModule({
    imports: [
        SharedModule,
        TranslateModule
    ],
    declarations: [
        ListComponent
    ],
    exports: [ListComponent]
})
export class ListModule {
}
