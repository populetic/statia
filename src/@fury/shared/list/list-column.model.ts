export class ListColumn {
    name?: string;
    property?: string;
    visible?: boolean;
    canFilter?: boolean;
    isAdvancedFilter?: boolean;
    isFilterSelected?: boolean;
    onlyForAdmin?: boolean;
    isBoolean?: boolean;
    isModelProperty?: boolean;
    isDate?: boolean;
    isSimpleFilter?: boolean;
    isNumber?: boolean;
    isCheckBox?: boolean;
    isCurrency?: boolean;
    displayFn?: any;
    advancedFilterValues?: any;
    canBeTranslated?: boolean;
    sort?: number;
}
