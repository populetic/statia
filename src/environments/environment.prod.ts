export const environment = {
    production: true,
    googleMapsApiKey: '',
    apiUrl: 'http://api.populetic.com',
    apiUrlStatia: 'http://api.populetic.com/statia',
    apiUrlPopuletic: 'http://api.populetic.com/populetic'
};
