export class LoginContract {
    id: string;
    username: string;
    email: string;
    name: string;
    surnames: string;
    completeName: string;
    countryId: string;
    countryName: string;
    languageId: string;
    languageName: string;
    languageCode: string;
    roleId: string;
    roleName: string;
    lawFirmId: string;
    lawFirmName: string;
    isActive: string;
    token: string;
    accessDate?: string;
    actualLanguage?: string;
    mainMenuId?: string;
    isAdmin: string;
    isLawFirmAdmin: string;

}
