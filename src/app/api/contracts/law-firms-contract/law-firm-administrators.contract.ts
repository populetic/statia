export class LawFirmAdministratorsContract {
    lawFirmId: string;
    userId: string;
    username: string;
    name: string;
    surnames: string;
}
