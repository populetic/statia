export class LawFirmContract {
    id?: string;
    name?: string;
    countryId?: string;
    countryName?: string;
    email?: string;
    phone?: string;
    fax?: string;
    address?: string;
    website?: string;
    createdAt?: string;
    updatedAt?: string;
}
