export class RoleContract {
    id?: string;
    name?: string;
    departmentId?: string;
    departmentName?: string;
    mainMenuId?: string;
    mainMenuName?: string;
    isAdmin?: string;
}
