
export class AirportContract {
    id: string;
    name: string;
    iata: string;
    nameAndIata: string;
    icao: string;
    countryId: string;
    countryName: string;
    cityId: string;
    cityName: string;
    isMain: string;
    latitude: string;
    longitude: string;
    createdAt: string;
    updatedAt: string;
}
