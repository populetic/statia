
export class AirlineContract {
    id: string;
    name: string;
    iata: string;
    nameAndIata: string;
    icao: string;
    flightstatsIata: string;
    isBankruptcy: string;
    hasCommunityLicense: string;
    isActive: string;
    createdAt: string;
    updatedAt: string;
}
