export class BillContract {
    id: string;
    series?: string;
    number?: string;
    createdAt?: string;
    total?: string;
    base?: string;
    billPreferenceId?: string;
    billTypeId?: string;
    billTypeName?: string;
    claimBillingDataId?: string;
    claimId?: string;
    clientId?: string;
    comments?: any; // TODO:
    disruptionTypeId?: string;
    expiresAt?: string;
    flightId?: string;
    iva?: string;
    ivaTotal?: string;
    parentBillId?: any; // TODO:
    billRef?: string;
    menuId?: string;
}
