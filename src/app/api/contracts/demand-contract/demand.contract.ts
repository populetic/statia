export class DemandContract {

    id?: string;
    demandId?: string;
    procedureNumber?: string;
    judgementNumber?: string;
    courtNumberId?: string;
    courtNumber?: string;
    amountRequested?: string;
    amountReceived?: string;
    amountInterests?: string;
    comments?: any; // TODO:
    isNoShow?: string;
    isOverseas?: string;
    hasFlightstats?: string;
    presentedAt?: string;
    admitedAt?: string;
    judgedAt?: string;
    cancellatedAt?: string;
    createdAt?: string;
    updatedAt?: string;
    courtTypeId?: string;
    courtTypeName?: string;
    courtTypeCountryId?: string;
    courtCityId?: string;
    courtCityName?: string;
    menuId?: string;
    disruptionTypeId?: string;
    lawsuiteTypeId?: string;
    airlineName?: string;
    airlineId?: string;
    lawFirmId?: string;
    lawFirmName?: string;

}
