export class PartnerContract {
    id: string;
    fiscalName: string;
    businessName: string;
    fee: string;
    isActive: string;
    createdAt: string;
    updatedAt: string;
}
