export class CourtContract {
    id?: string;
    courtTypeId?: string;
    address?: string;
    postalCode?: string;
    cityId?: string;
    countryId?: string;
    createdAt?: string;
    updatedAt?: string;
    courtTypeName?: string;
    cityName?: string;
    countryName?: string;
    name?: string;
}
