export class CourtTypeContract {
    id?: string;
    name?: string;
    countryId?: string;
    countryName?: string;
}

