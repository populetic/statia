export class CourtNumberContract {
    id: string;
    courtId?: string;
    courtTypeId?: string;
    courtTypeName?: string;
    address?: string;
    postalCode?: string;
    cityId?: string;
    cityName?: string;
    countryId?: string;
    countryName?: string;
    number?: string;
    fax?: string;
    phone?: string;
    createdAt?: string;
    updatedAt?: string;
}
