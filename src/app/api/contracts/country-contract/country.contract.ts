export class CountryContract {
    competencePriority: string;
    id: string;
    isActive: string;
    isUe: string;
    iso: string;
    name: string;
    regionId: string;
    regionName: string;
}
