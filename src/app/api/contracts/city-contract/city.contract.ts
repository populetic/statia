export class CityContract {
    id: string;
    countryId: string;
    countryName: string;
    name: string;
    isActive: string;
}
