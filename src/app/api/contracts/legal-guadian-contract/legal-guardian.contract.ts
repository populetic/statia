export class LegalGuardianContract {
    id: string;
    name: string;
    surnames: string;
    idCard: string;
    documentationTypeId: string;
    documentationTypeName: string;
    idCardExpiresAt: string;
}
