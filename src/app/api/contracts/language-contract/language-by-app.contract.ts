export class LanguageByAppContract {
    appId: string;
    appName: string;
    languageCode: string;
    languageName: string;
    languageClaimreference: string;
    isDefault: boolean;
}
