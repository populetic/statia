export class LanguageContract {
    id: string;
    code: string;
    name: string;
}
