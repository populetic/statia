import { PropertyContract } from '@app/layout/dialogs/menu-property-dialog/property.contract';
import { OptionContract } from '../option-contract/option.contract';

export class MenuContract {
    id?: string;
    menu?: string;
    name: string;
    parentId: string;
    position: string;
    icon: string;
    hasCheckbox: string;
    hasMultipleDelete: string;
    hasTableMenu: string;
    options?: Array<OptionContract>;
    properties?: Array<PropertyContract>;
    parentName?: string;
}
