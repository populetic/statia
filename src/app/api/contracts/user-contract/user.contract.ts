export class UserContract {
    id: string;
    isActive: string;
    name: string;
    surnames: string;
    username: string;
    completeName: string;
    email: string;
    roleId: string;
    roleName: string;
    isAdmin: string;
    isLawFirmAdmin: string;
    isSystem: string;
    countryId: string;
    countryName: string;
    languageId: string;
    languageCode: string;
    languageName: string;
    lawFirmId: string;
    lawFirmName: string;
    password: string;
    createdAt: string;
    updatedAt: string;
}
