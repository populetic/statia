export class OptionContract {
    id: string;
    name: string;
    icon: string;
    onHeaderMenu: string;
    onTableMenu: string;
    position?: string;

    constructor(contract?: OptionContract) {
        this.id = (contract && contract.id ) ? contract.id : null;
        this.name = (contract && contract.name ) ? contract.name : null;
        this.icon = (contract && contract.icon ) ? contract.icon : null;
        this.onHeaderMenu = (contract && contract.onHeaderMenu ) ? contract.onHeaderMenu : '0';
        this.onTableMenu = (contract && contract.onTableMenu ) ? contract.onTableMenu : '0';
        this.position = (contract && contract.position ) ? contract.position : null;
    }
}
