export class ProvinceContract {
    id: string;
    countryId: string;
    countryName: string;
    name: string;
    isActive: string;
}
