export class CompanionContract {
    claimId?: string;
    name?: string;
    surnames?: string;
    email?: string;
    idCard: string;
    claimStatusId?: string;
    claimDocumentationStatusId?: string;
    finishedAt?: string;
    companionClaimId?: string;
    companionName?: string;
    companionSurnames?: string;
    companionEmail?: string;
    isFinished?: string;
    amountEstimated?: string;
    amountTickets?: string;
}
