import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ApiAirlineAlternativeService } from './services/api-airline-alternative-service/api-airline-alternative.service';
import { ApiAirlineReasonService } from './services/api-airline-reason-service/api-airline-reason.service';
import { ApiAirlineService } from './services/api-airlines-service/api-airlines.service';
import { ApiAirportService } from './services/api-airport-service/api-airport.service';
import { ApiCityService } from './services/api-city-service/api-city.service';
import { ApiClaimService } from './services/api-claim-service/api-claim.service';
import { ApiClientService } from './services/api-client-service/api-client.service';
import { ApiCountryService } from './services/api-country-service/api-country.service';
import { ApiCourtNumberService } from './services/api-court-service/api-court-number.service';
import { ApiCourtTypesService } from './services/api-court-service/api-court-type.service';
import { ApiCourtService } from './services/api-court-service/api-court.service';
import { ApiDemandService } from './services/api-demand-service/api-demand.service';
import { ApiDepartmentService } from './services/api-department-service/api-department.service';
import { ApiDocumentationTypeService } from './services/api-documentation-type-service/api-documentation-type.service';
import { ApiClaimConnectionFlightsService } from './services/api-flight-service/api-claim-connection-flights.service';
import { ApiConnectionFlightService } from './services/api-flight-service/api-connection-flights.service';
import { ApiFlightService } from './services/api-flight-service/api-flight.service';
import { ApiLanguageByAppService } from './services/api-language-service/api-language-by-app.service';
import { ApiLanguageService } from './services/api-language-service/api-language.service';
import { ApiLawFirmAdministratorService } from './services/api-law-firm-services/api-law-firm-administrators.service';
import { ApiLawFirmService } from './services/api-law-firm-services/api-law-firm.service';
import { ApiLawsuiteTypeService } from './services/api-lawsuite-type-service/api-lawsuite-type.service';
import { ApiLegalGuardianService } from './services/api-legal-guardian-service/api-legal-guardian.service';
import { ApiLoginService } from './services/api-login.service/api-login.service';
import { ApiLuggageIssueMomentService } from './services/api-luggage-issue-moment-service/api-luggage-issue-moment.service';
import { ApiMenuService } from './services/api-menu-service/api-menu.service';
import { ApiOptionService } from './services/api-options-service/api-option.service';
import { ApiPartnerService } from './services/api-partner-service/api-partner.service';
import { ApiProvinceService } from './services/api-province-service/api-province.service';
import { ApiRoleService } from './services/api-role-service/api-role.service';
import { ApiUserService } from './services/api-user-service/api-user.service';

@NgModule({
    declarations: [],
    imports: [
      CommonModule
    ],
    providers: [
        ApiLawFirmAdministratorService,
        ApiLawFirmService,
        ApiCourtService,
        ApiCourtNumberService,
        ApiCourtTypesService,
        ApiMenuService,
        ApiOptionService,
        ApiUserService,
        ApiRoleService,
        ApiDepartmentService,
        ApiLanguageByAppService,
        ApiLanguageService,
        ApiCityService,
        ApiAirportService,
        ApiAirlineService,
        ApiCountryService,
        ApiClientService,
        ApiClaimService,
        ApiFlightService,
        ApiClaimConnectionFlightsService,
        ApiConnectionFlightService,
        ApiProvinceService,
        ApiLoginService,
        ApiAirlineAlternativeService,
        ApiPartnerService,
        ApiDocumentationTypeService,
        ApiLegalGuardianService,
        ApiLawsuiteTypeService,
        ApiAirlineReasonService,
        ApiLuggageIssueMomentService,
        ApiDemandService,
    ]
  })
  export class ApiModule {}
