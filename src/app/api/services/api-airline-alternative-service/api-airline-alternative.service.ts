import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AirlineAlternativeContract } from '@app/pages/company/auxiliaries/airline-alternatives/airline-alternative.contract';
import { Airline } from '@app/pages/company/auxiliaries/airlines/airline.model';
import { BaseApiServiceInterface } from '@app/shared/interfaces/base-api-service.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class ApiAirlineAlternativeService implements BaseApiServiceInterface<AirlineAlternativeContract> {
    public resourceUrl: string = environment.apiUrl + '/airline-alternatives';
    public headers: HttpHeaders;

    constructor(private http: HttpClient) {}

    getAll(param: CustomParams = null): Observable<Result<AirlineAlternativeContract[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(param: CustomParams = null): Promise<Result<AirlineAlternativeContract[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number|string): Observable<Result<AirlineAlternativeContract>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(param: Airline): Observable<Result<AirlineAlternativeContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: Airline): Promise<Result<AirlineAlternativeContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: Airline): Observable<Result<AirlineAlternativeContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: Airline): Promise<Result<AirlineAlternativeContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: CustomParams): Observable<Result<any>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const Airlines = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', Airlines);
    }
}
