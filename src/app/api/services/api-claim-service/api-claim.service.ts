import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ClaimContract } from '@app/api/contracts/claim-contract/claim.contract';
import { BaseApiServiceInterface } from '@app/shared/interfaces/base-api-service.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { SessionService } from '@app/shared/services/session.service';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { Companion } from '@app/pages/company/companions/companion.model';
import { CompanionContract } from '@app/api/contracts/companion-contract/companion.contract';

@Injectable({ providedIn: 'root' })
export class ApiClaimService implements BaseApiServiceInterface<ClaimContract> {
    public resourceUrl: string = environment.apiUrl + '/claims';
    public headers: HttpHeaders;

    constructor(
        private http: HttpClient,
        private sessionService: SessionService
    ) {}


    getAll(customParams: CustomParams = null): Observable<Result<ClaimContract[]>> {
        const queryString = StatiaFunctions.getQueryString(customParams);
        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<ClaimContract[]>> {
        const queryString = StatiaFunctions.getQueryString(customParams);
        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getAllByEmailAndStatus(customParams: CustomParams = null): Observable<Result<ClaimContract[]>> {
        const queryString = StatiaFunctions.getQueryString(customParams);
        return this.http.get(environment.apiUrl + '"/claims-by-email-and-status/' + queryString);
    }

    getByRef(id: number|string): Observable<Result<ClaimContract>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    getById(id: number|string): Observable<Result<ClaimContract>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(param: ClaimContract): Observable<Result<ClaimContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: ClaimContract): Promise<Result<ClaimContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: ClaimContract): Observable<Result<ClaimContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: ClaimContract): Promise<Result<ClaimContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: CustomParams): Observable<Result<any>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const Claims = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', Claims);
    }

    getAllCompanionsByMainClaimId(id: string) {
        return this.http.get(environment.apiUrl + '/claims-get-all-companions/' + id);
    }

    getAllPossibleSameFlightTravelers(claimId: string): Observable<Result<CompanionContract[]>> {
        return this.http.get(environment.apiUrl + '/claims-get-all-possible-same-flight-travelers/' + claimId);
    }
}
