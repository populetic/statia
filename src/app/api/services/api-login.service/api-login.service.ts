import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginContract } from '@app/api/contracts/login-contract/login.contract';
import { BaseApiServiceInterface } from '@app/shared/interfaces/base-api-service.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';


@Injectable()
export class ApiLoginService {
    public resourceUrl: string = environment.apiUrl + '/users';
    public headers: HttpHeaders;

    constructor(
        private http: HttpClient
    ) { }

    login(param: CustomParams): Observable<Result<LoginContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/login', params);
    }

}
