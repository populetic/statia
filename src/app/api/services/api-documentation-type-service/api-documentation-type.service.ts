import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DocumentationTypeContract } from '@app/pages/company/auxiliaries/document-types/documentation-type.contract';
import { DocumentationType } from '@app/pages/company/auxiliaries/document-types/documentation-type.model';
import { BaseApiServiceInterface } from '@app/shared/interfaces/base-api-service.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ApiDocumentationTypeService implements BaseApiServiceInterface<DocumentationTypeContract> {
    public resourceUrl: string = environment.apiUrl + '/documentation-types';
    public headers: HttpHeaders;

    constructor(
        private http: HttpClient
    ) {}

    getAll(param: CustomParams = null): Observable<Result<DocumentationTypeContract[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(param: CustomParams = null): Promise<Result<DocumentationTypeContract[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number|string): Observable<Result<DocumentationTypeContract>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(param: DocumentationTypeContract): Observable<Result<DocumentationTypeContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: DocumentationTypeContract): Promise<Result<DocumentationTypeContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: DocumentationTypeContract): Observable<Result<DocumentationTypeContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: DocumentationTypeContract): Promise<Result<DocumentationTypeContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<DocumentationTypeContract>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const DocumentationTypes = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', DocumentationTypes);
    }

    public get userMainDocumentationTypes(): DocumentationType[] {
        return [
            new DocumentationType({id: '1', name: 'id_card'}),
            new DocumentationType({id: '2', name: 'passport'}),
            new DocumentationType({id: '14', name: 'tax_identification'})
        ];
    }

}
