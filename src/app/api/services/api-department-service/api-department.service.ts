import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DepartmentContract } from '@app/api/contracts/department-contract/department.contract';
import { BaseApiServiceInterface } from '@app/shared/interfaces/base-api-service.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';


@Injectable()
export class ApiDepartmentService implements BaseApiServiceInterface<DepartmentContract> {
    public resourceUrl: string = environment.apiUrlStatia + '/departments';
    public headers: HttpHeaders;

    constructor(
        private http: HttpClient
    ) { }

    getAll(customParams: CustomParams = null): Observable<Result<DepartmentContract[]>> {
        const queryString: string = StatiaFunctions.getQueryString(customParams);

        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<DepartmentContract[]>> {
        const queryString: string = StatiaFunctions.getQueryString(customParams);

        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number | string): Observable<Result<DepartmentContract>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(contract: DepartmentContract): Observable<Result<DepartmentContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(contract: DepartmentContract): Promise<Result<DepartmentContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number | string, contract: DepartmentContract): Observable<Result<DepartmentContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number | string, contract: DepartmentContract): Promise<Result<DepartmentContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(customParams: DepartmentContract): Observable<Result<DepartmentContract>> {
        const json: any = JSON.stringify(customParams);
        const stringParams: string = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const menuHeader = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', menuHeader);
    }
}
