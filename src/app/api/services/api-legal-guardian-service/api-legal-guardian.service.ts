import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LegalGuardianContract } from '@app/api/contracts/legal-guadian-contract/legal-guardian.contract';
import { BaseApiServiceInterface } from '@app/shared/interfaces/base-api-service.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ApiLegalGuardianService implements BaseApiServiceInterface<LegalGuardianContract> {
    public resourceUrl = environment.apiUrl + '/legal-guardians';
    public headers: HttpHeaders;

    constructor(private http: HttpClient) {}

    getAll(param: CustomParams = null): Observable<Result<LegalGuardianContract[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(param: CustomParams = null): Promise<Result<LegalGuardianContract[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number|string): Observable<Result<LegalGuardianContract>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(contract: LegalGuardianContract): Observable<Result<LegalGuardianContract>> {
        const json = JSON.stringify(contract);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(contract: LegalGuardianContract): Promise<Result<LegalGuardianContract>> {
        const json = JSON.stringify(contract);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, contract: LegalGuardianContract): Observable<Result<LegalGuardianContract>> {
        const json = JSON.stringify(contract);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, contract: LegalGuardianContract): Promise<Result<LegalGuardianContract>> {
        const json = JSON.stringify(contract);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: CustomParams): Observable<Result<LegalGuardianContract>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const LegalGuardians = { headers: this.headers, params };
        return this.http.delete(this.resourceUrl + '/delete', LegalGuardians);
    }
}
