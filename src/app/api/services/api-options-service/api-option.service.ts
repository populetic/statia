import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { OptionContract } from '@app/api/contracts/option-contract/option.contract';
import { BaseApiServiceInterface } from '@app/shared/interfaces/base-api-service.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class ApiOptionService implements BaseApiServiceInterface<OptionContract> {
    public resourceUrl: string = environment.apiUrlStatia + '/options';
    public headers: HttpHeaders;

    constructor(
        private http: HttpClient
    ) { }

    getAll(customParams: CustomParams = null): Observable<Result<OptionContract[]>> {
        const queryString: string = StatiaFunctions.getQueryString(customParams);

        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<OptionContract[]>> {
        const queryString: string = StatiaFunctions.getQueryString(customParams);

        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number | string): Observable<Result<OptionContract>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(contract: OptionContract): Observable<Result<OptionContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(contract: OptionContract): Promise<Result<OptionContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number | string, contract: OptionContract): Observable<Result<OptionContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number | string, contract: OptionContract): Promise<Result<OptionContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(customParams: CustomParams): Observable<Result<any>> {
        const json: any = JSON.stringify(customParams);
        const stringParams: string = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const menuHeader = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', menuHeader);
    }

    editRoleMenusOptions(optionContract: OptionContract): Observable<Result<OptionContract>> {
        const json = JSON.stringify(optionContract);
        const params = 'json=' + json;

        return this.http.post(environment.apiUrlStatia + '/menus-role-options/edit', params);
    }
}
