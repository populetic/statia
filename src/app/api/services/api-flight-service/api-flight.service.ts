import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FlightContract } from '@app/api/contracts/flight-contract/flight.contract';
import { BaseApiServiceInterface } from '@app/shared/interfaces/base-api-service.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { Flight } from '@app/pages/company/flights/flight.model';

@Injectable()
export class ApiFlightService implements BaseApiServiceInterface<FlightContract> {
    public resourceUrl: string = environment.apiUrl + '/flights';
    public headers: HttpHeaders;

    constructor(private http: HttpClient) { }

    getAll(customParams: CustomParams = null): Observable<Result<FlightContract[]>> {
        const queryString: string = StatiaFunctions.getQueryString(customParams);

        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<FlightContract[]>> {
        const queryString: string = StatiaFunctions.getQueryString(customParams);

        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number | string): Observable<Result<FlightContract>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(contract: FlightContract): Observable<Result<FlightContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(contract: FlightContract): Promise<Result<FlightContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number | string, contract: FlightContract): Observable<Result<FlightContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editByModel(id: number | string, model: Flight): Observable<Result<FlightContract>> {
        const json: any = JSON.stringify(model);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number | string, contract: FlightContract): Promise<Result<FlightContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(customParams: FlightContract): Observable<Result<FlightContract>> {
        const json: any = JSON.stringify(customParams);
        const stringParams: string = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const Courts = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', Courts);
    }
}
