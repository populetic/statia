import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FlightContract } from '@app/api/contracts/flight-contract/flight.contract';
import { ConnectionFlightContract } from '@app/pages/company/connection-flights/connection-flight.contract';
import { BaseApiServiceInterface } from '@app/shared/interfaces/base-api-service.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class ApiClaimConnectionFlightsService implements BaseApiServiceInterface<ConnectionFlightContract> {
    public resourceUrl: string = environment.apiUrl + '/claim-connection-flights';
    public headers: HttpHeaders;

    constructor(private http: HttpClient) { }

    getAll(customParams: CustomParams = null): Observable<Result<ConnectionFlightContract[]>> {
        console.log('customParams', customParams)

        return this.http.get(this.resourceUrl + '/' + customParams.claimId);
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<ConnectionFlightContract[]>> {
        const queryString: string = StatiaFunctions.getQueryString(customParams);

        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number | string): Observable<Result<ConnectionFlightContract>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(contract: ConnectionFlightContract): Observable<Result<ConnectionFlightContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.post(this.resourceUrl + '/create/' + contract?.claimId, params);
    }

    createAsync(contract: ConnectionFlightContract): Promise<Result<ConnectionFlightContract>> {
        console.log('contract', contract)
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.post(this.resourceUrl + '/create/' + contract?.claimId, params).toPromise();
    }

    edit(id: number | string, contract: ConnectionFlightContract): Observable<Result<ConnectionFlightContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number | string, contract: ConnectionFlightContract): Promise<Result<ConnectionFlightContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(customParams: ConnectionFlightContract): Observable<Result<ConnectionFlightContract>> {
        const json: any = JSON.stringify(customParams);
        const stringParams: string = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const Courts = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', Courts);
    }

    getAllClaimConnectionFlightsByClaimId(id: number | string): Observable<Result<ConnectionFlightContract>> {
        return this.http.get(environment.apiUrl + '/claim-connection-flights/' + id);
    }

    deleteAllClaimConnectionFlightByClaimId(id: string): Observable<Result<ConnectionFlightContract>> {
        return this.http.delete(this.resourceUrl + '/claim-connection-flights/delete-all/' + id);
    }

    addClaimConnectionFlightsAsync(claimId: any, param: any): Promise<any> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);

        return this.http.post(this.resourceUrl + '/add/' + claimId, params).toPromise();
    }
}
