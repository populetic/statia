import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DemandContract } from '@app/api/contracts/demand-contract/demand.contract';
import { DemandRelatedClaim } from '@app/pages/company/demands/demand-related-claims.interface';
import { BaseApiServiceInterface } from '@app/shared/interfaces/base-api-service.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ApiDemandService implements BaseApiServiceInterface<DemandContract> {
    public resourceUrl: string = environment.apiUrl + '/demands';
    public headers: HttpHeaders;

    constructor( private http: HttpClient ) {}

    getAll(param: CustomParams = null): Observable<Result<DemandContract[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(param: CustomParams = null): Promise<Result<DemandContract[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number|string): Observable<Result<DemandContract>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(param: DemandContract): Observable<Result<DemandContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: DemandContract): Promise<Result<DemandContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: DemandContract): Observable<Result<DemandContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: DemandContract): Promise<Result<DemandContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<any>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const Demands = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', Demands);
    }

    createDemandRelatedClaims(demandId: string, claimIds: string[]): Promise<Result<any>> {
        const json = JSON.stringify({demandId, claimIds});
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(environment.apiUrl + '/demand-related-claims/create' , params).toPromise();
    }

    changeClaimLawFirm(lawFirmId: string, claimIds: string[]): Promise<Result<any>> {
        const json = JSON.stringify({lawFirmId, claimIds});
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(environment.apiUrl + '/claims-change-law-firm' , params).toPromise();
    }

    // TODO: DemandRelatedClaim => DemandRelatedClaimContract
    getRelatedClaims(demandId: string): Observable<Result<DemandRelatedClaim[]>> {
        return this.http.get(environment.apiUrl + '/demand-related-claims/' + demandId);
    }
}
