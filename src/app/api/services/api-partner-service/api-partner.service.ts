import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PartnerContract } from '@app/api/contracts/partner-contract/partner.contract';
import { Partner } from '@app/pages/company/partners/partner.model';
import { BaseApiServiceInterface } from '@app/shared/interfaces/base-api-service.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ApiPartnerService implements BaseApiServiceInterface<PartnerContract> {
    public resourceUrl: string = environment.apiUrl + '/partners';
    public headers: HttpHeaders;

    constructor( private http: HttpClient ) {}

    getAll(param: CustomParams = null): Observable<Result<PartnerContract[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(param: CustomParams = null): Promise<Result<PartnerContract[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number|string): Observable<Result<PartnerContract>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(param: PartnerContract): Observable<Result<PartnerContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: PartnerContract): Promise<Result<PartnerContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: PartnerContract): Observable<Result<PartnerContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: PartnerContract): Promise<Result<PartnerContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<PartnerContract>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const Partners = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', Partners);
    }
}
