import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LawsuiteTypeContract } from '@app/api/contracts/lawsuite-type-contract/lawsuite-type.contract';
import { BaseApiServiceInterface } from '@app/shared/interfaces/base-api-service.interface';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ApiLawsuiteTypeService implements BaseApiServiceInterface<LawsuiteTypeContract> {
    public resourceUrl: string = environment.apiUrl + '/lawsuite-types';
    public headers: HttpHeaders;

    constructor(
        private http: HttpClient
    ) {}

    getAll(param: CustomParams = null): Observable<Result<LawsuiteTypeContract[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(param: CustomParams = null): Promise<Result<LawsuiteTypeContract[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number|string): Observable<Result<LawsuiteTypeContract>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(param: LawsuiteTypeContract): Observable<Result<LawsuiteTypeContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: LawsuiteTypeContract): Promise<Result<LawsuiteTypeContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: LawsuiteTypeContract): Observable<Result<LawsuiteTypeContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: LawsuiteTypeContract): Promise<Result<LawsuiteTypeContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<LawsuiteTypeContract>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const LawsuiteTypes = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', LawsuiteTypes);
    }
}
