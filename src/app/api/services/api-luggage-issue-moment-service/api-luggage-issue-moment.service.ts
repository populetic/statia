import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LuggageIssueMomentContract } from '@app/api/contracts/luggage-issue-moment-contract/luggage-issue-moment.contract';
import { BaseApiServiceInterface } from '@app/shared/interfaces/base-api-service.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ApiLuggageIssueMomentService implements BaseApiServiceInterface<LuggageIssueMomentContract> {
    public resourceUrl: string = environment.apiUrl + '/luggage-issue-moment';
    public headers: HttpHeaders;

    constructor(
        private http: HttpClient
    ) {}

    getAll(param: CustomParams = null): Observable<Result<LuggageIssueMomentContract[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(param: CustomParams = null): Promise<Result<LuggageIssueMomentContract[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number|string): Observable<Result<LuggageIssueMomentContract>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(param: LuggageIssueMomentContract): Observable<Result<LuggageIssueMomentContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: LuggageIssueMomentContract): Promise<Result<LuggageIssueMomentContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: LuggageIssueMomentContract): Observable<Result<LuggageIssueMomentContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: LuggageIssueMomentContract): Promise<Result<LuggageIssueMomentContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<LuggageIssueMomentContract>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const LuggageIssueMoments = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', LuggageIssueMoments);
    }
}
