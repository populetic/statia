import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BillContract } from '@app/api/contracts/bill-contract/bill.contract';
import { ClaimContract } from '@app/api/contracts/claim-contract/claim.contract';
import { ClientContract } from '@app/api/contracts/clientscontract/client.contract';
import { DemandContract } from '@app/api/contracts/demand-contract/demand.contract';
import { BaseApiServiceInterface } from '@app/shared/interfaces/base-api-service.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class ApiClientService implements BaseApiServiceInterface<ClientContract> {
    public resourceUrl: string = environment.apiUrl + '/clients';
    public headers: HttpHeaders;

    constructor(private http: HttpClient) { }

    getAll(customParams: CustomParams = null): Observable<Result<ClientContract[]>> {
        const queryString: string = StatiaFunctions.getQueryString(customParams);

        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<ClientContract[]>> {
        const queryString: string = StatiaFunctions.getQueryString(customParams);

        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number | string): Observable<Result<ClientContract>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(contract: ClientContract): Observable<Result<ClientContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(contract: ClientContract): Promise<Result<ClientContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number | string, contract: ClientContract): Observable<Result<ClientContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number | string, contract: ClientContract): Promise<Result<ClientContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(customParams: ClientContract): Observable<Result<ClientContract>> {
        const json: any = JSON.stringify(customParams);
        const stringParams: string = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const menuHeader = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', menuHeader);
    }

    getRelatedClaims(clientId: string): Observable<Result<ClaimContract[]>> {
        return this.http.get(environment.apiUrl + '/client-related-claims' + '/' + clientId);
    }

    getRelatedDemands(clientId: string): Observable<Result<DemandContract[]>> {
        return this.http.get(environment.apiUrl + '/client-related-demands' + '/' + clientId);
    }

    getRelatedBills(clientId: string): Observable<Result<BillContract[]>> {
        return this.http.get(environment.apiUrl + '/client-related-bills/' + clientId);
    }
}
