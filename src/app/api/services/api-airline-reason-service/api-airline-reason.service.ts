import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AirlineReasonContract } from '@app/api/contracts/airline-reason-contract/airline-reason.contract';
import { BaseApiServiceInterface } from '@app/shared/interfaces/base-api-service.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ApiAirlineReasonService implements BaseApiServiceInterface<AirlineReasonContract> {
    public resourceUrl: string = environment.apiUrl + '/airline-reasons';
    public headers: HttpHeaders;

    constructor(private http: HttpClient) {}

    getAll(param: CustomParams = null): Observable<Result<AirlineReasonContract[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(param: CustomParams = null): Promise<Result<AirlineReasonContract[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number|string): Observable<Result<AirlineReasonContract>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(param: AirlineReasonContract): Observable<Result<AirlineReasonContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: AirlineReasonContract): Promise<Result<AirlineReasonContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: AirlineReasonContract): Observable<Result<AirlineReasonContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: AirlineReasonContract): Promise<Result<AirlineReasonContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<AirlineReasonContract>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const AirlineReasons = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', AirlineReasons);
    }
}
