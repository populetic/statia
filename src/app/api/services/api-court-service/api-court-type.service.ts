import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CourtTypeContract } from '@app/api/contracts/court-contract/court-type.contract';
import { BaseApiServiceInterface } from '@app/shared/interfaces/base-api-service.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ApiCourtTypesService implements BaseApiServiceInterface<CourtTypeContract> {
    public resourceUrl: string = environment.apiUrl + '/court-types';
    public headers: HttpHeaders;

    constructor( private http: HttpClient ) {}

    getAll(customParams: CustomParams = null): Observable<Result<CourtTypeContract[]>> {
        const queryString: string = StatiaFunctions.getQueryString(customParams);
        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<CourtTypeContract[]>> {
        const queryString = StatiaFunctions.getQueryString(customParams);
        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number|string): Observable<Result<CourtTypeContract>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(courtTypeContract: CourtTypeContract): Observable<Result<CourtTypeContract>> {
        const json: any = JSON.stringify(courtTypeContract);
        const params: string = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(courtTypeContract: CourtTypeContract): Promise<Result<CourtTypeContract>> {
        const json: any = JSON.stringify(courtTypeContract);
        const params: string = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, courtTypeContract: CourtTypeContract): Observable<Result<CourtTypeContract>> {
        courtTypeContract.id = id.toString();

        const json: any = JSON.stringify(courtTypeContract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, courtTypeContract: CourtTypeContract): Promise<Result<CourtTypeContract>> {
        courtTypeContract.id = id.toString();

        const json: any = JSON.stringify(courtTypeContract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(courtTypeContract: CourtTypeContract): Observable<Result<CourtTypeContract>> {
        const json: any = JSON.stringify(courtTypeContract);
        const stringParams: string = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const CourtTypes = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', CourtTypes);
    }
}
