import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseApiServiceInterface } from '@app/shared/interfaces/base-api-service.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { CourtNumberContract } from '@app/api/contracts/court-contract/court-number.contract';

@Injectable({ providedIn: 'root' })
export class ApiCourtNumberService implements BaseApiServiceInterface<CourtNumberContract> {
    public resourceUrl: string = environment.apiUrl + '/courts-number';
    public headers: HttpHeaders;

    constructor( private http: HttpClient ) {}

    getAll(customParams: CustomParams = null): Observable<Result<CourtNumberContract[]>> {
        const queryString: string = StatiaFunctions.getQueryString(customParams);
        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<CourtNumberContract[]>> {
        const queryString: string = StatiaFunctions.getQueryString(customParams);
        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number|string): Observable<Result<CourtNumberContract>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(courtNumberContract: CourtNumberContract): Observable<Result<CourtNumberContract>> {
        const json: any = JSON.stringify(courtNumberContract);
        const params: string = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(courtNumberContract: CourtNumberContract): Promise<Result<CourtNumberContract>> {
        const json: any = JSON.stringify(courtNumberContract);
        const params: string = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, courtNumbersContract: CourtNumberContract): Observable<Result<CourtNumberContract>> {
        courtNumbersContract.id = id.toString();

        const json: any = JSON.stringify(courtNumbersContract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, courtNumbersContract: CourtNumberContract): Promise<Result<CourtNumberContract>> {
        courtNumbersContract.id = id.toString();

        const json: any = JSON.stringify(courtNumbersContract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: CourtNumberContract): Observable<Result<CourtNumberContract>> {
        const json: any = JSON.stringify(param);
        const stringParams: string = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const CourtNumbers = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', CourtNumbers);
    }
}
