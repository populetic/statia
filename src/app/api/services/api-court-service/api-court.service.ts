import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CourtContract } from '@app/api/contracts/court-contract/court.contract';
import { BaseApiServiceInterface } from '@app/shared/interfaces/base-api-service.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class ApiCourtService implements BaseApiServiceInterface<CourtContract> {
    public resourceUrl: string = environment.apiUrl + '/courts';
    public headers: HttpHeaders;

    constructor(private http: HttpClient) { }

    getAll(customParams: CustomParams = null): Observable<Result<CourtContract[]>> {
        const queryString: string = StatiaFunctions.getQueryString(customParams);

        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<CourtContract[]>> {
        const queryString: string = StatiaFunctions.getQueryString(customParams);

        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number | string): Observable<Result<CourtContract>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(contract: CourtContract): Observable<Result<CourtContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(contract: CourtContract): Promise<Result<CourtContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number | string, contract: CourtContract): Observable<Result<CourtContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number | string, contract: CourtContract): Promise<Result<CourtContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(customParams: CourtContract): Observable<Result<CourtContract>> {
        const json: any = JSON.stringify(customParams);
        const stringParams: string = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const Courts = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', Courts);
    }
}
