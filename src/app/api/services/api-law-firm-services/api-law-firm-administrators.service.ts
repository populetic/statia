import { environment } from '@environments/environment';
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { Result } from '@app/shared/models/result.model';
import { LawFirmAdministratorsContract } from '@app/api/contracts/law-firms-contract/law-firm-administrators.contract';
import { BaseApiServiceInterface } from '@app/shared/interfaces/base-api-service.interface';

@Injectable({ providedIn: 'root' })
export class ApiLawFirmAdministratorService implements BaseApiServiceInterface<LawFirmAdministratorsContract> {
    resourceUrl: string = environment.apiUrl + '/law-firm-administrators';
    public headers: HttpHeaders;

    constructor( private http: HttpClient ) {  }

    getAll(customParams: CustomParams = null): Observable<Result<LawFirmAdministratorsContract[]>> {
        const queryString: string = StatiaFunctions.getQueryString(customParams);
        return this.http.get(this.resourceUrl + queryString);
    }

    getLawFirmAdministrators(): Promise<Result<LawFirmAdministratorsContract>> {
        return this.http.get(this.resourceUrl).toPromise();
    }

    create(param: CustomParams): Observable<Result<LawFirmAdministratorsContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    deleteByParam(param: CustomParams): Observable<Result<any>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const options = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', options);
    }

    getAllAsync(param: CustomParams): Promise<Result<LawFirmAdministratorsContract[]>> {
        throw new Error('Method not implemented.');
    }
    getById(id: string | number): Observable<Result<LawFirmAdministratorsContract>> {
        throw new Error('Method not implemented.');
    }
    createAsync(param: LawFirmAdministratorsContract): Promise<Result<LawFirmAdministratorsContract>> {
        throw new Error('Method not implemented.');
    }
    edit(id: string | number, param: LawFirmAdministratorsContract): Observable<Result<LawFirmAdministratorsContract>> {
        throw new Error('Method not implemented.');
    }
    editAsync(id: string | number, param: LawFirmAdministratorsContract): Promise<Result<LawFirmAdministratorsContract>> {
        throw new Error('Method not implemented.');
    }
}
