import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LawFirmContract } from '@app/api/contracts/law-firms-contract/law-firm.contract';
import { BaseApiServiceInterface } from '@app/shared/interfaces/base-api-service.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ApiLawFirmService implements BaseApiServiceInterface<LawFirmContract> {
    resourceUrl: string = environment.apiUrl + '/law-firms';
    public headers: HttpHeaders;

    constructor( private http: HttpClient ) {  }

    getAll(customParams: CustomParams = null): Observable<Result<LawFirmContract[]>> {
        const queryString: string = StatiaFunctions.getQueryString(customParams);
        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<LawFirmContract[]>> {
        const queryString: string = StatiaFunctions.getQueryString(customParams);
        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number|string): Observable<Result<LawFirmContract>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    edit(id: number|string, lawFirmContract: LawFirmContract): Observable<Result<LawFirmContract>> {
        lawFirmContract.id = id.toString();

        const json: any = JSON.stringify(lawFirmContract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    create(param: CustomParams): Observable<Result<LawFirmContract>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    deleteByParam(param: CustomParams): Observable<Result<any>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const options = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', options);
    }

    createAsync(param: LawFirmContract): Promise<Result<LawFirmContract>> {
        throw new Error('Method not implemented.');
    }
    editAsync(id: string | number, param: LawFirmContract): Promise<Result<LawFirmContract>> {
        throw new Error('Method not implemented.');
    }
}
