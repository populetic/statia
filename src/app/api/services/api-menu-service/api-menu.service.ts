import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MenuContract } from '@app/api/contracts/menu-contract/menu.contract';
import { BaseApiServiceInterface } from '@app/shared/interfaces/base-api-service.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class ApiMenuService implements BaseApiServiceInterface<MenuContract> {
    public resourceUrl: string = environment.apiUrlStatia + '/menus';
    public headers: HttpHeaders;

    constructor(
        private http: HttpClient
    ) { }

    getAll(customParams: CustomParams = null): Observable<Result<MenuContract[]>> {
        const queryString: string = StatiaFunctions.getQueryString(customParams);

        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<MenuContract[]>> {
        const queryString: string = StatiaFunctions.getQueryString(customParams);

        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number | string): Observable<Result<MenuContract>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(contract: MenuContract): Observable<Result<MenuContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(contract: MenuContract): Promise<Result<MenuContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number | string, contract: MenuContract): Observable<Result<MenuContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number | string, contract: MenuContract): Promise<Result<MenuContract>> {
        const json: any = JSON.stringify(contract);
        const params: string = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(customParams: CustomParams): Observable<Result<any>> {
        const json: any = JSON.stringify(customParams);
        const stringParams: string = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const menuHeader = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', menuHeader);
    }

    getByRoleId(id: number | string): Observable<Result<MenuContract[]>> {
        return this.http.get(this.resourceUrl + '/by-role/' + id, { headers: this.headers });
    }

    deletePropertyByParam(param: CustomParams): Observable<any> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });

        const options = {
            headers: this.headers,
            params
        };

        return this.http.delete(this.resourceUrl + '-properties/delete', options);
    }

    editOptions(param: CustomParams): Observable<any> {
        const json = JSON.stringify(param);
        const params = 'json=' + json;

        return this.http.post(this.resourceUrl + '-options/edit', params, { headers: this.headers });
    }

    createProperty(param: CustomParams): Observable<any> {
        const json = JSON.stringify(param);
        const params = 'json=' + json;

        return this.http.post(this.resourceUrl + '-properties/create', params, { headers: this.headers });
    }

    editProperty(param: CustomParams): Observable<any> {
        const json = JSON.stringify(param);
        const params = 'json=' + json;

        return this.http.put(this.resourceUrl + '-properties/edit', params, { headers: this.headers });
    }

    editRoleMenusOptions(param: CustomParams): Observable<any> {
        const json = JSON.stringify(param);
        const params = 'json=' + json;

        return this.http.post(environment.apiUrlStatia + '/menus-role-options/edit', params);
    }

    getMenusRoleOption(menuId: string, roleId: string): Observable<Result<MenuContract[]>> {
        const param = { menuId: menuId, roleId: roleId };
        const json = JSON.stringify(param);
        const params = 'json=' + json;

        return this.http.post(environment.apiUrlStatia + '/menus-role-options', params);
    }
}
