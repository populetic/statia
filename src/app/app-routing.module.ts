import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from '@app/layout/layout.component';
import { MENU_ID } from './shared/enums/menu.enum';
import { AuthGuard } from './shared/guards/auth.guard';
import { ScopeGuard } from './shared/guards/scope.guard';
import { LoadResolver } from './utils/load-resolver';

const routes: Routes = [
    {
        path: '',
        resolve: { routeResolver: LoadResolver },
        children: [
            {
                path: '',
                canLoad: [AuthGuard],
                component: LayoutComponent,
                children : [
                    {
                        path: 'statia-configuration/menus',
                        loadChildren: () => import('./pages/statia-configuration/menus/menu.module').then(m => m.MenuModule),
                        data: { menuId: MENU_ID.menus },
                        canLoad: [ScopeGuard]
                    },
                    {
                        path: 'statia-configuration/options',
                        loadChildren: () => import('./pages/statia-configuration/options/option.module').then(m => m.OptionModule),
                        data: { menuId: MENU_ID.options },
                        canLoad: [ScopeGuard]
                    },
                    {
                        path: 'customer-management/clients',
                        loadChildren: () => import('./pages/company/clients/client.module').then(m => m.ClientModule),
                        data: { menuId: MENU_ID.clients },
                        canLoad: [ScopeGuard]
                    },
                    {
                        path: 'claims',
                        loadChildren: () => import('./pages/company/claims/claim.module').then(m => m.ClaimModule),
                    },
                    {
                        path: 'accounting-finance/bill-preferences',
                        loadChildren: () => import('./pages/company/bill-preferences/bill-preference.module')
                            .then(m => m.BillPreferenceModule),
                        data: { menuId: MENU_ID['bill-preferences'] },
                        canLoad: [ScopeGuard]
                    },
                    {
                        path: 'accounting-finance/bills',
                        loadChildren: () => import('./pages/company/bills/bill.module').then(m => m.BillModule),
                        data: { menuId: MENU_ID.bills },
                        canLoad: [ScopeGuard]
                    },
                    {
                        path: 'accounting-finance/pending-to-create-bill',
                        loadChildren: () => import('./pages/company/bills/bill.module').then(m => m.BillModule),
                        data: { menuId: MENU_ID['pending-to-create-bill'] },
                        canLoad: [ScopeGuard]
                    },
                    {
                        path: 'accounting-finance/claims-billing-data',
                        loadChildren: () => import('./pages/company/claims-billing-data/claim-billing-data.module')
                            .then(m => m.ClaimBillingDataModule),
                        data: { menuId: MENU_ID['claims-billing-data'] },
                        canLoad: [ScopeGuard]
                    },
                    {
                        path: 'legal/demands',
                        loadChildren: () => import('./pages/company/demands/demand.module').then(m => m.DemandModule),
                        data: { menuId: MENU_ID.demands },
                        canLoad: [ScopeGuard]
                    },
                    {
                        path: 'legal/claimants',
                        loadChildren: () => import('./pages/company/clients/client.module').then(m => m.ClientModule),
                        data: { menuId: MENU_ID.claimants },
                        canLoad: [ScopeGuard]
                    },
                    {
                        path: 'legal/courts',
                        loadChildren: () => import('./pages/company/courts/court.module').then(m => m.CourtModule),
                        data: { menuId: MENU_ID.courts },
                        canLoad: [ScopeGuard]
                    },
                    {
                        path: 'management/departments',
                        loadChildren: () => import('./pages/management/departments/department.module').then(m => m.DepartmentModule),
                        data: { menuId: MENU_ID.departments },
                        canLoad: [ScopeGuard]
                    },
                    {
                        path: 'management/roles',
                        loadChildren: () => import('./pages/management/roles/role.module').then(m => m.RoleModule),
                        data: { menuId: MENU_ID.roles },
                        canLoad: [ScopeGuard]
                    },
                    {
                        path: 'management/users',
                        loadChildren: () => import('./pages/management/users/user.module').then(m => m.UserModule),
                        data: { menuId: MENU_ID.users },
                        canLoad: [ScopeGuard]
                    },
                    {
                        path: 'auxiliaries/law-firms',
                        loadChildren: () => import('./pages/company/law-firms/law-firm.module').then(m => m.LawFirmModule),
                        data: { menuId: MENU_ID['law-firms'] },
                        canLoad: [ScopeGuard]
                    },
                    // redirection to mainMenu
                    {
                        path: 'main-menu/' + MENU_ID.menus,
                        redirectTo: 'statia-configuration/menus'
                    },
                    {
                        path: 'main-menu/' + MENU_ID.options,
                        redirectTo: 'statia-configuration/options'
                    },
                    {
                        path: 'main-menu/' + MENU_ID.clients,
                        redirectTo: 'customer-management/clients'
                    },
                    {
                        path: 'main-menu/' + MENU_ID['active-claims'],
                        redirectTo: 'claims/active-claims'
                    },
                    {
                        path: 'main-menu/' + MENU_ID['closed-claims'],
                        redirectTo: 'claims/closed-claims'
                    },
                    {
                        path: 'main-menu/' + MENU_ID['bill-preferences'],
                        redirectTo: 'accounting-finance/bill-preferences'
                    },
                    {
                        path: 'main-menu/' + MENU_ID.bills,
                        redirectTo: 'accounting-finance/bills'
                    },
                    {
                        path: 'main-menu/' + MENU_ID['pending-to-create-bill'],
                        redirectTo: 'accounting-finance/pending-to-create-bill'
                    },
                    {
                        path: 'main-menu/' + MENU_ID['claims-billing-data'],
                        redirectTo: 'accounting-finance/claims-billing-data'
                    },
                    {
                        path: 'main-menu/' + MENU_ID.demands,
                        redirectTo: 'legal/demands'
                    },
                    {
                        path: 'main-menu/' + MENU_ID.departments,
                        redirectTo: 'management/departments'
                    },
                    {
                        path: 'main-menu/' + MENU_ID.courts,
                        redirectTo: 'legal/courts'
                    },
                    {
                        path: 'main-menu/' + MENU_ID.roles,
                        redirectTo: 'management/roles'
                    },
                    {
                        path: 'main-menu/' + MENU_ID.users,
                        redirectTo: 'management/users'
                    },
                    {
                        path: 'main-menu/' + MENU_ID['law-firms'],
                        redirectTo: 'auxiliaries/law-firms'
                    },
                ]
            },
            {
                path: 'login',
                loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule),
                pathMatch: 'full'
            },
            { path: '404', redirectTo: '' },
            { path: '**', redirectTo: '' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
