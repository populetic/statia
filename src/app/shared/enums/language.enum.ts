export enum LANGUAGE {
    EN = 'en',
    ES = 'es',
    FR = 'fr',
    PT = 'it',
    DE = 'de'
}
