export enum MODEL_PROPERTY_NAME {
    AIRLINE = 'airlineId',
    CLAIM_STATUS = 'claimStatusId',
    CLAIM_DOCUMENTATION_STATUS = 'claimDocumentationStatusId',
    DISRUPTION_TYPE = 'disruptionTypeId',
    LANGUAGE = 'languageId',
    LAW_FIRM = 'lawFirmId',
    PARTNER = 'partnerId',
    USER = 'userId',
    LANGUAGE_SELECTOR = 'languageIdSelector',
    COUNTRY = 'countryId',
    PROVINCE = 'provinceId',
    DOCUMENTATION_TYPE = 'documentationTypeId',
    LAW_SUITE_TYPE = 'lawsuiteTypeId',
    AIRLINE_REASON = 'airlineReasonId',
    AIRPORT = 'airportId',
    AIRLINE_ALTERNATIVE = 'airlineAlternativeId',
    LUGGAGE_ISSUE_MOMENT = 'luggageIssueMomentId',
    BOOLEAN_SELECTOR = 'booleanSelector',
    COURT = 'courtId',
    COURT_TYPE = 'courtTypeId',
    COURT_NUMBER = 'courtNumber',
    CITY = 'cityId',
    COURT_CITY = 'courtCityId',
    LEGAL_GUARDIAN = 'legalGuardianId'
}
