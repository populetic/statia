export interface LanguageApp {
    appId: string;
    appName: string;
    languageId: string;
    languageCode: string;
    languageName: string;
    isDefault: string;
}
