export interface UserLoggedData {
    completeName?: string;
    name?: string;
    roleName?: string;
    username?: string;
}
