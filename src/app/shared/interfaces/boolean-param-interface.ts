export interface BooleanParam {
    [ key: string ]: boolean;
}
