export interface FilterValue {
    id?: string | number;
    name?: string;
}

