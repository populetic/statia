import { Observable } from 'rxjs';
import { Result } from '../models/result.model';
import { CustomParams } from './custom-params.interface';
import { HttpHeaders } from '@angular/common/http';

export interface BaseApiServiceInterface<T> {
    resourceUrl: string;
    headers: HttpHeaders;

    getAll(param: CustomParams): Observable<Result<T[]>>;
    getAllAsync(param: CustomParams): Promise<Result<T[]>>;
    getById(id: number|string): Observable<Result<T>>;

    create(param: T): Observable<Result<T>>;
    createAsync(param: T): Promise<Result<T>>;
    edit(id: number|string, param: T): Observable<Result<T>>;
    editAsync(id: number|string, param: T): Promise<Result<T>>;
    deleteByParam(param: any): Observable<Result<T>>;
}
