export interface AccessMenu {
    pathMenu: string;
    canAccess: boolean;
}
