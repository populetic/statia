import { Injectable } from '@angular/core';
import { CanLoad, Route } from '@angular/router';
import { SessionService } from '../services/session.service';

@Injectable({providedIn: 'root'})
export class AuthGuard implements CanLoad {
    constructor(
        private sessionService: SessionService
    ) { }

    canLoad(route: Route) {
        return this.sessionService.isUserLogged;
    }
}
