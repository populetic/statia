import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment } from '@angular/router';
import { SessionService } from '../services/session.service';
import { StatiaService } from '../services/statia.service';

@Injectable({
    providedIn: 'root'
})
export class ScopeGuard implements CanLoad {

    constructor(
        private statiaService: StatiaService,
        private sessionService: SessionService,
    ) { }

    canLoad(route: Route, segments: UrlSegment[]): boolean {
        if (route?.data?.menuId && this.sessionService.isUserLogged) {
            return this.statiaService.canAccessMenu(route?.data?.menuId);
        }
        return false;
    }
}
