export const LOCAL_STORAGE = 'localStorage';
export const SESSION_STORAGE = 'sessionStorage';

// Session
export const STATIA_SESSION = 'statia_session';
export const STATIA_LANGUAGES = 'statia_languages';
export const STATIA_AVAILABLE_MENUS = 'statia_available_menus';
export const STATIA_TABLE_RELATED_DATA = 'statia_table_related_data';

// APP
export const ID_TRANSLATION_SYSTEM = 6;
export const ID_MENU_ACTIVE_CLAIMS = '9';
export const ID_MENU_CLOSED_CLAIMS = '10';
export const ID_MENU_BILL = '15';
export const ID_MENU_DEMAND = '26';
export const ID_MENU_LAWFIRM = '66';

// ADVANCED FILTERS
export const FILTER_SELECTOR_LIMIT = 10;
export const DATE_START = 'DateStart';
export const DATE_END = 'DateEnd';

// POPULETIC
export const POPULETIC_LANGUAGES = 'populetic_languages';
export const POPULETIC_COUNTRIES = 'populetic_countries';
export const POPULETIC_CITIES = 'populetic_cities';
export const POPULETIC_LAW_FIRMS = 'populetic_law_firms';
export const POPULETIC_ROLES = 'populetic_roles';
export const POPULETIC_DEPARTMENTS = 'populetic_departments';
export const POPULETIC_MENUS = 'populetic_menus';
export const POPULETIC_DISRUPTION_TYPES = 'populetic_disruption_types';
export const POPULETIC_CLAIM_STATUS = 'populetic_claim_status';
export const POPULETIC_AIRLINES = 'populetic_airlines';
export const POPULETIC_CLAIM_DOCUMENTATION_STATUS = 'populetic_claim_documentation_status';
export const POPULETIC_PARTNERS = 'populetic_partners';
export const POPULETIC_USERS = 'populetic_users';
export const POPULETIC_PROVINCES = 'populetic_provinces';
export const POPULETIC_DOCUMENT_TYPES = 'populetic_document_types';
export const POPULETIC_LAWSUITE_TYPES = 'populetic_lawsuite_types';
export const POPULETIC_AIRLINE_REASONS = 'populetic_airline_reasons';
export const POPULETIC_AIRPORTS = 'populetic_aiports';
export const POPULETIC_AIRLINE_ALTERNATIVES = 'populetic_airline_alternatives';
export const POPULETIC_LUGGAGE_ISSUE_MOMENTS = 'populetic_luggage_issue_moments';
export const POPULETIC_CREATE_DEMAND = 'populetic_create_demand';
export const POPULETIC_COURTS = 'populetic_courts';
export const POPULETIC_COURT_TYPES = 'populetic_court_types';
export const POPULETIC_COURT_NUMBERS = 'populetic_court_numbers';

// PATTERN
export const EMAIL_PATTERN = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
export const PASSWORD_PATTERN = '(?=.*[0-9]).{5,}';
export const ID_CARD_PATTERN = '[a-zA-Z0-9]+';
export const AMOUNT_PATTERN = '^\d*(\.\d{0,2})?$';

export const DISRUPTION_ID_DELAY = '1';
export const DISRUPTION_ID_CANCELLATION = '2';
export const DISRUPTION_ID_OVERBOOKING = '3';
export const DISRUPTION_ID_LUGGAGE_LOST = '4';
export const DISRUPTION_ID_LUGGAGE_DELAY = '5';
export const DISRUPTION_ID_LUGGAGE_DAMAGE = '6';
export const DISRUPTION_LUGGAGE_IDS = [
    DISRUPTION_ID_LUGGAGE_LOST,
    DISRUPTION_ID_LUGGAGE_DELAY,
    DISRUPTION_ID_LUGGAGE_DAMAGE
];

export const FLIGHT_DATA_ORIGIN_ID_MANUAL = '5';
export const FLIGHT_DATA_ORIGIN_ID_MANUAL_STATIA = '6';
export const FLIGHT_DATA_ORIGIN_MANUAL_IDS = [
    FLIGHT_DATA_ORIGIN_ID_MANUAL,
    FLIGHT_DATA_ORIGIN_ID_MANUAL_STATIA
];

export const AIRLINE_ALTERNATIVE_ID_NOTHING = '1';
export const AIRLINE_ALTERNATIVE_ID_REFUND = '2';
export const AIRLINE_ALTERNATIVE_ID_FLIGHT = '3';

export const SIZES = {
    SMALL: 'SMALL',
    MIDDLE: 'MIDDLE',
    LARGE: 'LARGE'
};
export const PAGINATOR = {
    [SIZES.SMALL]: {
        SIZE: '5',
        OPTIONS: ['5', '10', '25', '50']
    },
    [SIZES.MIDDLE]: {
        SIZE: '10',
        OPTIONS: ['10', '20', '40', '80']
    },
    [SIZES.LARGE]: {
        SIZE: '25',
        OPTIONS: ['25', '50', '100', '500']
    }
};

// TABS - RELATED ENTITIES
export const RELATED_ENTITIES = {
    CLIENT: {
        CLAIMS: 'clientRelatedClaims',
        DEMANDS: 'clientRelatedDemands',
        BILLS: 'clientRelatedBills'
    },
    DEMAND: {
        COMPANIONS: 'createDemandCompanions',
        POSSIBLE_COMPANIONS: 'createDemandPossibleCompanions'
    },
    LAWFIRM: {
        ADMINISTRATORS: 'lawFirmAdministrator'
    }
};

export const DOCUMENTATION_STATUS = {
    REVIEW_PENDING: '1',
    CORRECT_DOCUMENTATION: '3',
    REVISION_CORRECT: '5'
};

export const CLAIM_STATUS = {
    REVIEW_PENDING: '1',
    LACK_OF_JUDICIAL_COMPETITION: '6',
    EXTRAJUDICIAL_SENT: '4',
    EXTRAJUDICIAL_PENDING: '3',
    ASSIGNED_DEP_LEGAL: '8',
    PENDING_DEP_LEGAL: '7'
};

export const RESPONSE_API_STATUS = {
    SUCCESS: 1,
    ERROR: 0
};

export const FORM_OPERATIONS = {
    CREATE: '1',
    EDIT: '2',
    DELETE: '3',
    PRINT_RESULTS: '5',
    CHANGE_STATUS: '6'
};

export const TIMEOUTS = {
    '1': 1000,
    '2': 2000,
    '3': 3000,
    '4': 4000,
    '5': 5000,
    '6': 6000,
    '7': 7000,
    '8': 8000,
    '9': 9000,
    '10': 10000
};
