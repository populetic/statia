import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { SimpleDialogComponent } from '@app/layout/dialogs/simple-dialog/simple-dialog.component';
import { Observable } from 'rxjs/internal/Observable';
import { TIMEOUTS } from '../constants/constants';
import { SessionService } from './session.service';
@Injectable({ providedIn: 'root' })
export abstract class BaseService {

    public http: HttpClient;
    public headers: HttpHeaders;
    public sessionService: SessionService;

    constructor(
        @Inject(HttpClient) http: HttpClient,
        public resourceUrl: string,
        sessionService: SessionService
    ) {
        this.http = http;
        this.sessionService = sessionService;
    }

    getAll(param: any): Observable<any> {
        const queryString = this.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(param: any): Promise<any> {
        const queryString = this.getQueryString(param);

        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: any): Observable<any> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(param: any): Observable<any> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: any): Promise<any> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);

        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: any, param: any): Observable<any> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: any, param: any): Promise<any> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<any> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });

        const options = {
            headers: this.headers,
            params
        };

        return this.http.delete(this.resourceUrl + '/delete', options);
    }

    manageServiceResult(
        result: any, pathToRedirect: string,
        router: Router, snackbar: MatSnackBar,
        dialog: MatDialog
    ) {
        if (result.status === 1) {
            snackbar.open(result.message, 'Close', { duration: TIMEOUTS['5'] });

            if (pathToRedirect !== null) {
                if (pathToRedirect.search(':id') !== -1) {
                    router.navigate([pathToRedirect.replace(':id', result?.response?.id)]);
                } else {
                    router.navigate([pathToRedirect]);
                }
            }
        } else {
            dialog.open(SimpleDialogComponent, {
                data: {
                    title: result.message,
                    content: result.message !== null ? result.message : ''
                }
            }).afterClosed().subscribe();
        }
    }

    private getQueryString(param: any) {
        let queryString = '';

        if (param && param !== null) {
            queryString = '?' + Object.keys(param).map(key => key + '=' + param[key]).join('&');
        }

        return queryString;
    }

}
