import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs/internal/Observable';
import { ID_TRANSLATION_SYSTEM } from '../constants/constants';
import { LanguageApp } from '../interfaces/language.app.interface';
import { Result } from '../models/result.model';
import { BaseInterface } from '../interfaces/base.interface';
import { CustomParams } from '../interfaces/custom-params.interface';
import { StatiaFunctions } from '@app/utils/statia.functions';
@Injectable({ providedIn: 'root' })
export class LanguagesService implements BaseInterface<LanguageApp> {
    private appIdTranslationSystem: number = ID_TRANSLATION_SYSTEM;
    resourceUrl: string = environment.apiUrl + '/languages';
    headers: HttpHeaders;

    constructor(
        private http: HttpClient
    ) { }

    //#region interface functions
    getAll(param: CustomParams = null): Observable<Result<LanguageApp[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(param: CustomParams = null): Promise<Result<LanguageApp[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number|string): Observable<Result<LanguageApp>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(param: LanguageApp): Observable<Result<LanguageApp>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: LanguageApp): Promise<Result<LanguageApp>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: LanguageApp): Observable<Result<LanguageApp>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: LanguageApp): Promise<Result<LanguageApp>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<LanguageApp>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const LanguageApps = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', LanguageApps);
    }
    //#endregion

    //#region Specific public languages function
    getAllLanguagesByApp(): Observable<Result<LanguageApp[]>> {
        const path = this.resourceUrl + '-by-app/' + this.appIdTranslationSystem;
        return this.http.get(path);
    }

    getAllAsyncLanguagesByApp(): Promise<Result<LanguageApp[]>> {
        const path = this.resourceUrl + '-by-app/' + this.appIdTranslationSystem;
        return this.http.get(path).toPromise();
    }
    //#endregion
}
