import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AirlineAlternative } from '@app/pages/company/auxiliaries/airline-alternatives/airline-alternative.model';
import { AirlineAlternativeService } from '@app/pages/company/auxiliaries/airline-alternatives/airline-alternative.service';
import { AirlineReasonService } from '@app/pages/company/auxiliaries/airline-reasons/airline-reason.service';
import { Airline } from '@app/pages/company/auxiliaries/airlines/airline.model';
import { AirlineService } from '@app/pages/company/auxiliaries/airlines/airline.service';
import { Airport } from '@app/pages/company/auxiliaries/airports/airport.model';
import { AirportService } from '@app/pages/company/auxiliaries/airports/airport.service';
import { City } from '@app/pages/company/auxiliaries/cities/city.model';
import { CityService } from '@app/pages/company/auxiliaries/cities/city.service';
import { ClaimDocumentationStatus } from '@app/pages/company/auxiliaries/claim-documentation-status/claim-documentation-status.model';
import { ClaimDocumentationStatusService } from '@app/pages/company/auxiliaries/claim-documentation-status/claim-documentation-status.service';
import { ClaimStatus } from '@app/pages/company/auxiliaries/claim-status/claim-status.model';
import { ClaimStatusService } from '@app/pages/company/auxiliaries/claim-status/claim-status.service';
import { Country } from '@app/pages/company/auxiliaries/countries/country.model';
import { CountryService } from '@app/pages/company/auxiliaries/countries/country.service';
import { RequiredCountry } from '@app/pages/company/auxiliaries/countries/required.country.enum';
import { RequiredCountryWithJudicialProcess } from '@app/pages/company/auxiliaries/countries/required.country.with_judicial_process.enum';
import { DisruptionType } from '@app/pages/company/auxiliaries/disruption-types/disruption-type.model';
import { DisruptionTypeService } from '@app/pages/company/auxiliaries/disruption-types/disruption-type.service';
import { DocumentationType } from '@app/pages/company/auxiliaries/document-types/documentation-type.model';
import { DocumentationTypeService } from '@app/pages/company/auxiliaries/document-types/documentation-type.service';
import { Language } from '@app/pages/company/auxiliaries/languages/language.model';
import { LanguageService } from '@app/pages/company/auxiliaries/languages/language.service';
import { LawsuiteType } from '@app/pages/company/auxiliaries/lawsuite-types/lawsuite-type.model';
import { LawsuiteTypeService } from '@app/pages/company/auxiliaries/lawsuite-types/lawsuite-type.service';
import { LuggageIssueMoment } from '@app/pages/company/auxiliaries/luggage-issue-moment/luggage-issue-moment.model';
import { LuggageIssueMomentService } from '@app/pages/company/auxiliaries/luggage-issue-moment/luggage-issue-moment.service';
import { Province } from '@app/pages/company/auxiliaries/provinces/province.model';
import { ProvinceService } from '@app/pages/company/auxiliaries/provinces/province.service';
import { CourtNumber } from '@app/pages/company/courts/court-number/court-number.model';
import { CourtType } from '@app/pages/company/courts/court-type/court-type.model';
import { Court } from '@app/pages/company/courts/court.model';
import { CourtService } from '@app/pages/company/courts/court.service';
import { LawFirm } from '@app/pages/company/law-firms/law-firm.model';
import { LawFirmService } from '@app/pages/company/law-firms/law-firm.service';
import { Partner } from '@app/pages/company/partners/partner.model';
import { PartnerService } from '@app/pages/company/partners/partner.service';
import { User } from '@app/pages/management/users/user.model';
import { UserService } from '@app/pages/management/users/user.service';
import { Menu } from '@app/pages/statia-configuration/menus/menu.model';
import { MenuService } from '@app/pages/statia-configuration/menus/menu.service';
import { Option } from '@app/pages/statia-configuration/options/option.model';
import { environment } from '@environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin, Observable } from 'rxjs';
import {
    POPULETIC_AIRLINES,
    POPULETIC_AIRLINE_ALTERNATIVES,
    POPULETIC_AIRLINE_REASONS,
    POPULETIC_AIRPORTS,
    POPULETIC_CITIES,
    POPULETIC_CLAIM_DOCUMENTATION_STATUS,
    POPULETIC_CLAIM_STATUS,
    POPULETIC_COUNTRIES,
    POPULETIC_COURTS,
    POPULETIC_COURT_NUMBERS,
    POPULETIC_COURT_TYPES,
    POPULETIC_DISRUPTION_TYPES,
    POPULETIC_DOCUMENT_TYPES,
    POPULETIC_LANGUAGES,
    POPULETIC_LAWSUITE_TYPES,
    POPULETIC_LAW_FIRMS,
    POPULETIC_LUGGAGE_ISSUE_MOMENTS,
    POPULETIC_PARTNERS,
    POPULETIC_PROVINCES,
    POPULETIC_USERS,
    STATIA_LANGUAGES,
    TIMEOUTS
} from '../constants/constants';
import { MODEL_PROPERTY_NAME } from '../enums/model-property-name.enum';
import { CustomParams } from '../interfaces/custom-params.interface';
import { FilterValue } from '../interfaces/filter-values.interface';
import { LanguageApp } from '../interfaces/language.app.interface';
import { Result } from '../models/result.model';
import { LanguagesService } from './languages.service';
import { SessionService } from './session.service';
import { CourtNumberService } from '@app/pages/company/courts/court-number/court-number.service';
import { OptionService } from '@app/pages/statia-configuration/options/option.service';


@Injectable({ providedIn: 'root' })
export class StatiaService {

    constructor(
        private http: HttpClient,
        private snackbar: MatSnackBar,
        private menuService: MenuService,
        private cityService: CityService,
        private userService: UserService,
        private courtService: CourtService,
        private translate: TranslateService,
        private sessionService: SessionService,
        private airlineService: AirlineService,
        private lawFirmService: LawFirmService,
        private partnerService: PartnerService,
        private countryService: CountryService,
        private airportService: AirportService,
        private provinceService: ProvinceService,
        private languageService: LanguageService,
        private languagesService: LanguagesService,
        private claimStatusService: ClaimStatusService,
        private courtNumberService: CourtNumberService,
        private lawsuiteTypeService: LawsuiteTypeService,
        private airlineReasonService: AirlineReasonService,
        private disruptionTypeService: DisruptionTypeService,
        private documentationTypeService: DocumentationTypeService,
        private airlineAlternativeService: AirlineAlternativeService,
        private luggageIssueMomentService: LuggageIssueMomentService,
        private claimDocumentationStatusService: ClaimDocumentationStatusService,
        private optionService: OptionService
    ) { }

    getAllMenuOptionsByRole(roleId: string): Promise<any> {
        const param = { roleId: roleId };
        const json = JSON.stringify(param);
        const params = 'json=' + json;

        return this.http.post(environment.apiUrlStatia + '/menus-role-options', params).toPromise();
    }

    /** Recogida de datos para montar recursos de filtrados avanzados */
    async getPropertyValues(field: string, formatResponse?: boolean, translate: boolean = false, customFilters?: CustomParams) {
        let filterValues: Array<any> = [];
        let idField = 'id';
        let nameField = 'name';
        formatResponse = (formatResponse === undefined) ? true : formatResponse;

        switch (field) {
            case MODEL_PROPERTY_NAME.AIRLINE:
                await new Promise((resolve) => {
                    let airlines: Airline[];
                    airlines = this.sessionService.getItem(POPULETIC_AIRLINES);

                    if (!airlines) {
                        this.airlineService.getAllAsync(null).then((result: Result<Airline[]>) => {
                            if (result.status === 1) {
                                this.sessionService.setItem(POPULETIC_AIRLINES, JSON.stringify(result.response));
                                filterValues = result.response;
                                resolve();
                            }
                        });
                    } else {
                        filterValues = airlines;
                        resolve();
                    }
                });
                break;

            case MODEL_PROPERTY_NAME.CLAIM_STATUS:
                await new Promise((resolve) => {
                    let claimStatus: ClaimStatus[];
                    claimStatus = this.sessionService.getItem(POPULETIC_CLAIM_STATUS);

                    if (!claimStatus) {
                        this.claimStatusService.getAllAsync(null).then((result: Result<ClaimStatus[]>) => {
                            if (result.status === 1) {
                                this.sessionService.setItem(POPULETIC_CLAIM_STATUS, JSON.stringify(result.response));
                                filterValues = result.response;
                                resolve();
                            }
                        });
                    } else {
                        filterValues = claimStatus;
                        resolve();
                    }
                });
                break;

            case MODEL_PROPERTY_NAME.CLAIM_DOCUMENTATION_STATUS:
                await new Promise((resolve) => {
                    let claimDocumentationStatus: ClaimDocumentationStatus[];
                    claimDocumentationStatus = this.sessionService.getItem(POPULETIC_CLAIM_DOCUMENTATION_STATUS);

                    if (!claimDocumentationStatus) {
                        this.claimDocumentationStatusService.getAllAsync(null).then(
                            (result: Result<ClaimDocumentationStatus[]>) => {
                                if (result.status === 1) {
                                    this.sessionService.setItem(POPULETIC_CLAIM_DOCUMENTATION_STATUS, JSON.stringify(result.response));
                                    filterValues = result.response;
                                    resolve();
                                }
                            });
                    } else {
                        filterValues = claimDocumentationStatus;
                        resolve();
                    }
                });
                break;

            case MODEL_PROPERTY_NAME.DISRUPTION_TYPE:
                await new Promise((resolve) => {
                    let disruptionTypes: DisruptionType[];
                    disruptionTypes = this.sessionService.getItem(POPULETIC_DISRUPTION_TYPES);

                    if (!disruptionTypes) {
                        this.disruptionTypeService.getAllAsync(null).then((result: Result<DisruptionType[]>) => {
                            if (result.status === 1) {
                                this.sessionService.setItem(POPULETIC_DISRUPTION_TYPES, JSON.stringify(result.response));
                                filterValues = result.response;
                                resolve();
                            }
                        });
                    } else {
                        filterValues = disruptionTypes;
                        resolve();
                    }
                });
                break;

            case MODEL_PROPERTY_NAME.LANGUAGE:
                await new Promise((resolve) => {
                    let languages: LanguageApp[];
                    idField = 'languageId';
                    nameField = 'languageName';
                    languages = this.sessionService.getItem(STATIA_LANGUAGES);

                    if (!languages) {
                        this.languagesService.getAllAsyncLanguagesByApp().then((result: Result<LanguageApp[]>) => {
                            if (result.status === 1) {
                                this.sessionService.setItem(STATIA_LANGUAGES, JSON.stringify(result.response));
                                filterValues = formatResponse ?
                                    this.formatData(result.response, idField, nameField) : result.response;
                                resolve();
                            }
                        });
                    } else {
                        filterValues = formatResponse ? this.formatData(languages, idField, nameField) : languages;
                        resolve();
                    }
                });
                break;

            case MODEL_PROPERTY_NAME.LAW_FIRM:
                await new Promise((resolve) => {
                    let lawFirms: LawFirm[];
                    lawFirms = this.sessionService.getItem(POPULETIC_LAW_FIRMS);

                    if (!lawFirms) {
                        this.lawFirmService.getAllAsync(null).then((result: Result<LawFirm[]>) => {
                            if (result.status === 1) {
                                this.sessionService.setItem(POPULETIC_LAW_FIRMS, JSON.stringify(result.response));
                                filterValues = result.response;
                                resolve();
                            }
                        });
                    } else {
                        filterValues = lawFirms;
                        resolve();
                    }
                });
                break;

            case MODEL_PROPERTY_NAME.PARTNER:
                await new Promise((resolve) => {
                    let partners: Partner[];
                    nameField = 'fiscalName';
                    partners = this.sessionService.getItem(POPULETIC_PARTNERS);

                    if (!partners) {
                        this.partnerService.getAllAsync(null).then((result: Result<Partner[]>) => {
                            if (result.status === 1) {
                                this.sessionService.setItem(POPULETIC_PARTNERS, JSON.stringify(result.response));
                                filterValues = formatResponse ? this.formatData(result.response, idField, nameField) : result.response;
                                resolve();
                            }
                        });
                    } else {
                        filterValues = formatResponse ? this.formatData(partners, idField, nameField) : partners;
                        resolve();
                    }
                });
                break;

            case MODEL_PROPERTY_NAME.USER:
                await new Promise((resolve) => {

                    let users: User[];
                    nameField = 'completeName';
                    users = this.sessionService.getItem(POPULETIC_USERS);
                    if (!users) {
                        this.userService.getAllAsync(customFilters).then((result: Result<User[]>) => {
                            if (result.status === 1) {
                                this.sessionService.setItem(POPULETIC_USERS, JSON.stringify(result.response));
                                filterValues = formatResponse ? this.formatData(result.response, idField, nameField) : result.response;
                                resolve();
                            }
                        });
                    } else {
                        filterValues = formatResponse ? this.formatData(users, idField, nameField) : users;
                        resolve();
                    }
                });
                break;

            case MODEL_PROPERTY_NAME.LANGUAGE_SELECTOR:
                await new Promise((resolve) => {
                    const languages: Language[] = this.sessionService.getItem(POPULETIC_LANGUAGES);

                    if (!languages) {
                        this.languageService.getAllAsync(null).then((result: Result<Language[]>) => {
                            if (result.status === 1) {
                                this.sessionService.setItem(POPULETIC_LANGUAGES, JSON.stringify(result.response));
                                filterValues = formatResponse ? this.formatData(result.response, idField, nameField) : result.response;
                                resolve();
                            }
                        });
                    } else {
                        filterValues = formatResponse ? this.formatData(languages, idField, nameField) : languages;
                        resolve();
                    }
                });
                break;

            case MODEL_PROPERTY_NAME.COUNTRY:
                await new Promise((resolve) => {
                    let countries: Array<Country> = this.sessionService.getItem(POPULETIC_COUNTRIES);
                    // translate = true; // TODO: SI ( no de momento, migration BD)

                    if (!countries) {
                        this.countryService.getAllAsync(null).then((result: Result<Country[]>) => {
                            if (result.status === 1) {
                                this.sessionService.setItem(POPULETIC_COUNTRIES, JSON.stringify(result.response));

                                filterValues = this.checkCountriesByCustomFilter(customFilters, result.response);

                                filterValues = formatResponse ? this.formatData(filterValues, idField, nameField) : filterValues;

                                resolve();
                            }
                        });
                    } else {
                        countries = this.checkCountriesByCustomFilter(customFilters, countries);
                        filterValues = formatResponse ? this.formatData(countries, idField, nameField) : countries;
                        resolve();
                    }
                });
                break;
            case MODEL_PROPERTY_NAME.PROVINCE:
                await new Promise((resolve) => {
                    const provinces: Province[] = this.sessionService.getItem(POPULETIC_PROVINCES);

                    if (!provinces) {
                        this.provinceService.getAllAsync(null).then((result: Result<Province[]>) => {
                            if (result.status === 1) {
                                this.sessionService.setItem(POPULETIC_PROVINCES, JSON.stringify(result.response));
                                filterValues = formatResponse ? this.formatData(result.response, idField, nameField) : result.response;
                                resolve();
                            }
                        });
                    } else {
                        filterValues = formatResponse ? this.formatData(provinces, idField, nameField) : provinces;
                        resolve();
                    }
                });
                break;

            case MODEL_PROPERTY_NAME.DOCUMENTATION_TYPE:
                await new Promise((resolve) => {
                    const documentationTypes: DocumentationType[] =
                        this.sessionService.getItem(POPULETIC_DOCUMENT_TYPES);

                    if (!documentationTypes) {
                        this.documentationTypeService.getAllAsync(null).then((result: Result<Province[]>) => {
                            if (result.status === 1) {
                                this.sessionService.setItem(POPULETIC_DOCUMENT_TYPES, JSON.stringify(result.response));
                                filterValues = result.response;
                                resolve();
                            }
                        });
                    } else {
                        filterValues = documentationTypes;
                        resolve();
                    }
                });
                break;

            case MODEL_PROPERTY_NAME.LAW_SUITE_TYPE:
                await new Promise((resolve) => {
                    const lawSuiteTypes: LawsuiteType[] = this.sessionService.getItem(POPULETIC_LAWSUITE_TYPES);

                    if (!lawSuiteTypes) {
                        this.lawsuiteTypeService.getAllAsync(null).then((result: Result<LawsuiteType[]>) => {
                            if (result.status === 1) {
                                this.sessionService.setItem(POPULETIC_LAWSUITE_TYPES, JSON.stringify(result.response));
                                filterValues = result.response;
                                resolve();
                            }
                        });
                    } else {
                        filterValues = lawSuiteTypes;
                        resolve();
                    }
                });
                break;

            case MODEL_PROPERTY_NAME.AIRLINE_REASON:
                await new Promise((resolve) => {
                    const airlineReasons: LawsuiteType[] = this.sessionService.getItem(POPULETIC_AIRLINE_REASONS);

                    if (!airlineReasons) {
                        this.airlineReasonService.getAllAsync(null).then((result: Result<LawsuiteType[]>) => {
                            if (result.status === 1) {
                                this.sessionService.setItem(POPULETIC_AIRLINE_REASONS, JSON.stringify(result.response));
                                filterValues = result.response;
                                resolve();
                            }
                        });
                    } else {
                        filterValues = airlineReasons;
                        resolve();
                    }
                });
                break;

            case MODEL_PROPERTY_NAME.AIRPORT:
                await new Promise((resolve) => {
                    let airports: Airport[];
                    airports = this.sessionService.getItem(POPULETIC_AIRPORTS);

                    if (!airports) {
                        this.airportService.getAllAsync(null).then((result: Result<Airport[]>) => {
                            if (result.status === 1) {
                                this.sessionService.setItem(POPULETIC_AIRPORTS, JSON.stringify(result.response));
                                filterValues = result.response;
                                resolve();
                            }
                        });
                    } else {
                        filterValues = airports;
                        resolve();
                    }
                });
                break;

            case MODEL_PROPERTY_NAME.AIRLINE_ALTERNATIVE:
                await new Promise((resolve) => {
                    let airlineAlternatives: AirlineAlternative[];
                    airlineAlternatives = this.sessionService.getItem(POPULETIC_AIRLINE_ALTERNATIVES);

                    if (!airlineAlternatives) {
                        this.airlineAlternativeService.getAllAsync(null)
                            .then((result: Result<AirlineAlternative[]>) => {
                                if (result.status === 1) {
                                    this.sessionService.setItem(POPULETIC_AIRLINE_ALTERNATIVES, JSON.stringify(result.response));
                                    filterValues = result.response;
                                    resolve();
                                }
                            });
                    } else {
                        filterValues = airlineAlternatives;
                        resolve();
                    }
                });
                break;

            case MODEL_PROPERTY_NAME.LUGGAGE_ISSUE_MOMENT:
                await new Promise((resolve) => {
                    let luggageIssueMoments: LuggageIssueMoment[];
                    luggageIssueMoments = this.sessionService.getItem(POPULETIC_LUGGAGE_ISSUE_MOMENTS);

                    if (!luggageIssueMoments) {
                        this.luggageIssueMomentService.getAllAsync(null)
                            .then((result: Result<LuggageIssueMoment[]>) => {
                                if (result.status === 1) {
                                    this.sessionService.setItem(POPULETIC_LUGGAGE_ISSUE_MOMENTS, JSON.stringify(result.response));
                                    filterValues = result.response;
                                    resolve();
                                }
                            });
                    } else {
                        filterValues = luggageIssueMoments;
                        resolve();
                    }
                });
                break;

            case MODEL_PROPERTY_NAME.LEGAL_GUARDIAN:
            case MODEL_PROPERTY_NAME.BOOLEAN_SELECTOR:
                translate = true;
                field = MODEL_PROPERTY_NAME.BOOLEAN_SELECTOR;
                filterValues = [{ id: '1', name: 'yes' }, { id: '0', name: 'no' }];
                break;

            case MODEL_PROPERTY_NAME.COURT:
                    await new Promise((resolve) => {
                        nameField = 'courtCityAndCourtType';
                        let court: Court[];
                        court = this.sessionService.getItem(POPULETIC_COURTS);

                    if (!court) {
                        this.courtService.getAllAsync(null)
                        .then((result: Result<Court[]>) => {
                            if (result.status === 1) {
                                this.sessionService.setItem(POPULETIC_COURTS, JSON.stringify(result.response));
                                filterValues = result.response;
                                resolve();
                            }
                        });
                    } else {
                        filterValues = court;
                        resolve();
                    }
                });
                break;

            case MODEL_PROPERTY_NAME.COURT_TYPE:
                await new Promise((resolve) => {
                    let courtTypes: CourtType[];
                    courtTypes = this.sessionService.getItem(POPULETIC_COURT_TYPES);

                    if (!courtTypes) {
                        this.courtService.getCourtTypes()
                            .then((result: Result<CourtType[]>) => {
                                if (result.status === 1) {
                                    this.sessionService.setItem(POPULETIC_COURT_TYPES, JSON.stringify(result.response));
                                    filterValues = result.response;
                                    resolve();
                                }
                            });
                    } else {
                        filterValues = courtTypes;
                        resolve();
                    }
                });
                break;

            case MODEL_PROPERTY_NAME.COURT_NUMBER:
                await new Promise((resolve) => {
                    nameField = 'number';
                    let courtNumber: CourtNumber[];
                    courtNumber = this.sessionService.getItem(POPULETIC_COURT_NUMBERS);

                    if (!courtNumber) {
                        this.courtNumberService.getAllAsync(null)
                            .then((result: Result<CourtNumber[]>) => {
                                if (result.status === 1) {
                                    this.sessionService.setItem(POPULETIC_COURT_NUMBERS, JSON.stringify(result.response));
                                    filterValues = result.response;
                                    resolve();
                                }
                            });
                    } else {
                        filterValues = courtNumber;
                        resolve();
                    }
                });
                break;

            case MODEL_PROPERTY_NAME.CITY:
            case MODEL_PROPERTY_NAME.COURT_CITY:
                await new Promise((resolve) => {
                    const cities: City[] =  this.sessionService.getItem(POPULETIC_CITIES);

                    if (!cities) {
                        this.cityService.getAllAsync(null)
                            .then((result: Result<City[]>) => {
                                if (result.status === 1) {
                                    this.sessionService.setItem(POPULETIC_CITIES, JSON.stringify(result.response));
                                    filterValues = this.checkCitiesByCustomFilter(customFilters, result.response);
                                    resolve();
                                }
                            });
                    } else {
                        filterValues = this.checkCitiesByCustomFilter(customFilters, cities);
                        resolve();
                    }
                });
                break;
        }
        nameField = formatResponse ? 'name' : nameField;
        filterValues = translate ? this.translateField(filterValues, field, nameField, idField) : filterValues;
        return this.sortByField(filterValues, nameField);
    }

    checkCountriesByCustomFilter(customFilters?: CustomParams, countries?: Array<Country>) {
        let countryEnumRequired = null;

        if (customFilters == null) {
            return countries;
        }

        if (customFilters?.checkenumCountriesWithJudicialProcess) {
            countryEnumRequired = RequiredCountryWithJudicialProcess;
        }

        let  countriesFiltered: Array<Country> = countries;

        if (customFilters?.checkActive && customFilters?.checkActive === true) {
            countriesFiltered = countries?.filter(x => x.isActive);
        }

        if (customFilters?.checkenumCountriesWithJudicialProcess) {
            countriesFiltered = countriesFiltered.filter((x: Country) => this.validRequiredCountry(x?.id, null, countryEnumRequired ));
        }

        return countriesFiltered;
    }

    checkCitiesByCustomFilter(customFilters?: CustomParams, cities?: Array<City>): Array<City> {
        let countryEnumRequired = null;

        if (customFilters == null) {
            return cities?.filter((c: City) => c.isActive &&
                    this.validRequiredCountry(c?.countryId, null, RequiredCountry));
        }

        if (customFilters?.checkenumCountriesWithJudicialProcess) {
            countryEnumRequired = RequiredCountryWithJudicialProcess;
        } else {
            countryEnumRequired = RequiredCountry;
        }

        let citiesFiltered: Array<City> = null;

        if (customFilters?.checkActive != null) {
            if (customFilters?.checkActive) {
                citiesFiltered = cities?.filter((c: City) => c.isActive);
            }

            citiesFiltered = citiesFiltered.filter((c: City) => this.validRequiredCountry(c?.countryId, null, countryEnumRequired));
        } else {
            citiesFiltered = cities?.filter((c: City) => c.isActive && this.validRequiredCountry(c?.countryId, null, countryEnumRequired));
        }

        return citiesFiltered;
    }

    translateField(items: any[], field: string, nameField: string, idField: string): Array<any> {
        items.forEach(((item: any) =>
            item[nameField] = item[idField] ? this.translate.instant(field + item[idField]) : item[nameField]
        ));

        return items;
    }

    sortByField(data: any[], nameField: string, type: string  = ''): Array<any> {
        if (data !== null) {
            switch (type) {
                case 'number':
                    data.sort(
                        (a, b) => Number(a[nameField]) <  Number(b[nameField]) ? -1 : Number(a[nameField]) >  Number(b[nameField]) ? 1 : 0);
                    break;
                default:
                    data.sort((a, b) => a[nameField] < b[nameField] ? -1 : a[nameField] > b[nameField] ? 1 : 0);
                    break;
            }
        }

        return data;
    }

    filterByField(items: any, field: string, filter: string): Array<any> {
        return items.filter(
            (item: any) => item[field] === filter
        );
    }

    formatData(values: any[], id: string, name: string): Array<FilterValue> {
        const formattedData: Array<FilterValue> = [];
        values.forEach((value: any) => {
            formattedData.push({
                'id': value[id],
                'name': value[name]
            });
        });

        return formattedData;
    }

    _filter(value: string, arrayOfValues: any[], fieldUsedToFilter: (string | number)[]): string[] {
        const valueToFilter = String(value).toLocaleLowerCase();

        return arrayOfValues.filter((filteredValues: { [x: string]: string; }) => {
            let acceptedValue: boolean;

            fieldUsedToFilter.forEach((field: string | number) => {
                if (!acceptedValue && filteredValues[field] !== null) {
                    const filteredValue: string = String(filteredValues[field]);
                    acceptedValue = filteredValue.toLocaleLowerCase().indexOf(valueToFilter) !== -1;
                }
            });

            return acceptedValue;
        });
    }


    formatDate(value: string, hasTime: boolean = false): Date {
        if (value) {
            if (hasTime) {
                value = value.replace('T', ' ');
            }

            const date = new Date(value);
            const year: number = date.getFullYear();
            const month: number = date.getMonth();
            const day: number = date.getDate();

            let hours: number;
            let mins: number;
            let secs: number;

            if (hasTime) {
                hours = date.getHours();
                mins = date.getMinutes();
                secs = date.getSeconds();
            }

            return new Date(Date.UTC(year, month, day, hours, mins, secs, 0));
        }

        return null;
    }

    formatDateTimeToBD(dateTime: Date): string {
        return dateTime.toISOString().replace('T', ' ').slice(0, -5);
    }

    canAccessMenu(menuId: string): boolean {
        const menu: Menu = this.menuService.getMenuByIdFromStorage(menuId);
        return menu ? true : false;
    }

    pathMenu(menuId: string, editor?: string): any {
        const editorDefault: string = editor ? '/' + editor + '/' : '/';
        const menusByRoleId: Menu[] = this.menuService.getMenusStorage();
        let pathMenu: string;
        menusByRoleId.forEach((menu: Menu) => {
            if (menu?.id === menuId) {
                return pathMenu = '/' + menu.parentName + '/' + menu.name + editorDefault;
            }
        });

        return pathMenu;
    }

    buildMenuAccess(menuIds: string[], editor?: string): any[] {
        const accessMenu: any[] = [];
        menuIds.forEach((id: string) => {
            const pathMenu = this.pathMenu(id, editor);
            const canAccess = pathMenu ? this.canAccessMenu(id) : false;
            accessMenu[id] = { pathMenu, canAccess };
        });

        return accessMenu;
    }

    validRequiredCountry(countryId: string, countryName: string,  enumRequiredCountry: any): boolean {
        if (countryId || countryName) {
            for (const key in enumRequiredCountry) {
                if (Object.prototype.hasOwnProperty.call(enumRequiredCountry, key) &&
                    (countryId?.toString() === enumRequiredCountry[key] || countryName === key)) {

                    return true;
                }
            }
        }

        return false;
    }

    async getMenusByRole(roleId: string) {
        return new Promise((resolve) => {
            const availableMenus: Menu[] = [];
            const menuObs: Observable<Result<Menu[]>> = this.menuService.getAll(null);
            const optObs: Observable<Result<Menu[]>> = this.getMenuRoleOption(null, roleId);

            forkJoin([menuObs, optObs]).subscribe(
                (values: [Result<Menu[]>, Result<Menu[]>]) => {
                    if (values[0]?.response?.length && values[1]?.response?.length) {
                        values[1].response
                            .map((menu: Menu) => availableMenus.push(this.filterMenuById(menu, values[0].response)))
                            .some(() => {
                                this.menuService.setMenusStorage(availableMenus);
                                resolve(true);
                                return true;
                            });
                    }
                },
                (error: any) => {
                    const duration = { duration: TIMEOUTS['3'] };
                    this.snackbar.open(this.translate.instant('snbErrorGetMenusByRole') + JSON.stringify(error), 'Close', duration);
                }
            );

        });
    }

    getMenuRoleOption(menuId: string, roleId: string): Observable<Result<Menu[]>> {
        return this.menuService.getMenusRoleOption(menuId, roleId);
    }

    private filterMenuById(menuWithOptions: Menu, menus: Menu[]): Menu {
        const menuFiltered: Menu = menus.find((m: Menu) => m.id === menuWithOptions.id);
        menuFiltered.options = menuWithOptions?.options ?? [];
        return menuFiltered;
    }

}
