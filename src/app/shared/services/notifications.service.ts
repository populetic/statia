import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { SimpleDialogComponent } from '@app/layout/dialogs/simple-dialog/simple-dialog.component';
import { TIMEOUTS } from '../constants/constants';

@Injectable({ providedIn: 'root' })
export class NotificationsService {
    // TODO we set this function provisionally here until we decide class to set.
    manageServiceResult(
        result: any, pathToRedirect: string,
        router: Router, snackbar: MatSnackBar,
        dialog: MatDialog
    ) {
        if (result.status === 1) {
            snackbar.open(result.message, 'Close', { duration: TIMEOUTS['5'] });

            if (pathToRedirect !== null) {
                if (pathToRedirect.search(':id') !== -1) {
                    router.navigate([pathToRedirect.replace(':id', result?.response?.id)]);
                } else {
                    router.navigate([pathToRedirect]);
                }
            }
        } else {
            dialog.open(SimpleDialogComponent, {
                data: {
                    title: result.message,
                    content: result.message !== null ? result.message : ''
                }
            }).afterClosed().subscribe();
        }
    }
}
