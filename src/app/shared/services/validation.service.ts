import { Injectable } from '@angular/core';
import { AbstractControl, ValidatorFn } from '@angular/forms';

@Injectable({ providedIn: 'root' })
export class ValidationService {

    constructor() { }

    validateFieldValue(Services: any[], fieldToValidate: string, isInsideOnArray: boolean = false ): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } | null => {
            let index = 0;

            if (control.value !== '' && control.value !== null) {
                index = Services.findIndex(Service => {
                    return String(Service[fieldToValidate]) === String(control.value);
                });
            }

            return index < 0 && !isInsideOnArray || index >= 0 && isInsideOnArray ? { selectedValueError: { value: control.value } } : null;
        };
    }

    validValue(field: any): boolean {
        if (field === undefined || field === null || field === '') {
            return false;
        }
        return true;
    }
}
