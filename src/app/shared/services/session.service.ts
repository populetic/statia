import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { RelatedDataType } from '@app/layout/table-related-data/table-related-configuration.interface';
import { Login } from '@app/pages/login/login.model';
import { environment } from '@environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { LOCAL_STORAGE, SESSION_STORAGE, STATIA_SESSION, STATIA_TABLE_RELATED_DATA } from '../constants/constants';
@Injectable({providedIn: 'root'})
export class SessionService {

    constructor(
        private translate?: TranslateService,
        private router?: Router
    ) {}

    get user(): Login {
        return this.getItem(STATIA_SESSION);
    }

    set user(user: Login) {
        this.setItem(STATIA_SESSION, JSON.stringify(user));
        this.translate.use(user.actualLanguage);
    }

    get roleId(): string {
        const user: Login = this.user;
        return (user && user.roleId) ? user.roleId : null;
    }

    get lawFirmId(): string {
        const user: Login = this.user;
        return (user && user.lawFirmId) ? user.lawFirmId : null;
    }

    get token(): string {
        const user: Login = this.user;
        return (user && user.token) ? user.token : null;
    }

    get actualLanguage(): string {
        const user: Login = this.user;
        return (user && user.actualLanguage) ? user.actualLanguage : null;
    }

    set actualLanguage(language: string) {
        const user: Login = this.user;
        if (user?.actualLanguage ) {
            user.actualLanguage = language;
            this.user = user;
        } else {
            this.translate.use(language);
        }
    }

    get isUserLogged(): boolean {
        let isLogged = false;

        if (typeof(Storage) !== 'undefined') {
            const user: Login = this.user;
            if (user !== null) {
                const expirationHours: number = 24 * 30 * 60 * 60 * 1000;
                const actualTime: number = (new Date().getTime()) - user.accessDate;
                if (user.accessDate && (actualTime > expirationHours)) {
                    this.clear();
                } else {
                    isLogged = true;
                }
            } else {
                this.clear();
            }
        }

        return isLogged;
    }

    setItem(key: string, value: string, type: string = LOCAL_STORAGE): void {
        value = (value && environment && environment.production) ? btoa(value) : value;
        if (type === LOCAL_STORAGE) {
            localStorage.setItem(key, value);
            return;
        }

        sessionStorage.setItem(key, value);
    }

    getItem(key: string, type: string = LOCAL_STORAGE): any {
        let value: any;
        if (type === LOCAL_STORAGE) {
            value = localStorage.getItem(key);
            value = (value && environment && environment.production) ? atob(value) : value;
            return value ? JSON.parse(value) : null;
        }

        value = sessionStorage.getItem(key);
        value = (value && environment && environment.production) ? atob(value) : value;
        return value ? JSON.parse(value) : null;
    }

    removeItem(key: string, type: string = LOCAL_STORAGE): void  {
        if (type === LOCAL_STORAGE) {
            return localStorage.removeItem(key);
        }

        return sessionStorage.removeItem(key);
    }

    clear(): void {
        localStorage.clear();
        sessionStorage.clear();
    }

    // Storage Data from TableRelatedData
    getDataTableFromStorage(): RelatedDataType[] | {} {
        const dataTable: RelatedDataType[] = this.getItem(STATIA_TABLE_RELATED_DATA, SESSION_STORAGE);
        return dataTable ?? {};
    }

    getDataByTab(currentTab: string): RelatedDataType[] {
        const dataByTabs: any = this.getDataTableFromStorage();
        return dataByTabs && dataByTabs[currentTab] ? dataByTabs[currentTab] : null;
    }

    setDataByTab(currentTab: string, value: RelatedDataType[]): void {
        const dataByTabs: any = this.getDataTableFromStorage();
        dataByTabs[currentTab] = value;
        this.setItem(STATIA_TABLE_RELATED_DATA, JSON.stringify(dataByTabs), SESSION_STORAGE);
    }

    cleanDataTabs(): void {
        this.removeItem(STATIA_TABLE_RELATED_DATA, SESSION_STORAGE);
    }

    cleanCurrentDataTabs(currentTab: string): void {
        const dataTable: RelatedDataType[] = this.getItem(STATIA_TABLE_RELATED_DATA, SESSION_STORAGE);
        for (const key in dataTable) {
            if (key === currentTab) {
               delete dataTable[currentTab];
               this.setItem(STATIA_TABLE_RELATED_DATA, JSON.stringify(dataTable), SESSION_STORAGE);
            }
        }
    }

    logOut() {
        if (this.isUserLogged) {
            this.clear();
        }

        this.router.navigate(['/login']);
    }

}
