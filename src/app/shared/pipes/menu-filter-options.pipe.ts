import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'menuFilterOptions' })
export class MenuFilterOptionsPipe implements PipeTransform {
    transform(items: any[]): any {
        if (!items) {
            return items;
        }

        return items.filter(item => item.canFilter);
    }
}

@Pipe({ name: 'menuAdvancedFilterOptions' })
export class MenuAdvancedFilterOptionsPipe implements PipeTransform {
    transform(items: any[]): any {
        if (!items) {
            return items;
        }

        return items.filter(item => item.isAdvancedFilter);
    }
}
