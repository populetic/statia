export class Result<T> {
    message?: string;
    response?: T;
    status?: number;
    errors?: string[];
}
