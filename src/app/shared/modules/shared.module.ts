import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TabDirective } from '@app/shared/directives/tab.directive';
import { MaterialModule } from '@fury/shared/material-components.module';
import { MenuAdvancedFilterOptionsPipe, MenuFilterOptionsPipe } from '@pipes/menu-filter-options.pipe';
import {SortByPipe } from '@pipes/sort-by.pipe';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        RouterModule
    ],
    declarations: [
        TabDirective,
        MenuFilterOptionsPipe,
        MenuAdvancedFilterOptionsPipe,
        SortByPipe
    ],
    exports: [
        CommonModule,
        FormsModule,
        MaterialModule,

        TabDirective,
        MenuFilterOptionsPipe,
        MenuAdvancedFilterOptionsPipe,
        SortByPipe
    ]
})

export class SharedModule { }
