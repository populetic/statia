export class Login  {
    id: string;
    username: string;
    email: string;
    name: string;
    surnames: string;
    completeName: string;
    countryId: string;
    countryName: string;
    languageId: string;
    languageName: string;
    languageCode: string;
    roleId: string;
    roleName: string;
    isAdmin: boolean;
    lawFirmId: string;
    lawFirmName: string;
    isActive: boolean;
    token: string;
    accessDate?: number;
    actualLanguage?: string;
    mainMenuId?: string;
    isLawFirmAdmin: boolean;


    constructor(model?: Login) {
        this.id = (model && model.id) ? model.id : null;
        this.username = (model && model.username) ? model.username : null;
        this.email = (model && model.email) ? model.email : null;
        this.name = (model && model.name) ? model.name : null;
        this.surnames = (model && model.surnames) ? model.surnames : null;
        this.countryId = (model && model.countryId) ? model.countryId : null;
        this.languageId = (model && model.languageId) ? model.languageId : null;
        this.roleId = (model && model.roleId) ? model.roleId : null;
        this.lawFirmId = (model && model.lawFirmId) ? model.lawFirmId : null;
        this.isActive = (model && model.isActive) ? model.isActive : false;
        this.completeName = (model && model.completeName) ? model.completeName : null;
        this.countryName = (model && model.countryName) ? model.countryName : null;
        this.languageName = (model && model.languageName) ? model.languageName : null;
        this.languageCode = (model && model.languageCode) ? model.languageCode : null;
        this.roleName = (model && model.roleName) ? model.roleName : null;
        this.isAdmin = (model && model.isAdmin) ? model.isAdmin : false;
        this.lawFirmName = (model && model.lawFirmName) ? model.lawFirmName : null;
        this.token = (model && model.token) ? model.token : null;
        this.accessDate = new Date().getTime();
        this.actualLanguage = (model && model.languageCode) ? model.languageCode : null;
        this.mainMenuId = (model && model.mainMenuId) ? model.mainMenuId : null;
        this.isAdmin = (model && model.isAdmin) ? model.isAdmin : false;
        this.isLawFirmAdmin = (model && model.isLawFirmAdmin) ? model.isLawFirmAdmin : false;
    }
}
