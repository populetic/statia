
import { Login } from './login.model';
import { LoginContract } from '../../api/contracts/login-contract/login.contract';

export class LoginTranslator {
    static translateContractToModel(contract: LoginContract): Login {
        const model: Login = {
            id: (contract && contract.id) ? contract.id : null,
            username: (contract && contract.username) ? contract.username : null,
            email: (contract && contract.email) ? contract.email : null,
            name: (contract && contract.name) ? contract.name : null,
            surnames: (contract && contract.surnames) ? contract.surnames : null,
            countryId: (contract && contract.countryId) ? contract.countryId : null,
            languageId: (contract && contract.languageId) ? contract.languageId : null,
            roleId: (contract && contract.roleId) ? contract.roleId : null,
            lawFirmId: (contract && contract.lawFirmId) ? contract.lawFirmId : null,
            isActive: (contract && contract.isActive === '1') ? true : false,
            completeName: (contract && contract.completeName) ? contract.completeName : null,
            countryName: (contract && contract.countryName) ? contract.countryName : null,
            languageName: (contract && contract.languageName) ? contract.languageName : null,
            languageCode: (contract && contract.languageCode) ? contract.languageCode : null,
            roleName: (contract && contract.roleName) ? contract.roleName : null,
            lawFirmName: (contract && contract.lawFirmName) ? contract.lawFirmName : null,
            token: (contract && contract.token) ? contract.token : null,
            accessDate: null,
            actualLanguage: (contract && contract.languageCode) ? contract.languageCode : null,
            mainMenuId: (contract && contract.mainMenuId) ? contract.mainMenuId : null,
            isAdmin: (contract && contract.isAdmin === '1') ? true : false,
            isLawFirmAdmin: (contract && contract.isLawFirmAdmin === '1') ? true : false
        };

        return new Login(model);
    }
}
