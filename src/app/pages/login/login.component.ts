import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { TIMEOUTS } from '@app/shared/constants/constants';
import { Result } from '@app/shared/models/result.model';
import { SessionService } from '@app/shared/services/session.service';
import { StatiaService } from '@app/shared/services/statia.service';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { Login } from './login.model';
import { LoginService } from './login.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [fadeInUpAnimation],
    providers: [LoginService]
})
export class LoginComponent implements OnInit, OnDestroy {

    public form: FormGroup;

    public inputType = 'password';
    public visible = false;
    public hideForgetPass = false;
    subscriptions: Subscription[] = [];

    constructor(
        private router: Router,
        private fb: FormBuilder,
        private cd: ChangeDetectorRef,
        private snackbar: MatSnackBar,
        private loginService: LoginService,
        private translate: TranslateService,
        private statiaService: StatiaService,
        private sessionService: SessionService,
    ) { }

    ngOnInit() {
        this.form = this.fb.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
            rememberSession: ['']
        });
        if (this.sessionService.isUserLogged) {
            this.router.navigate(['/']); // TODO: navigate to 'home'?
        }
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
    }

    send() {
        if (this.form.invalid) {
            this.snackbar.open( this.translate.instant('snbCompleteAllFields'), 'Close', { duration: TIMEOUTS['3'] });
            return;
        }

        let param: any;
        param = this.form.value;

        const loginSubs: Subscription = this.loginService.login(param).subscribe(
            (result: Result<Login>) => {
                if (result.status === 1) {
                    if (typeof (Storage) !== 'undefined') {
                        const user: Login = new Login(result.response);
                        this.sessionService.user = user;
                        this.statiaService.getMenusByRole(user?.roleId)
                            .then((data: boolean) => {
                                const mainMenu: string =  data && user.mainMenuId ? user.mainMenuId : '';
                                this.router.navigate(['main-menu', mainMenu]);
                            });
                    } else {
                        this.snackbar.open(this.translate.instant('snbErrorLocalStorageUnavailable'), 'Close', { duration: TIMEOUTS['3'] });
                        return;
                    }
                } else {
                    this.snackbar.open(this.translate.instant('snbErrorLogin'), 'Close', { duration: TIMEOUTS['10'] });
                }
            }
        );
        this.subscriptions.push(loginSubs);
    }

    toggleVisibility() {
        if (this.visible) {
            this.inputType = 'password';
            this.visible = false;
            this.cd.markForCheck();
        } else {
            this.inputType = 'text';
            this.visible = true;
            this.cd.markForCheck();
        }
    }
}
