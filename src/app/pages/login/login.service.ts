import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginContract } from '@app/api/contracts/login-contract/login.contract';
import { ApiLoginService } from '@app/api/services/api-login.service/api-login.service';
import { ApiRoleService } from '@app/api/services/api-role-service/api-role.service';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { Login } from './login.model';
import { LoginTranslator } from './login.translator';

@Injectable({ providedIn: 'root' })
export class LoginService {


    constructor(
        private apiLoginService: ApiLoginService
    ) {
    }

    login(param: CustomParams): Observable<Result<Login>> {
        return this.apiLoginService.login(param).pipe(
            map((result: Result<LoginContract>) => {
                const newResult: Result<Login> = StatiaFunctions.getResultModel<LoginContract, Login>(result);
                newResult.response = LoginTranslator.translateContractToModel(result.response);

                return newResult;
            })
        );
    }
}
