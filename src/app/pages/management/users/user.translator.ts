import { User } from './user.model';
import { UserContract } from '../../../api/contracts/user-contract/user.contract';

export class UserTranslator {
    static translateContractToModel(contract: UserContract): User {
        const model: User = {
            id: (contract && contract.id) ? contract.id : null,
            isActive: (contract && contract.isActive === '1') ? true : false,
            isSystem: (contract && contract.isSystem === '1') ? true : false,
            name: (contract && contract.name) ? contract.name : null,
            surnames: (contract && contract.surnames) ? contract.surnames : null,
            username: (contract && contract.username) ? contract.username : null,
            completeName: (contract && contract.completeName) ? contract.completeName : null,
            email: (contract && contract.email) ? contract.email : null,
            roleId: (contract && contract.roleId) ? contract.roleId : null,
            roleName: (contract && contract.roleName) ? contract.roleName : null,
            isAdmin: (contract && contract.isAdmin === '1') ? true : false,
            isLawFirmAdmin: (contract && contract.isLawFirmAdmin === '1') ? true : false,
            countryId: (contract && contract.countryId) ? contract.countryId : null,
            countryName: (contract && contract.countryName) ? contract.countryName : null,
            languageId: (contract && contract.languageId) ? contract.languageId : null,
            languageCode: (contract && contract.languageCode) ? contract.languageCode : null,
            languageName: (contract && contract.languageName) ? contract.languageName : null,
            lawFirmId: (contract && contract.lawFirmId) ? contract.lawFirmId : null,
            lawFirmName: (contract && contract.lawFirmName) ? contract.lawFirmName : null,
            password: (contract && contract.password) ? contract.password : null,
            createdAt: (contract && contract.createdAt) ? new Date(contract.createdAt) : null,
            updatedAt: (contract && contract.updatedAt) ? new Date(contract.updatedAt) : null,
        };
        return new User(model);
    }
}
