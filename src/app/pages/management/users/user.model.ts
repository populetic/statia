export class User {
    id: string;
    isActive: boolean;
    isSystem: boolean;
    name: string;
    surnames: string;
    username: string;
    completeName: string;
    email: string;
    roleId: string;
    roleName: string;
    isAdmin: boolean;
    isLawFirmAdmin: boolean;
    countryId: string;
    countryName: string;
    languageId: string;
    languageCode: string;
    languageName: string;
    lawFirmId: string;
    lawFirmName: string;
    password: string;
    createdAt: Date;
    updatedAt: Date;

    constructor(model?: User) {
        this.id = (model && model.id) ? model.id : null;
        this.isActive = (model && model.isActive) ? model.isActive : false;
        this.isSystem = (model && model.isSystem) ? model.isSystem : false;
        this.name = (model && model.name) ? model.name : null;
        this.surnames = (model && model.surnames) ? model.surnames : null;
        this.username = (model && model.username) ? model.username : null;
        this.completeName = (model && model.completeName) ? model.completeName : null;
        this.email = (model && model.email) ? model.email : null;
        this.roleId = (model && model.roleId) ? model.roleId : null;
        this.roleName = (model && model.roleName) ? model.roleName : null;
        this.isAdmin = (model && model.isAdmin) ? model.isAdmin : false;
        this.isLawFirmAdmin = (model && model.isLawFirmAdmin) ? model.isLawFirmAdmin : false;
        this.countryId = (model && model.countryId) ? model.countryId : null;
        this.countryName = (model && model.countryName) ? model.countryName : null;
        this.languageId = (model && model.languageId) ? model.languageId : null;
        this.languageCode = (model && model.languageCode) ? model.languageCode : null;
        this.languageName = (model && model.languageName) ? model.languageName : null;
        this.lawFirmId = (model && model.lawFirmId) ? model.lawFirmId : null;
        this.lawFirmName = (model && model.lawFirmName) ? model.lawFirmName : null;
        this.password = (model && model.password) ? model.password : null;
        this.createdAt = (model && model.createdAt) ? model.createdAt : null;
        this.updatedAt = (model && model.updatedAt) ? model.updatedAt : null;
    }
}
