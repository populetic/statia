import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MainTableModule } from '@app/layout/main-table/main-table.module';
import { FurySharedModule } from '@fury/fury-shared.module';
import { BreadcrumbsModule } from '@fury/shared/breadcrumbs/breadcrumbs.module';
import { MaterialModule } from '@fury/shared/material-components.module';
import { TranslateModule } from '@ngx-translate/core';

import { UserEditorComponent } from './user-editor/user-editor.component';
import { UserComponent } from './user.component';
import { FormComponentsModule } from '@app/layout/form-components/form-components.module';
import { UserRoutingModule } from './user-routing.module';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        BreadcrumbsModule,
        MaterialModule,
        FurySharedModule,

        UserRoutingModule,
        MainTableModule,
        TranslateModule,
        FormComponentsModule
    ],
    declarations: [UserComponent, UserEditorComponent],
    exports: [UserComponent]
})

export class UserModule {
}
