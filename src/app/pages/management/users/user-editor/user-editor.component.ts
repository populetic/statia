import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { RoleService } from '@app/pages/management/roles/role.service';
import { EMAIL_PATTERN, PASSWORD_PATTERN, POPULETIC_ROLES, FORM_OPERATIONS } from '@app/shared/constants/constants';
import { MODEL_PROPERTY_NAME } from '@app/shared/enums/model-property-name.enum';
import { Result } from '@app/shared/models/result.model';
import { SessionService } from '@app/shared/services/session.service';
import { StatiaService } from '@app/shared/services/statia.service';
import { ValidationService } from '@app/shared/services/validation.service';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { Subscription } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { User } from '../user.model';
import { UserService } from '../user.service';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { PendingInterceptorService } from '@fury/shared/loading-indicator/pending-interceptor.service';
import { NotificationsService } from '@app/shared/services/notifications.service';

@Component({
    selector: 'app-user-editor',
    templateUrl: './user-editor.component.html',
    animations: [fadeInRightAnimation, fadeInUpAnimation]
})

export class UserEditorComponent implements OnInit, OnDestroy {

    public user: User;
    private id: any;
    public parentRoute: string;
    public formAction: string;
    public currentEditor: string;
    public breadcrumbs: string[];
    public editorFormData: FormGroup;
    public isLoading = false;
    public canUseFormOperation: boolean;
    private menuId: string;

    private availableCountries: any;
    public availableRoles: any;
    public availableLanguages: any;
    public availableLawFirms: any;
    public filteredCountries: Observable<any>;

    private emailPattern = EMAIL_PATTERN;
    private passwordPattern = PASSWORD_PATTERN;

    subscriptions: Subscription[] = [];

    constructor(
        private router: Router,
        private dialog: MatDialog,
        private snackbar: MatSnackBar,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private userService: UserService,
        private roleService: RoleService,
        private sessionService: SessionService,
        private validationService: ValidationService,
        private notificationsService: NotificationsService,
        private pendingInterceptorService: PendingInterceptorService,
        private statiaService: StatiaService
    ) {
        this.user = new User();
    }

    ngOnInit() {
        this.route.paramMap.subscribe(paramMap => { this.id = paramMap.get('id'); });
        this.route.data.subscribe(data => this.menuId = data?.menuId);

        this.formAction = (this.id === null ? 'create' : 'edit');
        const routeToReplace = (this.id === null ? '/' + this.formAction : '/' + this.formAction + '/' + this.id);
        this.parentRoute = this.router.url.replace(routeToReplace, '');

        const routeItems = this.parentRoute.split('/').filter(item => item !== null && item !== '');
        this.breadcrumbs = routeItems;

        if (this.id !== null) {
            this.canUseFormOperation = StatiaFunctions.EnabledOption(this.menuId, FORM_OPERATIONS.EDIT);
            const sub: Subscription = this.pendingInterceptorService.status$
                .subscribe((value: boolean) => this.isLoading = value);
            this.breadcrumbs.push('edit');
        } else {
            this.canUseFormOperation = StatiaFunctions.EnabledOption(this.menuId, FORM_OPERATIONS.CREATE);
            this.currentEditor = 'create';
        }

        this.createFormGroup();
        this.configureForm();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
    }

    private createFormGroup() {
        const userForm: any = {
            name: ['', Validators.required],
            surnames: ['', Validators.required],
            username: ['', Validators.required],
            email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
            isActive: [false, Validators.required],
            isSystem: [false, Validators.required],
            roleId: ['', Validators.required],
            countryName: ['', Validators.required],
            languageId: ['', Validators.required],
            lawFirmId: ['']
        };

        if (this.formAction === 'create') {
            userForm.password = [
                '', [Validators.required, Validators.pattern(this.passwordPattern)]
            ];
        } else {
            userForm.password = ['', Validators.pattern(this.passwordPattern)];
            userForm.confirmPassword = [''];
        }

        this.editorFormData = this.formBuilder.group(userForm);
    }

    private async configureForm() {
        // TODO: utilizar funcion getPropertyValues statia service
        await new Promise((resolve) => {
            const roles: any = this.sessionService.getItem(POPULETIC_ROLES);
            if (roles) {
                this.availableRoles = roles;
                resolve();
            } else {
                this.roleService.getAllAsync(null).then((result: any) => {
                    this.availableRoles = result.response;
                    this.sessionService.setItem(POPULETIC_ROLES, JSON.stringify(result.response));
                    resolve();
                });
            }
        });

        this.availableLanguages =
            await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.LANGUAGE_SELECTOR, false, true);
        this.availableLawFirms = await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.LAW_FIRM);
        this.availableCountries = await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.COUNTRY);

        this.filteredCountries = this.editorFormData.controls['countryName'].valueChanges.pipe(
            map(value => (value ?
                this.statiaService._filter(value, this.availableCountries, ['name']) :
                this.availableCountries.slice()))
        );

        this.editorFormData.controls['countryName'].setValidators(
            [Validators.required, this.validationService.validateFieldValue(this.availableCountries, 'name')]
        );

        if (this.formAction === 'edit') {
            this.changeConfirmPasswordValidation();
            this.getData();
        }
    }

    changeConfirmPasswordValidation() {
        this.editorFormData.controls['password'].valueChanges.subscribe(value => {
            const confirmPasswordValidators = [Validators.required, this.validatePassword()];

            if (value !== '') {
                this.editorFormData.controls['confirmPassword'].setValidators(confirmPasswordValidators);
            } else {
                this.editorFormData.controls['confirmPassword'].setValidators([]);
            }

            this.editorFormData.controls['confirmPassword'].updateValueAndValidity();
        });
    }

    getData() {
        const subs: Subscription = this.userService.getById(this.id).subscribe(
            (result: Result<User>) => {

                this.user = result.response;

                this.editorFormData.patchValue({ name: this.user.name });
                this.editorFormData.patchValue({ surnames: this.user.surnames });
                this.editorFormData.patchValue({ username: this.user.username });
                this.editorFormData.patchValue({ email: this.user.email });
                this.editorFormData.patchValue({ isActive: this.user.isActive });
                this.editorFormData.patchValue({ isSystem: this.user.isSystem });
                this.editorFormData.patchValue({ roleId: this.user.roleId });
                this.editorFormData.patchValue({ countryName: this.user.countryName });
                this.editorFormData.patchValue({ languageId: this.user.languageId });
                this.editorFormData.patchValue({ lawFirmId: this.user.lawFirmId });

                this.currentEditor = this.user.completeName;
            }
        );
        this.subscriptions.push(subs);
    }

    get emailFormControl() {
        return this.editorFormData.get('email');
    }

    get countryFormControl() {
        return this.editorFormData.get('countryName');
    }

    get passwordFormControl() {
        return this.editorFormData.get('password');
    }

    get confirmPasswordFormControl() {
        return this.editorFormData.get('confirmPassword');
    }

    onSubmit() {
        if (this.editorFormData.invalid) {
            return;
        }

        let pathToRedirect: string = null;
        let param: any;
        param = this.editorFormData.value;

        const selectedCountry = this.availableCountries.filter(
            (country: { name: any; }) => country.name === param.countryName
        );

        param.countryId = selectedCountry[0].id;
        let sub: Subscription;

        if (this.formAction === 'create') {
            pathToRedirect = this.parentRoute;
            sub = this.userService.create(param).subscribe(
                result => {
                    this.notificationsService.manageServiceResult(result, pathToRedirect, this.router, this.snackbar, this.dialog);
                }
            );
        } else {
            sub = this.userService.edit(this.id, param).subscribe(
                result => {
                    this.notificationsService.manageServiceResult(result, pathToRedirect, this.router, this.snackbar, this.dialog);
                }
            );
        }
        this.subscriptions.push(sub);
    }

    validatePassword(): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } | null => {
            let result = null;

            if (control.value !== this.passwordFormControl.value) {
                result = { confirmPasswordError: { value: control.value } };
            }

            return result;
        };
    }

}
