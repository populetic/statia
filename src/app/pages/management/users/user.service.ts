import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserContract } from '@app/api/contracts/user-contract/user.contract';
import { ApiUserService } from '@app/api/services/api-user-service/api-user.service';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from './user.model';
import { UserTranslator } from './user.translator';
@Injectable({ providedIn: 'root' })
export class UserService  implements BaseInterface<User> {
    public resourceUrl = environment.apiUrl + '/users';
    public headers: HttpHeaders;

    constructor (
        public http: HttpClient,
        public apiUserService: ApiUserService
    ) { }

    getAll(customParams: CustomParams = null): Observable<Result<User[]>> {
        return this.apiUserService.getAll(customParams)
            .pipe(
                map((result: Result<UserContract[]>) => {
                    const newResult: Result<User[]> = StatiaFunctions.getResultModel<UserContract[], User[]>(result);
                    newResult.response = result.response.map((user: UserContract) => UserTranslator.translateContractToModel(user));
                    return  newResult;
                })
            );
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<User[]>> {
        return new Promise((resolve, reject) => {
            this.apiUserService.getAllAsync(customParams)
                .then((result: Result<UserContract[]>) => {
                    const newResult: Result<User[]> = StatiaFunctions.getResultModel<UserContract[], User[]>(result);
                    newResult.response = result.response.map((user: UserContract) => UserTranslator.translateContractToModel(user));
                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    getById(id: number|string): Observable<Result<User>> {
        return this.apiUserService.getById(id).pipe(
            map((result: Result<UserContract>) => {
                const newResult: Result<User> = StatiaFunctions.getResultModel<UserContract, User>(result);
                const user: User = UserTranslator.translateContractToModel(result.response);
                newResult.response = user;

                return  newResult;
            })
        );
    }

    create(param: User): Observable<Result<User>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: User): Promise<Result<User>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: User): Observable<Result<User>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: User): Promise<Result<User>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    // stand by trello task 223
    deleteByParam(param: any): Observable<Result<User>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const options = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', options);
    }
}
