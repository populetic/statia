import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DepartmentContract } from '@app/api/contracts/department-contract/department.contract';
import { ApiDepartmentService } from '@app/api/services/api-department-service/api-department.service';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Department } from './department.model';
import { DepartmentTranslator } from './department.translator';

@Injectable({ providedIn: 'root' })
export class DepartmentService implements BaseInterface<Department> {
    public resourceUrl = environment.apiUrlStatia + '/departments';
    public headers: HttpHeaders;

    constructor(private http: HttpClient, private apiDepartmerntService: ApiDepartmentService) { }

    getAll(customParams: CustomParams = null): Observable<Result<Department[]>> {
        return this.apiDepartmerntService.getAll(customParams)
            .pipe(
                map((result: Result<DepartmentContract[]>) => {
                    const newResult: Result<Department[]> = StatiaFunctions.getResultModel<DepartmentContract[], Department[]>(result);
                    newResult.response = result.response.map((department: DepartmentContract) =>
                        DepartmentTranslator.translateContractToModel(department));
                    return  newResult;
                })
            );
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<Department[]>> {
        return new Promise((resolve, reject) => {
            this.apiDepartmerntService.getAllAsync(customParams)
                .then((result: Result<DepartmentContract[]>) => {
                    const newResult: Result<Department[]> = StatiaFunctions.getResultModel<DepartmentContract[], Department[]>(result);
                    newResult.response = result.response.map((department: Department) =>
                        DepartmentTranslator.translateContractToModel(department));
                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    getById(id: number|string): Observable<Result<Department>> {
        return this.apiDepartmerntService.getById(id).pipe(
            map((result: Result<DepartmentContract>) => {
                const newResult: Result<Department> = StatiaFunctions.getResultModel<DepartmentContract, Department>(result);
                const department: Department = DepartmentTranslator.translateContractToModel(result.response);
                newResult.response = department;

                return  newResult;
            })
        );
    }

    create(param: Department): Observable<Result<Department>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: Department): Promise<Result<Department>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: Department): Observable<Result<Department>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: Department): Promise<Result<Department>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    // stand by trello task 223
    deleteByParam(param: any): Observable<Result<Department>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const Departments = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', Departments);
    }
}
