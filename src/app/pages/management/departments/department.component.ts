import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DepartmentService } from './department.service';

@Component({
    selector: 'app-menu',
    template: `<app-main-table
                    [actualMenuId]="menuId"
                    [actualMenuService]="menuService"
                    [actualMenuFilters]="menuFilters">
                </app-main-table>`
})

export class DepartmentComponent implements OnInit {

    public menuId: string;
    public menuFilters: Array<any>;

    constructor(
        private activeRoute: ActivatedRoute,
        public menuService: DepartmentService
    ) {}

    ngOnInit() {
        this.activeRoute.data.subscribe(
            data => this.menuId = data.menuId
        );
    }
}

