import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MainTableModule } from '@app/layout/main-table/main-table.module';
import { FurySharedModule } from '@fury/fury-shared.module';
import { BreadcrumbsModule } from '@fury/shared/breadcrumbs/breadcrumbs.module';
import { MaterialModule } from '@fury/shared/material-components.module';
import { DepartmentRoutingModule } from './deparment-routing.module';
import { DepartmentEditorComponent } from './department-editor/department-editor.component';
import { DepartmentComponent } from './department.component';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        BreadcrumbsModule,
        MaterialModule,
        FurySharedModule,

        DepartmentRoutingModule,
        MainTableModule
    ],
    declarations: [DepartmentComponent, DepartmentEditorComponent],
    exports: [DepartmentComponent]
})

export class DepartmentModule {
}
