export class Department {
    id: string;
    name: string;

    constructor(model?: Department) {
        this.id = (model && model.id) ? model.id : null;
        this.name = (model && model.name) ? model.name : null;
    }

}
