import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { Subscription } from 'rxjs';
import { Department } from '../department.model';
import { DepartmentService } from '../department.service';
import { NotificationsService } from '@app/shared/services/notifications.service';

@Component({
    selector: 'app-department-editor',
    templateUrl: './department-editor.component.html',
    animations: [fadeInRightAnimation, fadeInUpAnimation]
})

export class DepartmentEditorComponent implements OnInit, OnDestroy {

    private department: Department;
    private id: any;
    public parentRoute: string;
    public currentEditor: string;
    public breadcrumbs: string[];
    public formAction: string;
    public editorFormData: FormGroup;
    subscriptions: Subscription[] = [];

    constructor(
        private snackbar: MatSnackBar,
        private dialog: MatDialog,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private notificationsService: NotificationsService,
        private departmentService: DepartmentService
    ) {
        this.department = new Department();
    }

    ngOnInit() {
        this.route.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id');
        });

        this.formAction = (this.id === null ? 'create' : 'edit');
        const routeToReplace = (this.id === null ? '/' + this.formAction : '/' + this.formAction + '/' + this.id);
        this.parentRoute = this.router.url.replace(routeToReplace, '');

        const routeItems = this.parentRoute.split('/').filter(item => item !== null && item !== '');
        this.breadcrumbs = routeItems;

        if (this.id !== null) {
            this.breadcrumbs.push('edit');
            this.getData();
        } else {
            this.currentEditor = 'create';
        }

        this.createFormGroup();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
    }

    getData() {
        const departmentSubs: Subscription = this.departmentService.getById(this.id).subscribe(
            result => {
                this.department = result.response;
                this.editorFormData.patchValue({ name: this.department.name });

                this.currentEditor = this.department.name;
            }
        );

        this.subscriptions.push(departmentSubs);
    }

    createFormGroup() {
        this.editorFormData = this.formBuilder.group({ name: ['', Validators.required] });
    }

    onSubmit() {
        if (this.editorFormData.invalid) {
            return;
        }

        let param: any;
        param = this.editorFormData.value;
        let departmentSubs: Subscription;

        if (this.formAction === 'create') {
            departmentSubs = this.departmentService.create(param).subscribe(
                result => {
                    this.notificationsService.manageServiceResult(result, this.parentRoute, this.router, this.snackbar, this.dialog);
                }
            );
        } else {
            departmentSubs = this.departmentService.edit(this.id, param).subscribe(
                result => {
                    this.notificationsService.manageServiceResult(result, this.parentRoute, this.router, this.snackbar, this.dialog);
                }
            );
        }

        this.subscriptions.push(departmentSubs);
    }

}
