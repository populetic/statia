import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DepartmentEditorComponent } from './department-editor/department-editor.component';
import { DepartmentComponent } from './department.component';

const routes: Routes = [
        {
            path: '',
            component: DepartmentComponent
        },
        {
            path: 'create',
            component: DepartmentEditorComponent
        },
        {
            path: 'edit/:id',
            component: DepartmentEditorComponent
        }
    ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DepartmentRoutingModule { }
