import { Department } from './department.model';
import { DepartmentContract } from '../../../api/contracts/department-contract/department.contract';

export class DepartmentTranslator {
    static translateContractToModel(contract: DepartmentContract): Department {
        const model: Department = {
            id: (contract && contract.id) ? contract.id : null,
            name: (contract && contract.name) ? contract.name : null
        };

        return new Department(model);
    }
}
