import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { RoleService } from './role.service';

@Component({
    selector: 'app-menu',
    template: `<app-main-table [actualMenuId]="menuId" [actualMenuService]="menuService"></app-main-table>`
})

export class RoleComponent implements OnInit {

    public menuId: string;

    constructor(
        private activeRoute: ActivatedRoute,
        public menuService: RoleService
    ) {}

    ngOnInit() {
        this.activeRoute.data.subscribe(
            data => this.menuId = data.menuId
        );
    }
}

