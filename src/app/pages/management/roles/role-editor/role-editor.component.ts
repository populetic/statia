import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { DepartmentService } from '@app/pages/management/departments/department.service';
import { Menu } from '@app/pages/statia-configuration/menus/menu.model';
import { MenuService } from '@app/pages/statia-configuration/menus/menu.service';
import { POPULETIC_DEPARTMENTS, POPULETIC_MENUS } from '@app/shared/constants/constants';
import { Result } from '@app/shared/models/result.model';
import { SessionService } from '@app/shared/services/session.service';
import { StatiaService } from '@app/shared/services/statia.service';
import { ValidationService } from '@app/shared/services/validation.service';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Role } from '../role.model';
import { RoleService } from '../role.service';
import { NotificationsService } from '@app/shared/services/notifications.service';
import { OptionService } from '@app/pages/statia-configuration/options/option.service';
import { Option } from '@app/pages/statia-configuration/options/option.model';

@Component({
    selector: 'app-role-editor',
    templateUrl: './role-editor.component.html',
    animations: [fadeInRightAnimation, fadeInUpAnimation]
})

export class RoleEditorComponent implements OnInit, OnDestroy {

    private id: any;
    private role: Role;

    public parentRoute: string;
    public formAction: string;
    public currentEditor: string;
    public breadcrumbs: string[];
    public editorFormData: FormGroup;

    public availableDepartments: any;
    public filteredDepartments: Observable<any>;

    public availableMenus: Menu[];
    public filteredMenus: Observable<any>;

    public menusOptions: any;
    subscriptions: Subscription[] = [];

    constructor(
        private router: Router,
        private dialog: MatDialog,
        private snackbar: MatSnackBar,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private roleService: RoleService,
        private menuService: MenuService,
        private optionService: OptionService,
        private statiaService: StatiaService,
        private validationService: ValidationService,
        private sessionService: SessionService,
        private notificationsService: NotificationsService,
        private departmentService: DepartmentService
    ) {
        this.role = new Role();
    }

    ngOnInit() {
        this.route.paramMap.subscribe(paramMap => { this.id = paramMap.get('id'); });

        this.formAction = (this.id === null ? 'create' : 'edit');
        const routeToReplace = (this.id === null ? '/' + this.formAction : '/' + this.formAction + '/' + this.id);
        this.parentRoute = this.router.url.replace(routeToReplace, '');

        const routeItems = this.parentRoute.split('/').filter(item => item !== null && item !== '');
        this.breadcrumbs = routeItems;

        if (this.id !== null) {
            this.breadcrumbs.push('edit');
        } else {
            this.currentEditor = 'create';
        }

        this.createFormGroup();
        this.configureForm();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
    }

    createFormGroup() {
        const roleForm: any = {
            name: ['', Validators.required],
            departmentName: ['', Validators.required],
            mainMenuName: [''],
            isAdmin: [false],
        };

        this.editorFormData = this.formBuilder.group(roleForm);
    }

    private async configureForm() {
        await new Promise((resolve) => {
            const departments: any = this.sessionService.getItem(POPULETIC_DEPARTMENTS);
            if (departments) {
                this.availableDepartments = departments;
                resolve();
            } else {
                this.departmentService.getAllAsync(null).then((result: any) => {
                    this.availableDepartments = result.response;
                    this.sessionService.setItem(POPULETIC_DEPARTMENTS, JSON.stringify(result.response));
                    resolve();
                });
            }
        });

        this.filteredDepartments = this.editorFormData.controls['departmentName'].valueChanges.pipe(
            startWith(''),
            map(value => (value ? this._filterDepartments(value) : this.availableDepartments.slice()))
        );

        this.editorFormData.controls['departmentName'].setValidators(
            [Validators.required, this.validationService.validateFieldValue(this.availableDepartments, 'name')]
        );

        await new Promise((resolve) => {
            const menus: Menu[] = this.sessionService.getItem(POPULETIC_MENUS);
            if (menus) {
                this.availableMenus = menus;
                resolve();
            } else {
                this.menuService.getAllAsync(null).then((result: Result<Menu[]>) => {
                    this.sessionService.setItem(POPULETIC_MENUS, JSON.stringify(result.response));
                    this.availableMenus = result.response;
                    resolve();
                });
            }
        });

        this.filteredMenus = this.editorFormData.controls['mainMenuName'].valueChanges.pipe(
            startWith(''),
            map(value => ( value ? this._filterMenus(value) : this.availableMenus.filter((menu: Menu) => menu.parentId !== null).slice()))
        );

        this.editorFormData.controls['mainMenuName'].setValidators(
            [this.validationService.validateFieldValue(this.availableMenus, 'name')]
        );

        if (this.formAction === 'edit') {
            this.createAvailableMenusOptionsSelector();
            this.getData();
        }
    }

    private _filterDepartments(value: string): string[] {
        const filterValue = value.toLowerCase();

        return this.availableDepartments.filter((department: { name: string; }) => {
            return department.name.toLowerCase().indexOf(filterValue) === 0;
        });
    }

    private _filterMenus(value: string): Menu[] {
        const filterValue = value.toLowerCase();

        return this.availableMenus.filter((menu: Menu) => menu.name.toLowerCase().indexOf(filterValue) === 0 && menu.parentId !== null);
    }

    getData() {
        const sub: Subscription = this.roleService.getById(this.id).subscribe(
            result => {
                this.role = result.response;

                this.editorFormData.patchValue({ name: this.role.name });
                this.editorFormData.patchValue({ departmentName: this.role.departmentName });
                this.editorFormData.patchValue({ isAdmin: this.role.isAdmin });

                if (this.role.mainMenuName !== null) {
                    this.editorFormData.patchValue({ mainMenuName: this.role.mainMenuName });
                }

                this.currentEditor = this.role.name[0].toUpperCase() + this.role.name.substr(1).toLowerCase();
            }
        );
        this.subscriptions.push(sub);
    }

    get departmentFormControl() {
        return this.editorFormData.get('departmentName');
    }

    get menuFormControl() {
        return this.editorFormData.get('mainMenuName');
    }

    onSubmit() {
        if (this.editorFormData.invalid) {
            return;
        }

        let mainMenuIdVal = null;
        let pathToRedirect: string = null;
        let param: any;
        param = this.editorFormData.value;

        const selectedDepartment = this.availableDepartments.filter(
            (department: { name: any; }) => department.name === param.departmentName
        );

        param.departmentId = selectedDepartment[0].id;

        if (param.mainMenuName !== '') {
            const selectedMainMenu = this.availableMenus.filter((menu: Menu) => menu.name === param.mainMenuName);
            mainMenuIdVal = selectedMainMenu[0].id;
        }

        param.mainMenuId = mainMenuIdVal;

        let sub: Subscription;
        if (this.formAction === 'create') {
            sub = this.roleService.create(param).subscribe(
                result => {
                    pathToRedirect = this.parentRoute + '/edit/:id';
                    this.notificationsService.manageServiceResult(result, pathToRedirect, this.router, this.snackbar, this.dialog);
                }
            );
        } else {
            sub = this.roleService.edit(this.id, param).subscribe(
                result => {
                    this.notificationsService.manageServiceResult(result, pathToRedirect, this.router, this.snackbar, this.dialog);
                }
            );
        }
        this.subscriptions.push(sub);
    }

    private async createAvailableMenusOptionsSelector() {
        let activatedMenusOptions: Menu[];

        /* Se recogen los menus activos y sus funcionalidades activas para el rol */
        await new Promise((resolve) => {
            this.statiaService.getAllMenuOptionsByRole(this.id).then((result: any) => {
                activatedMenusOptions = result.response;
                resolve();
            });
        });

        const menusOptionsList: any = [];

        /* Montamos menusOptionsList que contendrá todos los menus disponibles en Statia con marcando como isSelected
        los que ya están activos/habilitados para el rol a editar */
        this.availableMenus.forEach(function (menu: Menu) {
            const activeMenu: boolean = activatedMenusOptions.some((activatedMenu: Menu) =>  activatedMenu.id === menu.id);

            if (menu.parentId === null) {
                menusOptionsList.push({
                    id: menu.id,
                    name: menu.name,
                    position: menu.position,
                    isSelected: activeMenu,
                    children: []
                });
            } else {
                /* Indice de menu padre en menusOptionsList para añadir este submenu como children */
                const parentMenuIndex = menusOptionsList.findIndex(
                    (parentMenu: { name: string; }) => parentMenu.name === menu.parentName
                );

                /* Indice de menu en activatedMenusOptions para visualizar las opciones ya activadas */
                const activeMenuIndex = activatedMenusOptions.findIndex((activatedMenu: Menu) => activatedMenu.id === menu.id);

                menu.options.forEach(function (option: any) {
                    let isOptionSelected = false;

                    if (activeMenuIndex > 0) {
                        isOptionSelected = activatedMenusOptions[activeMenuIndex].options.some(
                            function (activatedOption: { id: any; }) {
                                return activatedOption.id === option.id;
                            });
                    }

                    option.isSelected = isOptionSelected;
                    option.isEnabled = activeMenu; // Si el menu está activado las opciones estarán habilitadas
                });

                menusOptionsList[parentMenuIndex].children.push({
                    id: menu.id,
                    name: menu.name,
                    icon: menu.icon,
                    isSelected: activeMenu,
                    options: menu.options
                });
            }
        }, this);

        menusOptionsList.sort(function (a: any, b: any) {
            if (Number(a.position) > Number(b.position)) {
                return 1;
            }
            if (Number(a.position) < Number(b.position)) {
                return -1;
            }
            return 0;
        });

        this.menusOptions = menusOptionsList;
    }

    menuChanged(event: any, parentMenu: any, subMenu: any | Boolean) {
        if (subMenu === false) {
            parentMenu.isSelected = event.checked;
        } else {
            subMenu.isSelected = event.checked;

            if (!event.checked) {
                subMenu.options.forEach(function (option: any) {
                    option.isSelected = false;
                    option.isEnabled = false;
                });
            } else {
                subMenu.options.forEach(function (option: any) {
                    option.isEnabled = true;
                });
            }
        }
    }

    optionChanged(event: any, option: any) {
        option.isSelected = event.checked;
    }

    saveMenuConfiguration() {
        const menusOptionsEdited: any = {};

        menusOptionsEdited.roleId = this.id;
        menusOptionsEdited.menusOptions = [];

        this.menusOptions.forEach(function (parentMenu: any) {
            if (parentMenu.isSelected) {
                menusOptionsEdited.menusOptions.push({
                    'menuId': parentMenu.id,
                    'quickaccess': 0,
                    'optionIds': []
                });
            }

            const selectedSubmenus = parentMenu.children.filter((subMenu: { isSelected: any; }) => {
                return subMenu.isSelected;
            });

            selectedSubmenus.forEach(function (subMenu: any) {
                const selectedOptionsIds = [];
                const selectedOptions = subMenu.options.filter((option: { isSelected: any; }) => {
                    return option.isSelected;
                });

                if (selectedOptions.length > 0) {
                    for (const option of selectedOptions) {
                        selectedOptionsIds.push(option.id);
                    }
                }

                menusOptionsEdited.menusOptions.push({
                    'menuId': subMenu.id,
                    'quickaccess': 0,
                    'optionIds': selectedOptionsIds
                });
            });

        });

        const sub: Subscription = this.optionService.editRoleMenusOptions(menusOptionsEdited).subscribe(
            (result) => {
                const pathToRedirect = null;
                this.notificationsService.manageServiceResult(result, pathToRedirect, this.router, this.snackbar, this.dialog);
            }
        );
        this.subscriptions.push(sub);
    }
}
