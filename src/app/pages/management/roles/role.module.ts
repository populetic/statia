import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { TranslateModule } from '@ngx-translate/core';

import { BreadcrumbsModule } from '@fury/shared/breadcrumbs/breadcrumbs.module';
import { FurySharedModule } from '@fury/fury-shared.module';
import { MaterialModule } from '@fury/shared/material-components.module';

import { MatExpansionModule } from '@angular/material/expansion';

import { MainTableModule } from '@app/layout/main-table/main-table.module';

import { RoleComponent } from './role.component';
import { RoleEditorComponent } from './role-editor/role-editor.component';
import { RoleRoutingModule } from './role-routing.module';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        BreadcrumbsModule,
        MaterialModule,
        FurySharedModule,
        MatExpansionModule,
        TranslateModule,

        RoleRoutingModule,
        MainTableModule
    ],
    declarations: [RoleComponent, RoleEditorComponent],
    exports: [RoleComponent]
})

export class RoleModule {
}
