export class Role {
    id?: string;
    name?: string;
    departmentId?: string;
    departmentName?: string;
    mainMenuId?: string;
    mainMenuName?: string;
    isAdmin?: boolean;

    constructor(model?: Role) {
        this.id = (model && model.id) ? model.id : null;
        this.name = (model && model.name) ? model.name : null;
        this.departmentId = (model && model.departmentId) ? model.departmentId : null;
        this.departmentName = (model && model.departmentName) ? model.departmentName : null;
        this.mainMenuId = (model && model.mainMenuId) ? model.mainMenuId : null;
        this.mainMenuName = (model && model.mainMenuName) ? model.mainMenuName : null;
        this.isAdmin = (model && model.isAdmin) ? model.isAdmin : false;
    }

}
