import { Role } from './role.model';
import { RoleContract } from '../../../api/contracts/role-contract/role.contract';

export class RoleTranslator {
    static translateContractToModel(contract: RoleContract): Role {
        const model: Role = {
            id: (contract && contract.id) ? contract.id : null,
            name: (contract && contract.name) ? contract.name : null,
            departmentId: (contract && contract.departmentId) ? contract.departmentId : null,
            departmentName: (contract && contract.departmentName) ? contract.departmentName : null,
            mainMenuId: (contract && contract.mainMenuId) ? contract.mainMenuId : null,
            mainMenuName: (contract && contract.mainMenuName) ? contract.mainMenuName : null,
            isAdmin: (contract && contract.isAdmin === '1') ? true : false
        };

        return new Role(model);
    }
}
