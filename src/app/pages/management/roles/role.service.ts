import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiRoleService } from '@app/api/services/api-role-service/api-role.service';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Role } from './role.model';
import { RoleTranslator } from './role.translator';
import { RoleContract } from '@app/api/contracts/role-contract/role.contract';

@Injectable({ providedIn: 'root' })
export class RoleService implements BaseInterface<Role> {
    public resourceUrl = environment.apiUrlStatia + '/roles';
    public headers: HttpHeaders;

    constructor(private  http: HttpClient, private apiRoleService: ApiRoleService) { }

    getAll(customParams: CustomParams = null): Observable<Result<Role[]>> {
        return this.apiRoleService.getAll(customParams)
            .pipe(
                map((result: Result<RoleContract[]>) => {
                    const newResult: Result<Role[]> = StatiaFunctions.getResultModel<RoleContract[], Role[]>(result);
                    newResult.response = result.response.map((role: RoleContract) => RoleTranslator.translateContractToModel(role));
                    return  newResult;
                })
            );
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<Role[]>> {
        return new Promise((resolve, reject) => {
            this.apiRoleService.getAllAsync(customParams)
                .then((result: Result<RoleContract[]>) => {
                    const newResult: Result<Role[]> = StatiaFunctions.getResultModel<RoleContract[], Role[]>(result);
                    newResult.response = result.response.map((role: RoleContract) => RoleTranslator.translateContractToModel(role));
                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    getById(id: number|string): Observable<Result<Role>> {
        return this.apiRoleService.getById(id).pipe(
            map((result: Result<RoleContract>) => {
                const newResult: Result<Role> = StatiaFunctions.getResultModel<RoleContract, Role>(result);
                const role: Role = RoleTranslator.translateContractToModel(result.response);
                newResult.response = role;

                return  newResult;
            })
        );
    }

    create(param: Role): Observable<Result<Role>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: Role): Promise<Result<Role>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: Role): Observable<Result<Role>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: Role): Promise<Result<Role>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<Role>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const Roles = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', Roles);
    }
}
