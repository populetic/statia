import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoleEditorComponent } from './role-editor/role-editor.component';
import { RoleComponent } from './role.component';

const routes: Routes = [
        {
            path: '',
            component: RoleComponent
        },
        {
            path: 'create',
            component: RoleEditorComponent
        },
        {
            path: 'edit/:id',
            component: RoleEditorComponent
        }
    ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RoleRoutingModule { }
