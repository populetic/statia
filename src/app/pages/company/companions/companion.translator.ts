import { CompanionContract } from '../../../api/contracts/companion-contract/companion.contract';
import { Companion } from './companion.model';
import { CLAIM_STATUS, DOCUMENTATION_STATUS } from '@app/shared/constants/constants';

export class CompanionTranslator {
    static translateContractCompanionToModel(contract: CompanionContract): Companion {
        const model: Companion = {
            claimId: contract?.companionClaimId ?? null,
            name: contract?.companionName ?? null,
            surnames: contract?.companionSurnames ?? null,
            email: contract?.companionEmail ?? null,
            idCard: contract?.idCard ?? null,
            claimStatusId: contract?.claimStatusId ?? null,
            claimDocumentationStatusId: contract?.claimDocumentationStatusId ?? null,
            finishedAt: contract?.finishedAt ? new Date(contract?.finishedAt) :  null,
            completeName: contract?.companionName + ' ' + contract?.companionSurnames,
            isSelected: true,
            isClaimant: this.isClaimant(contract),
            amountEstimated: contract?.amountEstimated ? Number(contract?.amountEstimated) : 0,
            amountTickets: contract?.amountTickets ? Number(contract?.amountTickets) : 0
        };

        return new Companion(model);
    }

    static translateContractPossibleCompanionToModel(contract: CompanionContract): Companion {
        const model: Companion = {
            claimId: contract?.claimId ?? null,
            name: contract?.name ?? null,
            surnames: contract?.surnames ?? null,
            email: contract?.email ?? null,
            idCard: contract?.idCard ?? null,
            claimStatusId: contract?.claimStatusId ?? null,
            claimDocumentationStatusId: contract?.claimDocumentationStatusId ?? null,
            finishedAt: contract?.finishedAt ? new Date(contract?.finishedAt) :  null,
            completeName: contract?.name + ' ' + contract?.surnames,
            isSelected: false,
            isClaimant: this.isClaimant(contract),
            amountEstimated: contract?.amountEstimated ? Number(contract?.amountEstimated) : 0,
            amountTickets: contract?.amountTickets ? Number(contract?.amountTickets) : 0
        };

        return new Companion(model);

    }

    static isClaimant(contract: CompanionContract): boolean {
        if (contract?.finishedAt && (contract?.claimDocumentationStatusId === DOCUMENTATION_STATUS.CORRECT_DOCUMENTATION ||
            contract?.claimDocumentationStatusId === DOCUMENTATION_STATUS.REVISION_CORRECT) &&
            (contract?.claimStatusId === CLAIM_STATUS.ASSIGNED_DEP_LEGAL ||
                contract?.claimStatusId === CLAIM_STATUS.PENDING_DEP_LEGAL ||
                contract?.claimStatusId === CLAIM_STATUS.EXTRAJUDICIAL_SENT ||
                contract?.claimStatusId === CLAIM_STATUS.EXTRAJUDICIAL_PENDING ||
                contract?.claimStatusId === CLAIM_STATUS.LACK_OF_JUDICIAL_COMPETITION)) {
            return true;
        }

        return false;
    }
}
