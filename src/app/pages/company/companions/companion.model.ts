export class Companion {
    claimId: string;
    name: string;
    surnames: string;
    email: string;
    idCard: string;
    claimStatusId: string;
    claimDocumentationStatusId: string;
    finishedAt: Date;
    isSelected?: boolean;
    completeName?: string;
    isClaimant?: boolean;
    amountEstimated?: number;
    amountTickets?: number;

    constructor(model?: Companion) {
        this.claimId = model?.claimId ?? null;
        this.name = model?.name ?? null;
        this.surnames = model?.surnames ?? null;
        this.email = model?.email ?? null;
        this.idCard = model?.idCard ?? null;
        this.claimStatusId = model?.claimStatusId ?? null;
        this.claimDocumentationStatusId = model?.claimDocumentationStatusId ?? null;
        this.finishedAt = model?.finishedAt ?? null;
        this.completeName = model?.completeName ?? null;
        this.isSelected = model?.isSelected ?? false;
        this.isClaimant = model?.isClaimant ?? false;
        this.amountEstimated = model?.amountEstimated ?? 0;
        this.amountTickets = model?.amountTickets ?? 0;
    }
}
