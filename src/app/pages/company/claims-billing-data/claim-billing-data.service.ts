import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { ClaimBillingData } from './claim-billing-data.model';

@Injectable({ providedIn: 'root' })
export class ClaimBillingDataService implements BaseInterface<ClaimBillingData> {
    public resourceUrl: string = environment.apiUrl + '/claims-billing-data';
    public headers: HttpHeaders;

    constructor( private http: HttpClient ) {}

    //#region interface functions
    getAll(param: CustomParams = null): Observable<Result<ClaimBillingData[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(param: CustomParams = null): Promise<Result<ClaimBillingData[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number|string): Observable<Result<ClaimBillingData>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(param: ClaimBillingData): Observable<Result<ClaimBillingData>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: ClaimBillingData): Promise<Result<ClaimBillingData>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: ClaimBillingData): Observable<Result<ClaimBillingData>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: ClaimBillingData): Promise<Result<ClaimBillingData>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<ClaimBillingData>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const ClaimBillingDatas = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', ClaimBillingDatas);
    }

}
