import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClaimBillingDataEditorComponent } from './claim-billing-data-editor/claim-billing-data-editor.component';
import { ClaimBillingDataComponent } from './claim-billing-data.component';

const routes: Routes = [
        {
            path: '',
            component: ClaimBillingDataComponent
        },
        {
            path: 'create',
            component: ClaimBillingDataEditorComponent
        },
        {
            path: 'edit/:id',
            component: ClaimBillingDataEditorComponent,
        }
    ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ClaimBillingDataRoutingModule { }
