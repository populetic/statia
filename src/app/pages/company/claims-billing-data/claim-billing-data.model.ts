export class ClaimBillingData {
    id: string;
    accountHolder: string;
    accountNumber: string;
    claimId: string;
    createdAt: Date;
    isValid: boolean; // TODO: translator
    swift: string;

    constructor(model?: ClaimBillingData) {
        this.id = (model && model.id) ? model.id : null;
        this.accountHolder = (model && model.accountHolder) ? model.accountHolder : null;
        this.accountNumber = (model && model.accountNumber) ? model.accountNumber : null;
        this.claimId = (model && model.claimId) ? model.claimId : null;
        this.createdAt = (model && model.createdAt) ? model.createdAt : null;
        this.isValid = (model && model.isValid) ? model.isValid : false;
        this.swift = (model && model.swift) ? model.swift : null;
    }

}
