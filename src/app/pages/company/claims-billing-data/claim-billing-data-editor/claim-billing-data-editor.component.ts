import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { Subscription } from 'rxjs';
import { ClaimBillingData } from '../claim-billing-data.model';
import { ClaimBillingDataService } from '../claim-billing-data.service';
import { PendingInterceptorService } from '@fury/shared/loading-indicator/pending-interceptor.service';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { FORM_OPERATIONS } from '@app/shared/constants/constants';

@Component({
    selector: 'app-claim-billing-data-editor',
    templateUrl: './claim-billing-data-editor.component.html',
    animations: [fadeInRightAnimation, fadeInUpAnimation]
})

export class ClaimBillingDataEditorComponent implements OnInit, OnDestroy {

    public claimBillingData: ClaimBillingData;
    private id: any;
    public parentRoute: string;
    public formAction: string;
    public currentEditor: string;
    public breadcrumbs: string[];
    public editorFormData: FormGroup;
    subscriptions: Subscription[] = [];

    public canUseFormOperation: boolean;
    public isLoading = false;
    private menuId: string;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private claimBillingDataService: ClaimBillingDataService,
        private pendingInterceptorService: PendingInterceptorService
    ) {
        this.claimBillingData = new ClaimBillingData();
    }

    ngOnInit() {
        this.route.paramMap.subscribe(paramMap => { this.id = paramMap.get('id'); });

        this.formAction = (this.id === null ? 'create' : 'edit');
        const routeToReplace = (this.id === null ? '/' + this.formAction : '/' + this.formAction + '/' + this.id);
        this.parentRoute = this.router.url.replace(routeToReplace, '');

        const routeItems = this.parentRoute.split('/').filter(item => item !== null && item !== '');
        this.breadcrumbs = routeItems;

        this.canUseFormOperation = StatiaFunctions.EnabledOption(this.menuId, FORM_OPERATIONS[this.formAction.toUpperCase()]);
        this.breadcrumbs.push(this.formAction);

        if (this.id !== null) {
            this.createFormGroup();
            this.configureForm();
        } else {
            this.currentEditor = 'create';
        }
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
    }

    private createFormGroup() {
        const claimBillingData: any = {
            name: ['', Validators.required],
            surnames: ['', Validators.required]
        };

        this.editorFormData = this.formBuilder.group(claimBillingData);
    }

    private async configureForm() {
    }

    getData() {
        const claimSubs: Subscription = this.claimBillingDataService.getById(this.id).subscribe(
            result => {
                this.claimBillingData = result.response;
                this.currentEditor = ''; // TODO: campo identificativo
            }
        );

        this.subscriptions.push(claimSubs);
    }

    onSubmit() {
        if (this.editorFormData.invalid) {
            return;
        }

        let param: any;
        param = this.editorFormData.value;
    }

}
