import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ClaimBillingDataService } from './claim-billing-data.service';

@Component({
    selector: 'app-menu',
    template: `<app-main-table [actualMenuId]="menuId" [actualMenuService]="menuService"></app-main-table>`
})

export class ClaimBillingDataComponent implements OnInit {

    public menuId: string;

    constructor(
        private activeRoute: ActivatedRoute,
        public menuService: ClaimBillingDataService
    ) {}

    ngOnInit() {
        this.activeRoute.data.subscribe(
            data => this.menuId = data.menuId
        );
    }
}
