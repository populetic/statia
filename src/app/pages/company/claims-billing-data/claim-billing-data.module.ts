import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FurySharedModule } from '@fury/fury-shared.module';
import { BreadcrumbsModule } from '@fury/shared/breadcrumbs/breadcrumbs.module';
import { MaterialModule } from '@fury/shared/material-components.module';
import { MainTableModule } from '@app/layout/main-table/main-table.module';
import { ClaimBillingDataComponent } from './claim-billing-data.component';
import { ClaimBillingDataEditorComponent } from './claim-billing-data-editor/claim-billing-data-editor.component';
import { FormComponentsModule } from '@app/layout/form-components/form-components.module';
import { ClaimBillingDataRoutingModule } from './claim-billing-data-routing.module';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        BreadcrumbsModule,
        MaterialModule,
        FurySharedModule,

        ClaimBillingDataRoutingModule,
        MainTableModule,
        FormComponentsModule
    ],
    declarations: [ClaimBillingDataComponent, ClaimBillingDataEditorComponent],
    exports: [ClaimBillingDataComponent]
})

export class ClaimBillingDataModule {
}
