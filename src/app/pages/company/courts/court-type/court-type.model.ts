export class CourtType {
    id: string;
    name: string;
    countryId: string;
    countryName: string;
}
