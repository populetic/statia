import { Injectable } from '@angular/core';
import { ApiCourtTypesService } from '@app/api/services/api-court-service/api-court-type.service';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CourtType } from './court-type.model';
import { CourtTypesTranslator } from './court-type.translator';
import { CourtTypeContract } from '@app/api/contracts/court-contract/court-type.contract';

@Injectable({ providedIn: 'root' })
export class CourtTypeService implements BaseInterface<CourtType> {
    constructor( private apiCourtTypeService: ApiCourtTypesService ) {}

    getAll(customParams: CustomParams = null): Observable<Result<CourtType[]>> {
        return this.apiCourtTypeService.getAll(customParams)
            .pipe(
                map((result: Result<CourtTypeContract[]>) => {
                    const newResult: Result<CourtType[]> = StatiaFunctions.getResultModel<CourtTypeContract[], CourtType[]>(result);
                    newResult.response = result.response.map((courtType: CourtTypeContract) =>
                         CourtTypesTranslator.translateContractToModel(courtType));

                    return  newResult;
                })
            );
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<CourtType[]>> {
        return new Promise((resolve, reject) => {
            this.apiCourtTypeService.getAllAsync(customParams)
                .then((result: Result<CourtTypeContract[]>) => {
                    const newResult: Result<CourtType[]> = StatiaFunctions.getResultModel<CourtTypeContract[], CourtType[]>(result);
                    newResult.response = result.response.map((courtType: CourtTypeContract) =>
                    CourtTypesTranslator.translateContractToModel(courtType));

                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    getById(id: string): Observable<Result<CourtType>> {
        return this.apiCourtTypeService.getById(id).pipe(
            map((result: Result<CourtTypeContract>) => {
                const newResult: Result<CourtType> = StatiaFunctions.getResultModel<CourtTypeContract, CourtType>(result);
                const courtTypeTranslated: CourtType = CourtTypesTranslator.translateContractToModel(result.response);
                newResult.response =  courtTypeTranslated;

                return  newResult;
            })
        );
    }

    create(courtType: CourtType): Observable<Result<CourtType>> {
        const contract = CourtTypesTranslator.translateModelToContract(courtType);
        return this.apiCourtTypeService.create(contract)
            .pipe(
                map((result: Result<CourtTypeContract>) => {
                    const newResult: Result<CourtType> = StatiaFunctions.getResultModel<CourtTypeContract, CourtType>(result);
                    newResult.response =  CourtTypesTranslator.translateContractToModel(result.response);
                    return  newResult;
                })
            );
    }

    createAsync(courtType: CourtType): Promise<Result<CourtType>> {
        return new Promise((resolve, reject) => {
            this.apiCourtTypeService.createAsync(CourtTypesTranslator.translateModelToContract(courtType))
                .then((result: Result<CourtTypeContract>) => {
                    const newResult: Result<CourtType> = StatiaFunctions.getResultModel<CourtTypeContract, CourtType>(result);
                    newResult.response =  CourtTypesTranslator.translateContractToModel(result.response);
                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    edit(id: number|string, courtType: CourtType): Observable<Result<CourtType>> {
        const contract = CourtTypesTranslator.translateModelToContract(courtType);
        return this.apiCourtTypeService.edit(id, contract)
            .pipe(
                map((result: Result<CourtTypeContract>) => {
                    const newResult: Result<CourtType> = StatiaFunctions.getResultModel<CourtTypeContract, CourtType>(result);
                    newResult.response =  CourtTypesTranslator.translateContractToModel(result.response);
                    return  newResult;
                })
            );
    }

    editAsync(id: number|string, courtType: CourtType): Promise<Result<CourtType>> {
        return new Promise((resolve, reject) => {
            this.apiCourtTypeService.editAsync(id, CourtTypesTranslator.translateModelToContract(courtType))
                .then((result: Result<CourtTypeContract>) => {
                    const newResult: Result<CourtType> = StatiaFunctions.getResultModel<CourtTypeContract, CourtType>(result);
                    newResult.response =  CourtTypesTranslator.translateContractToModel(result.response);
                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    deleteByParam(courtType: CourtType): Observable<Result<CourtType>> {
        const contract: CourtTypeContract = CourtTypesTranslator.translateModelToContract(courtType);
        return this.apiCourtTypeService.deleteByParam(contract)
            .pipe(
                map((result: Result<CourtTypeContract>) => {
                    const newResult: Result<CourtType> = StatiaFunctions.getResultModel<CourtTypeContract, CourtType>(result);
                    newResult.response =  CourtTypesTranslator.translateContractToModel(result.response);
                    return  newResult;
                })
            );
    }
}
