
import { CourtType } from './court-type.model';
import { CourtContract } from '@app/api/contracts/court-contract/court.contract';
import { CourtTypeContract } from '@app/api/contracts/court-contract/court-type.contract';

export class CourtTypesTranslator {
    static translateContractToModel(contract: CourtContract): CourtType {
        const model = new CourtType();

        model.id = contract?.id ?? '';
        model.name = contract?.name ?? '';
        model.countryId = contract?.countryId ?? '';
        model.countryName = contract?.countryName ?? '';

        return model;
    }

    static translateModelToContract(model: CourtType): CourtTypeContract {
        const contract: CourtTypeContract = {
            countryId: model?.countryId ?? '',
            name: model?.name ?? ''
        };

        return contract;
    }
}
