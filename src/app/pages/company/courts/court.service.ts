import { Injectable } from '@angular/core';

import { ApiCourtTypesService } from '@app/api/services/api-court-service/api-court-type.service';
import { ApiCourtService } from '@app/api/services/api-court-service/api-court.service';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { CourtType } from './court-type/court-type.model';
import { CourtTypesTranslator } from './court-type/court-type.translator';
import { Court } from './court.model';
import { CourtTranslator } from './court.translator';
import { CourtContract } from '@app/api/contracts/court-contract/court.contract';
import { CourtTypeContract } from '@app/api/contracts/court-contract/court-type.contract';

@Injectable({ providedIn: 'root' })
export class CourtService implements BaseInterface<Court> {
    constructor(private apiCourtService: ApiCourtService, private apiCourtTypesService: ApiCourtTypesService) {}

    getAll(customParams: CustomParams = null): Observable<Result<Court[]>> {
        return this.apiCourtService.getAll(customParams)
            .pipe(
                map((result: Result<CourtContract[]>) => {
                    const newResult: Result<Court[]> = StatiaFunctions.getResultModel<CourtContract[], Court[]>(result);
                    newResult.response = result.response.map((court: CourtContract) => CourtTranslator.translateContractToModel(court));
                    return  newResult;
                })
            );
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<Court[]>> {
        return new Promise((resolve, reject) => {
            this.apiCourtService.getAllAsync(customParams)
                .then((result: Result<CourtContract[]>) => {
                    const newResult: Result<Court[]> = StatiaFunctions.getResultModel<CourtContract[], Court[]>(result);
                    newResult.response = result.response.map((court: CourtContract) => CourtTranslator.translateContractToModel(court));
                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    getById(id: number|string): Observable<Result<Court>> {
        return this.apiCourtService.getById(id).pipe(
            map((result: Result<CourtContract>) => {
                const newResult: Result<Court> = StatiaFunctions.getResultModel<CourtContract, Court>(result);
                const court: Court = CourtTranslator.translateContractToModel(result.response);
                newResult.response = court;

                return  newResult;
            })
        );
    }

    create(court: Court): Observable<Result<Court>> {
        const contract = CourtTranslator.translateModelToContract(court);
        return this.apiCourtService.create(contract)
            .pipe(
                map((result: Result<CourtContract>) => {
                    const newResult: Result<Court> = StatiaFunctions.getResultModel<CourtContract, Court>(result);
                    newResult.response =  CourtTranslator.translateContractToModel(result.response);
                    return  newResult;
                })
            );
    }

    createAsync(param: Court): Promise<Result<Court>> {
        return new Promise((resolve, reject) => {
            this.apiCourtService.createAsync(CourtTranslator.translateModelToContract(param))
                .then((result: Result<CourtContract>) => {
                    const newResult: Result<Court> = StatiaFunctions.getResultModel<CourtContract, Court>(result);
                    newResult.response =  CourtTranslator.translateContractToModel(result.response);
                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    edit(id: number|string, param: Court): Observable<Result<Court>> {
        const contract = CourtTranslator.translateModelToContract(param);
        return this.apiCourtService.edit(id, contract)
            .pipe(
                map((result: Result<CourtContract>) => {
                    const newResult: Result<Court> = StatiaFunctions.getResultModel<CourtContract, Court>(result);
                    newResult.response =  CourtTranslator.translateContractToModel(result.response);
                    return  newResult;
                })
            );
    }

    editAsync(id: number|string, param: Court): Promise<Result<Court>> {
        return new Promise((resolve, reject) => {
            this.apiCourtService.editAsync(id, CourtTranslator.translateModelToContract(param))
                .then((result: Result<CourtContract>) => {
                    const newResult: Result<Court> = StatiaFunctions.getResultModel<CourtContract, Court>(result);
                    newResult.response =  CourtTranslator.translateContractToModel(result.response);
                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    deleteByParam(param: any): Observable<Result<any>> {
        const contract = CourtTranslator.translateModelToContract(param);
        return this.apiCourtService.deleteByParam(contract);
    }

    getCourtTypes(): Promise<Result<CourtType[]>> {
        return new Promise((resolve, reject) => {
            this.apiCourtTypesService.getAllAsync().then((result: Result<CourtTypeContract[]>) => {
                const newResult: Result<CourtType[]> = StatiaFunctions.getResultModel<CourtTypeContract[], CourtType[]>(result);
                newResult.response = result.response.map((court: CourtContract) => CourtTypesTranslator.translateContractToModel(court));
                resolve(newResult);
            }).catch((err) => {
                reject(err);
            });
        });
    }
}
