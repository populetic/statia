import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CourtEditorComponent } from './court-editor/court-editor.component';
import { CourtComponent } from './court.component';

const routes: Routes = [
        {
            path: '',
            component: CourtComponent
        },
        {
            path: 'create',
            component: CourtEditorComponent
        },
        {
            path: 'edit/:id',
            component: CourtEditorComponent
        }
    ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CourtRoutingModule { }
