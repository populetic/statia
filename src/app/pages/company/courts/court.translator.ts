import { CourtContract } from '@app/api/contracts/court-contract/court.contract';
import { TranslateService } from '@ngx-translate/core';
import { Court } from './court.model';

export class CourtTranslator {
    static translateContractToModel(contract?: CourtContract, translate?: TranslateService) {
        const model: Court = {
            id: contract?.id ?? '',

            courtTypeId: contract?.courtTypeId ?? '',
            address: contract?.address ?? '',
            postalCode: contract?.postalCode ?? '',
            cityId: contract?.cityId ?? '',
            countryId: contract?.countryId ?? '',
            createdAt: contract?.createdAt && contract?.createdAt !== '0000-00-00 00:00:00' ?
                new Date(contract.createdAt) : null,
            updatedAt: contract?.updatedAt && contract?.updatedAt !== '0000-00-00 00:00:00' ?
                new Date(contract?.updatedAt) : null,
            courtTypeName: contract?.courtTypeName ?? '',
            cityName: contract?.cityName ?? '',
            countryName: contract?.countryName ?? '',
            courtCityAndCourtType: contract?.cityName && contract?.courtTypeId ?
                 contract?.cityName + '-' +  translate?.instant('courtTypeId' + contract?.courtTypeId) : null
        };
        return new Court(model);
    }

    static translateModelToContract(model: Court): CourtContract {
        const contract: CourtContract = {
            courtTypeId: model?.courtTypeId ?? '',
            address: model?.address ?? '',
            postalCode: model?.postalCode ?? '',
            cityId: model?.cityId ?? '',
            countryId: model?.countryId ?? ''
        };

        return contract;
    }
}
