import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { FurySharedModule } from '@fury/fury-shared.module';
import { BreadcrumbsModule } from '@fury/shared/breadcrumbs/breadcrumbs.module';
import { MaterialModule } from '@fury/shared/material-components.module';

import { MainTableModule } from '@app/layout/main-table/main-table.module';

import { CourtComponent } from './court.component';
import { CourtEditorComponent } from './court-editor/court-editor.component';
import { CourtRoutingModule } from './court-routing.module';
import { SharedModule } from '@app/shared/modules/shared.module';
import { LayoutModule } from '@angular/cdk/layout';
import { FormComponentsModule } from '@app/layout/form-components/form-components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        BreadcrumbsModule,
        MaterialModule,
        FurySharedModule,

        CourtRoutingModule,
        MainTableModule,
        SharedModule,
        LayoutModule,
        FormComponentsModule,
        TranslateModule
    ],
    declarations: [CourtComponent, CourtEditorComponent],
    exports: [CourtComponent]
})

export class CourtModule {
}
