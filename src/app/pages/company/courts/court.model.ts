export class Court {
    id: string;
    courtTypeId?: string;
    courtTypeName?: string;
    address?: string;
    name?: string;
    postalCode?: string;
    cityId?: string;
    cityName?: string;
    countryId?: string;
    countryName?: string;
    createdAt?: Date;
    updatedAt?: Date;
    courtCityAndCourtType?: string;

    constructor(model?: Court) {
        this.id = model?.id ?? '';
        this.courtTypeId = model?.courtTypeId ?? '';
        this.address = model?.address ?? '';
        this.postalCode = model?.postalCode ?? '';
        this.cityId = model?.cityId ?? '';
        this.countryId = model?.countryId ?? '';
        this.createdAt = model?.createdAt ?? null;
        this.updatedAt = model?.updatedAt ?? null;
        this.courtTypeName = model?.courtTypeName ?? '';
        this.name = model?.name ?? '';
        this.cityName = model?.cityName ?? '';
        this.countryName = model?.countryName ?? '';
        this.courtCityAndCourtType = model?.courtCityAndCourtType ?? '';
    }
}
