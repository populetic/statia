import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { FORM_OPERATIONS } from '@app/shared/constants/constants';
import { MODEL_PROPERTY_NAME } from '@app/shared/enums/model-property-name.enum';
import { Result } from '@app/shared/models/result.model';
import { NotificationsService } from '@app/shared/services/notifications.service';
import { StatiaService } from '@app/shared/services/statia.service';
import { ValidationService } from '@app/shared/services/validation.service';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { PendingInterceptorService } from '@fury/shared/loading-indicator/pending-interceptor.service';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { City } from '../../auxiliaries/cities/city.model';
import { Country } from '../../auxiliaries/countries/country.model';
import { CourtNumber } from '../court-number/court-number.model';
import { CourtNumberService } from '../court-number/court-number.service';
import { CourtType } from '../court-type/court-type.model';
import { Court } from '../court.model';
import { CourtService } from '../court.service';

@Component({
    selector: 'app-court-editor',
    templateUrl: './court-editor.component.html',
    animations: [fadeInRightAnimation, fadeInUpAnimation]
})
export class CourtEditorComponent implements OnInit, OnDestroy {
    private id: any;
    public court: CourtNumber;
    public parentRoute: string;
    public formAction: string;
    public currentEditor: string;
    public breadcrumbs: string[];
    public editorFormData: FormGroup;
    public canUseFormOperation: boolean;
    public isLoading = false;
    public subscriptions: Array<Subscription> = new Array<Subscription>();
    private menuId: string;
    private countries: any[];
    public cities: Array<City> = null;
    public duplicateFound = false;
    public courtTypes: Array<CourtType> = null;
    public filteredCountries: Observable<Country[]>;
    public filteredCities: Observable<City[]>;
    public filteredCourtTypes: Observable<Court[]>;
    public dataCourtsNumber: Array<CourtNumber> = null;
    public readOnly = true;

    constructor(
        private router: Router,
        private dialog: MatDialog,
        private snackbar: MatSnackBar,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private courtService: CourtService,
        private activeRoute: ActivatedRoute,
        private statiaService: StatiaService,
        private validationService: ValidationService,
        private courtNumberService: CourtNumberService,
        private notificationsService: NotificationsService,
        private pendingInterceptorService: PendingInterceptorService
    ) {
        this.court = new CourtNumber();
    }

    ngOnInit() {
        this.route.paramMap.subscribe(paramMap => { this.id = paramMap.get('id'); });
        this.activeRoute.data.subscribe(data => this.menuId = data.menuId);

        this.formAction = (this.id === null ? 'create' : 'edit');
        const routeToReplace = (this.id === null ? '/' + this.formAction : '/' + this.formAction + '/' + this.id);
        this.parentRoute = this.router.url.replace(routeToReplace, '');

        const routeItems = this.parentRoute.split('/').filter(item => item !== null && item !== '');
        this.breadcrumbs = routeItems;
        const sub: Subscription = this.pendingInterceptorService.status$.subscribe((value: boolean) => this.isLoading = value);
        this.subscriptions.push(sub);

        if (this.id !== null) {
            this.breadcrumbs.push('edit');
            this.canUseFormOperation = StatiaFunctions.EnabledOption(this.menuId, FORM_OPERATIONS.EDIT);

        } else {
            this.canUseFormOperation = StatiaFunctions.EnabledOption(this.menuId, FORM_OPERATIONS.CREATE);
            this.currentEditor = 'create';
            this.readOnly = false;
        }

        this.createFormGroup();
        this.configureForm();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
    }

    private createFormGroup() {
        const courtForm: any = {
            countryName: ['', Validators.required],
            cityName: ['', Validators.required],
            courtTypeName: ['', Validators.required],
            number: ['', Validators.required],
            courtTypeId: [''],
            address: [''],
            postalCode: [''],
            fax: [''],
            phone: ['']
        };

        this.editorFormData = this.formBuilder.group(courtForm);
    }

    private async configureForm() {
        this.countries = await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.COUNTRY, false, false,
                                                    {checkenumCountriesWithJudicialProcess: true});
        this.cities = await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.CITY, false, false ,
                                                    {checkenumCountriesWithJudicialProcess: true});
        this.courtTypes = await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.COURT_TYPE, false, true);
        this.dataCourtsNumber = await this.getCourtNumbersFromAPI();

        this.getData();
        this.validadorForm();
    }

    async getCourtNumbersFromAPI(): Promise<Array<CourtNumber>> {
        return await new Promise((resolve) => {
            this.courtNumberService.getAllAsync(null)
                    .then((result: Result<CourtNumber[]>) => {
                        if (result.status === 1) {
                            resolve(result.response);
                        } else {
                            this.notificationsService.manageServiceResult(result, null, this.router, this.snackbar, this.dialog);
                        }
                    });
        });
    }

    setValidatorsCountries() {
        this.editorFormData.controls['countryName'].setValidators(
            [Validators.required, this.validationService.validateFieldValue(this.countries, 'name')]
        );
    }

    setValidatorsCities() {
        const countryName = this.editorFormData.get('countryName').value;
        const cities = this.cities.filter((x: City) => x.countryName === countryName);

        this.editorFormData.controls['cityName'].setValidators(
            [Validators.required, this.validationService.validateFieldValue(cities, 'name')]
        );
    }

    setValidatorsCourtTypes() {
        const countryName = this.editorFormData.get('countryName').value;
        const courtyTypes = this.courtTypes.filter((x: CourtType) => x.countryName === countryName);

        this.editorFormData.controls['courtTypeName'].setValidators(
            [Validators.required, this.validationService.validateFieldValue(courtyTypes, 'name')]
        );
    }

    setValidatorsNumbers() {
        const numbersForFilter: Array<CourtNumber> = this.getCourtNumber(false);
        this.editorFormData.controls['number'].setValidators(
            [Validators.required, this.validationService.validateFieldValue(numbersForFilter, 'number', true)]
        );
    }

    valueChangesCountryName() {
        this.filteredCountries = this.editorFormData.controls['countryName'].valueChanges.pipe(
            startWith(''),
            map((value: string) => {
                if (this.formAction === 'edit') {
                    return;
                }

                this.setValidatorsCourtTypes();
                this.setValidatorsCities();
                this.setValidatorsNumbers();

                return value ? this.statiaService._filter(value, this.countries, ['name']) : this.countries.slice();
            })
        );
    }

    valueChangesCityName() {
        this.filteredCities = this.editorFormData.controls['cityName'].valueChanges.pipe(
            startWith(''),
            map((value: string) => {
                if (this.formAction === 'edit') {
                    return;
                }

                let citiesFiltered = null;
                if (this.cities) {
                    this.statiaService.filterByField(this.countries, 'name', this.editorFormData.get('countryName').value)
                        .map((res: any) => {
                            if (res && res.id) {
                                const citiesFilteredByCountry = this.statiaService._filter(res.name, this.cities, ['countryName']);
                                citiesFiltered = (value ? this.statiaService._filter(value, citiesFilteredByCountry, ['name']) :
                                    citiesFilteredByCountry.slice());

                                    this.editorFormData.get('number').setValue('');
                                    this.setValidatorsNumbers();
                            }
                        });
                }

                return citiesFiltered;
            })
        );
    }

    valueChangesCourtTypeName() {
        this.filteredCourtTypes = this.editorFormData.controls['courtTypeName'].valueChanges.pipe(
            startWith(''),
            map((value: string) => {
                if (this.formAction === 'edit') {
                    return;
                }

                let courtsFiltered = null;
                if (this.courtTypes) {
                    const countryName = this.editorFormData.get('countryName').value;
                    const courtsByCountry = this.courtTypes.filter(x => x.countryName === countryName);

                    this.editorFormData.get('number').setValue('');
                    this.CourtTypeChangeValue(value);

                    courtsFiltered = (value ? this.statiaService._filter(value, courtsByCountry, ['name']) : courtsByCountry.slice());
                    this.setValidatorsNumbers();
                }
                return courtsFiltered;
            })
        );
    }

    validadorForm() {
        if (this.formAction === 'edit') {
            return;
        }

        this.setValidatorsCountries();

        this.valueChangesCountryName();
        this.valueChangesCityName();
        this.valueChangesCourtTypeName();
    }

    isInsideDataArray() {
        const countryName = this.editorFormData.get('countryName').value;
        const cityName = this.editorFormData.get('cityName').value;
        const courtTypeName = this.editorFormData.get('courtTypeName').value;
        const isCountryNameInsideArray = this.countries.find(x => x.name === countryName);
        const isCityNameInsideArray = this.cities.find(x => x.name === cityName);
        const isCourtTypeNameInsideArray = this.courtTypes.find(x => x.name === courtTypeName);

        const resultIsInsideDataArray = isCountryNameInsideArray !== undefined &&
                                        isCityNameInsideArray !== undefined &&
                                        isCourtTypeNameInsideArray !== undefined;

        return resultIsInsideDataArray;
    }

    getData() {
        if (this.formAction === 'create') {
            return;
        }

        const courtSubs: Subscription = this.courtNumberService.getById(this.id).subscribe(
            (result: Result<CourtNumber>) => {
                if (result.status === 1) {
                    this.court = result.response;
                    this.currentEditor = this.court.cityName + ' ' + this.court.courtTypeName + ' ' + this.court.number;
                    this.editorFormData.patchValue(this.court);

                    this.courtTypes = this.courtTypes.filter(x => x.countryId === this.court.countryId);

                    this.editorFormData.markAllAsTouched();
                }
            }
        );

        this.subscriptions.push(courtSubs);
    }

    isValidForm(): boolean {
        const resIsInsideDataArray = this.isInsideDataArray();
        if (!resIsInsideDataArray) {
            return false;
        }

        this.editorFormData.markAllAsTouched();

        return this.editorFormData.invalid;
    }

    getCourtKeyByCountryAndCityAndCourtType() {
        const res: Array<CourtNumber> = this.getCourtNumber(false);

        return (res == null || res.length === 0) ? null : res[res.length - 1];
    }

    private getCourtNumber(findNumber: boolean): Array<CourtNumber> {
        const params = this.getParams();

        const res: Array<CourtNumber> = this.dataCourtsNumber.filter((x: CourtNumber) =>
            x.countryId === params.countryId &&
            x.cityId === params.cityId &&
            x.courtTypeId === params.courtTypeId &&
            (x.number === params.number && findNumber || !findNumber )
        );
        return res;
    }

    optionCountrySelected(ev: MatAutocompleteSelectedEvent) {
        if (ev) {
            const country = this.countries.find(x => x.name === ev.option.value);
            if (country) {
                this.court.countryId = country.id;
                this.court.countryName = country.name;

                this.editorFormData.get('cityName').setValue('');
                this.editorFormData.get('courtTypeName').setValue('');
                this.editorFormData.get('number').setValue('');
            }
        }
    }

    filterByField(values: any[], field: string, value: string): any {
        const result = this.statiaService.filterByField(values, field, value);
        return (result) ? result[0] : null;
    }

    getParams(): CourtNumber {
        let param: any;

        param = this.editorFormData.value;

        param.number = this.editorFormData.get('number').value;

        let selCountry: Country = this.filterByField(this.countries, 'name', param.countryName);
        let selCity: City = this.filterByField(this.cities, 'name', param.cityName);
        let selCourtType: CourtType = this.filterByField(this.courtTypes, 'name', param.courtTypeName);

        if (this.formAction === 'edit') {
            if ( selCountry === undefined) {
                const countryName = this.editorFormData.get('countryName').value;
                selCountry = this.countries.find(x => x.name === countryName);
            }

            if ( selCity === undefined) {
                const cityName = this.editorFormData.get('cityName').value;
                selCity = this.cities.find(x => x.name === cityName);
            }

            if ( selCourtType === undefined) {
                const courtTypeName = this.editorFormData.get('courtTypeName').value;
                selCourtType = this.courtTypes.find(x => x.name === courtTypeName);
            }
        }

        param.cityId = selCity?.id ?? null;
        param.cityName = selCity?.name  ?? '';

        param.countryId = selCountry?.id ?? null;
        param.countryName = selCountry?.name ?? '';

        param.courtTypeId = selCourtType?.id ?? null;
        param.courtTypeName = selCourtType?.name ?? '';

        return param;
    }

    CourtTypeChangeValue(event) {
        const params = this.getParams();

        if (this.formAction === 'edit') {
            return;
        }

        if (params.countryId !== '' && params.cityId !== '' && event !== '') {
            const courts: Array<CourtNumber> = this.dataCourtsNumber.filter(x =>
                x.countryId === params.countryId &&
                x.cityId === params.cityId &&
                x.courtTypeName === event
                );

            if (courts && courts.length > 0) {
                this.editorFormData.get('address').setValue(courts[courts.length - 1].address);
                this.editorFormData.get('postalCode').setValue(courts[courts.length - 1].postalCode);
            }
        }
    }

    createCourtsNumber(params: CourtNumber) {
        this.courtNumberService.create(params).subscribe((resCourtNumbers: Result<CourtNumber>) => {
            if (resCourtNumbers.status === 1) {
                this.readOnly = true;
                this.editorFormData.patchValue(resCourtNumbers.response);
                this.court = resCourtNumbers.response;
                this.id =  resCourtNumbers?.response?.id;
                this.formAction = 'edit';
            }

        this.notificationsService.manageServiceResult(resCourtNumbers, null, this.router, this.snackbar, this.dialog);
        });
    }


    create() {
        const params = this.getParams();
        const resGetCourtKeyByCountryAndCityAndCourtType = this.getCourtKeyByCountryAndCityAndCourtType();

        if ( resGetCourtKeyByCountryAndCityAndCourtType === null) {
            this.courtService.create(params).subscribe((resCourt: Result<Court>) => {
                if (resCourt.status === 1) {
                    const courtNumber: CourtNumber = new CourtNumber(params);
                    courtNumber.courtId = resCourt?.response?.id;
                    this.createCourtsNumber(courtNumber);
                } else {
                    this.notificationsService.manageServiceResult(resCourt, null, this.router, this.snackbar, this.dialog);
                }
            });
        } else {
            const id = resGetCourtKeyByCountryAndCityAndCourtType.courtId;
            this.courtService.edit(id, params).subscribe((resCourt: Result<CourtNumber>) => {
                if (resCourt.status === 1) {
                    params.courtId = id;
                    this.createCourtsNumber(params);
                } else {
                    this.notificationsService.manageServiceResult(resCourt, null, this.router, this.snackbar, this.dialog);
                }
            });
        }
    }

    update() {
        const params = this.getParams();

        this.courtService.edit(this.court.courtId, params).subscribe((resCourt: Result<Court>) => {
            if (resCourt.status === 1) {
                const courtNumber = new CourtNumber(params);

                courtNumber.courtId = this.court.courtId;

                this.courtNumberService.edit(this.id, courtNumber).subscribe(resCourtNumbers => {
                    this.notificationsService.manageServiceResult(resCourtNumbers, null, this.router, this.snackbar, this.dialog);
                });
            } else {
                this.notificationsService.manageServiceResult(resCourt, null, this.router, this.snackbar, this.dialog);
            }
        });
    }

    onSubmit() {
        if (this.isValidForm()) {
            return;
        }

        if (this.formAction === 'create') {
            this.create();
        } else {
            this.update();
        }
    }
}
