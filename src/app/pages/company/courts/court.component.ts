import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CourtNumberService } from './court-number/court-number.service';

@Component({
    selector: 'app-menu',
    template: `<app-main-table [actualMenuId]="menuId" [actualMenuService]="menuService"></app-main-table>`
})

export class CourtComponent implements OnInit {

    public menuId: string;

    constructor(
        private activeRoute: ActivatedRoute,
        public menuService: CourtNumberService
    ) {}

    ngOnInit() {
        this.activeRoute.data.subscribe(
            data => this.menuId = data.menuId
        );
    }
}
