import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ApiCourtNumberService } from '@app/api/services/api-court-service/api-court-number.service';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CourtNumber } from './court-number.model';
import { CourtNumberTranslator } from './court-number.translator';
import { CourtNumberContract } from '@app/api/contracts/court-contract/court-number.contract';

@Injectable({ providedIn: 'root' })
export class CourtNumberService implements BaseInterface<CourtNumber> {
    public resourceUrl: string = environment.apiUrl + '/courts-number';
    public headers: HttpHeaders;

    constructor( private apiCourtNumberService: ApiCourtNumberService ) {}

    getAll(customParams: CustomParams = null): Observable<Result<CourtNumber[]>> {
        return this.apiCourtNumberService.getAll(customParams)
            .pipe(
                map((result: Result<CourtNumberContract[]>) => {
                    const newResult: Result<CourtNumber[]> = StatiaFunctions.getResultModel<CourtNumberContract[], CourtNumber[]>(result);
                    newResult.response = result.response.map((courtNumber: CourtNumberContract) =>
                            CourtNumberTranslator.translateContractToModel(courtNumber));

                    return  newResult;
                })
            );
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<CourtNumber[]>> {
        return new Promise((resolve, reject) => {
            this.apiCourtNumberService.getAllAsync(customParams)
                .then((result: Result<CourtNumberContract[]>) => {
                    const newResult: Result<CourtNumber[]> = StatiaFunctions.getResultModel<CourtNumberContract[], CourtNumber[]>(result);
                    newResult.response = result.response.map((courtNumber: CourtNumberContract) =>
                        CourtNumberTranslator.translateContractToModel(courtNumber));

                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    getById(id: number|string): Observable<Result<CourtNumber>> {
        return this.apiCourtNumberService.getById(id).pipe(
            map((result: Result<CourtNumberContract>) => {
                const newResult: Result<CourtNumber> = StatiaFunctions.getResultModel<CourtNumberContract, CourtNumber>(result);
                const courtNumber: CourtNumber = CourtNumberTranslator.translateContractToModel(result.response);
                newResult.response = courtNumber;

                return  newResult;
            })
        );
    }

    create(courtNumber: CourtNumber): Observable<Result<CourtNumber>> {
        const contract = CourtNumberTranslator.translateModelToContract(courtNumber);
        return this.apiCourtNumberService.create(contract)
            .pipe(
                map((result: Result<CourtNumberContract>) => {
                    const newResult: Result<CourtNumber> = StatiaFunctions.getResultModel<CourtNumberContract, CourtNumber>(result);
                    newResult.response =  CourtNumberTranslator.translateContractToModel(result.response);
                    return  newResult;
                })
            );
    }

    createAsync(courtNumber: CourtNumber): Promise<Result<CourtNumber>> {
        return new Promise((resolve, reject) => {
            this.apiCourtNumberService.createAsync(CourtNumberTranslator.translateModelToContract(courtNumber))
                .then((result: Result<CourtNumberContract>) => {
                    const newResult: Result<CourtNumber> = StatiaFunctions.getResultModel<CourtNumberContract, CourtNumber>(result);
                    newResult.response =  CourtNumberTranslator.translateContractToModel(result.response);
                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    edit(id: number|string, courtNumber: CourtNumber): Observable<Result<CourtNumber>> {
        const contract = CourtNumberTranslator.translateModelToContract(courtNumber);
        return this.apiCourtNumberService.edit(id, contract)
            .pipe(
                map((result: Result<CourtNumberContract>) => {
                    const newResult: Result<CourtNumber> = StatiaFunctions.getResultModel<CourtNumberContract, CourtNumber>(result);
                    newResult.response =  CourtNumberTranslator.translateContractToModel(result.response);
                    return  newResult;
                })
            );
    }

    editAsync(id: number|string, courtNumber: CourtNumber): Promise<Result<CourtNumber>> {
        return new Promise((resolve, reject) => {
            this.apiCourtNumberService.editAsync(id, CourtNumberTranslator.translateModelToContract(courtNumber))
                .then((result: Result<CourtNumberContract>) => {
                    const newResult: Result<CourtNumber> = StatiaFunctions.getResultModel<CourtNumberContract, CourtNumber>(result);
                    newResult.response =  CourtNumberTranslator.translateContractToModel(result.response);
                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    deleteByParam(courtNumber: CourtNumber): Observable<Result<CourtNumber>> {
        const contract = CourtNumberTranslator.translateModelToContract(courtNumber);
        return this.apiCourtNumberService.deleteByParam(contract)
            .pipe(
                map((result: Result<CourtNumberContract>) => {
                    const newResult: Result<CourtNumber> = StatiaFunctions.getResultModel<CourtNumberContract, CourtNumber>(result);
                    newResult.response =  CourtNumberTranslator.translateContractToModel(result.response);
                    return  newResult;
                })
            );
    }
}
