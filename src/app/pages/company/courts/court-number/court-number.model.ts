
export class CourtNumber {
    id: string;
    courtId?: string;
    courtTypeId?: string;
    courtTypeName?: string;
    address?: string;
    postalCode?: string;
    cityId?: string;
    cityName?: string;
    countryId?: string;
    countryName?: string;
    number: number;
    fax: string;
    phone: string;
    createdAt?: Date;
    updatedAt?: Date;

    constructor(model?: CourtNumber) {
        this.id = model?.id ?? '';
        this.courtId = model?.courtId ?? '';
        this.courtTypeId = model?.courtTypeId ?? '';
        this.address = model?.address ?? '';
        this.postalCode = model?.postalCode ?? '';
        this.cityId = model?.cityId ?? '';
        this.countryId = model?.countryId ?? '';
        this.createdAt = model?.createdAt ?? null;
        this.updatedAt = model?.updatedAt ?? null;
        this.courtTypeName = model?.courtTypeName ?? '';
        this.cityName = model?.cityName ?? '';
        this.countryName = model?.countryName ?? '';
        this.number = model?.number ?? null;
        this.fax = model?.fax ?? '';
        this.phone = model?.phone ?? '';
    }
}
