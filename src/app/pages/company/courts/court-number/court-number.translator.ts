import { CourtNumber } from './court-number.model';
import { CourtNumberContract } from '@app/api/contracts/court-contract/court-number.contract';

export class CourtNumberTranslator {
    static translateContractToModel(contract?: CourtNumberContract): CourtNumber {
        const model: CourtNumber = {
            id: contract?.id ?? '',
            courtId: contract?.courtId ?? '',
            courtTypeId: contract?.courtTypeId ?? '',
            address: contract?.address ?? '',
            postalCode: contract?.postalCode ?? '',
            cityId: contract?.cityId ?? '',
            countryId: contract?.countryId ?? '',
            createdAt: contract?.createdAt && contract?.createdAt !== '0000-00-00 00:00:00' ?
                new Date(contract?.createdAt) : null,
            updatedAt: contract?.updatedAt && contract?.updatedAt !== '0000-00-00 00:00:00' ?
                new Date(contract?.updatedAt) : null,
            courtTypeName: contract?.courtTypeName ?? '',
            cityName: contract?.cityName ?? '',
            countryName: contract?.countryName ?? '',
            number: contract?.number ? Number(contract?.number) : null,
            fax: contract?.fax ?? '',
            phone: contract?.phone ?? '',
        };
        return new CourtNumber(model);
    }

    static translateModelToContract(model: CourtNumber): CourtNumberContract {
        const contract: CourtNumberContract = {
            id: model?.id ?? '',
            courtId: model?.courtId ?? '',
            number: model?.number ? String(model?.number) : '',
            fax: model?.fax ?? '',
            phone: model?.phone ?? ''
        };

        return contract;
    }
}
