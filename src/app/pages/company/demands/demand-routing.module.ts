import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateDemandComponent } from './demand-editor/create-demand/create-demand.component';
import { DemandEditorComponent } from './demand-editor/demand-editor.component';
import { DemandComponent } from './demand.component';
import { MENU_ID } from '@app/shared/enums/menu.enum';

const routes: Routes = [
        {
            path: '',
            component: DemandComponent
        },
        {
            path: 'create',
            component: CreateDemandComponent
        },
        {
            path: 'edit/:id',
            component: DemandEditorComponent
        }
    ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DemandRoutingModule { }
