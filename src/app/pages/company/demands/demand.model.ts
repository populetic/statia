export class Demand {
    id: string;
    procedureNumber?: string;
    judgementNumber?: string;
    courtNumber?: string;
    courtNumberId?: string;
    courtTypeId?: string;
    courtTypeName?: string;
    courtTypeCountryId?: string;
    courtCompleteName?: string;
    courtCityAndCourtType?: string;
    amountRequested?: number;
    amountReceived?: number;
    amountInterests?: number;
    comments?: any; // TODO:
    isNoShow?: boolean; // TODO?: translator
    isOverseas?: boolean;
    hasFlightstats?: boolean;
    presentedAt?: Date;
    admittedAt?: Date;
    judgedAt?: Date;
    cancelledAt?: Date;
    createdAt?: Date;
    updatedAt?: Date;
    courtCityId?: string;
    courtCityName?: string;
    menuId?: string;
    disruptionTypeId?: string;
    lawsuiteTypeId?: string;
    airlineName?: string;
    airlineId?: string;
    lawFirmId?: string;
    lawFirmName?: string;

    constructor(model?: Demand) {
        this.id = model?.id ? model.id : '';
        this.procedureNumber = model?.procedureNumber ?? '';
        this.judgementNumber = model?.judgementNumber ?? '';
        this.courtNumberId = model?.courtNumberId ?? '';
        this.amountRequested = model?.amountRequested ?? null;
        this.amountReceived = model?.amountReceived ?? null;
        this.amountInterests = model?.amountInterests ?? null;
        this.comments = model?.comments ?? '';
        this.isNoShow = model?.isNoShow ?? false;
        this.isOverseas = model?.isOverseas ?? false;
        this.hasFlightstats = model?.hasFlightstats ?? false;
        this.presentedAt = model?.presentedAt ?? null;
        this.admittedAt = model?.admittedAt ?? null;
        this.judgedAt = model?.judgedAt ?? null;
        this.cancelledAt = model?.cancelledAt ?? null;
        this.createdAt = model?.createdAt ?? null;
        this.updatedAt = model?.updatedAt ?? null;
        this.courtTypeId = model?.courtTypeId ?? null ;
        this.courtTypeName = model?.courtTypeName ?? '';
        this.courtTypeCountryId = model?.courtTypeCountryId ?? '';
        this.courtCityId = model?.courtCityId ?? '';
        this.courtCityName = model?.courtCityName ?? '';
        this.menuId = model?.menuId ?? '';
        this.courtCompleteName = model?.courtCompleteName ?? '';
        this.courtCityAndCourtType = model?.courtCityAndCourtType ?? '';
        this.courtNumber = model?.courtNumber ?? '';
        this.disruptionTypeId = model?.disruptionTypeId ?? '';
        this.lawsuiteTypeId = model?.lawsuiteTypeId ?? '';
        this.airlineName = model?.airlineName ?? '';
        this.airlineId = model?.airlineId ?? '';
        this.lawFirmId = model?.lawFirmId ?? '';
        this.lawFirmName = model?.lawFirmName ?? '';
    }

}
