import { DemandContract } from '@app/api/contracts/demand-contract/demand.contract';
import { ID_MENU_DEMAND } from '@app/shared/constants/constants';
import { TranslateService } from '@ngx-translate/core';
import { Demand } from './demand.model';

export class DemandTranslator {

    static translateContractToModel(contract?: DemandContract, translate?: TranslateService): Demand {
        const model: Demand = {
            id: contract?.id || contract?.demandId ? this.getId(contract?.id, contract?.demandId) : '',
            procedureNumber: contract.procedureNumber ?? '',
            judgementNumber: contract.judgementNumber ?? '',
            courtNumberId: contract.courtNumberId ?? '',
            amountRequested: contract.amountRequested ? Number(contract.amountRequested) : null,
            amountReceived: contract.amountReceived ? Number(contract.amountReceived) : null,
            amountInterests: contract.amountInterests ? Number(contract.amountInterests) : null,
            comments: contract.comments ?? '',
            isNoShow: contract.isNoShow && contract.isNoShow === '1' ? true : false,
            isOverseas: contract.isOverseas && contract.isOverseas === '1' ? true : false,
            hasFlightstats: contract.hasFlightstats && contract.hasFlightstats === '1' ? true : false,
            presentedAt: contract.presentedAt && contract?.presentedAt !== '0000-00-00 00:00:00' ?
                new Date(contract.presentedAt) : null,
            admittedAt: contract.admitedAt && contract.admitedAt !== '0000-00-00 00:00:00' ?
                new Date(contract.admitedAt) : null,
            judgedAt: contract.judgedAt && contract.judgedAt && contract.judgedAt !== '0000-00-00 00:00:00' ?
                new Date(contract.judgedAt) : null,
            cancelledAt: contract.cancellatedAt && contract.cancellatedAt !== '0000-00-00 00:00:00' ?
                new Date(contract.cancellatedAt) : null,
            createdAt: contract.createdAt && contract.createdAt !== '0000-00-00 00:00:00' ?
                new Date(contract.createdAt) : null,
            updatedAt: contract.updatedAt && contract.updatedAt !== '0000-00-00 00:00:00' ?
                new Date(contract.updatedAt) : null,
            courtTypeId: contract?.courtTypeId ?? '',
            courtTypeName: contract?.courtTypeName ?? '',
            courtTypeCountryId: contract?.courtTypeCountryId ?? '',
            courtNumber: contract?.courtNumber ?? '',
            courtCityId: contract?.courtCityId ?? '',
            courtCityName: contract?.courtCityName ?? '',
            menuId: ID_MENU_DEMAND,
            courtCompleteName: this.getCourtCompleteName(translate , contract?.courtCityName, contract?.courtTypeId, contract?.courtNumber),
            courtCityAndCourtType: contract?.courtCityName && contract?.courtTypeId ?
                contract?.courtCityName + '-' +  translate.instant('courtTypeId' + contract?.courtTypeId) : null,
            disruptionTypeId: contract?.disruptionTypeId ?? '',
            lawsuiteTypeId: contract?.lawsuiteTypeId ?? '',
            airlineName: contract?.airlineName ?? '',
            airlineId: contract?.airlineId ?? '',
            lawFirmId: contract?.lawFirmId ?? '',
            lawFirmName: contract?.lawFirmName ?? '',
        };

        return new Demand(model);
    }

    static getId(id: string, demandId: string): string {
        if (id) {
            return id;
        } else if (demandId) {
            return demandId;
        }
        return null;
    }


     static getCourtCompleteName(translate: TranslateService , courtCityName: string, courtTypeId: string, courtNumber: string) {
        if (courtTypeId && courtTypeId) {
            return courtCityName + '-' + translate.instant('courtTypeId' + courtTypeId) + '-' + courtNumber;
        }

        return null;
    }

    // static translateModelToContract(model?: Demand): DemandContract {
    //     const contract: DemandContract = {
    //         id: model?.id ?? null,
    //         procedureNumber: model?.procedureNumber ?? null,
    //         judgementNumber: model?.judgementNumber ?? null,
    //         courtNumberId: model?.courtNumberId ?? null,
    //         amountRequested: model?.amountRequested ? String(model?.amountRequested) : null,
    //         amountReceived: model?.amountReceived ? String(model?.amountReceived) : null,
    //         amountInterests: model?.amountInterests ? String(model?.amountInterests) : null,
    //         comments: model?.comments ?? null,
    //         isNoShow: model?.isNoShow ? '1' : '0',
    //         isOverseas: model?.isOverseas ? '1' : '0',
    //         hasFlightstats: model?.hasFlightstats ? '1' : '0',
    //         presentedAt: model?.presentedAt ? String(model?.presentedAt) : null,
    //         admitedAt: model?.admittedAt ? String(model?.admittedAt) : null,
    //         judgedAt: model?.judgedAt ? String(model?.judgedAt) : null,
    //         cancellatedAt: model?.cancelledAt ? String(model?.cancelledAt) : null,
    //         createdAt: model?.createdAt ? String(model?.createdAt) : null,
    //         updatedAt: model?.updatedAt ? String(model.updatedAt) : null,
    //         courtTypeId: model?.courtTypeId ?? null,
    //         courtTypeName: model?.courtTypeName ?? null,
    //         courtTypeCountryId: model?.courtTypeCountryId ?? null,
    //         courtNumber: model?.courtNumber ?? null,
    //         courtCityId: model?.cityId ?? null,
    //         cityName: model?.cityName ?? null
    //     };
    //     return contract;
    //  }
}
