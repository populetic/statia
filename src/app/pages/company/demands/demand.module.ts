import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MainTableModule } from '@app/layout/main-table/main-table.module';
import { FurySharedModule } from '@fury/fury-shared.module';
import { BreadcrumbsModule } from '@fury/shared/breadcrumbs/breadcrumbs.module';
import { MaterialModule } from '@fury/shared/material-components.module';
import { CreateDemandComponent } from './demand-editor/create-demand/create-demand.component';
import { DemandEditorComponent } from './demand-editor/demand-editor.component';
import { DemandComponent } from './demand.component';
import { SharedModule } from '@app/shared/modules/shared.module';
import { FormComponentsModule } from '@app/layout/form-components/form-components.module';
import { TranslateModule } from '@ngx-translate/core';
import { LayoutModule } from '@app/layout/layout.module';
import { DemandRoutingModule } from './demand-routing.module';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        BreadcrumbsModule,
        MaterialModule,
        FurySharedModule,

        DemandRoutingModule,
        MainTableModule,
        SharedModule,
        LayoutModule,
        FormComponentsModule,
        TranslateModule
    ],
    declarations: [
        DemandComponent,
        DemandEditorComponent,
        CreateDemandComponent
    ],
    exports: [DemandComponent]
})

export class DemandModule {
}
