export interface DemandRelatedClaim {
    demandId: string;
    claimId: string;
}
