import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { TableRelatedData } from '@app/layout/table-related-data/table-related-data.interface';
import { Claim } from '@app/pages/company/claims/claim.model';
import { ClaimService } from '@app/pages/company/claims/claim.service';
import { Companion } from '@app/pages/company/companions/companion.model';
import { MenuService } from '@app/pages/statia-configuration/menus/menu.service';
import { Option } from '@app/pages/statia-configuration/options/option.model';
import { OptionService } from '@app/pages/statia-configuration/options/option.service';
import { AMOUNT_PATTERN, FORM_OPERATIONS, ID_MENU_DEMAND, PAGINATOR, POPULETIC_CREATE_DEMAND, RELATED_ENTITIES, SESSION_STORAGE, SIZES } from '@app/shared/constants/constants';
import { MODEL_PROPERTY_NAME } from '@app/shared/enums/model-property-name.enum';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { NotificationsService } from '@app/shared/services/notifications.service';
import { SessionService } from '@app/shared/services/session.service';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { PendingInterceptorService } from '@fury/shared/loading-indicator/pending-interceptor.service';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { Demand } from '../../demand.model';
import { DemandService } from '../../demand.service';

@Component({
    selector: 'app-create-demand',
    templateUrl: './create-demand.component.html',
    animations: [
        fadeInRightAnimation,
        fadeInUpAnimation
    ]
})
export class CreateDemandComponent implements OnInit, OnDestroy {
    public SIZES = SIZES;
    public PAGINATOR = PAGINATOR;
    public AMOUNT_PATTERN = AMOUNT_PATTERN;
    public MODEL_PROPERTY_NAME  = MODEL_PROPERTY_NAME;
    public RELATED_ENTITIES = RELATED_ENTITIES;
    public COMPANIONS = this.RELATED_ENTITIES?.DEMAND?.COMPANIONS;
    public POSSIBLE_COMPANIONS = this.RELATED_ENTITIES?.DEMAND?.POSSIBLE_COMPANIONS;

    public claimId: string;
    public parentRoute: string;

    public companions: {[key: string]: Companion[]} = {
        [this.COMPANIONS]: [],
        [this.POSSIBLE_COMPANIONS]: []
    };
    public editorFormData: FormGroup;
    public canUseFormOperation: boolean;
    public claim: Claim;
    public withoutSelectedCompanions = false;
    public amountRequested = 0;
    public isLoading = false;
    public claimIdCompanions: string[] = [];
    public haveCompanions = true;
    public parentRouteClaimEditor = '/claims/active-claims';
    private demandPath = '/legal/demands/edit/:id';
    private subscriptions: Subscription[] = [];
    public loadForm: boolean;
    public currentEditor: string;
    public breadcrumbs: string[];

    constructor(
        private router: Router,
        private dialog: MatDialog,
        private snackbar: MatSnackBar,
        private formBuilder: FormBuilder,
        private claimService: ClaimService,
        private translate: TranslateService,
        public demandService: DemandService,
        private sessionService: SessionService,
        private pendingInterceptorService: PendingInterceptorService,
        private notificationsService: NotificationsService,
        private optionService: OptionService

    ) {
        this.cleanDataTabs();
        const params: CustomParams  = this.sessionService.getItem(POPULETIC_CREATE_DEMAND, SESSION_STORAGE);
        this.claimId = params?.claimId;
    }

    ngOnInit(): void {
        this.parentRoute = this.router.url;
        this.breadcrumbs = this.router.url.split('/').filter(item => item !== null && item !== '');

        if (this.claimId) {
            const sub: Subscription = this.pendingInterceptorService.status$.subscribe((value: boolean) => this.isLoading = value);
            this.subscriptions.push(sub);
            this.parentRouteClaimEditor =
                this.claimId ? this.parentRouteClaimEditor + '/edit/' + this.claimId : this.parentRouteClaimEditor;
            this.canUseFormOperation = StatiaFunctions.EnabledOption(ID_MENU_DEMAND,  FORM_OPERATIONS.EDIT);
            this.createFormGroup();
            this.configureForm();
        }
    }

    ngOnDestroy(): void {
        this.cleanDataTabs();
        this.sessionService.removeItem(POPULETIC_CREATE_DEMAND, SESSION_STORAGE);
        this.subscriptions.map((s: Subscription) => s.unsubscribe());
    }

    private createFormGroup(): void {

        const demandForm: any = {
            claimId: [''],
            disruptionTypeId: [''],
            lawsuiteTypeId: [''],
            amountRequested: ['', Validators.required],
            isNoShow: [false],
            isOverseas: [false],
            hasFlightstats: [false]
        };

        this.editorFormData = this.formBuilder.group(demandForm);
    }

    private async configureForm() {
        this.claimService.getById(this.claimId)
            .subscribe((result: Result<Claim>) => {
                this.claim = result.response;
                this.amountRequested = this.claim?.amountEstimated + this.claim?.amountTickets;

                const disruptionTypeId: string = this.translate.instant(this.MODEL_PROPERTY_NAME.DISRUPTION_TYPE +
                    result.response.disruptionTypeId);
                const lawsuiteTypeId: string = this.translate.instant(this.MODEL_PROPERTY_NAME.LAW_SUITE_TYPE +
                    result.response.lawsuiteTypeId);

                this.editorFormData.patchValue({claimId: this.claimId, disruptionTypeId, lawsuiteTypeId});
                this.editorFormData.markAllAsTouched();
            });

    }

    setCompanions(dataTable: TableRelatedData<Companion[]>): void {
        if (dataTable.currentTab && dataTable?.dataFromTable) {
            this.companions[dataTable.currentTab] = dataTable.dataFromTable;
            this.loopsAllCompanions();
        }
    }

    loopsAllCompanions(): void {
        this.amountRequested = this.claim?.amountEstimated + this.claim?.amountTickets;
        this.claimIdCompanions = [];
        this.haveCompanions = false;
        for (const key in this.companions) {
            if (this.companions.hasOwnProperty(key)) {
                this.companions[key].forEach((c: Companion) => {
                    this.haveCompanions = true;
                    if (c?.isSelected && c.claimId) {
                        this.amountRequested += c?.amountEstimated + c?.amountTickets;
                        this.claimIdCompanions.push(c.claimId);
                    }
                });
            }
        }
        this.editorFormData.get('amountRequested').setValue(this.amountRequested > 0 ? this.amountRequested : '');
    }

    onSubmit(): void {
        if (this.isInvalidForm) {
            return;
        }

        const params: any = this.editorFormData.value;
        params.isNoShow = params?.isNoShow ? '1' : '0';
        params.isOverseas = params?.isOverseas ? '1'  : '0';
        params.hasFlighttats = params?.hasFlightstats ? '1' : '0';

        this.demandService.create(params).subscribe((resultDemand: Result<Demand>) => {
            this.notificationsService.manageServiceResult(resultDemand, null, null, this.snackbar, this.dialog);
            if (resultDemand.status === 1) {
                this.claimIdCompanions.push(this.claimId);
                this.demandService.createDemandRelatedClaims(resultDemand.response?.id, this.claimIdCompanions )
                .then((resultDR: Result<any>) => {
                    if (resultDR.status === 1) {
                        resultDemand.message = resultDR?.message + ' x ' + this.claimIdCompanions?.length ;
                        return this.demandService.changeClaimLawFirm(this.claim?.lawFirmId, this.claimIdCompanions);
                    } else {
                        this.notificationsService.manageServiceResult(resultDR, null, null, this.snackbar, this.dialog);
                    }

                })
                .then( (result: Result<any>) => {

                    if (result?.status === 1) {
                        this.notificationsService.manageServiceResult(resultDemand, this.demandPath, this.router, this.snackbar,
                            this.dialog);
                            return;
                    }

                    this.notificationsService.manageServiceResult(result, null, null, this.snackbar, this.dialog);
                    }
                );

            }

        });
    }

    cleanDataTabs(): void {
        this.sessionService.cleanDataTabs();
    }

    get isInvalidForm(): boolean {
        return this.editorFormData.invalid || !this.canUseFormOperation;
    }
}
