import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { FORM_OPERATIONS, RESPONSE_API_STATUS } from '@app/shared/constants/constants';
import { MODEL_PROPERTY_NAME } from '@app/shared/enums/model-property-name.enum';
import { Result } from '@app/shared/models/result.model';
import { NotificationsService } from '@app/shared/services/notifications.service';
import { SessionService } from '@app/shared/services/session.service';
import { StatiaService } from '@app/shared/services/statia.service';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { PendingInterceptorService } from '@fury/shared/loading-indicator/pending-interceptor.service';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of, Subscription } from 'rxjs';
import { distinctUntilChanged, map, tap } from 'rxjs/operators';
import { City } from '../../auxiliaries/cities/city.model';
import { CourtNumber } from '../../courts/court-number/court-number.model';
import { CourtType } from '../../courts/court-type/court-type.model';
import { Court } from '../../courts/court.model';
import { Demand } from '../demand.model';
import { DemandService } from '../demand.service';

@Component({
    selector: 'app-demand-editor',
    templateUrl: './demand-editor.component.html',
    animations: [fadeInRightAnimation, fadeInUpAnimation]
})

export class DemandEditorComponent implements OnInit, OnDestroy {

    public MODEL_PROPERTY_NAME = MODEL_PROPERTY_NAME;
    public demand: Demand;
    public id: string;
    public parentRoute: string;
    public formAction: string;
    public currentEditor: string;
    public breadcrumbs: string[];
    public editorFormData: FormGroup;
    private subscriptions: Subscription[] = [];
    public canUseFormOperation: boolean;
    private menuId: string;
    public availableCities: City[];
    public availableCourtTypes: CourtType[];
    public availableCourts: Court[];
    public availableCourtNumbers: CourtNumber[];
    public filteredCourts: Observable<any[]>;
    public filteredCourtNumbers: Observable<any[]>;
    public selectCourt: Court;
    public selectCourtNumber: CourtNumber;
    public isLoading = false;

    constructor(
        private router: Router,
        private dialog: MatDialog,
        private route: ActivatedRoute,
        private snackbar: MatSnackBar,
        private formBuilder: FormBuilder,
        private translate: TranslateService,
        public demandService: DemandService,
        private statiaService: StatiaService,
        private sessionService: SessionService,
        private notificationsService: NotificationsService,
        private pendingInterceptorService: PendingInterceptorService,
    ) { }

    ngOnInit() {
        this.route.paramMap.subscribe(paramMap => { this.id = paramMap.get('id'); });
        this.route.data.subscribe(data => this.menuId = data?.menuId);

        this.formAction = (this.id === null ? 'create' : 'edit');
        const routeToReplace = (this.id === null ?
            '/' + this.formAction : '/' + this.formAction + '/' + this.id);
        this.parentRoute = this.router.url.replace(routeToReplace, '');

        const routeItems = this.parentRoute.split('/').filter(item => item !== null && item !== '');
        this.breadcrumbs = routeItems;

        this.breadcrumbs.push(this.formAction);

        const sub: Subscription = this.pendingInterceptorService.status$.subscribe((value: boolean) => this.isLoading = value);
        this.subscriptions.push(sub);

        if (this.id) {
            this.canUseFormOperation = StatiaFunctions.EnabledOption(this.menuId, FORM_OPERATIONS.EDIT);
            this.createFormGroup();
            this.configureForm();
        }
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
        this.cleanDataTabs();
    }

    private createFormGroup(): void {
        const demandForm: any = {
            airlineName: [''],
            disruptionTypeId: [''],
            lawsuiteTypeId: [''],
            demandId: [],
            procedureNumber: [],
            judgementNumber: [],
            courtCityAndCourtType: [],
            courtNumber: [],
            amountRequested: ['', Validators.required],
            amountReceived: [],
            amountInterests: [],
            comments: [],
            presentedAt: [],
            admittedAt: [],
            judgedAt: [],
            cancelledAt: [{value: '', disabled: true}]
        };

        this.editorFormData = this.formBuilder.group(demandForm);
    }

    private async configureForm() {

        const subDemand: Subscription = this.demandService.getById(this.id).subscribe(
            async (result: Result<Demand>) => {
                if (result?.status === RESPONSE_API_STATUS.SUCCESS) {
                    this.demand = result?.response;
                    this.setDemandData(result?.response);
                }
        });
        this.subscriptions.push(subDemand);

        this.availableCourts = await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.COURT, false);

        this.availableCourtNumbers =  await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.COURT_NUMBER, false);

        this.courtCityAndCourtTypeValueChanges();

        this.courtNumberValueChanges();
    }

    private courtNumberValueChanges() {
        const subCourtNumber: Subscription = this.editorFormData.controls['courtNumber'].valueChanges.pipe(
            distinctUntilChanged(),
            tap((number: number) => {
                this.selectCourtNumber = this.availableCourtNumbers.find(
                    (c: CourtNumber) => c.courtId === this.selectCourt?.id && c.number === number);
            })
        ).subscribe();
        this.subscriptions.push(subCourtNumber);
    }

    private courtCityAndCourtTypeValueChanges() {
        this.filteredCourts = this.editorFormData.controls['courtCityAndCourtType'].valueChanges.pipe(
            distinctUntilChanged(),
            map((courtCityAndType: string) => {
                if (this.availableCourtNumbers && courtCityAndType) {
                    this.selectCourtNumber = null;
                    this.editorFormData.controls['courtNumber'].setValue('');
                    const filteredCourt: any[] =
                        this.statiaService._filter(courtCityAndType, this.availableCourts, ['courtCityAndCourtType']);
                    if (filteredCourt?.length === 1) {
                        this.filteredCourtNumbers = of((courtCityAndType ? this.checkCourtNumbers(filteredCourt[0]) : []));
                    } else {
                        this.filteredCourtNumbers = of([]);
                    }
                    return filteredCourt;
                }
            })
        );
    }

    checkCourtNumbers(court: Court): CourtNumber[] {
        if (court) {
            this.selectCourt = court;
            let courtsNumber: CourtNumber[] = this.availableCourtNumbers.filter(
                (c: CourtNumber) => c.courtId === court.id && c.courtTypeId === court.courtTypeId
            );
            courtsNumber = this.statiaService.sortByField(courtsNumber, 'number', 'number');
            return courtsNumber;
        }

        return null;
    }

    setDemandData(demand: Demand) {
        this.currentEditor = demand?.procedureNumber + ' # ' + demand?.judgementNumber;
        this.editorFormData.patchValue(demand);
        const disruptionTypeId: string = this.translate.instant(MODEL_PROPERTY_NAME.DISRUPTION_TYPE + demand?.disruptionTypeId);
        const lawsuiteTypeId: string =  this.translate.instant(MODEL_PROPERTY_NAME.LAW_SUITE_TYPE + demand?.lawsuiteTypeId);
        this.editorFormData.patchValue({demandId: demand.id, disruptionTypeId, lawsuiteTypeId});
        this.canUseFormOperation = demand?.cancelledAt !== null ? false : this.canUseFormOperation;
    }

    onSubmit(): void {
        if (this.editorFormData.invalid || !this.canUseFormOperation || this.demand?.cancelledAt) {
            return;
        }
        this.updateDemand(this.editorFormData.value);
    }

    updateDemand(params: any) {
        params.presentedAt = this.transformDate(params?.presentedAt);
        params.admitedAt = this.transformDate(params?.admittedAt);
        params.judgedAt = this.transformDate(params?.judgedAt);
        params.courtNumberId = this.selectCourtNumber?.id;

        this.demandService.edit(this.id, params).subscribe(
            (result: Result<Demand>) => {
                if (result?.status === RESPONSE_API_STATUS.SUCCESS) {
                    this.demand = result?.response;
                    this.setDemandData(result?.response);
                }
                this.notificationsService.manageServiceResult(result, null, null, this.snackbar, this.dialog);
        });
    }


    transformDate(dateToTransform: Date): string {
        return dateToTransform ? this.statiaService.formatDateTimeToBD(dateToTransform) : null;
    }

    cleanDataTabs(): void {
        // this.tableRelatedDataService.cleanDataTabs();
        this.sessionService.cleanDataTabs();
    }


    get defaultCourtNumber() {
        return this.demand?.courtNumber ?? null;
    }

    cancelDemand() {
        if (this.editorFormData.invalid || !this.canUseFormOperation || this.demand?.cancelledAt) {
            return;
        }
        this.setDemandData(this.demand);
        const params: any = this.editorFormData.value;
        params.cancellatedAt = this.transformDate(new Date());
        this.updateDemand(params);
    }

}
