import { Injectable } from '@angular/core';
import { DemandContract } from '@app/api/contracts/demand-contract/demand.contract';
import { ApiDemandService } from '@app/api/services/api-demand-service/api-demand.service';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DemandRelatedClaim } from './demand-related-claims.interface';
import { Demand } from './demand.model';
import { DemandTranslator } from './demand.translator';

@Injectable({ providedIn: 'root' })
export class DemandService implements BaseInterface<Demand> {

    private translator = DemandTranslator;

    constructor(
        private translate: TranslateService,
        private apiDemandService: ApiDemandService
    ) {}

    getAll(param: CustomParams = null): Observable<Result<Demand[]>> {
        return this.apiDemandService.getAll(param)
            .pipe(
                map((result: Result<DemandContract[]>) =>
                    this.buildResultArrayDemand<DemandContract, Demand>(result, this.translator)
                )
            );
    }

    getAllAsync(param: CustomParams = null): Promise<Result<Demand[]>> {
        return new Promise((resolve, reject) => {
            this.apiDemandService.getAllAsync(param)
                .then((result: Result<DemandContract[]>) =>
                    resolve(this.buildResultArrayDemand(result, this.translator))
                )
                .catch((err) => reject(err));
        });
    }

    getById(id: number|string): Observable<Result<Demand>> {
        return this.apiDemandService.getById(id)
            .pipe(
                map((result: Result<DemandContract>) => this.buildResultDemand(result, this.translator))
            );
    }

    create(param: Demand): Observable<Result<Demand>> {
        // TODO: return this.apiDemandService.create()
        return null;
    }

    createAsync(param: Demand): Promise<Result<Demand>> {
        // TODO: return this.apiDemandService.createAsync()
        return null;
    }

    edit(id: number|string, param: Demand): Observable<Result<Demand>> {
        // TODO: return this.apiDemandService.edit()
        return null;
    }

    editAsync(id: number|string, param: Demand): Promise<Result<Demand>> {
        // TODO: return this.apiDemandService.editAsync()
        return null;
    }

    deleteByParam(param: CustomParams): Observable<Result<any>> {
        return this.apiDemandService.deleteByParam(param);
    }

    createDemandRelatedClaims(demandId: string, claimIds: string[]): Promise<Result<any>> {
        return this.apiDemandService.createDemandRelatedClaims(demandId, claimIds);
    }

    changeClaimLawFirm(lawFirmId: string, claimIds: string[]): Promise<Result<any>> {
        return this.apiDemandService.createDemandRelatedClaims(lawFirmId, claimIds);
    }

    getRelatedClaims(demandId: string): Observable<Result<DemandRelatedClaim[]>> {
        // TODO: checks translator
        return this.apiDemandService.getRelatedClaims(demandId);
    }

    buildResultArrayDemand<Contract, Model>(result: Result<Contract[]>, translator: any): Result<Model[]> {
        const newResult: Result<Model[]> = StatiaFunctions.getResultModel<Contract[], Model[]>(result);
        newResult.response = result?.response.map((contract: Contract) => translator.translateContractToModel(contract, this.translate));
        return newResult;
    }

    buildResultDemand<Contract, Model>(result: Result<Contract>, translator: any): Result<Model> {
        const newResult: Result<Model> = StatiaFunctions.getResultModel<Contract, Model>(result);
        newResult.response = translator.translateContractToModel(result?.response, this.translate);
        return newResult;
    }
}
