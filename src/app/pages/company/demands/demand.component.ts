import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Login } from '@app/pages/login/login.model';
import { SessionService } from '@app/shared/services/session.service';
import { DemandService } from './demand.service';


@Component({
    selector: 'app-menu',
    template: `<app-main-table
                [actualMenuId]="menuId"
                [actualMenuService]="menuService"
                [actualMenuFilters]="menuFilters"
                [actualMenuSortActive]="sortActive"
                [actualMenuSortDirection]="sortDirection">
            </app-main-table>`
})

export class DemandComponent implements OnInit {

    public menuId: string;
    public menuFilters: Array<any>;
    public sortActive: string;
    public sortDirection: string;
    private availableMenuFilters: any = {
        26: {'cancellated': 0}
    };

    constructor(
        public menuService: DemandService,
        private activeRoute: ActivatedRoute,
        private sessionService: SessionService
    ) {}

    ngOnInit() {
        this.activeRoute.data.subscribe(
            data => this.menuId = data.menuId
        );

        // TODO: Mejorar esta asignación de filtros fijos a menús, poner bbdd (?) => service.getMenu(id): conf. by menu
        const user: Login = this.sessionService.user;
        if (user?.lawFirmId !== null) {
            this.availableMenuFilters[this.menuId].lawFirmId = user.lawFirmId;
        }

        this.menuFilters = this.availableMenuFilters[this.menuId];

        /** Default order */
        this.sortActive = 'createdAt';
        this.sortDirection = 'desc';
    }
}
