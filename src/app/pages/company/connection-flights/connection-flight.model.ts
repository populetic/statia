export class ConnectionFlight {
    claimId?: string;
    connectionFlightId?: string;
    order: number;
    airportDepartureId?: string;
    airportDepartureName?: string;
    airportDepartureIata?: string;
    airportDepartureNameAndIata: string;
    airportArrivalId?: string;
    airportArrivalName?: string;
    airportArrivalIata?: string;
    airportArrivalNameAndIata: string;
    arrivalScheduledTime: string;
    arrivalActualTime?: string;
    isDisrupted: boolean;
    distance: number;
    createdAt?: string;
    id?: string;

    constructor(model?: ConnectionFlight) {
        this.claimId = (model && model.claimId) ? model.claimId : null;
        this.connectionFlightId = (model && model.connectionFlightId) ? model.connectionFlightId : null;
        this.order = (model && model.order) ? model.order : 0;
        this.airportDepartureId = (model && model.airportDepartureId) ? model.airportDepartureId : null;
        this.airportDepartureName = (model && model.airportDepartureName) ? model.airportDepartureName : null;
        this.airportDepartureIata = (model && model.airportDepartureIata) ? model.airportDepartureIata : null;
        this.airportDepartureNameAndIata =
            (model && model.airportDepartureNameAndIata) ? model.airportDepartureNameAndIata : null;
        this.airportArrivalId = (model && model.airportArrivalId) ? model.airportArrivalId : null;
        this.airportArrivalName = (model && model.airportArrivalName) ? model.airportArrivalName : null;
        this.airportArrivalIata = (model && model.airportArrivalIata) ? model.airportArrivalIata : null;
        this.airportArrivalNameAndIata =
            (model && model.airportArrivalNameAndIata) ? model.airportArrivalNameAndIata : null;
        this.arrivalScheduledTime = (model && model.arrivalScheduledTime) ? model.arrivalScheduledTime : null;
        this.arrivalActualTime = (model && model.arrivalActualTime) ? model.arrivalActualTime : null;
        this.isDisrupted = (model && model.isDisrupted) ? model.isDisrupted : false;
        this.distance = (model && model.distance) ? model.distance : null;
        this.createdAt = (model && model.createdAt) ? model.createdAt : null;
        this.id = (model && model.id) ? model.id : null;
    }

}
