export class ConnectionFlightContract {
    claimId?: string;
    connectionFlightId?: string;
    order: number;
    airportDepartureId?: string;
    airportDepartureName?: string;
    airportDepartureIata?: string;
    airportDepartureNameAndIata: string;
    airportArrivalId?: string;
    airportArrivalName?: string;
    airportArrivalIata?: string;
    airportArrivalNameAndIata: string;
    arrivalScheduledTime: string;
    arrivalActualTime?: string;
    isDisrupted: string;
    distance: number;
    createdAt?: string;
    id?: string;
}
