import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { ConnectionFlight } from './connection-flight.model';
import { map } from 'rxjs/operators';
import { ConnectionFlightTranslator } from './connection-flight.translator';
import { ConnectionFlightContract } from './connection-flight.contract';
import { ApiClaimConnectionFlightsService } from '@app/api/services/api-flight-service/api-claim-connection-flights.service';

@Injectable({ providedIn: 'root' })
export class ClaimConnectionFlightService implements BaseInterface<ConnectionFlight> {
    public resourceUrl: string = environment.apiUrl + '/claim-connection-flights';
    public headers: HttpHeaders;
    private translator = ConnectionFlightTranslator;

    constructor(
        private http: HttpClient,
        private apiClaimConnectionFlightsService: ApiClaimConnectionFlightsService
    ) {}

    getAll(customParams: CustomParams = null): Observable<Result<ConnectionFlight[]>> {
        return this.apiClaimConnectionFlightsService.getAll(customParams)
        .pipe(
            map((result: Result<ConnectionFlightContract[]>) => {
                const newResult: Result<ConnectionFlight[]> =
                    StatiaFunctions.getResultModel<ConnectionFlightContract[], ConnectionFlight[]>(result);
                newResult.response = result.response.map((connectionFlight: ConnectionFlightContract) =>
                    ConnectionFlightTranslator.translateContractToModel(connectionFlight));
                return  newResult;
            })
        );
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<ConnectionFlight[]>> {
        return new Promise((resolve, reject) => {
            this.apiClaimConnectionFlightsService.getAllAsync(customParams)
                .then((result: Result<ConnectionFlightContract[]>) => {
                    const newResult: Result<ConnectionFlight[]> =
                        StatiaFunctions.getResultModel<ConnectionFlightContract[], ConnectionFlight[]>(result);
                    newResult.response = result.response.map((connectionFlight: ConnectionFlightContract) =>
                        ConnectionFlightTranslator.translateContractToModel(connectionFlight));
                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    getById(id: number|string): Observable<Result<ConnectionFlight>> {
        return this.apiClaimConnectionFlightsService.getById(id).pipe(
            map((result: Result<ConnectionFlightContract>) =>
                StatiaFunctions.buildResultModel<ConnectionFlightContract, ConnectionFlight>(result, this.translator)
            )
        );
    }

    create(model: ConnectionFlight): Observable<Result<ConnectionFlight>> {
        return this.apiClaimConnectionFlightsService.create(this.translator.translateModelToContract(model)).pipe(
            map((result: Result<ConnectionFlightContract>) =>
                StatiaFunctions.buildResultModel<ConnectionFlightContract, ConnectionFlight>(result, this.translator)
            )
        );
    }

    createAsync(model: ConnectionFlight): Promise<Result<ConnectionFlight>> {
        console.log('model', model)
        return new Promise((resolve, reject) => {
            this.apiClaimConnectionFlightsService.createAsync(this.translator.translateModelToContract(model))
                .then((result: Result<ConnectionFlightContract>) =>
                    resolve(StatiaFunctions.buildResultModel<ConnectionFlightContract, ConnectionFlight>(result, this.translator))
                )
                .catch((err) => reject(err));
        });
    }

    edit(id: number|string, param: ConnectionFlight): Observable<Result<ConnectionFlight>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: ConnectionFlight): Promise<Result<ConnectionFlight>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<ConnectionFlight>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const ConnectionFlights = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', ConnectionFlights);
    }

    addClaimConnectionFlightsAsync(claimId: any, param: any): Promise<any> {
        console.log('claimId, param', claimId, param)
        return new Promise((resolve, reject) => {
            this.apiClaimConnectionFlightsService.addClaimConnectionFlightsAsync(claimId, param)
                .then((result: Result<ConnectionFlightContract>) =>
                    resolve(StatiaFunctions.buildResultModel<ConnectionFlightContract, ConnectionFlight>(result, this.translator))
                )
                .catch((err) => reject(err));
        });
    }
}
