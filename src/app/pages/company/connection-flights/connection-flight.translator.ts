import { ConnectionFlight } from './connection-flight.model';
import { ConnectionFlightContract } from './connection-flight.contract';
import { stringToKeyValue } from '@angular/flex-layout/extended/typings/style/style-transforms';

export class ConnectionFlightTranslator {
    static translateContractToModel(contract: ConnectionFlightContract): ConnectionFlight {
        const model: ConnectionFlight = {
            claimId: (contract && contract.claimId) ? contract.claimId : null,
            connectionFlightId: (contract && contract.connectionFlightId) ? contract.connectionFlightId : null,
            order: (contract && contract.order) ? contract.order : 0,
            airportDepartureId: (contract && contract.airportDepartureId) ? contract.airportDepartureId : null,
            airportDepartureName: (contract && contract.airportDepartureName) ? contract.airportDepartureName : null,
            airportDepartureIata: (contract && contract.airportDepartureIata) ? contract.airportDepartureIata : null,
            airportDepartureNameAndIata:
                (contract && contract.airportDepartureNameAndIata) ? contract.airportDepartureNameAndIata : null,
            airportArrivalId: (contract && contract.airportArrivalId) ? contract.airportArrivalId : null,
            airportArrivalName: (contract && contract.airportArrivalName) ? contract.airportArrivalName : null,
            airportArrivalIata: (contract && contract.airportArrivalIata) ? contract.airportArrivalIata : null,
            airportArrivalNameAndIata:
                (contract && contract.airportArrivalNameAndIata) ? contract.airportArrivalNameAndIata : null,
            arrivalScheduledTime:
                (contract && contract.arrivalScheduledTime && contract.arrivalScheduledTime)
                ? contract.arrivalScheduledTime.replace(' ', 'T') : null,
            arrivalActualTime:
                (contract && contract.arrivalActualTime && contract.arrivalActualTime)
                ? contract.arrivalActualTime.replace(' ', 'T') : null,
            isDisrupted: (contract && contract.isDisrupted === '1') ? true : false,
            distance: (contract && contract.distance) ? contract.distance : 0,
            createdAt: (contract && contract.createdAt) ? contract.createdAt : null,
            id: (contract && contract.id) ? contract.id : null
        };

        return new ConnectionFlight(model);
    }

    static translateModelToContract(model: ConnectionFlight): ConnectionFlightContract {
        const contract: ConnectionFlightContract = {
            claimId : model?.claimId ?? null,
            connectionFlightId : model?.connectionFlightId ?? null,
            order: model?.order ?? null,
            airportDepartureId : model?.airportDepartureId ?? null,
            airportDepartureName : model?.airportDepartureName ?? null,
            airportDepartureIata : model?.airportDepartureIata ?? null,
            airportDepartureNameAndIata: model?.airportDepartureNameAndIata ?? null,
            airportArrivalId : model?.airportArrivalId ?? null,
            airportArrivalName : model?.airportArrivalName ?? null,
            airportArrivalIata : model?.airportArrivalIata ?? null,
            airportArrivalNameAndIata: model?.airportArrivalNameAndIata ?? null,
            arrivalScheduledTime: model?.arrivalScheduledTime ?? null,
            arrivalActualTime : model?.arrivalActualTime ?? null,
            isDisrupted: model?.isDisrupted ? '1' : '0',
            distance: model?.distance ?? null,
            createdAt : model?.createdAt ?? null,
            id : model?.id ?? null,
        };

        return contract;
    }
}
