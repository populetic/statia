export class Flight {
    id: string;
    claimId?: string;
    number: string;
    date: string;
    status: string;
    airlineId: string;
    airlineName: string;
    airlineIata: string;
    airlineNameAndIata: string;
    airportDepartureId: string;
    airportDepartureName: string;
    airportDepartureIata: string;
    airportDepartureNameAndIata: string;
    airportDepartureCountryId: string;
    airportArrivalId: string;
    airportArrivalName: string;
    airportArrivalIata: string;
    airportArrivalNameAndIata: string;
    airportArrivalCountryId: string;
    departureScheduledTime: string;
    departureActualTime?: string;
    arrivalScheduledTime: string;
    arrivalActualTime: string;
    delay: number;
    isDisrupted: boolean;
    flightDataOriginId: string;
    flightDataOriginName: string;
    distance: number;
    createdAt: string;

    constructor(model?: Flight) {
        this.id = (model && model.id) ? model.id : null;
        this.claimId = (model && model.claimId) ? model.claimId : null;
        this.number = (model && model.number) ? model.number : null;
        this.date = (model && model.date) ? model.date : null;
        this.status = (model && model.status) ? model.status : null;
        this.airlineId = (model && model.airlineId) ? model.airlineId : null;
        this.airlineName = (model && model.airlineName) ? model.airlineName : null;
        this.airlineIata = (model && model.airlineIata) ? model.airlineIata : null;
        this.airlineNameAndIata = (model && model.airlineNameAndIata) ? model.airlineNameAndIata : null;
        this.airportDepartureId = (model && model.airportDepartureId) ? model.airportDepartureId : null;
        this.airportDepartureName = (model && model.airportDepartureName) ? model.airportDepartureName : null;
        this.airportDepartureIata = (model && model.airportDepartureIata) ? model.airportDepartureIata : null;
        this.airportDepartureNameAndIata =
            (model && model.airportDepartureNameAndIata) ? model.airportDepartureNameAndIata : null;
        this.airportDepartureCountryId =
            (model && model.airportDepartureCountryId) ? model.airportDepartureCountryId : null;
        this.airportArrivalId = (model && model.airportArrivalId) ? model.airportArrivalId : null;
        this.airportArrivalName = (model && model.airportArrivalName) ? model.airportArrivalName : null;
        this.airportArrivalIata = (model && model.airportArrivalIata) ? model.airportArrivalIata : null;
        this.airportArrivalNameAndIata =
            (model && model.airportArrivalNameAndIata) ? model.airportArrivalNameAndIata : null;
        this.airportArrivalCountryId = (model && model.airportArrivalCountryId) ? model.airportArrivalCountryId : null;
        this.departureScheduledTime = (model && model.departureScheduledTime) ? model.departureScheduledTime : null;
        this.departureActualTime = (model && model.departureActualTime) ? model.departureActualTime : null;
        this.arrivalScheduledTime = (model && model.arrivalScheduledTime) ? model.arrivalScheduledTime : null;
        this.arrivalActualTime = (model && model.arrivalActualTime) ? model.arrivalActualTime : null;
        this.delay = (model && model.delay) ? model.delay : null;
        this.isDisrupted = (model && model.isDisrupted) ? model.isDisrupted : false;
        this.flightDataOriginId = (model && model.flightDataOriginId) ? model.flightDataOriginId : null;
        this.flightDataOriginName = (model && model.flightDataOriginName) ? model.flightDataOriginName : null;
        this.distance = (model && model.distance) ? model.distance : null;
        this.createdAt = (model && model.createdAt) ? model.createdAt : null;
    }

}
