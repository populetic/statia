import { FlightContract } from '@app/api/contracts/flight-contract/flight.contract';
import { Flight } from './flight.model';


export class FlightTranslator {
    static translateContractToModel(contract: FlightContract): Flight {
        const model: Flight = {
            id: (contract && contract.id) ? contract.id : null,
            claimId: (contract && contract.claimId) ? contract.claimId : null,
            number: (contract && contract.number) ? contract.number : null,
            date: (contract && contract.date) ? contract.date : null,
            status: (contract && contract.status) ? contract.status : null,
            airlineId: (contract && contract.airlineId) ? contract.airlineId : null,
            airlineName: (contract && contract.airlineName) ? contract.airlineName : null,
            airlineIata: (contract && contract.airlineIata) ? contract.airlineIata : null,
            airlineNameAndIata: (contract && contract.airlineNameAndIata) ? contract.airlineNameAndIata : null,
            airportDepartureId: (contract && contract.airportDepartureId) ? contract.airportDepartureId : null,
            airportDepartureName: (contract && contract.airportDepartureName) ? contract.airportDepartureName : null,
            airportDepartureIata: (contract && contract.airportDepartureIata) ? contract.airportDepartureIata : null,
            airportDepartureNameAndIata:
                (contract && contract.airportDepartureNameAndIata) ? contract.airportDepartureNameAndIata : null,
            airportDepartureCountryId:
                (contract && contract.airportDepartureCountryId) ? contract.airportDepartureCountryId : null,
            airportArrivalId: (contract && contract.airportArrivalId) ? contract.airportArrivalId : null,
            airportArrivalName: (contract && contract.airportArrivalName) ? contract.airportArrivalName : null,
            airportArrivalIata: (contract && contract.airportArrivalIata) ? contract.airportArrivalIata : null,
            airportArrivalNameAndIata:
                (contract && contract.airportArrivalNameAndIata) ? contract.airportArrivalNameAndIata : null,
            airportArrivalCountryId:
                (contract && contract.airportArrivalCountryId) ? contract.airportArrivalCountryId : null,
            departureScheduledTime:
                (contract && contract.departureScheduledTime && contract.departureScheduledTime)
                ? contract.departureScheduledTime.replace(' ', 'T') : null,
            departureActualTime:
                (contract && contract.departureActualTime && contract.departureActualTime)
                ? contract.departureActualTime.replace(' ', 'T') : null,
            arrivalScheduledTime:
                (contract && contract.arrivalScheduledTime && contract.arrivalScheduledTime)
                ? contract.arrivalScheduledTime.replace(' ', 'T') : null,
            arrivalActualTime:
                (contract && contract.arrivalActualTime && contract.arrivalActualTime)
                ? contract.arrivalActualTime.replace(' ', 'T') : null,
            delay: (contract && contract.delay) ? contract.delay : 0,
            flightDataOriginId: (contract && contract.flightDataOriginId) ? contract.flightDataOriginId : null,
            flightDataOriginName: (contract && contract.flightDataOriginName) ? contract.flightDataOriginName : null,
            isDisrupted: (contract && contract.isDisrupted === '1') ? true : false,
            distance: (contract && contract.distance) ? contract.distance : 0,
            createdAt: (contract && contract.createdAt) ? contract.createdAt : null
        };

        return new Flight(model);
    }
}
