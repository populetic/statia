import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FlightContract } from '@app/api/contracts/flight-contract/flight.contract';
import { ApiFlightService } from '@app/api/services/api-flight-service/api-flight.service';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Flight } from './flight.model';
import { FlightTranslator } from './flight.translator';

@Injectable({ providedIn: 'root' })
export class FlightService implements BaseInterface<Flight> {
    public resourceUrl: string = environment.apiUrl + '/flights';
    public headers: HttpHeaders;

    constructor( private http: HttpClient, private apiFlightService: ApiFlightService ) {}

    //#region interface functions
    getAll(customParams: CustomParams = null): Observable<Result<Flight[]>> {
        return this.apiFlightService.getAll(customParams)
            .pipe(
                map((result: Result<FlightContract[]>) => {
                    const newResult: Result<Flight[]> = StatiaFunctions.getResultModel<FlightContract[], Flight[]>(result);
                    newResult.response = result.response.map((flight: FlightContract) => FlightTranslator.translateContractToModel(flight));
                    return  newResult;
                })
            );
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<Flight[]>> {
        return new Promise((resolve, reject) => {
            this.apiFlightService.getAllAsync(customParams)
                .then((result: Result<FlightContract[]>) => {
                    const newResult: Result<Flight[]> = StatiaFunctions.getResultModel<FlightContract[], Flight[]>(result);
                    newResult.response = result.response.map((flight: FlightContract) => FlightTranslator.translateContractToModel(flight));
                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    getById(id: number|string): Observable<Result<Flight>> {
        return this.apiFlightService.getById(id).pipe(
            map((result: Result<FlightContract>) => {
                const newResult: Result<Flight> = StatiaFunctions.getResultModel<FlightContract, Flight>(result);
                const flight: Flight = FlightTranslator.translateContractToModel(result.response);
                newResult.response = flight;

                return  newResult;
            })
        );
    }

    create(param: Flight): Observable<Result<Flight>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: Flight): Promise<Result<Flight>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, flight: Flight): Observable<Result<Flight>> {
        return this.apiFlightService.editByModel(id, flight).pipe(
            map((result: Result<FlightContract>) => {
                const newResult: Result<Flight> = StatiaFunctions.getResultModel<FlightContract, Flight>(result);
                const flightResult: Flight = FlightTranslator.translateContractToModel(result.response);
                newResult.response = flightResult;

                return  newResult;
            })
        );
    }

    editAsync(id: number|string, param: Flight): Promise<Result<Flight>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<Flight>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const Flights = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', Flights);
    }
    //#endregion

    //#region Non implemented functions
    calculateDelay(arrivalScheduledTime: any, arrivalActualTime: any) {
        const timeDiff = arrivalActualTime - arrivalScheduledTime;
        return Math.round(timeDiff / 60000);
    }
    //#endregion
}
