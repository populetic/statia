import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '@app/pages/management/users/user.model';
import { EMAIL_PATTERN, FORM_OPERATIONS, RELATED_ENTITIES, SIZES, TIMEOUTS } from '@app/shared/constants/constants';
import { MODEL_PROPERTY_NAME } from '@app/shared/enums/model-property-name.enum';
import { FilterValue } from '@app/shared/interfaces/filter-values.interface';
import { Result } from '@app/shared/models/result.model';
import { NotificationsService } from '@app/shared/services/notifications.service';
import { SessionService } from '@app/shared/services/session.service';
import { StatiaService } from '@app/shared/services/statia.service';
import { ValidationService } from '@app/shared/services/validation.service';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { PendingInterceptorService } from '@fury/shared/loading-indicator/pending-interceptor.service';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { LawFirmAdministratorService } from '../law-firm-administrators.service';
import { LawFirm } from '../law-firm.model';
import { LawFirmService } from '../law-firm.service';

@Component({
  selector: 'app-law-firms-editor',
  templateUrl: './law-firm-editor.component.html',
  animations: [
    fadeInRightAnimation,
    fadeInUpAnimation
]
})
export class LawFirmEditorComponent implements OnInit, OnDestroy {

    private emailPattern = EMAIL_PATTERN;
    public RELATED_ENTITIES = RELATED_ENTITIES;
    public SIZES = SIZES;

    public lawFirm: LawFirm;
    public id: string;
    public menuId: string;
    public parentRoute: string;
    public formAction: string;
    public currentEditor: string;
    public breadcrumbs: string[];
    public editorFormData: FormGroup;
    public isLawFirmAdmin: true;
    public lawFirmAdminUsers: Array<any>;
    public lawFirmAdminUsersOri: Array<any>;

    private subscriptions: Subscription[] = [];
    public isLoading = false;
    public canUseFormOperation: boolean;

    public filteredCountries: Observable<any[]>;
    public availableCountries: any[];
    public existingLawFirmsName: any[];
    public forceToReloadData = false;

    constructor(
        private router: Router,
        private dialog: MatDialog,
        private snackbar: MatSnackBar,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private translate: TranslateService,
        private statiaService: StatiaService,
        private sessionService: SessionService,
        private lawFirmsService: LawFirmService,
        private validationService: ValidationService,
        private notificationsService: NotificationsService,
        private LawFirmAdminService: LawFirmAdministratorService,
        private pendingInterceptorService: PendingInterceptorService,
    ) {
        this.lawFirm = new LawFirm();
        this.cleanDataTabs();
    }

    ngOnInit(): void {
        this.route.paramMap.subscribe(paramMap => this.id = paramMap.get('id'));
        this.route.data.subscribe(data => this.menuId = data?.menuId);

        this.formAction = (this.id === null ? 'create' : 'edit');
        const routeToReplace = (this.id === null ? '/' + this.formAction : '/' + this.formAction + '/' + this.id);
        this.parentRoute = this.router.url.replace(routeToReplace, '');

        const routeItems = this.parentRoute.split('/').filter(item => item !== null && item !== '');
        this.breadcrumbs = routeItems;
        const sub: Subscription = this.pendingInterceptorService.status$.subscribe((value: boolean) => this.isLoading = value);
        this.subscriptions.push(sub);
        this.canUseFormOperation = StatiaFunctions.EnabledOption(this.menuId, FORM_OPERATIONS[this.formAction.toUpperCase()]);
        this.breadcrumbs.push(this.formAction);

        this.createFormGroup();
        this.configureForm();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
        this.cleanDataTabs();
    }

    private createFormGroup() {
        const lawFirmsForm: any = {
            name: ['', Validators.required],
            countryName: ['', Validators.required],
            email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
            phone: ['', Validators.required],
            fax: [''],
            address: [''],
            website: [''],
        };
        this.editorFormData = this.formBuilder.group(lawFirmsForm);
    }

    getData() {
        const lawFirmSubs: Subscription = this.lawFirmsService.getById(this.id).subscribe(
            (result: Result<LawFirm>) => {
                this.lawFirm = result.response;
                this.editorFormData.patchValue(result.response);
                this.currentEditor = this.lawFirm.name;
            }
        );
        this.subscriptions.push(lawFirmSubs);
    }

    onSubmit() {
        if (this.editorFormData.invalid || !this.canUseFormOperation) {
            return;
        }
        let param: any;
        param = this.editorFormData.value;
        const selCountry: FilterValue = this.filterByField(this.availableCountries, 'name', param.countryName);
        param.countryId = (selCountry && selCountry.id) ? selCountry.id : null;

        if ( this.id !== null) {
            this.updateLawFirm(param);
        } else {
            this.createLawFirm(param);
        }
    }

    filterByField(values: any[], field: string, value: string): any {
        const result = this.statiaService.filterByField(values, field, value);
        return (result) ? result[0] : null;
    }

    private async configureForm() {
        this.availableCountries = await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.COUNTRY);
        this.existingLawFirmsName = await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.LAW_FIRM);

        if (this.id !== null) {
            this.getData();
        }
        this.validatorForm();
    }

    validatorForm() {
        this.setValidatorsCountries();
        this.valueChangesCountryName();
        this.valueChangesLawFirmsName();
    }

    setValidatorsCountries() {
        this.editorFormData.controls['countryName'].setValidators(
            [Validators.required, this.validationService.validateFieldValue(this.availableCountries, 'name')]
        );
    }

    valueChangesCountryName() {
        this.filteredCountries = this.editorFormData.controls['countryName'].valueChanges.pipe(
            tap((value: string) => {
                const countryName = value;
                const newLawFirmName = this.editorFormData.get('name').value;
                this.existingLawFirmsName.forEach(x => {
                    if (x.name === newLawFirmName && x.countryName === countryName) {
                        this.editorFormData.get('countryName').setErrors( { selectedDuplicateValueError: { value: countryName } });
                    }
                });
            }),
            map(value => (value ? this.statiaService._filter(value, this.availableCountries, ['name']) : this.availableCountries.slice()))
        );
    }

    valueChangesLawFirmsName() {
       this.editorFormData.controls['name'].valueChanges.pipe(
           tap((value: string) => {
               const newLawFirmName = value;
               const countryName = this.editorFormData.get('countryName').value;
               this.existingLawFirmsName.forEach(x => {
                   if (x.name === newLawFirmName && x.countryName === countryName) {
                       this.editorFormData.get('name').setErrors( { selectedDuplicateValueError: { value: newLawFirmName } });
                   }
               });
           })
        ).subscribe();
    }

    updateLawFirm(param: any): void {
        const sub: Subscription = this.lawFirmsService.edit(this.id, param).subscribe(
            (resultLawFirm: Result<LawFirm>) => {
                this.notificationsService.manageServiceResult(resultLawFirm, null, this.router, this.snackbar, this.dialog);
            }
        );
        this.subscriptions.push(sub);
    }

    createLawFirm(param: any): void {
        let pathToRedirect: string = null;
        const sub: Subscription = this.lawFirmsService.create(param).subscribe(
            (resultLawFirm: Result<LawFirm>) => {
                if (resultLawFirm.status === 1) {
                    pathToRedirect = this.parentRoute + '/edit/:id';
                }
                this.notificationsService.manageServiceResult(resultLawFirm, pathToRedirect, this.router, this.snackbar, this.dialog);
            }
        );
        this.subscriptions.push(sub);
    }

    cleanDataTabs(): void {
        this.sessionService.cleanDataTabs();
    }

    cleanCurrentDataTab() {
        this.sessionService.cleanCurrentDataTabs('lawFirmAdministrator');
    }

    setUsers(users: any): void {
        this.forceToReloadData = false;
        this.lawFirmAdminUsers = users.dataFromTable.filter((user: User) => user.isLawFirmAdmin === true);
        this.lawFirmAdminUsersOri = this.lawFirmAdminUsersOri ?? this.lawFirmAdminUsers;
    }

    saveButtonEvent() {
        this.cleanCurrentDataTab();
        this.LawFirmAdminService.updateLawFirmAdministrators(this.id, this.lawFirmAdminUsers, this.lawFirmAdminUsersOri)
        .then(() => {
            this.lawFirmAdminUsersOri = null;
            this.forceToReloadData = true;
        })
        .catch((error) =>
            this.snackbar.open(this.translate.instant('snbErrorUpdateLawFirmAdministrators') + JSON.stringify(error)
            , 'Close', { duration: TIMEOUTS['3'] }));
    }
}
