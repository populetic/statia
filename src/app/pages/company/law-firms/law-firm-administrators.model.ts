export class LawFirmAdministrators {
    lawFirmId: string;
    userId: string;
    username: string;
    name: string;
    surnames: string;

    constructor(model?: LawFirmAdministrators) {

        this.lawFirmId = (model && model.lawFirmId) ? model.lawFirmId : null;
        this.userId = (model && model.userId) ? model.userId : null;
        this.username = (model && model.username) ? model.username : null;
        this.name = (model && model.name) ? model.name : null;
        this.surnames = (model && model.surnames) ? model.surnames : null;
    }
}
