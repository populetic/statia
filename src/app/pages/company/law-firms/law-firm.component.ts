import { Component, OnInit } from '@angular/core';
import { LawFirmService } from './law-firm.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-menu',
  template: `<app-main-table
        [actualMenuId]="menuId"
        [actualMenuService]="menuService"
        [actualMenuFilters]="menuFilters"
        [actualMenuSortActive]="sortActive"
        [actualMenuSortDirection]="sortDirection"
  >
  </app-main-table>`
})
export class LawFirmComponent implements OnInit {
    public menuId: string;
    public menuFilters: Array<any>;
    public sortActive: string;
    public sortDirection: string;

    constructor(
        private activeRoute: ActivatedRoute,
        public menuService: LawFirmService
    ) { }

    ngOnInit(): void {
        this.activeRoute.data.subscribe(
            data => this.menuId = data.menuId
        );

        /** Default order */
        this.sortActive = 'createdAt';
        this.sortDirection = 'desc';
    }

}
