import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LawFirmAdministratorsContract } from '@app/api/contracts/law-firms-contract/law-firm-administrators.contract';
import { ApiLawFirmAdministratorService } from '@app/api/services/api-law-firm-services/api-law-firm-administrators.service';
import { User } from '@app/pages/management/users/user.model';
import { UserService } from '@app/pages/management/users/user.service';
import { RELATED_ENTITIES, TIMEOUTS } from '@app/shared/constants/constants';
import { BaseServiceInterface } from '@app/shared/interfaces/base-service.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { SessionService } from '@app/shared/services/session.service';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { forkJoin, Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { LawFirmAdministrators } from './law-firm-administrators.model';
import { LawFirmAdministratorsTranslator } from './law-firm-administrators.translator';

@Injectable({ providedIn: 'root' })
export class LawFirmAdministratorService implements BaseServiceInterface<LawFirmAdministrators> {

    constructor(
        private sessionService: SessionService,
        private userService: UserService,
        private snackbar: MatSnackBar,
        private apiLawFirmAdministratorService: ApiLawFirmAdministratorService
    ) {  }

    getAll(customParams: CustomParams): Observable<Result<LawFirmAdministrators[]>> {
        return this.apiLawFirmAdministratorService.getAll(customParams)
        .pipe(
            map((result: Result<LawFirmAdministratorsContract[]>) => {
                const newResult: Result<LawFirmAdministrators[]> =
                StatiaFunctions.getResultModel<LawFirmAdministratorsContract[], LawFirmAdministrators[]>(result);

                newResult.response = result.response.map((lawFirmAdministrators: LawFirmAdministratorsContract) =>
                LawFirmAdministratorsTranslator.translateContractToModel(lawFirmAdministrators));

                return  newResult;
            })
        );
    }

    getLawFirmAdministrators(): Promise<Result<LawFirmAdministrators>> {
        return new Promise((resolve, reject) => {
            this.apiLawFirmAdministratorService.getLawFirmAdministrators()
                .then((result: Result<LawFirmAdministratorsContract>) => {
                    const newResult: Result<LawFirmAdministrators> =
                    StatiaFunctions.getResultModel<LawFirmAdministratorsContract, LawFirmAdministrators>(result);

                    newResult.response = LawFirmAdministratorsTranslator.translateContractToModel(result.response);

                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    create(customParams: CustomParams): Observable<Result<any>> {
        return this.apiLawFirmAdministratorService.create(customParams)
        .pipe(
            map((result: Result<LawFirmAdministratorsContract>) => {
                const newResult: Result<LawFirmAdministrators> =
                StatiaFunctions.getResultModel<LawFirmAdministratorsContract, LawFirmAdministrators>(result);

                newResult.response = LawFirmAdministratorsTranslator.translateContractToModel(result.response);

                return  newResult;
            })
        );
    }

    deleteByParam(customParams: CustomParams): Observable<Result<any>> {
        return this.apiLawFirmAdministratorService.deleteByParam(customParams);
    }

    getRelatedEntity(currentTab: string, lawFirmId: string): Promise<User[]> {
        return new Promise((resolve) => {
            const dataByTab: any = this.sessionService.getDataByTab(currentTab);
            if (!dataByTab) {
                const sub: Subscription = this.getRelatedEntityFromAPI(currentTab)
                    .subscribe((result: Result<User[]>) => {
                        const lawFirmUsers: User[] = this.checksUserLawFirm(result.response, lawFirmId);
                        this.sessionService.setDataByTab(currentTab, lawFirmUsers);
                        sub.unsubscribe();
                        resolve(lawFirmUsers);
                    });
            } else {
                resolve(dataByTab);
            }
        });
    }

    getRelatedEntityFromAPI(currentTab: string): Observable<Result<User[]>> {
        switch (currentTab) {
            case RELATED_ENTITIES.LAWFIRM.ADMINISTRATORS:
                return this.userService.getAll(null);
        }
    }

    checksUserLawFirm(users: User[], lawFirmId: string) {
        const lawFirmUsers = users.filter((user: User) => user.lawFirmId === lawFirmId || !user.lawFirmId);
        const activeUsers = lawFirmUsers.filter((user: User) => user.isActive === true);
        return activeUsers;
    }

    updateLawFirmAdministrators(lawFirmId: string, adminUsers: User[], adminUsersOld: User[]): Promise<any> {
        const createObs: any[] = [];
        const deleteObs: any[] = [];
        const editUsersObs: any[] = [];
        let customParams: CustomParams = { lawFirmId: '', userId: '' };

        adminUsers.forEach(admin => {
            if (admin.lawFirmId === null) {
                admin.lawFirmId = lawFirmId;
                editUsersObs.push(this.userService.edit(admin.id, admin));

            }
            if (!adminUsersOld.some((u: any) => admin.id === u.id)) {
                customParams = { lawFirmId: admin.lawFirmId, userId: admin.id };
                createObs.push(this.create(customParams));
            }
        });

        adminUsersOld.forEach(adminOld => {
            if (!adminUsers.some((u: any) => adminOld.id === u.id)) {
                customParams = { lawFirmId: adminOld.lawFirmId, userId: adminOld.id };
                deleteObs.push(this.deleteByParam(customParams));
            }
        });

        const requestAdmins: any[] = createObs.concat(deleteObs);
        const requestUsers: any[] = editUsersObs;

        if (requestUsers.length !== 0 ) {
            return this.editUsersWithLawFirmId(requestUsers, requestAdmins);
        } else {
            return this.editRelatedTable(requestAdmins);
        }
    }

    editUsersWithLawFirmId(resquestUsers: any, requestAdmins: any) {
        return new Promise((resolve, reject) => {
            forkJoin(resquestUsers).subscribe(
                () => {
                     this.editRelatedTable(requestAdmins).then((request) => resolve(request));
                },
                (error: any) => {
                    this.snackbar.open(error.message, 'close', { duration: TIMEOUTS['2'] });
                    reject(false);
                }
            );
        });
    }

    editRelatedTable(request: any) {
        const msgQueue = [];
        return new Promise((resolve, reject) => {
            forkJoin(request).subscribe(
                (res: [Result<any>]) => {
                    res.forEach(r => {
                        msgQueue.push(r);
                    });
                this.showNextSnackBar(msgQueue);
                resolve(true);
                },
                (error: any) => {
                    this.snackbar.open(error.message, 'close', { duration: TIMEOUTS['3'] });
                    reject(false);
                }
            );
        });

    }

    showNextSnackBar(msgQueue: Array<any>) {
        if (msgQueue.length === 0) {
          return;
        }
        const message = msgQueue.shift();
        const snackBarRef = this.snackbar.open(message.message, 'close', {duration: TIMEOUTS['2']});
        snackBarRef.afterDismissed().subscribe(() => {
          this.showNextSnackBar(msgQueue);
        });
    }

    getAllAsync(param: CustomParams): Promise<Result<LawFirmAdministrators[]>> {
        throw new Error('Method not implemented.');
    }
    getById(id: string | number): Observable<Result<LawFirmAdministrators>> {
        throw new Error('Method not implemented.');
    }
    createAsync(param: LawFirmAdministrators): Promise<Result<LawFirmAdministrators>> {
        throw new Error('Method not implemented.');
    }
    edit(id: string | number, param: LawFirmAdministrators): Observable<Result<LawFirmAdministrators>> {
        throw new Error('Method not implemented.');
    }
    editAsync(id: string | number, param: LawFirmAdministrators): Promise<Result<LawFirmAdministrators>> {
        throw new Error('Method not implemented.');
    }
}
