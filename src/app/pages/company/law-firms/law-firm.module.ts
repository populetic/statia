import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { BreadcrumbsModule } from '@fury/shared/breadcrumbs/breadcrumbs.module';
import { MaterialModule } from '@fury/shared/material-components.module';
import { FurySharedModule } from '@fury/fury-shared.module';
import { SharedModule } from '@app/shared/modules/shared.module';
import { FormComponentsModule } from '@app/layout/form-components/form-components.module';
import { TranslateModule } from '@ngx-translate/core';
import { LawFirmComponent } from './law-firm.component';
import { LawFirmEditorComponent } from './law-firm-editor/law-firm-editor.component';
import { MainTableModule } from '@app/layout/main-table/main-table.module';
import { LayoutModule } from '@app/layout/layout.module';
import { LawFirmRoutingModule } from './law-firm-routing.module';

@NgModule({
  declarations: [LawFirmComponent, LawFirmEditorComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BreadcrumbsModule,
    MaterialModule,
    FurySharedModule,

    LawFirmRoutingModule,
    MainTableModule,
    SharedModule,
    LayoutModule,
    FormComponentsModule,
    TranslateModule
  ],
  exports: [LawFirmComponent]
})

export class LawFirmModule { }
