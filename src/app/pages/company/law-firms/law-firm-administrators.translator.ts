import { LawFirmAdministrators } from './law-firm-administrators.model';
import { LawFirmAdministratorsContract } from '@app/api/contracts/law-firms-contract/law-firm-administrators.contract';


export class LawFirmAdministratorsTranslator {
    static translateContractToModel(contract: LawFirmAdministratorsContract): LawFirmAdministrators {
        const model: LawFirmAdministrators = {
            lawFirmId: (contract && contract.lawFirmId) ? contract.lawFirmId : null,
            userId: (contract && contract.userId) ? contract.userId : null,
            username: (contract && contract.username) ? contract.username : null,
            name: (contract && contract.name) ? contract.name : null,
            surnames: (contract && contract.surnames) ? contract.surnames : null,
        };
        return new LawFirmAdministrators(model);
    }

    static translateModelToContract(model: LawFirmAdministrators): LawFirmAdministratorsContract {
        const contract: LawFirmAdministratorsContract = {
            name: model?.name ? model.name.trim() : '',
            userId: model?.userId ?? '',
            username: model?.username ? model.username.trim() : '',
            surnames: model?.surnames ? model.surnames.trim() : '',
            lawFirmId: model?.lawFirmId ?? '',
        };

        return contract;
    }
}
