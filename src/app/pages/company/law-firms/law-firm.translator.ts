import { LawFirmContract } from '../../../api/contracts/law-firms-contract/law-firm.contract';
import { LawFirm } from './law-firm.model';


export class LawFirmTranslator {
    static translateContractToModel(contract: LawFirmContract): LawFirm {
        const model: LawFirm = {
            id: (contract && contract.id) ? contract.id : null,
            name: (contract && contract.name) ? contract.name.trim() : null,
            email: (contract && contract.email) ? contract.email.replace(/ /g, '').toLowerCase() : null,
            phone: (contract && contract.phone) ? contract.phone : null,
            fax: (contract && contract.fax) ? contract.fax : null,
            address: (contract && contract.address) ? contract.address : null,
            countryId: (contract && contract.countryId) ? contract.countryId : null,
            website: (contract && contract.website) ? contract.website : null,
            createdAt: (contract && contract.createdAt) ? new Date(contract.createdAt) : null,
            updatedAt: (contract && contract.updatedAt) ? new Date(contract.updatedAt) : null,
            countryName: (contract && contract.countryName) ? contract.countryName : null,
        };
        return new LawFirm(model);
    }

    static translateModelToContract(model: LawFirm): LawFirmContract {
        const contract: LawFirmContract = {
            name: model?.name ? model.name.trim() : '',
            countryId: model?.countryId ?? '',
            email: model?.email ? model.email.replace(/ /g, '')?.toLowerCase() : '',
            phone: model?.phone ?? '',
            fax: model?.fax ?? '',
            address: model?.address ?? '',
            website: model?.website ?? '',
        };

        return contract;
    }

}
