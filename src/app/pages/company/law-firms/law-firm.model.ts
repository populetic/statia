import { User } from '@app/pages/management/users/user.model';

export class LawFirm {
    id: string;
    name: string;
    countryId: string;
    countryName: string;
    email?: string;
    phone?: string;
    fax?: string;
    address?: string;
    website?: string;
    createdAt: Date;
    updatedAt: Date;

    constructor(model?: LawFirm) {
        this.id = (model && model.id) ? model.id : null;
        this.name = (model && model.name) ? model.name : null;
        this.countryId = (model && model.countryId) ? model.countryId : null;
        this.countryName = (model && model.countryName) ? model.countryName : null;
        this.email = (model && model.email) ? model.email : null;
        this.phone = (model && model.phone) ? model.phone : null;
        this.fax = (model && model.fax) ? model.fax : null;
        this.address = (model && model.address) ? model.address : null;
        this.website = (model && model.website) ? model.website : null;
        this.createdAt = (model && model.createdAt) ? model.createdAt : null;
        this.updatedAt = (model && model.updatedAt) ? model.updatedAt : null;
    }
}
