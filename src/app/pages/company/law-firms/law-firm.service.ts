import { Injectable } from '@angular/core';
import { LawFirmContract } from '@app/api/contracts/law-firms-contract/law-firm.contract';
import { ApiLawFirmService } from '@app/api/services/api-law-firm-services/api-law-firm.service';
import { BaseServiceInterface } from '@app/shared/interfaces/base-service.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LawFirm } from './law-firm.model';
import { LawFirmTranslator } from './law-firm.translator';

@Injectable({ providedIn: 'root' })
export class LawFirmService implements BaseServiceInterface<LawFirm> {

    constructor(private apiLawFirmService: ApiLawFirmService) {}

    getAll(customParams: CustomParams = null): Observable<Result<LawFirm[]>> {
        return this.apiLawFirmService.getAll(customParams)
            .pipe(
                map((result: Result<LawFirmContract[]>) => {
                    const newResult: Result<LawFirm[]> = StatiaFunctions.getResultModel<LawFirmContract[], LawFirm[]>(result);
                    newResult.response = result.response.map((lawFirm: LawFirmContract) =>
                        LawFirmTranslator.translateContractToModel(lawFirm));

                    return  newResult;
                })
            );
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<LawFirm[]>> {
        return new Promise((resolve, reject) => {
            this.apiLawFirmService.getAllAsync(customParams)
                .then((result: Result<LawFirmContract[]>) => {
                    const newResult: Result<LawFirm[]> = StatiaFunctions.getResultModel<LawFirmContract[], LawFirm[]>(result);
                    newResult.response = result.response.map((lawFirm: LawFirmContract) =>
                    LawFirmTranslator.translateContractToModel(lawFirm));

                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    getById(id: number|string): Observable<Result<LawFirm>> {
        return this.apiLawFirmService.getById(id).pipe(
            map((result: Result<LawFirmContract>) => {
                const newResult: Result<LawFirm> = StatiaFunctions.getResultModel<LawFirmContract, LawFirm>(result);
                const lawFirm: LawFirm = LawFirmTranslator.translateContractToModel(result.response);
                newResult.response = lawFirm;

                return  newResult;
            })
        );
    }

    create(lawFirm: LawFirm): Observable<Result<LawFirm>> {
        const contract = LawFirmTranslator.translateModelToContract(lawFirm);
        return this.apiLawFirmService.create(contract)
            .pipe(
                map((result: Result<LawFirmContract>) => {
                    const newResult: Result<LawFirm> = StatiaFunctions.getResultModel<LawFirmContract, LawFirm>(result);
                    newResult.response = LawFirmTranslator.translateContractToModel(result.response);
                    return  newResult;
                })
            );
    }

    edit(id: number|string, lawFirm: LawFirm): Observable<Result<LawFirm>> {
        const contract = LawFirmTranslator.translateModelToContract(lawFirm);
        return this.apiLawFirmService.edit(id, contract)
            .pipe(
                map((result: Result<LawFirmContract>) => {
                    const newResult: Result<LawFirm> = StatiaFunctions.getResultModel<LawFirmContract, LawFirm>(result);
                    newResult.response = LawFirmTranslator.translateContractToModel(result.response);
                    return  newResult;
                })
            );
    }

    deleteByParam(customParams: CustomParams): Observable<Result<any>> {
        return this.apiLawFirmService.deleteByParam(customParams);
    }

    createAsync(param: LawFirm): Promise<Result<LawFirm>> {
        throw new Error('Method not implemented.');
    }
    editAsync(id: string | number, param: LawFirm): Promise<Result<LawFirm>> {
        throw new Error('Method not implemented.');
    }
}
