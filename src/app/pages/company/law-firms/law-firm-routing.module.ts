import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LawFirmEditorComponent } from './law-firm-editor/law-firm-editor.component';
import { LawFirmComponent } from './law-firm.component';

const routes: Routes = [
        {
            path: '',
            component: LawFirmComponent
        },
        {
            path: 'create',
            component: LawFirmEditorComponent
        },
        {
            path: 'edit/:id',
            component: LawFirmEditorComponent
        }
    ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LawFirmRoutingModule { }
