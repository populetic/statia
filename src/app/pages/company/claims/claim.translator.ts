import { Claim } from './claim.model';
import { ClaimContract } from '../../../api/contracts/claim-contract/claim.contract';
import { ID_MENU_ACTIVE_CLAIMS, ID_MENU_CLOSED_CLAIMS } from '@app/shared/constants/constants';

export class ClaimTranslator {
    static translateContractToModel(contract: ClaimContract): Claim {
        const model: Claim = {
            id: (contract && (contract.id || contract.claimId )) ?
                this.getClaimId(contract?.id, contract?.claimId) : null,
            ref: (contract && contract.ref) ? contract.ref : null,
            airlineIata: (contract && contract.airlineIata) ? contract.airlineIata : null,
            airlineId: (contract && contract.airlineId) ? contract.airlineId : null,
            airlineName: (contract && contract.airlineName) ? contract.airlineName : null,
            amountEstimated: (contract && contract.amountEstimated) ? parseFloat(contract.amountEstimated) : null,
            amountReceived: (contract && contract.amountReceived) ? parseFloat(contract.amountReceived) : null,
            billNumber: (contract && contract.billNumber) ? contract.billNumber : null,
            billSeries: (contract && contract.billSeries) ? contract.billSeries : null,
            billTotal: (contract && contract.billTotal) ? parseFloat(contract.billTotal) : null,
            claimCompetenceCountryIso: (contract && contract.claimCompetenceCountryIso) ?
                contract.claimCompetenceCountryIso : null,
            claimDocumentationStatusName: (contract && contract.claimDocumentationStatusName) ?
                contract.claimDocumentationStatusName : null,
            claimStatusName: (contract && contract.claimStatusName) ? contract.claimStatusName : null,
            clientCountryId: (contract && contract.clientCountryId) ? contract.clientCountryId : null,
            clientEmail: (contract && contract.clientEmail) ? contract.clientEmail : null,
            clientName: (contract && contract.clientName) ? contract.clientName : null,
            clientSurname: (contract && contract.clientSurname) ? contract.clientSurname : null,
            completeName: (contract && contract.completeName) ? contract.completeName : null,
            demandId: (contract && contract.demandId) ? contract.demandId : null,
            disruptionTypeName: (contract && contract.disruptionTypeName) ? contract.disruptionTypeName : null,
            finishedAt: (contract && contract.finishedAt && contract?.finishedAt !== '0000-00-00 00:00:00') ?
                new Date(contract.finishedAt) : null,
            flightDate: (contract && contract.flightDate  && contract?.flightDate !== '0000-00-00 00:00:00') ?
                new Date(contract.flightDate) : null,
            flightId: (contract && contract.flightId) ? contract.flightId : null,
            languageCode: (contract && contract.languageCode) ? contract.languageCode : null,
            partnerBusinessName: (contract && contract.partnerBusinessName) ? contract.partnerBusinessName : null,
            reservationNumber: (contract && contract.reservationNumber) ? contract.reservationNumber : null,
            userId: (contract && contract.userId) ? contract.userId : null,
            username: (contract && contract.username) ? contract.username : null,
            userCompleteName: (contract && contract.userCompleteName) ? contract.userCompleteName : null,
            clientId: (contract && contract.clientId) ? contract.clientId : null,
            claimStatusId: (contract && contract.claimStatusId) ? contract.claimStatusId : null,
            claimDocumentationStatusId: (contract && contract.claimDocumentationStatusId) ?
                contract.claimDocumentationStatusId : null,
            isOnboardingFlight: (contract && contract.isOnboardingFlight === '1') ? true : false,
            disruptionTypeId: (contract && contract.disruptionTypeId) ? contract.disruptionTypeId : null,
            amountTickets: (contract && contract.amountTickets) ? parseFloat(contract.amountTickets) : null,
            fee: (contract && contract.fee) ? parseFloat(contract.fee) : null,
            hasConnectionFlight: (contract && contract.hasConnectionFlight === '1') ? true : false,
            delayOptionId: (contract && contract.delayOptionId) ? contract.delayOptionId : null,
            airlineReasonId: (contract && contract.airlineReasonId) ? contract.airlineReasonId : null,
            lawsuiteTypeId: (contract && contract.lawsuiteTypeId) ? contract.lawsuiteTypeId : null,
            description: (contract && contract.description) ? contract.description : null,
            mainClaimId: (contract && contract.mainClaimId) ? contract.mainClaimId : null,
            isRemarketingEnabled: (contract && contract.isRemarketingEnabled === '1') ? true : false,
            isFirstRemarketingSent: (contract && contract.isFirstRemarketingSent === '1') ? true : false,
            isClaimable: (contract && contract.isClaimable === '1') ? true : false,
            isExtrajudicialAirlineNegative: (contract && contract.isExtrajudicialAirlineNegative === '1') ? true : null,
            resolutionWayId: (contract && contract.resolutionWayId) ? contract.resolutionWayId : null,
            lawFirmId: (contract && contract.lawFirmId) ? contract.lawFirmId : null,
            extrajudicialSentAt: (contract && contract.extrajudicialSentAt &&
                contract?.extrajudicialSentAt !== '0000-00-00 00:00:00') ? new Date(contract.extrajudicialSentAt) : null,
            createdAt: (contract && contract.createdAt && contract?.createdAt !== '0000-00-00 00:00:00') ?
                new Date(contract.createdAt) : null,
            updatedAt: (contract && contract.updatedAt && contract?.updatedAt !== '0000-00-00 00:00:00') ?
                new Date(contract.updatedAt) : null,
            amountInitialCompensation: (contract && contract.amountInitialCompensation) ?
                Number(contract.amountInitialCompensation) : null,
            airlineAlternativeId: (contract && contract.airlineAlternativeId) ?
                contract.airlineAlternativeId : null,
            airlineAlternativeName: (contract && contract.airlineAlternativeName) ?
                contract.airlineAlternativeName : null,
            luggageIssueMomentId: (contract && contract.luggageIssueMomentId) ?
                contract.luggageIssueMomentId : null,
            luggageIssueMomentName: (contract && contract.luggageIssueMomentName) ?
                contract.luggageIssueMomentName : null,
            luggageDamageDescription: (contract && contract.luggageDamageDescription) ?
                contract.luggageDamageDescription : null,
            luggageReceivedAt: (contract && contract.luggageReceivedAt &&
                contract?.luggageReceivedAt !== '0000-00-00 00:00:00') ? new Date(contract.luggageReceivedAt) : null,
            flightNumber: (contract && contract.flightNumber) ? contract.flightNumber : null,
            clientCompleteName: (contract && contract.clientCompleteName) ? contract.clientCompleteName : null,
            idCard: (contract && contract.idCard) ? contract.idCard : null,
            documentationTypeId: (contract && contract.documentationTypeId) ?
                contract.documentationTypeId : null,
            email: (contract && contract.email) ? contract.email : null,
            languageId: (contract && contract.languageId) ? contract.languageId : null,
            isMinor: (contract && contract.isMinor === '1') ? true : false,
            claimCompetenceCountryId:
                (contract && contract.claimCompetenceCountryId) ? contract.claimCompetenceCountryId : null,
            claimPlacementCountryId:
                (contract && contract.claimPlacementCountryId) ? contract.claimPlacementCountryId : null,
            resolutionWayName: (contract && contract.resolutionWayName) ? contract.resolutionWayName : null,
            claimIsFinished: (contract?.claimIsFinished && contract?.claimIsFinished === '1') ? true : false,
            isCompanion: (contract?.isCompanion && contract?.isCompanion === '1') ? true : false,
            hasCompanion: (contract?.hasCompanion && contract?.hasCompanion === '1') ? true : false,
            isActive: (contract?.isActive && contract?.isActive === '1') ? true : false,
            menuId: this.getMenuId(contract?.isActive)
        };

        return new Claim(model);
    }

    static getClaimId(id: string, claimId: string): string {
        if (id) {
            return id;
        } else if (claimId) {
            return claimId;
        }

        return null;
    }

    static getMenuId(isActive: string): string {
        if (isActive === '1') {
            return ID_MENU_ACTIVE_CLAIMS;
        } else if (isActive === '0') {
            return ID_MENU_CLOSED_CLAIMS;
        }

        return null;
    }
}
