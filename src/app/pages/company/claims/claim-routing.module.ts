import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MENU_ID } from '@app/shared/enums/menu.enum';
import { ScopeGuard } from '@app/shared/guards/scope.guard';
import { ClaimEditorComponent } from './claim-editor/claim-editor.component';
import { ClaimComponent } from './claim.component';

const routes: Routes = [
        {
            path: 'create-claim',
            component: ClaimEditorComponent,
            data: { menuId: MENU_ID['create-claim'] },
            canLoad: [ScopeGuard]
        },

        // active-claims
        {
            path: 'active-claims',
            component: ClaimComponent,
            data: { menuId: MENU_ID['active-claims'] },
            canLoad: [ScopeGuard]
        },
        {
            path: 'active-claims/edit/:id',
            component: ClaimEditorComponent,
            data: { menuId: MENU_ID['active-claims'] },
            canLoad: [ScopeGuard]
        },

        // closed-claims
        {
            path: 'closed-claims',
            component: ClaimComponent,
            data: { menuId: MENU_ID['closed-claims'] },
            canLoad: [ScopeGuard]
        },
        {
            path: 'closed-claims/edit/:id',
            component: ClaimEditorComponent,
            data: { menuId: MENU_ID['closed-claims'] },
            canLoad: [ScopeGuard]
        }
    ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ClaimRoutingModule { }
