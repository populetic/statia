import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data } from '@angular/router';
import { Login } from '@app/pages/login/login.model';
import { SessionService } from '@app/shared/services/session.service';
import { ClaimService } from './claim.service';

@Component({
    selector: 'app-menu',
    template: `<app-main-table
                    [actualMenuId]="menuId"
                    [actualMenuService]="menuService"
                    [actualMenuFilters]="menuFilters"
                    [actualMenuSortActive]="sortActive"
                    [actualMenuSortDirection]="sortDirection">
                </app-main-table>`
})

export class ClaimComponent implements OnInit {

    public menuId: number;
    public menuFilters: Array<any>;
    public sortActive: string;
    public sortDirection: string;

    // TODO: Mejorar esta asignación de filtros fijos a menús, poner bbdd (?)
    // TODO: MenuFilters definir datos en enum ?, puede venir de bbdd
    private availableMenuFilters: any = {
        9: { 'finished': 1, 'closed': 0 },
        10: { 'finished': 1, 'closed': 1 }
    };

    constructor(
        public menuService: ClaimService,
        private activeRoute: ActivatedRoute,
        private sessionService: SessionService
    ) { }

    ngOnInit() {
        this.activeRoute.data.subscribe((data: Data) => this.menuId = data.menuId);

        // TODO: Mejorar esta asignación de filtros fijos a menús, poner bbdd (?)
        const user: Login = this.sessionService.user;
        if (user !== null) {
            this.availableMenuFilters[this.menuId].userId = user.id;

            if (user.lawFirmId !== null && !user.isAdmin) {
                this.availableMenuFilters[this.menuId].lawFirmId = user.lawFirmId;
            }
        }

        this.menuFilters = this.availableMenuFilters[this.menuId];

        /** Default order */
        this.sortActive = 'finishedAt';
        this.sortDirection = 'desc';
    }
}
