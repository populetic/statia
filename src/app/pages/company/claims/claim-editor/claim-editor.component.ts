import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmActionDialogComponent } from '@app/layout/dialogs/confirm-action/confirm-action-dialog.component';
import { ConnectionFlightDialogComponent } from '@app/layout/dialogs/connection-flight-dialog/connection-flight-dialog.component';
import { Flight } from '@app/pages/company/flights/flight.model';
import { FlightService } from '@app/pages/company/flights/flight.service';
import { Login } from '@app/pages/login/login.model';
import { User } from '@app/pages/management/users/user.model';
import { AIRLINE_ALTERNATIVE_ID_REFUND, DISRUPTION_ID_LUGGAGE_DAMAGE, DISRUPTION_ID_LUGGAGE_DELAY, FLIGHT_DATA_ORIGIN_ID_MANUAL_STATIA, FLIGHT_DATA_ORIGIN_MANUAL_IDS, FORM_OPERATIONS, ID_MENU_DEMAND, PAGINATOR, POPULETIC_CREATE_DEMAND, SESSION_STORAGE } from '@app/shared/constants/constants';
import { MODEL_PROPERTY_NAME } from '@app/shared/enums/model-property-name.enum';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { NotificationsService } from '@app/shared/services/notifications.service';
import { SessionService } from '@app/shared/services/session.service';
import { StatiaService } from '@app/shared/services/statia.service';
import { ValidationService } from '@app/shared/services/validation.service';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { ListColumn } from '@fury/shared/list/list-column.model';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { DisruptionTypeService } from '../../auxiliaries/disruption-types/disruption-type.service';
import { ConnectionFlight } from '../../connection-flights/connection-flight.model';
import { ClaimConnectionFlightService } from '../../connection-flights/claim-connection-flight.service';
import { Claim } from '../claim.model';
import { ClaimService } from '../claim.service';

@Component({
    selector: 'app-claim-editor',
    templateUrl: './claim-editor.component.html',
    styleUrls: ['./claim-editor.component.scss'],
    animations: [fadeInRightAnimation, fadeInUpAnimation]
})

export class ClaimEditorComponent implements OnInit, OnDestroy {

    private userLogged: Login;
    public parentRoute: string;
    public formAction: string;
    public currentEditor: string;
    public breadcrumbs: string[];
    public editorFormData: FormGroup;
    public canUseFormOperation: boolean;
    public canCreateDemand = false;
    private subscriptions: Subscription[] = [];

    public datePipe = new DatePipe('en-GB');

    public filteredClaimStatus: Observable<any>;
    private availableUsers: any;
    public filteredUsers: Observable<any>;
    public availableLanguages: any;
    public availableLawsuiteTypes: any;
    public availableCountries: any;
    public availableCountriesCompetence: any;
    public availableCountriesPlacement: any;
    public availableDisruptionTypes: any;
    private availableAirlines: any;
    public filteredAirlines: Observable<any>;
    public availableAirlineReasons: any;
    private availableAirports: any;
    public filteredDepartureAirports: Observable<any>;
    public filteredArrivalAirports: Observable<any>;
    public availableAirlineAlternatives: any;
    public availableLuggageIssueMoments: any;

    private claimId: any;
    public claim: Claim;
    public flight: Flight;
    private connectionFlights: Array<ConnectionFlight>;
    public actualDisruptionSelected: string;
    public showLuggageInfo: boolean;
    public showAirlineCompensation: boolean;
    public luggageDelayId = DISRUPTION_ID_LUGGAGE_DELAY;
    public luggageDamageId = DISRUPTION_ID_LUGGAGE_DAMAGE;

    /** Connection flights table */
    public pageSizeOptions = PAGINATOR.SMALL.OPTIONS;
    public pageSize = PAGINATOR.SMALL.SIZE;


    public columnsConnFlightsTable: any;
    public dataConnFlightsTable: MatTableDataSource<any>;
    @ViewChild('ConnFlightsMatPaginator') connFlightsTablePaginator: MatPaginator;
    @ViewChild('ConnFlightsMatSort') connFlightsSort: MatSort;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private snackbar: MatSnackBar,
        private dialog: MatDialog,
        private formBuilder: FormBuilder,
        private sessionService: SessionService,
        private validationService: ValidationService,
        private statiaService: StatiaService,
        private claimService: ClaimService,
        private flightService: FlightService,
        private connectionFlightsService: ClaimConnectionFlightService,
        private notificationsService: NotificationsService,
        private disruptionTypesService: DisruptionTypeService
    ) {
        this.claim = new Claim();
        this.flight = new Flight();

        this.columnsConnFlightsTable = [] as ListColumn[];
        this.dataConnFlightsTable = new MatTableDataSource<any>();
    }

    ngOnInit() {
        this.userLogged = this.sessionService.user;

        this.route.paramMap.subscribe(paramMap => { this.claimId = paramMap.get('id'); });

        this.formAction = (this.claimId === null ? 'create' : 'edit');
        const routeToReplace =
            (this.claimId === null ? '/' + this.formAction : '/' + this.formAction + '/' + this.claimId);
        this.parentRoute = this.router.url.replace(routeToReplace, '');

        const routeItems = this.parentRoute.split('/').filter(item => item !== null && item !== '');
        this.breadcrumbs = routeItems;

        if (this.claimId !== null) {
            this.breadcrumbs.push('edit');
        } else {
            this.currentEditor = 'create';
        }

        this.createFormGroup();
        this.createConnFlightsTable();
        this.configureForm();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
    }

    private createFormGroup() {
        const claimForm: any = {
            claimStatusName: [null],
            amountEstimated: [null],
            amountReceived: [null],
            lawsuiteTypeId: [null, Validators.required],
            claimCompetenceCountryId: [null],
            claimPlacementCountryId: [null],
            username: [null],
            languageId: [null],
            disruptionTypeId: [null],
            reservationNumber: [null, Validators.required],
            airlineNameAndIata: [null],
            airlineReasonId: [null, Validators.required],
            description: [null],
            airlineAlternativeId: [null, Validators.required],
            luggageReceivedAt: [null],
            amountInitialCompensation: [null],
            luggageIssueMomentId: [null],
            luggageDamageDescription: [null],
            /** Flight data */
            flightNumber: [null, Validators.required],
            flightDate: [null, Validators.required],
            airportDepartureNameAndIata: [null, Validators.required],
            airportArrivalNameAndIata: [null, Validators.required],
            flightDistance: [null],
            departureScheduledTime: [null, Validators.required],
            departureActualTime: [null, Validators.required],
            arrivalScheduledTime: [null, Validators.required],
            arrivalActualTime: [null],
            /** Claim data required to send request */
            ref: [null],
            clientId: [null],
            flightId: [null],
            userId: [null],
            claimStatusId: [null],
            claimDocumentationStatusId: [null],
            isOnboardingFlight: [null],
            amountTickets: [null],
            fee: [null],
            hasConnectionFlight: [null],
            delayOptionId: [null],
            mainClaimId: [null],
            isRemarketingEnabled: [null],
            isFirstRemarketingSent: [null],
            isClaimable: [null],
            isExtrajudicialAirlineNegative: [null],
            resolutionWayId: [null],
            lawFirmId: [null],
            finishedAt: [null],
            extrajudicialSentAt: [null]
        };

        this.editorFormData = this.formBuilder.group(claimForm);
    }

    private async configureForm() {
        this.availableUsers = await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.USER, false);
        this.availableAirlines = await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.AIRLINE, false);
        this.availableAirlineReasons = await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.AIRLINE_REASON);
        this.availableAirports = await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.AIRPORT, false);
        this.availableLanguages =
            await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.LANGUAGE_SELECTOR, false, true);
        this.availableLawsuiteTypes =
            await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.LAW_SUITE_TYPE);
        this.availableDisruptionTypes =
            await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.DISRUPTION_TYPE, false, true);
        this.availableAirlineAlternatives =
            await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.AIRLINE_ALTERNATIVE);
        this.availableLuggageIssueMoments =
            await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.LUGGAGE_ISSUE_MOMENT);

        /** Available users to be selected */
        if ( !this.userLogged.isAdmin ) {
            this.availableUsers = this.availableUsers.filter((user: User) => user.id === this.userLogged.id);
        } else {
            this.availableUsers = this.availableUsers.filter((user: User) => !user.isSystem);
        }
        this.filteredUsers = this.editorFormData.controls['username'].valueChanges.pipe(
            map(value => (value ?
                this.statiaService._filter(value, this.availableUsers, ['username']) :
                this.availableUsers.slice()))
        );
        this.editorFormData.controls['username'].setValidators(
            [this.validationService.validateFieldValue(this.availableUsers, 'username')]
        );

        this.filteredAirlines = this.editorFormData.controls['airlineNameAndIata'].valueChanges.pipe(
            map(value => (value ?
                this.statiaService._filter(value, this.availableAirlines, ['nameAndIata']) :
                this.availableAirlines.slice()))
        );
        this.editorFormData.controls['airlineNameAndIata'].setValidators(
            [Validators.required, this.validationService.validateFieldValue(this.availableAirlines, 'nameAndIata')]
        );

        this.filteredDepartureAirports = this.editorFormData.controls['airportDepartureNameAndIata'].valueChanges.pipe(
            map(value => (value ?
                this.statiaService._filter(value, this.availableAirports, ['nameAndIata']) :
                this.availableAirports.slice()))
        );
        this.editorFormData.controls['airportDepartureNameAndIata'].setValidators(
            [Validators.required, this.validationService.validateFieldValue(this.availableAirports, 'nameAndIata')]
        );

        this.filteredArrivalAirports = this.editorFormData.controls['airportArrivalNameAndIata'].valueChanges.pipe(
            map(value => (value ?
                this.statiaService._filter(value, this.availableAirports, ['nameAndIata']) :
                this.availableAirports.slice()))
        );
        this.editorFormData.controls['airportArrivalNameAndIata'].setValidators(
            [Validators.required, this.validationService.validateFieldValue(this.availableAirports, 'nameAndIata')]
        );

        /**
         * Recogemos competencia y emplazamiento para esta claim; si no existe mostramos listado de países para escoger
         * TODO: a futuro siempre habrá una competencia o emplazamiento disponibles; sino significa que realmente no
         * se puede gestionar esta reclamación --> NO VIABLE (?)
         * */
        await new Promise((resolve) => {
            this.claimService.getAllClaimCompetenceAsync(this.claimId).then((result: Result<any[]>) => {
                this.availableCountriesCompetence =
                    result?.response?.filter((competence: any) => competence.isSelectable === '1');
                resolve();
            });
        });

        await new Promise((resolve) => {
            this.claimService.getAllClaimPlacementAsync(this.claimId).then((result: Result<any[]>) => {
                this.availableCountriesPlacement =
                    result?.response?.filter((placement: any) => placement.isSelectable === '1');
                resolve();
            });
        });

        if (this.availableCountriesCompetence?.length === 0 || this.availableCountriesPlacement?.length === 0) {
            const allCountries = await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.COUNTRY, false);
            this.availableCountries = this.statiaService.sortByField(
                allCountries.filter(country => country.competencePriority > 0),
                'competencePriority'
            );

            const formattedCountriesObject = this.statiaService.formatData(this.availableCountries, 'id', 'name');

            if (this.availableCountriesCompetence.length === 0) {
                this.availableCountriesCompetence = formattedCountriesObject;
            }

            if (this.availableCountriesPlacement.length === 0) {
                this.availableCountriesPlacement = formattedCountriesObject;
            }
        }

        this.editorFormData.controls['disruptionTypeId'].valueChanges.subscribe(value => {
            this.actualDisruptionSelected = value;
            this.controlLuggageFields(value);
        });

        this.editorFormData.controls['airlineAlternativeId'].valueChanges.subscribe(value => {
            this.controlAirlineAlternativeFields(value);
        });

        if (this.formAction === 'edit') {
            this.getFormData();
        }
    }

    getFormData() {
        const subs: Subscription = this.claimService.getById(this.claimId).subscribe(
            (result: Result<Claim>) => {
                this.claim = result.response;

                this.setClaimDataInForm();
                this.getFlightData(this.claim.flightId);
                this.getConnectionFlightsData(this.claimId);

                this.canUseFormOperation = this.claimService.canEditClaim(this.claim);
                this.canCreateDemand = StatiaFunctions.EnabledOption(ID_MENU_DEMAND, FORM_OPERATIONS.CREATE);
            }
        );

        this.subscriptions.push(subs);
    }

    setClaimDataInForm(): any {
        this.currentEditor = '#' + this.claim.ref + ' - ' + this.claim.clientCompleteName;
        this.editorFormData.patchValue(this.claim);
    }

    setFlightDataInForm(): any {
        this.editorFormData.patchValue(this.flight);
        this.editorFormData.patchValue({ flightDate: this.flight.date });
        this.editorFormData.patchValue({ flightNumber: this.flight.number });
        this.editorFormData.patchValue({ flightDistance: this.flight.distance });
    }

    getFlightData(claimId: string) {
        this.flightService.getById(claimId).subscribe(
            (flightResult: Result<Flight>) => {
                this.flight = flightResult.response;
                this.setFlightDataInForm();
            }
        );
    }

    getConnectionFlightsData(claimId: string) {
        this.claimService.getAllClaimConnectionFlights(claimId).subscribe(
            (result: Result<any>) => {
                this.connectionFlights = result.response;
                this.dataConnFlightsTable = new MatTableDataSource(this.connectionFlights);
                this.claim.hasConnectionFlight = (this.connectionFlights.length > 0);
            }
        );
    }

    async onSubmit() {
        if (this.editorFormData.invalid) {
            return;
        }

        let isFlightModified: boolean;
        let param: any;
        param = this.editorFormData.value;

        /** Formatted claim data */
        param.finishedAt = this.statiaService.formatDateTimeToBD(this.claim.finishedAt);
        if (this.claim.extrajudicialSentAt !== null) {
            param.extrajudicialSentAt = this.statiaService.formatDateTimeToBD(this.claim.extrajudicialSentAt);
        }

        /** Formatted flight data */
        const selectedAirline = this.statiaService
            .filterByField(this.availableAirlines, 'nameAndIata', param.airlineNameAndIata);
        param.airlineId = selectedAirline[0].id;

        const selectedDepAirport = this.statiaService
            .filterByField(this.availableAirports, 'nameAndIata', param.airportDepartureNameAndIata);
        param.airportDepartureId = selectedDepAirport[0].id;

        const selectedArrAirport = this.statiaService
            .filterByField(this.availableAirports, 'nameAndIata', param.airportArrivalNameAndIata);
        param.airportArrivalId = selectedArrAirport[0].id;

        param.flightDate = (param && param.flightDate) ?
            this.datePipe.transform(param.flightDate, 'yyyy-MM-dd') : null;

        if (this.formAction === 'create') {
            // TODO: Implementar cuando se desarrolle formulario para creación;
            return;
        } else {
            /** Flight information */
            isFlightModified = this.isFlightDataModified(param);
            if (isFlightModified) {
                let arrivalActualTime = new Date(param.arrivalScheduledTime);

                if (param.arrivalActualTime !== null) {
                    const arrivalScheduledTime = new Date(param.arrivalScheduledTime);
                    arrivalActualTime = new Date(param.arrivalActualTime);

                    param.delay = this.flightService.calculateDelay(arrivalScheduledTime, arrivalActualTime);
                } else {
                    param.delay = null;
                }

                await this.editClaimFlight(param);
            }

            /** Compensation */
            const connectionFlights = (param.hasConnectionFlight ? this.connectionFlights : null);
            await this.claimService.calculateEstimatedCompensation(param, this.flight, connectionFlights)
                .then(function (value) { param.amountEstimated = value; });

            /** Additional claim information */
            param.additionalInformation = {
                amountInitialCompensation: param.amountInitialCompensation,
                airlineAlternativeId: param.airlineAlternativeId,
                luggageReceivedAt: param.luggageReceivedAt,
                luggageIssueMomentId: param.luggageIssueMomentId,
                luggageDamageDescription: param.luggageDamageDescription
            };

            return;

            if (this.claim.amountEstimated !== param.amountEstimated) {
                this.openConfirmEditDialog('titleUpdateClaimDialog',
                    ['msgNewCompensation.1 ' + param.amountEstimated + 'msgNewCompensation.2'],
                    param);
            } else {
                this.editClaim(this.claimId, param);
            }
        }
    }

    isFlightDataModified(formValues: any) {
        if ((this.flight.airlineId !== formValues.airlineId) ||
            (this.flight.number !== formValues.flightNumber) ||
            (this.flight.date !== formValues.flightDate) ||
            (this.flight.airportArrivalId !== formValues.airportArrivalId) ||
            (this.flight.airportDepartureId !== formValues.airportDepartureId) ||
            (this.flight.departureScheduledTime !== formValues.departureScheduledTime) ||
            (this.flight.departureActualTime !== formValues.departureActualTime) ||
            (this.flight.arrivalScheduledTime !== formValues.arrivalScheduledTime) ||
            (this.flight.arrivalActualTime !== formValues.arrivalActualTime)) {
            return true;
        }

        return false;
    }

    async editClaimFlight(formValues: any) {
        const flightData = { ...this.flight };
        flightData.airlineId = formValues.airlineId;
        flightData.number = formValues.flightNumber;
        flightData.date = formValues.flightDate;
        flightData.airportDepartureId = formValues.airportDepartureId;
        flightData.airportArrivalId = formValues.airportArrivalId;
        flightData.delay = formValues.delay;

        const departureScheduledTime = this.statiaService.formatDate(formValues.departureScheduledTime, true);
        flightData.departureScheduledTime = this.statiaService.formatDateTimeToBD(departureScheduledTime);

        const departureActualTime = this.statiaService.formatDate(formValues.departureActualTime, true);
        flightData.departureActualTime = this.statiaService.formatDateTimeToBD(departureActualTime);

        const arrivalScheduledTime = this.statiaService.formatDate(formValues.arrivalScheduledTime, true);
        flightData.arrivalScheduledTime = this.statiaService.formatDateTimeToBD(arrivalScheduledTime);

        if (formValues.arrivalActualTime !== null) {
            const arrivalActualTime = this.statiaService.formatDate(formValues.arrivalActualTime, true);
            flightData.arrivalActualTime = this.statiaService.formatDateTimeToBD(arrivalActualTime);
        } else {
            flightData.arrivalActualTime = formValues.arrivalActualTime;
        }

        if (FLIGHT_DATA_ORIGIN_MANUAL_IDS.includes(this.flight.flightDataOriginId)) {
            // edit manual flight
            await new Promise((resolve) => {
                this.flightService.editAsync(this.flight.id, flightData).then(
                    (result: Result<Flight>) => {
                        if (result.status) {
                            this.flight = result.response;
                        }
                        resolve();
                    }
                );
            });
        } else {
            // new flight
            flightData.flightDataOriginId = FLIGHT_DATA_ORIGIN_ID_MANUAL_STATIA;
            flightData.claimId = this.claimId;

            await new Promise((resolve) => {
                this.flightService.createAsync(flightData).then(
                    (result: Result<Flight>) => {
                        if (result.status) {
                            this.flight = result.response;
                        }
                        resolve();
                    }
                );
            });
        }

        this.setFlightDataInForm();
    }

    editClaim(claimId: string, param: any) {
        let claimSubs: Subscription;

        claimSubs = this.claimService.edit(claimId, param).subscribe(
            (result: Result<Claim>) => {
                if (result.status) {
                    this.notificationsService.manageServiceResult(result, null, null, this.snackbar, this.dialog);
                    this.getFormData();
                }
            }
        );

        this.subscriptions.push(claimSubs);
    }

    /** Dynamic inputs */
    controlLuggageFields(disruptionId: string) {
        const isLuggageSelected = this.disruptionTypesService.isLuggageDisruption(disruptionId);
        this.showLuggageInfo = isLuggageSelected;

        if (isLuggageSelected) {
            if (disruptionId === DISRUPTION_ID_LUGGAGE_DELAY) {
                this.editorFormData.controls['luggageReceivedAt'].setValidators([Validators.required]);
            } else {
                this.editorFormData.controls['luggageReceivedAt'].setValidators([]);
            }

            this.editorFormData.controls['luggageIssueMomentId'].setValidators([Validators.required]);
        } else {
            this.editorFormData.controls['luggageReceivedAt'].setValidators([]);
            this.editorFormData.controls['luggageIssueMomentId'].setValidators([]);
        }
    }

    controlAirlineAlternativeFields(alternativeId: string) {
        if (alternativeId === AIRLINE_ALTERNATIVE_ID_REFUND) {
            this.showAirlineCompensation = true;
            this.editorFormData.controls['amountInitialCompensation'].setValidators([Validators.required]);
        } else {
            this.showAirlineCompensation = false;
            this.editorFormData.controls['amountInitialCompensation'].setValidators([]);
        }
    }

    clearInput(input: string) {
        this.editorFormData.controls[input].reset();
    }

    /** Confirm dialog */
    private openConfirmEditDialog(title: string, messages: Array<String>, formValues: any) {
        const dialogData: any = {
            title: title,
            messages: messages,
        };
        const propertyDialog = this.dialog.open(ConfirmActionDialogComponent, {
            width: '375px',
            data: dialogData
        });

        propertyDialog.afterClosed().subscribe(action => {
            if (action) {
                this.editClaim(this.claimId, formValues);
            }
        });
    }

    /** Table & Dialog - connection flights */
    private createConnFlightsTable() {
        const optionsColumns: Array<any> = [
            { name: 'airportDepartureNameAndIata' },
            { name: 'airportArrivalNameAndIata' },
            { name: 'distance', isNumber: true },
            { name: 'arrivalScheduledTime', isDate: true },
            { name: 'arrivalActualTime', isDate: true },
            { name: 'actions' }
        ];

        this.columnsConnFlightsTable = optionsColumns as ListColumn[];

        this.dataConnFlightsTable.paginator = this.connFlightsTablePaginator;
        this.dataConnFlightsTable.sort = this.connFlightsSort;
    }

    get visibleColumnsConnFlightsTable() {
        return this.columnsConnFlightsTable.map((column: { name: any; }) => column.name);
    }

    openAddConnectionFlight(): void {
        const dialogData: any = {
            claimId: this.claimId,
            action: 'create',
            title: 'titleNewConnectionFlight',
            connectionFlight: {
                order: 0,
                airportDepartureNameAndIata: null,
                airportArrivalNameAndIata: null,
                arrivalScheduledTime: null,
                arrivalActualTime: null
            }
        };

        this.openConnectionFlightDialog(ConnectionFlightDialogComponent, dialogData);
    }

    openEditConnectionFlight(connectionFlight: ConnectionFlight, $event: Event): void {
        $event.preventDefault();

        const dialogData: any = {
            claimId: this.claimId,
            action: 'edit',
            title: 'titleEditConnectionFlight',
            connectionFlight: connectionFlight
        };
        this.openConnectionFlightDialog(ConnectionFlightDialogComponent, dialogData);
    }

    private openConnectionFlightDialog(DialogComponent: any, dialogData: ConnectionFlight) {
        const propertyDialog = this.dialog.open(DialogComponent, {
            width: '475px',
            data: dialogData
        });

        propertyDialog.afterClosed().subscribe(saved => {
            if (saved) {
                this.getConnectionFlightsData(this.claimId);
            }
        });
    }

    deleteConnectionFlight(connectionFlightId: string | number) {
        let sub: Subscription = this.claimService.deleteClaimConnectionFlight(this.claimId, connectionFlightId)
            .subscribe(
                result => {
                    this.notificationsService.manageServiceResult(result, null, null, this.snackbar, this.dialog);
                }
            );
        this.subscriptions.push(sub);

        const param = { connectionFlightId };
        sub = this.connectionFlightsService.deleteByParam(param).subscribe(
            result => {
                this.getConnectionFlightsData(this.claimId);
                this.notificationsService.manageServiceResult(result, null, null, this.snackbar, this.dialog);
            }
        );
        this.subscriptions.push(sub);
    }

    generateDemand() {
        if (this.canCreateDemand) {
            const params: CustomParams = { claimId: this.claimId };
            this.sessionService.setItem(POPULETIC_CREATE_DEMAND, JSON.stringify(params), SESSION_STORAGE);
            this.router.navigate(['/legal/demands/create']);
        }
    }
}
