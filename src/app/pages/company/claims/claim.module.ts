import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MainTableModule } from '@app/layout/main-table/main-table.module';
import { SharedModule } from '@app/shared/modules/shared.module';
import { FurySharedModule } from '@fury/fury-shared.module';
import { BreadcrumbsModule } from '@fury/shared/breadcrumbs/breadcrumbs.module';
import { MaterialModule } from '@fury/shared/material-components.module';
import { ClaimEditorComponent } from './claim-editor/claim-editor.component';
import { ClaimComponent } from './claim.component';
import { FormComponentsModule } from '@app/layout/form-components/form-components.module';
import { TranslateModule } from '@ngx-translate/core';
import { ClaimRoutingModule } from './claim-routing.module';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        BreadcrumbsModule,
        MaterialModule,
        FurySharedModule,
        SharedModule,

        ClaimRoutingModule,
        MainTableModule,
        FormComponentsModule,
        TranslateModule
    ],
    declarations: [ClaimComponent, ClaimEditorComponent],
    exports: [ClaimComponent]
})

export class ClaimModule {
}
