import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ClaimContract } from '@app/api/contracts/claim-contract/claim.contract';
import { ApiClaimService } from '@app/api/services/api-claim-service/api-claim.service';
import { Login } from '@app/pages/login/login.model';
import { DISRUPTION_ID_LUGGAGE_DELAY, FORM_OPERATIONS, ID_MENU_ACTIVE_CLAIMS, RELATED_ENTITIES } from '@app/shared/constants/constants';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { SessionService } from '@app/shared/services/session.service';
import { DateFunctions } from '@app/utils/date.functions';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { CompanionContract } from '../../../api/contracts/companion-contract/companion.contract';
import { Companion } from '../companions/companion.model';
import { CompanionTranslator } from '../companions/companion.translator';
import { ConnectionFlight } from '../connection-flights/connection-flight.model';
import { ClaimConnectionFlightService } from '../connection-flights/claim-connection-flight.service';
import { Flight } from '../flights/flight.model';
import { Claim } from './claim.model';
import { ClaimTranslator } from './claim.translator';

@Injectable({ providedIn: 'root' })
export class ClaimService implements BaseInterface<Claim> {
    public resourceUrl: string = environment.apiUrl + '/claims';
    public headers: HttpHeaders;

    constructor(
        private http: HttpClient,
        private apiClaimService: ApiClaimService,
        private claimConnectionFlightService: ClaimConnectionFlightService,
        private sessionService: SessionService
    ) {}

    getAll(customParams: CustomParams = null): Observable<Result<Claim[]>> {
        return this.apiClaimService.getAll(customParams)
            .pipe(
                map((result: Result<ClaimContract[]>) => {
                    const newResult: Result<Claim[]> = StatiaFunctions.getResultModel<ClaimContract[], Claim[]>(result);
                    newResult.response = result.response.map((claim: ClaimContract) => ClaimTranslator.translateContractToModel(claim));
                    return  newResult;
                })
            );
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<Claim[]>> {
        return new Promise((resolve, reject) => {
            this.apiClaimService.getAllAsync(customParams)
                .then((result: Result<ClaimContract[]>) => {
                    const newResult: Result<Claim[]> = StatiaFunctions.getResultModel<ClaimContract[], Claim[]>(result);
                    newResult.response = result.response.map((claim: ClaimContract) => ClaimTranslator.translateContractToModel(claim));
                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    getById(id: number|string): Observable<Result<Claim>> {
        return this.apiClaimService.getById(id).pipe(
            map((result: Result<ClaimContract>) => {
                const newResult: Result<Claim> = StatiaFunctions.getResultModel<ClaimContract, Claim>(result);
                const claim: Claim = ClaimTranslator.translateContractToModel(result.response);
                newResult.response = claim;

                return  newResult;
            })
        );
    }

    getAllByEmailAndStatus(param: CustomParams = null): Observable<Result<Claim[]>> {
        return this.apiClaimService.getAllByEmailAndStatus(param).pipe(
            map((result: Result<ClaimContract[]>) => {
                const newResult: Result<Claim[]> = StatiaFunctions.getResultModel<ClaimContract[], Claim[]>(result);
                newResult.response = result.response.map((claim: ClaimContract) => ClaimTranslator.translateContractToModel(claim));
                return  newResult;
            })
        );
    }

    create(param: Claim): Observable<Result<Claim>> {
        // TODO: apiClaimService.create()
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: Claim): Promise<Result<Claim>> {
        // TODO: apiClaimService.createAsync()
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: Claim): Observable<Result<Claim>> {
        // TODO: apiClaimService.edit()
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: Claim): Promise<Result<Claim>> {
        // TODO: apiClaimService.editAsync()
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: CustomParams): Observable<Result<any>> {
        return this.apiClaimService.deleteByParam(param);
    }

    getAllClaimCompetenceAsync(claimId: any): Promise<any> {
        // TODO: ClaimCompetenceService
        return this.http.get(this.resourceUrl + '-competence/' + claimId).toPromise();
    }

    getAllClaimPlacementAsync(claimId: any): Promise<any> {
        // TODO: ClaimCompetenceService
        return this.http.get(this.resourceUrl + '-placement/' + claimId).toPromise();
    }

    getAllClaimConnectionFlights(claimId: any): Observable<any> {
        const param: CustomParams = { claimId };
        return this.claimConnectionFlightService.getAll(param);
    }

    addClaimConnectionFlightsAsync(claimId: any, param: any): Promise<any> {
        // TODO: ClaimConnectionFlightsService
        return this.claimConnectionFlightService.addClaimConnectionFlightsAsync(claimId, param);
    }

    deleteClaimConnectionFlight(claimId: any, connFlightId: any): Observable<any> {
        // TODO: ClaimConnectionFlightsService
        const completeUrl = environment.apiUrl + '/claim-connection-flights/delete/' + claimId + '/' + connFlightId;
        return this.http.delete(completeUrl);
    }

    calculateEstimatedCompensationAsync(param: any): Promise<any> {
        // TODO: ClaimConnectionFlightsService
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);

        return this.http.post(this.resourceUrl + '/calculateEstimatedCompensation', params).toPromise();
    }

    getAllPossibleSameFlightTravelers(claimId: string): Observable<Result<Companion[]>> {
        return this.apiClaimService.getAllPossibleSameFlightTravelers(claimId)
            .pipe(
                map((result: Result<CompanionContract[]>) => {
                    const newResult: Result<Companion[]> = StatiaFunctions.getResultModel<CompanionContract[], Companion[]>(result);
                    newResult.response = result.response.map((companion: CompanionContract) =>
                        CompanionTranslator.translateContractCompanionToModel(companion));
                    return  newResult;
                })
            );
    }

    getAllCompanionsByMainClaimId(claimId: string): Observable<Result<Companion[]>> {
        return this.apiClaimService.getAllCompanionsByMainClaimId(claimId)
            .pipe(
                map((result: Result<CompanionContract[]>) => {
                    const newResult: Result<Companion[]> = StatiaFunctions.getResultModel<CompanionContract[], Companion[]>(result);
                    newResult.response = result.response.map((claim: CompanionContract) =>
                        CompanionTranslator.translateContractCompanionToModel(claim));
                    return  newResult;
                })
            );
    }

    async calculateEstimatedCompensation(claim: Claim, flight: Flight, connectionFlights: ConnectionFlight[]) {
        let estimatedCompensation: number;
        let daysLuggageDelay = 0;
        let distance = 0;

        if (claim.disruptionTypeId === DISRUPTION_ID_LUGGAGE_DELAY) {
            daysLuggageDelay = DateFunctions.CalculateDaysDiff(new Date(flight.date), claim.luggageReceivedAt);
        }

        if (connectionFlights !== null) {
            connectionFlights.forEach((connectionFlight: ConnectionFlight) => {
                distance += Number(connectionFlight.distance);
            });
        } else {
            distance = flight.distance;
        }

        const param = {
            disruptionTypeId: claim.disruptionTypeId,
            distance: distance,
            delay: flight.delay,
            luggageDelay: daysLuggageDelay
        };

        await new Promise((resolve) => {
            this.calculateEstimatedCompensationAsync(param).then(
                (result: Result<any>) => {
                    if (result.status) {
                        estimatedCompensation = result.response.amountEstimated;
                    }
                    resolve();
                }
            );
        });

        return estimatedCompensation;
    }

    canEditClaim(claim: Claim): boolean {
        let canEdit: boolean = StatiaFunctions.EnabledOption(ID_MENU_ACTIVE_CLAIMS, FORM_OPERATIONS.EDIT);
        const user: Login = this.sessionService.user;

        if (claim.lawFirmId !== null && user.lawFirmId !== claim.lawFirmId && !user.isAdmin) {
            canEdit = false;
        }

        return canEdit;
    }

    getRelatedEntity(currentTab: string, claimId: string): Promise<Companion[]> {
        return new Promise((resolve) => {
            const dataByTab: any = this.sessionService.getDataByTab(currentTab);
            if (!dataByTab) {
                const sub: Subscription = this.getRelatedEntityFromAPI(currentTab, claimId)
                    .subscribe((result: Result<Companion[]>) => {
                        const companions: Companion[] = this.checksCompanions(result.response);
                        this.sessionService.setDataByTab(currentTab, companions);
                        sub.unsubscribe();
                        resolve(companions);
                    });
            } else {
                resolve(dataByTab);
            }
        });
    }

    getRelatedEntityFromAPI(currentTab: string, claimId: string): Observable<Result<Companion[]>> {
        switch (currentTab) {
            case RELATED_ENTITIES.DEMAND.COMPANIONS:
                return this.getAllCompanionsByMainClaimId(claimId);
            case RELATED_ENTITIES.DEMAND.POSSIBLE_COMPANIONS:
                return this.getAllPossibleSameFlightTravelers(claimId);
        }
    }

    private checksCompanions(companions: Companion[]): Companion[] {
        return companions.filter((companion: Companion) => companion.isClaimant === true);
    }
}
