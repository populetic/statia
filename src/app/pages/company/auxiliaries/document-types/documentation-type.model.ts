export class DocumentationType {
    id: string;
    name: string;

    constructor(model?: DocumentationType) {
        this.id = (model && model.id) ? model.id : null;
        this.name = (model && model.name) ? model.name : null;
    }
}
