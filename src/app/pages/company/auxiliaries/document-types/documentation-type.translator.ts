import { DocumentationTypeContract } from './documentation-type.contract';
import { DocumentationType } from './documentation-type.model';

export class DocumentationTypeTranslator {
    static translateContractToModel(contract?: DocumentationTypeContract): DocumentationType {
        const model: DocumentationType = {
            id: (contract && contract.id) ? contract.id : null,
            name: (contract && contract.name) ? contract.name : null,
        };
        return new DocumentationType(model);
    }
}
