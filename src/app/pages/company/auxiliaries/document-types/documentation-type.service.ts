import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiDocumentationTypeService } from '@app/api/services/api-documentation-type-service/api-documentation-type.service';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DocumentationTypeContract } from './documentation-type.contract';
import { DocumentationType } from './documentation-type.model';
import { DocumentationTypeTranslator } from './documentation-type.translator';

@Injectable({ providedIn: 'root' })
export class DocumentationTypeService implements BaseInterface<DocumentationType> {
    public resourceUrl: string = environment.apiUrl + '/documentation-types';
    public headers: HttpHeaders;
    private translator = DocumentationTypeTranslator;

    constructor(
        private http: HttpClient,
        private apiDocumentationTypeService: ApiDocumentationTypeService
    ) {}

    getAll(param: CustomParams = null): Observable<Result<DocumentationType[]>> {
        return this.apiDocumentationTypeService.getAll(param)
            .pipe(
                map((result: Result<DocumentationTypeContract[]>) =>
                    StatiaFunctions.buildResultArrayModel<DocumentationTypeContract, DocumentationType>(result, this.translator)
                )
            );
    }

    getAllAsync(param: CustomParams = null): Promise<Result<DocumentationType[]>> {
        return new Promise((resolve, reject) => {
            this.apiDocumentationTypeService.getAllAsync(param)
                .then((result: Result<DocumentationTypeContract[]>) =>
                    resolve(StatiaFunctions.buildResultArrayModel(result, this.translator))
                )
                .catch((err) => reject(err));
        });
    }

    getById(id: number|string): Observable<Result<DocumentationType>> {
        return this.apiDocumentationTypeService.getById(id)
            .pipe(
                map((result: Result<DocumentationTypeContract>) => StatiaFunctions.buildResultModel(result, this.translator))
            );
    }

    create(param: DocumentationType): Observable<Result<DocumentationType>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: DocumentationType): Promise<Result<DocumentationType>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: DocumentationType): Observable<Result<DocumentationType>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: DocumentationType): Promise<Result<DocumentationType>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<DocumentationType>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const DocumentationTypes = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', DocumentationTypes);
    }

    public get userMainDocumentationTypes(): DocumentationType[] {
        return [
            new DocumentationType({id: '1', name: 'id_card'}),
            new DocumentationType({id: '2', name: 'passport'}),
            new DocumentationType({id: '14', name: 'tax_identification'})
        ];
    }

}
