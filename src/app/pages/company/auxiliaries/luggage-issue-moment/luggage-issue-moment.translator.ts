import { LuggageIssueMomentContract } from '../../../../api/contracts/luggage-issue-moment-contract/luggage-issue-moment.contract';
import { LuggageIssueMoment } from './luggage-issue-moment.model';

export class LuggageIssueMomentTranslator {
    static translateContractToModel(contract?: LuggageIssueMomentContract): LuggageIssueMoment {
        const model: LuggageIssueMoment = {
            id: (contract && contract.id) ? contract.id : null,
            name: (contract && contract.name) ? contract.name : null,
        };
        return new LuggageIssueMoment(model);
    }
}
