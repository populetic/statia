import { Injectable } from '@angular/core';
import { LuggageIssueMomentContract } from '@app/api/contracts/luggage-issue-moment-contract/luggage-issue-moment.contract';
import { ApiLuggageIssueMomentService } from '@app/api/services/api-luggage-issue-moment-service/api-luggage-issue-moment.service';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { LuggageIssueMoment } from './luggage-issue-moment.model';
import { LuggageIssueMomentTranslator } from './luggage-issue-moment.translator';

@Injectable({ providedIn: 'root' })
export class LuggageIssueMomentService implements BaseInterface<LuggageIssueMoment> {
    private translator = LuggageIssueMomentTranslator;

    constructor(
        private apiLuggageIssueMomentService: ApiLuggageIssueMomentService
    ) {}

    getAll(param: CustomParams = null): Observable<Result<LuggageIssueMoment[]>> {
        return this.apiLuggageIssueMomentService.getAll(param)
            .pipe(
                map((result: Result<LuggageIssueMomentContract[]>) =>
                    StatiaFunctions.buildResultArrayModel<LuggageIssueMomentContract, LuggageIssueMoment>(result, this.translator)
                )
            );
    }

    getAllAsync(param: CustomParams = null): Promise<Result<LuggageIssueMoment[]>> {
        return new Promise((resolve, reject) => {
            this.apiLuggageIssueMomentService.getAllAsync(param)
                .then((result: Result<LuggageIssueMomentContract[]>) =>
                    resolve(StatiaFunctions.buildResultArrayModel<LuggageIssueMomentContract, LuggageIssueMoment>(result, this.translator))
                )
                .catch((err) => reject(err));
        });
    }

    getById(id: number|string): Observable<Result<LuggageIssueMoment>> {
        return this.apiLuggageIssueMomentService.getById(id)
            .pipe(
                map((result: Result<LuggageIssueMomentContract>) =>
                    StatiaFunctions.buildResultModel<LuggageIssueMomentContract, LuggageIssueMoment>(result, this.translator)
                )
            );
    }

    create(model: LuggageIssueMoment): Observable<Result<LuggageIssueMoment>> {
        // TODO: return this.apiLuggageIssueMomentService.create()
        return null;
    }

    createAsync(model: LuggageIssueMoment): Promise<Result<LuggageIssueMoment>> {
        // TODO: return this.apiLuggageIssueMomentService.createAsync()
        return null;
    }

    edit(id: number|string, model: LuggageIssueMoment): Observable<Result<LuggageIssueMoment>> {
        // TODO: return this.apiLuggageIssueMomentService.edit()
        return null;
    }

    editAsync(id: number|string, model: LuggageIssueMoment): Promise<Result<LuggageIssueMoment>> {
        // TODO: return this.apiLuggageIssueMomentService.editAsync()
        return null;
    }

    deleteByParam(param: any): Observable<Result<LuggageIssueMoment>> {
        // TODO: return this.apiLuggageIssueMomentService.deleteByParam()
        return null;
    }
}
