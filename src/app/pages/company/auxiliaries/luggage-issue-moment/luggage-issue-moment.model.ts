export class LuggageIssueMoment {
    id: string;
    name: string;

    constructor(model?: LuggageIssueMoment) {
        this.id = (model && model.id) ? model.id : null;
        this.name = (model && model.name) ? model.name : null;
    }
}
