import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AirlineReasonContract } from '@app/api/contracts/airline-reason-contract/airline-reason.contract';
import { ApiAirlineReasonService } from '@app/api/services/api-airline-reason-service/api-airline-reason.service';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AirlineReason } from './airline-reason.model';
import { AirlineReasonTranslator } from './airline-reason.translator';

@Injectable({ providedIn: 'root' })
export class AirlineReasonService implements BaseInterface<AirlineReason> {
    public resourceUrl: string = environment.apiUrl + '/airline-reasons';
    public headers: HttpHeaders;
    private translator = AirlineReasonTranslator;

    constructor(
        private http: HttpClient,
        private apiAirlineReasonService: ApiAirlineReasonService
    ) {}

    getAll(param: CustomParams = null): Observable<Result<AirlineReason[]>> {
        return this.apiAirlineReasonService.getAll(param)
            .pipe(
                map((result: Result<AirlineReasonContract[]>) =>
                    StatiaFunctions.buildResultArrayModel<AirlineReasonContract, AirlineReason>(result, this.translator)
                )
            );
    }

    getAllAsync(param: CustomParams = null): Promise<Result<AirlineReason[]>> {
        return new Promise((resolve, reject) => {
            this.apiAirlineReasonService.getAllAsync(param)
                .then((result: Result<AirlineReasonContract[]>) =>
                    resolve(StatiaFunctions.buildResultArrayModel(result, this.translator))
                )
                .catch((err) => reject(err));
        });
    }

    getById(id: number|string): Observable<Result<AirlineReason>> {
        return this.apiAirlineReasonService.getById(id)
            .pipe(
                map((result: Result<AirlineReasonContract>) => StatiaFunctions.buildResultModel(result, this.translator))
            );
    }

    create(param: AirlineReason): Observable<Result<AirlineReason>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: AirlineReason): Promise<Result<AirlineReason>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: AirlineReason): Observable<Result<AirlineReason>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: AirlineReason): Promise<Result<AirlineReason>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<AirlineReason>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const AirlineReasons = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', AirlineReasons);
    }
}
