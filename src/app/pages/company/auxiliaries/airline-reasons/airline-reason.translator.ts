import { AirlineReasonContract } from '@app/api/contracts/airline-reason-contract/airline-reason.contract';
import { AirlineReason } from './airline-reason.model';

export class AirlineReasonTranslator {
    static translateContractToModel(contract?: AirlineReasonContract): AirlineReason {
        const model: AirlineReason = {
            id: (contract && contract.id) ? contract.id : null,
            name: (contract && contract.name) ? contract.name : null,
        };
        return new AirlineReason(model);
    }
}
