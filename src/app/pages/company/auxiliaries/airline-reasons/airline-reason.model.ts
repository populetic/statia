export class AirlineReason {
    id: string;
    name: string;

    constructor(model?: AirlineReason) {
        this.id = (model && model.id) ? model.id : null;
        this.name = (model && model.name) ? model.name : null;
    }
}
