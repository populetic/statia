export class City {
    id: string;
    countryId: string;
    countryName: string;
    name: string;
    isActive: boolean;

    constructor(model?: City) {
        this.id = model?.id ?? '';
        this.countryId = model?.countryId ?? '';
        this.countryName = model?.countryName ?? '';
        this.name = model?.name ?? '';
        this.isActive = model?.isActive ?? false;
    }
}
