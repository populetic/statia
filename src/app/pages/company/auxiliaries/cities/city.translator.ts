import { City } from './city.model';
import { CityContract } from '../../../../api/contracts/city-contract/city.contract';

export class CityTranslator {
    static translateContractToModel(contract?: CityContract): City {
        const model: City = {
            id: contract?.id ?? '',
            countryId: contract?.countryId ?? '',
            countryName: contract?.countryName ?? '',
            name: contract?.name ?? '',
            isActive: contract?.isActive && contract?.isActive === '1' ? true : false,
        };
        return new City(model);
    }
}
