import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { City } from './city.model';
import { ApiCityService } from '@app/api/services/api-city-service/api-city.service';
import { CityContract } from '@app/api/contracts/city-contract/city.contract';
import { map } from 'rxjs/operators';
import { CityTranslator } from './city.translator';

@Injectable({ providedIn: 'root' })
export class CityService implements BaseInterface<City> {
    public resourceUrl: string = environment.apiUrl + '/cities';
    public headers: HttpHeaders;

    constructor(private http: HttpClient, private apiCityService: ApiCityService) { }

    //#region interface functions
    getAll(customParams: CustomParams = null): Observable<Result<City[]>> {
        return this.apiCityService.getAll(customParams)
            .pipe(
                map((result: Result<CityContract[]>) => {
                    const newResult: Result<City[]> = StatiaFunctions.getResultModel<CityContract[], City[]>(result);
                    newResult.response = result.response.map((city: CityContract) => CityTranslator.translateContractToModel(city));
                    return  newResult;
                })
            );
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<City[]>> {
        return new Promise((resolve, reject) => {
            this.apiCityService.getAllAsync(customParams)
                .then((result: Result<CityContract[]>) => {
                    const newResult: Result<City[]> = StatiaFunctions.getResultModel<CityContract[], City[]>(result);
                    newResult.response = result.response.map((city: CityContract) => CityTranslator.translateContractToModel(city));
                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    getById(id: number|string): Observable<Result<City>> {
        return this.apiCityService.getById(id).pipe(
            map((result: Result<CityContract>) => {
                const newResult: Result<City> = StatiaFunctions.getResultModel<CityContract, City>(result);
                const city: City = CityTranslator.translateContractToModel(result.response);
                newResult.response = city;

                return  newResult;
            })
        );
    }

    create(param: City): Observable<Result<City>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: City): Promise<Result<City>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: City): Observable<Result<City>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: City): Promise<Result<City>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<City>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const Citys = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', Citys);
    }
    //#endregion
}
