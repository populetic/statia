import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiAirlineAlternativeService } from '@app/api/services/api-airline-alternative-service/api-airline-alternative.service';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Airline } from '../airlines/airline.model';
import { AirlineAlternativeContract } from './airline-alternative.contract';
import { AirlineAlternative } from './airline-alternative.model';
import { AirlineAlternativeTranslator } from './airline-alternative.translator';

@Injectable({ providedIn: 'root' })
export class AirlineAlternativeService implements BaseInterface<AirlineAlternative> {
    public resourceUrl: string = environment.apiUrl + '/airline-alternatives';
    public headers: HttpHeaders;
    private translator = AirlineAlternativeTranslator;

    constructor(
        private http: HttpClient,
        private apiAirlineAlternativeService: ApiAirlineAlternativeService
    ) {}

    getAll(param: CustomParams = null): Observable<Result<AirlineAlternative[]>> {
        return this.apiAirlineAlternativeService.getAll(param)
            .pipe(
                map((result: Result<AirlineAlternativeContract[]>) =>
                    StatiaFunctions.buildResultArrayModel<AirlineAlternativeContract, AirlineAlternative>(result, this.translator)
                )
            );
    }

    getAllAsync(param: CustomParams = null): Promise<Result<AirlineAlternative[]>> {
        return new Promise((resolve, reject) => {
            this.apiAirlineAlternativeService.getAllAsync(param)
                .then((result: Result<AirlineAlternativeContract[]>) =>
                    resolve(StatiaFunctions.buildResultArrayModel(result, this.translator))
                )
                .catch((err) => reject(err));
        });
    }

    getById(id: number|string): Observable<Result<AirlineAlternative>> {
        return this.apiAirlineAlternativeService.getById(id)
            .pipe(
                map((result: Result<AirlineAlternativeContract>) => StatiaFunctions.buildResultModel(result, this.translator))
            );
    }

    create(param: Airline): Observable<Result<AirlineAlternative>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: Airline): Promise<Result<AirlineAlternative>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: Airline): Observable<Result<AirlineAlternative>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: Airline): Promise<Result<AirlineAlternative>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<AirlineAlternative>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const Airlines = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', Airlines);
    }
}
