import { AirlineAlternativeContract } from './airline-alternative.contract';
import { AirlineAlternative } from './airline-alternative.model';

export class AirlineAlternativeTranslator {
    static translateContractToModel(contract?: AirlineAlternativeContract): AirlineAlternative {
        const model: AirlineAlternative = {
            id: (contract && contract.id) ? contract.id : null,
            name: (contract && contract.name) ? contract.name : null,
        };
        return new AirlineAlternative(model);
    }
}
