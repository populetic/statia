export class AirlineAlternative {
    id: string;
    name: string;

    constructor(model?: AirlineAlternative) {
        this.id = (model && model.id) ? model.id : null;
        this.name = (model && model.name) ? model.name : null;
    }
}
