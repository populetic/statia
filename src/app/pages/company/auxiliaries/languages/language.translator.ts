import { Language } from './language.model';
import { LanguageContract } from '../../../../api/contracts/language-contract/language.contract';

export class LanguageTranslator {
    static translateContractToModel(contract: LanguageContract): Language {
        const model: Language = {
            id: (contract && contract.id) ? contract.id : null,
            code: (contract && contract.code) ? contract.code : null,
            name: (contract && contract.name) ? contract.name : null,
        };

        return new Language(model);
    }
}
