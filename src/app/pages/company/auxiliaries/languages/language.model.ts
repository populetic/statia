export class Language {
    id: string;
    code: string;
    name: string;

    constructor(model?: Language) {
        this.id = (model && model.id) ? model.id : null;
        this.code = (model && model.code) ? model.code : null;
        this.name = (model && model.name) ? model.name : null;
    }
}
