import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { Language } from './language.model';
import { ApiLanguageService } from '@app/api/services/api-language-service/api-language.service';
import { LanguageContract } from '@app/api/contracts/language-contract/language.contract';
import { LanguageTranslator } from './language.translator';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class LanguageService implements BaseInterface<Language> {
    public resourceUrl: string = environment.apiUrl + '/languages';
    public headers: HttpHeaders;

    constructor(private http: HttpClient, private apiLanguageService: ApiLanguageService) {}

    getAll(customParams: CustomParams = null): Observable<Result<Language[]>> {
        return this.apiLanguageService.getAll(customParams)
            .pipe(
                map((result: Result<LanguageContract[]>) => {
                    const newResult: Result<Language[]> = StatiaFunctions.getResultModel<LanguageContract[], Language[]>(result);
                    newResult.response = result.response.map((language: LanguageContract) =>
                        LanguageTranslator.translateContractToModel(language));
                    return  newResult;
                })
            );
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<Language[]>> {
        return new Promise((resolve, reject) => {
            this.apiLanguageService.getAllAsync(customParams)
                .then((result: Result<LanguageContract[]>) => {
                    const newResult: Result<Language[]> = StatiaFunctions.getResultModel<LanguageContract[], Language[]>(result);
                    newResult.response = result.response.map((language: LanguageContract) =>
                        LanguageTranslator.translateContractToModel(language));
                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    getById(id: number|string): Observable<Result<Language>> {
        return this.apiLanguageService.getById(id).pipe(
            map((result: Result<LanguageContract>) => {
                const newResult: Result<Language> = StatiaFunctions.getResultModel<LanguageContract, Language>(result);
                const language: Language = LanguageTranslator.translateContractToModel(result.response);
                newResult.response = language;

                return  newResult;
            })
        );
    }

    create(param: Language): Observable<Result<Language>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: Language): Promise<Result<Language>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: Language): Observable<Result<Language>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: Language): Promise<Result<Language>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<Language>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const Languages = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', Languages);
    }
}
