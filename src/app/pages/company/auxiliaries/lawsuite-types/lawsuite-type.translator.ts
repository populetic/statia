import { LawsuiteTypeContract } from '../../../../api/contracts/lawsuite-type-contract/lawsuite-type.contract';
import { LawsuiteType } from './lawsuite-type.model';

export class LawsuiteTypeTranslator {
    static translateContractToModel(contract?: LawsuiteTypeContract): LawsuiteType {
        const model: LawsuiteType = {
            id: (contract && contract.id) ? contract.id : null,
            name: (contract && contract.name) ? contract.name : null,
        };
        return new LawsuiteType(model);
    }
}
