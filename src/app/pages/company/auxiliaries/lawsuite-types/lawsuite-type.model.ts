export class LawsuiteType {
    id: string;
    name: string;

    constructor(model?: LawsuiteType) {
        this.id = (model && model.id) ? model.id : null;
        this.name = (model && model.name) ? model.name : null;
    }
}
