import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LawsuiteTypeContract } from '@app/api/contracts/lawsuite-type-contract/lawsuite-type.contract';
import { ApiLawsuiteTypeService } from '@app/api/services/api-lawsuite-type-service/api-lawsuite-type.service';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LawsuiteType } from './lawsuite-type.model';
import { LawsuiteTypeTranslator } from './lawsuite-type.translator';

@Injectable({ providedIn: 'root' })
export class LawsuiteTypeService implements BaseInterface<LawsuiteType> {
    public resourceUrl: string = environment.apiUrl + '/lawsuite-types';
    public headers: HttpHeaders;
    private translator = LawsuiteTypeTranslator;

    constructor(
        private http: HttpClient,
        private apiLawsuiteTypeService: ApiLawsuiteTypeService
    ) {}

    getAll(param: CustomParams = null): Observable<Result<LawsuiteType[]>> {
        return this.apiLawsuiteTypeService.getAll(param)
        .pipe(
            map((result: Result<LawsuiteTypeContract[]>) =>
                StatiaFunctions.buildResultArrayModel<LawsuiteTypeContract, LawsuiteType>(result, this.translator)
            )
        );
    }

    getAllAsync(param: CustomParams = null): Promise<Result<LawsuiteType[]>> {
        return new Promise((resolve, reject) => {
            this.apiLawsuiteTypeService.getAllAsync(param)
                .then((result: Result<LawsuiteTypeContract[]>) =>
                    resolve(StatiaFunctions.buildResultArrayModel(result, this.translator))
                )
                .catch((err) => reject(err));
        });
    }

    getById(id: number|string): Observable<Result<LawsuiteType>> {
        return this.apiLawsuiteTypeService.getById(id)
            .pipe(
                map((result: Result<LawsuiteTypeContract>) => StatiaFunctions.buildResultModel(result, this.translator))
            );
    }

    create(param: LawsuiteType): Observable<Result<LawsuiteType>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: LawsuiteType): Promise<Result<LawsuiteType>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: LawsuiteType): Observable<Result<LawsuiteType>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: LawsuiteType): Promise<Result<LawsuiteType>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<LawsuiteType>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const LawsuiteTypes = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', LawsuiteTypes);
    }
}
