import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { BaseService } from '@app/shared/services/base.service';
import { SessionService } from '@shared/services/session.service';

import { environment } from '@environments/environment';
import { ClaimStatus } from './claim-status.model';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Observable } from 'rxjs';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';

@Injectable({ providedIn: 'root' })
export class ClaimStatusService implements BaseInterface<ClaimStatus> {
    public resourceUrl: string = environment.apiUrl + '/claim-status';
    public headers: HttpHeaders;

    constructor(private http: HttpClient) {}

    //#region interface functions
    getAll(param: CustomParams = null): Observable<Result<ClaimStatus[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(param: CustomParams = null): Promise<Result<ClaimStatus[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number|string): Observable<Result<ClaimStatus>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(param: ClaimStatus): Observable<Result<ClaimStatus>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: ClaimStatus): Promise<Result<ClaimStatus>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: ClaimStatus): Observable<Result<ClaimStatus>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: ClaimStatus): Promise<Result<ClaimStatus>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<ClaimStatus>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const ClaimStatuss = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', ClaimStatuss);
    }
    //#endregion
}
