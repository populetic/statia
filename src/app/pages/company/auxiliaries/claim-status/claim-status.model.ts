export class ClaimStatus {
    id: string;
    name: string;
    closed: boolean;

    constructor(model?: ClaimStatus) {
        this.id = (model && model.id) ? model.id : null;
        this.name = (model && model.name) ? model.name : null;
        this.closed = (model && model.closed) ? model.closed : false;
    }
}
