import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AirlineContract } from '@app/api/contracts/airlines-contract/airline.contract';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Airline } from './airline.model';
import { AirlineTranslator } from './airline.translator';
import { ApiAirlineService } from '@app/api/services/api-airlines-service/api-airlines.service';

@Injectable({ providedIn: 'root' })
export class AirlineService implements BaseInterface<Airline> {
    public resourceUrl: string = environment.apiUrl + '/airlines';
    public headers: HttpHeaders;

    constructor(private http: HttpClient, private apiAirlineService: ApiAirlineService) {}

    //#region interface functions
    getAll(customParams: CustomParams = null): Observable<Result<Airline[]>> {
        return this.apiAirlineService.getAll(customParams)
            .pipe(
                map((result: Result<AirlineContract[]>) => {
                    const newResult: Result<Airline[]> = StatiaFunctions.getResultModel<AirlineContract[], Airline[]>(result);
                    newResult.response = result.response.map((airline: AirlineContract) =>
                        AirlineTranslator.translateContractToModel(airline));
                    return  newResult;
                })
            );
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<Airline[]>> {
        return new Promise((resolve, reject) => {
            this.apiAirlineService.getAllAsync(customParams)
                .then((result: Result<AirlineContract[]>) => {
                    const newResult: Result<Airline[]> = StatiaFunctions.getResultModel<AirlineContract[], Airline[]>(result);
                    newResult.response = result.response.map((airline: AirlineContract) =>
                        AirlineTranslator.translateContractToModel(airline));
                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    getById(id: number|string): Observable<Result<Airline>> {
        return this.apiAirlineService.getById(id).pipe(
            map((result: Result<AirlineContract>) => {
                const newResult: Result<Airline> = StatiaFunctions.getResultModel<AirlineContract, Airline>(result);
                const airline: Airline = AirlineTranslator.translateContractToModel(result.response);
                newResult.response = airline;

                return  newResult;
            })
        );
    }

    create(param: Airline): Observable<Result<Airline>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: Airline): Promise<Result<Airline>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: Airline): Observable<Result<Airline>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: Airline): Promise<Result<Airline>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<Airline>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const Airlines = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', Airlines);
    }
    //#endregion
}
