import { AirlineContract } from '../../../../api/contracts/airlines-contract/airline.contract';
import { Airline } from './airline.model';

export class AirlineTranslator {
    static translateContractToModel(contract: AirlineContract): Airline {
        const model: Airline = {
            id: (contract && contract.id) ? contract.id : null,
            name: (contract && contract.name) ? contract.name : null,
            iata: (contract && contract.iata) ? contract.iata : null,
            nameAndIata: (contract && contract.nameAndIata) ? contract.nameAndIata : null,
            icao: (contract && contract.icao) ? contract.icao : null,
            flightstatsIata: (contract && contract.flightstatsIata) ? contract.flightstatsIata : null,
            isBankruptcy: (contract && contract.isBankruptcy && contract.isBankruptcy === '1') ? true : false,
            hasCommunityLicense:
                (contract && contract.hasCommunityLicense && contract.hasCommunityLicense === '1') ? true : false,
            isActive: (contract && contract.isActive && contract.isActive === '1') ? true : false,
            createdAt: (contract && contract.createdAt) ? contract.createdAt : null,
            updatedAt: (contract && contract.updatedAt) ? contract.updatedAt : null
        };

        return new Airline(model);
    }
}
