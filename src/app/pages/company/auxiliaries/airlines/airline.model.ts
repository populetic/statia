export class Airline {
    id: string;
    name: string;
    iata: string;
    nameAndIata: string;
    icao?: string;
    flightstatsIata: string;
    isBankruptcy: boolean;
    hasCommunityLicense: boolean;
    isActive: boolean;
    createdAt?: string;
    updatedAt?: string;

    constructor(model?: Airline) {
        this.id = (model && model.id) ? model.id : null;
        this.name = (model && model.name) ? model.name : null;
        this.iata = (model && model.iata) ? model.iata : null;
        this.nameAndIata = (model && model.nameAndIata) ? model.nameAndIata : null;
        this.icao = (model && model.icao) ? model.icao : null;
        this.flightstatsIata = (model && model.flightstatsIata) ? model.flightstatsIata : null;
        this.isBankruptcy = (model && model.isBankruptcy) ? model.isBankruptcy : false;
        this.hasCommunityLicense = (model && model.hasCommunityLicense) ? model.hasCommunityLicense : false;
        this.isActive = (model && model.isActive) ? model.isActive : false;
        this.createdAt = (model && model.createdAt) ? model.createdAt : null;
        this.updatedAt = (model && model.updatedAt) ? model.updatedAt : null;
    }
}
