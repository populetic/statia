import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { Country } from './country.model';
import { ApiCountryService } from '@app/api/services/api-country-service/api-country.service';
import { CountryContract } from '@app/api/contracts/country-contract/country.contract';
import { map } from 'rxjs/operators';
import { CountryTranslator } from './country.translator';

@Injectable({ providedIn: 'root' })
export class CountryService implements BaseInterface<Country> {
    public resourceUrl: string = environment.apiUrl + '/countries';
    public headers: HttpHeaders;

    constructor(private http: HttpClient, private apiCountryService: ApiCountryService) { }

    getAll(customParams: CustomParams = null): Observable<Result<Country[]>> {
        return this.apiCountryService.getAll(customParams)
            .pipe(
                map((result: Result<CountryContract[]>) => {
                    const newResult: Result<Country[]> = StatiaFunctions.getResultModel<CountryContract[], Country[]>(result);
                    newResult.response = result.response.map((country: CountryContract) =>
                        CountryTranslator.translateContractToModel(country));
                    return  newResult;
                })
            );
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<Country[]>> {
        return new Promise((resolve, reject) => {
            this.apiCountryService.getAllAsync(customParams)
                .then((result: Result<CountryContract[]>) => {
                    const newResult: Result<Country[]> = StatiaFunctions.getResultModel<CountryContract[], Country[]>(result);
                    newResult.response = result.response.map((country: CountryContract) =>
                        CountryTranslator.translateContractToModel(country));
                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    getById(id: number|string): Observable<Result<Country>> {
        return this.apiCountryService.getById(id).pipe(
            map((result: Result<CountryContract>) => {
                const newResult: Result<Country> = StatiaFunctions.getResultModel<CountryContract, Country>(result);
                const country: Country = CountryTranslator.translateContractToModel(result.response);
                newResult.response = country;

                return  newResult;
            })
        );
    }

    create(param: Country): Observable<Result<Country>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);

        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: Country): Promise<Result<Country>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);

        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: Country): Observable<Result<Country>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: Country): Promise<Result<Country>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);

        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<Country>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const Countrys = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', Countrys);
    }
}
