export class Country {
    competencePriority: string;
    id: string;
    isActive: boolean;
    isUe: boolean;
    iso: string;
    name: string;
    regionId: string;
    regionName: string;

    constructor(model?: Country) {
        this.competencePriority = (model && model.competencePriority) ? model.competencePriority : null;
        this.id = (model && model.id) ? model.id : null;
        this.isActive = (model && model.isActive) ? model.isActive : false;
        this.isUe = (model && model.isUe) ? model.isUe : false;
        this.iso = (model && model.iso) ? model.iso : null;
        this.name = (model && model.name) ? model.name : null;
        this.regionId = (model && model.regionId) ? model.regionId : null;
        this.regionName = (model && model.regionName) ? model.regionName : null;
    }
}
