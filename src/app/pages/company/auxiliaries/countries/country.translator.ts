import { CountryContract } from '../../../../api/contracts/country-contract/country.contract';
import { Country } from './country.model';

export class CountryTranslator {
    static translateContractToModel(contract: CountryContract): Country {
        const model: Country = {
           competencePriority: (contract && contract.competencePriority) ? contract.competencePriority : null,
           id: (contract && contract.id) ? contract.id : null,
           isActive: (contract && contract.isActive && contract.isActive === '1') ? true : false,
           isUe: (contract && contract.isUe && contract.isUe === '1') ? true : false,
           iso: (contract && contract.iso) ? contract.iso : null,
           name: (contract && contract.name) ? contract.name : null,
           regionId: (contract && contract.regionId) ? contract.regionId : null,
           regionName: (contract && contract.regionName) ? contract.regionName : null
        };

        return new Country(model);
    }
}
