import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { Airport } from './airport.model';
import { ApiAirportService } from '@app/api/services/api-airport-service/api-airport.service';
import { AirportContract } from '@app/api/contracts/airport-contract/airport.contract';
import { map } from 'rxjs/operators';
import { AirportTranslator } from './airport.translator';

@Injectable({ providedIn: 'root' })
export class AirportService implements BaseInterface<Airport> {
    public resourceUrl: string = environment.apiUrl + '/airports';
    public headers: HttpHeaders;

    constructor(private http: HttpClient, private apiAirportService: ApiAirportService) {}

    //#region interface functions
    getAll(customParams: CustomParams = null): Observable<Result<Airport[]>> {
        return this.apiAirportService.getAll(customParams)
            .pipe(
                map((result: Result<AirportContract[]>) => {
                    const newResult: Result<Airport[]> = StatiaFunctions.getResultModel<AirportContract[], Airport[]>(result);
                    newResult.response = result.response.map((airport: AirportContract) =>
                        AirportTranslator.translateContractToModel(airport));
                    return  newResult;
                })
            );
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<Airport[]>> {
        return new Promise((resolve, reject) => {
            this.apiAirportService.getAllAsync(customParams)
                .then((result: Result<AirportContract[]>) => {
                    const newResult: Result<Airport[]> = StatiaFunctions.getResultModel<AirportContract[], Airport[]>(result);
                    newResult.response = result.response.map((airport: AirportContract) =>
                        AirportTranslator.translateContractToModel(airport));
                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    getById(id: number|string): Observable<Result<Airport>> {
        return this.apiAirportService.getById(id).pipe(
            map((result: Result<AirportContract>) => {
                const newResult: Result<Airport> = StatiaFunctions.getResultModel<AirportContract, Airport>(result);
                const airport: Airport = AirportTranslator.translateContractToModel(result.response);
                newResult.response = airport;

                return  newResult;
            })
        );
    }

    create(param: Airport): Observable<Result<Airport>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: Airport): Promise<Result<Airport>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: Airport): Observable<Result<Airport>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: Airport): Promise<Result<Airport>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<Airport>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const Airports = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', Airports);
    }
    //#endregion
}
