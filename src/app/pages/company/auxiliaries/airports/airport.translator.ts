import { Airport } from './airport.model';
import { AirportContract } from '@app/api/contracts/airport-contract/airport.contract';

export class AirportTranslator {
    static translateContractToModel(contract: AirportContract): Airport {
        const model: Airport = {
            id: (contract && contract.id) ? contract.id : null,
            name: (contract && contract.name) ? contract.name : null,
            iata: (contract && contract.iata) ? contract.iata : null,
            nameAndIata: (contract && contract.nameAndIata) ? contract.nameAndIata : null,
            icao: (contract && contract.icao) ? contract.icao : null,
            countryId: (contract && contract.countryId) ? contract.countryId : null,
            countryName: (contract && contract.countryName) ? contract.countryName : null,
            cityId: (contract && contract.cityId) ? contract.cityId : null,
            cityName: (contract && contract.cityName) ? contract.cityName : null,
            isMain: (contract && contract.isMain && contract.isMain === '1') ? true : false,
            latitude: (contract && contract.latitude) ? contract.latitude : null,
            longitude: (contract && contract.longitude) ? contract.longitude : null,
            createdAt: (contract && contract.createdAt) ? contract.createdAt : null,
            updatedAt: (contract && contract.updatedAt) ? contract.updatedAt : null
        };

        return new Airport(model);
    }
}
