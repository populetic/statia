export class Airport {
    id: string;
    name: string;
    iata: string;
    nameAndIata: string;
    icao: string;
    countryId: string;
    countryName: string;
    cityId: string;
    cityName: string;
    isMain: boolean;
    latitude: string;
    longitude: string;
    createdAt: string;
    updatedAt: string;

    constructor(model?: Airport) {
        this.id = (model && model.id) ? model.id : null;
        this.name = (model && model.name) ? model.name : null;
        this.iata = (model && model.iata) ? model.iata : null;
        this.nameAndIata = (model && model.nameAndIata) ? model.nameAndIata : null;
        this.icao = (model && model.icao) ? model.icao : null;
        this.countryId = (model && model.countryId) ? model.countryId : null;
        this.countryName = (model && model.countryName) ? model.countryName : null;
        this.cityId = (model && model.cityId) ? model.cityId : null;
        this.cityName = (model && model.cityName) ? model.cityName : null;
        this.isMain = (model && model.isMain) ? model.isMain : false;
        this.latitude = (model && model.latitude) ? model.latitude : null;
        this.longitude = (model && model.longitude) ? model.longitude : null;
        this.createdAt = (model && model.createdAt) ? model.createdAt : null;
        this.updatedAt = (model && model.updatedAt) ? model.updatedAt : null;
    }
}
