import { Province } from './province.model';
import { ProvinceContract } from '@app/api/contracts/province-contract/province.contract';

export class ProvinceTranslator {
    static translateContractToModel(contract: ProvinceContract): Province {
        const model: Province = {
            id: (contract && contract.id) ? contract.id : null,
            countryId: (contract && contract.countryId) ? contract.countryId : null,
            countryName: (contract && contract.countryName) ? contract.countryName : null,
            name: (contract && contract.name) ? contract.name : null,
            isActive: (contract && contract.isActive && contract.isActive === '1') ? true : false
        };

        return new Province(model);
    }
}
