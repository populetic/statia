export class Province {
    id: string;
    countryId: string;
    countryName: string;
    name: string;
    isActive: boolean;

    constructor(model?: Province) {
        this.id = (model && model.id) ? model.id : null;
        this.countryId = (model && model.countryId) ? model.countryId : null;
        this.countryName = (model && model.countryName) ? model.countryName : null;
        this.name = (model && model.name) ? model.name : null;
        this.isActive = (model && model.isActive) ? model.isActive : false;
    }
}
