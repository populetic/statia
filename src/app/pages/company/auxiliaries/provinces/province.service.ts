import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ProvinceContract } from '@app/api/contracts/province-contract/province.contract';
import { ApiProvinceService } from '@app/api/services/api-province-service/api-province.service';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Province } from './province.model';
import { ProvinceTranslator } from './province.translator';

@Injectable({ providedIn: 'root' })
export class ProvinceService implements BaseInterface<Province> {
    public resourceUrl: string = environment.apiUrl + '/country-provinces';
    public headers: HttpHeaders;

    constructor(
        private http: HttpClient,
        private apiProvinceService: ApiProvinceService
    ) {}

    getAll(param: CustomParams = null): Observable<Result<Province[]>> {
        return this.apiProvinceService.getAll(param)
        .pipe(
            map((result: Result<ProvinceContract[]>) => {
                const newResult: Result<Province[]> = StatiaFunctions.getResultModel<ProvinceContract[], Province[]>(result);
                newResult.response = result?.response.map((c: ProvinceContract) => ProvinceTranslator.translateContractToModel(c));
                return newResult;
            })
        );
    }

    getAllAsync(param: CustomParams = null): Promise<Result<Province[]>> {
        return new Promise((resolve, reject) => {
            this.apiProvinceService.getAllAsync(param)
            .then((result: Result<ProvinceContract[]>) => {
                const newResult: Result<Province[]> = StatiaFunctions.getResultModel<ProvinceContract[], Province[]>(result);
                newResult.response = result?.response.map((c: ProvinceContract) => ProvinceTranslator.translateContractToModel(c));
                resolve(newResult);
            })
            .catch((err) => reject(err));
        });
    }

    getById(id: number|string): Observable<Result<Province>> {
        return this.apiProvinceService.getById(id)
        .pipe(
            map((result: Result<ProvinceContract>) => {
                const newResult: Result<Province> = StatiaFunctions.getResultModel<ProvinceContract, Province>(result);
                newResult.response = ProvinceTranslator.translateContractToModel(result?.response);
                return newResult;
            })
        );
    }

    create(param: Province): Observable<Result<Province>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: Province): Promise<Result<Province>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: Province): Observable<Result<Province>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: Province): Promise<Result<Province>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<Province>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const Provinces = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', Provinces);
    }
}
