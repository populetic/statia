import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { DisruptionType } from './disruption-type.model';
import { DISRUPTION_LUGGAGE_IDS } from '@app/shared/constants/constants';

@Injectable({ providedIn: 'root' })
export class DisruptionTypeService implements BaseInterface<DisruptionType> {
    public resourceUrl: string = environment.apiUrl + '/disruption-types';
    public headers: HttpHeaders;

    constructor(private http: HttpClient) {}

    //#region interface functions
    getAll(param: CustomParams = null): Observable<Result<DisruptionType[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(param: CustomParams = null): Promise<Result<DisruptionType[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number|string): Observable<Result<DisruptionType>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(param: DisruptionType): Observable<Result<DisruptionType>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: DisruptionType): Promise<Result<DisruptionType>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: DisruptionType): Observable<Result<DisruptionType>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: DisruptionType): Promise<Result<DisruptionType>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<DisruptionType>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const DisruptionTypes = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', DisruptionTypes);
    }
    //#endregion

    //#region Non interface functions
    isLuggageDisruption(disruptionId: string) {
        return DISRUPTION_LUGGAGE_IDS.includes(disruptionId);
    }
    //#endregion
}
