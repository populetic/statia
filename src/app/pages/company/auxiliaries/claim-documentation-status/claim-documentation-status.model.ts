export class ClaimDocumentationStatus {
    id: string;
    name: string;

    constructor(model?: ClaimDocumentationStatus) {
        this.id = (model && model.id) ? model.id : null;
        this.name = (model && model.name) ? model.name : null;
    }
}
