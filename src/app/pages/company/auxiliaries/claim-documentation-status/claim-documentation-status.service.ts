import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { ClaimDocumentationStatus } from './claim-documentation-status.model';

@Injectable({ providedIn: 'root' })
export class ClaimDocumentationStatusService implements BaseInterface<ClaimDocumentationStatus> {
    public resourceUrl: string = environment.apiUrl + '/claim-documentation-status';
    public headers: HttpHeaders;

    constructor(private http: HttpClient) { }

    //#region interface functions
    getAll(param: CustomParams = null): Observable<Result<ClaimDocumentationStatus[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString);
    }

    getAllAsync(param: CustomParams = null): Promise<Result<ClaimDocumentationStatus[]>> {
        const queryString = StatiaFunctions.getQueryString(param);
        return this.http.get(this.resourceUrl + queryString).toPromise();
    }

    getById(id: number|string): Observable<Result<ClaimDocumentationStatus>> {
        return this.http.get(this.resourceUrl + '/' + id);
    }

    create(param: ClaimDocumentationStatus): Observable<Result<ClaimDocumentationStatus>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: ClaimDocumentationStatus): Promise<Result<ClaimDocumentationStatus>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: ClaimDocumentationStatus): Observable<Result<ClaimDocumentationStatus>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: ClaimDocumentationStatus): Promise<Result<ClaimDocumentationStatus>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<ClaimDocumentationStatus>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const ClaimDocumentationStatuss = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', ClaimDocumentationStatuss);
    }
    //#endregion
}
