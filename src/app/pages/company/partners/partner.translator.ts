import { PartnerContract } from '@app/api/contracts/partner-contract/partner.contract';
import { Partner } from './partner.model';

export class PartnerTranslator {

    static translateContractToModel(contract: PartnerContract): Partner {
        const model: Partner = {
            id: contract?.id ?? null,
            fiscalName: contract?.fiscalName ?? null,
            businessName: contract?.businessName ?? null,
            fee: contract?.fee ? Number(contract?.fee) : 0,
            isActive: contract?.isActive && contract?.isActive === '1'  ? true : false,
            createdAt: contract?.createdAt ? new Date(contract?.createdAt) : null,
            updatedAt: contract?.updatedAt ? new Date(contract?.updatedAt) : null,
        };

        return new Partner(model);
    }
}
