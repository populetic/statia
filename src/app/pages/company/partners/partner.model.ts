export class Partner {
    id: string;
    fiscalName: string;
    businessName: string;
    fee: number;
    isActive: boolean;
    createdAt: Date;
    updatedAt: Date;

    constructor(model?: Partner) {
        this.id = (model && model.id) ? model.id : null;
        this.fiscalName = (model && model.fiscalName) ? model.fiscalName : null;
        this.businessName = (model && model.businessName) ? model.businessName : null;
        this.fee = (model && model.fee) ? model.fee : null;
        this.isActive = (model && model.isActive) ? model.isActive : false;
        this.createdAt = (model && model.createdAt) ? model.createdAt : null;
        this.updatedAt = (model && model.updatedAt) ? model.updatedAt : null;
    }
}
