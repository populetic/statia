import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PartnerContract } from '@app/api/contracts/partner-contract/partner.contract';
import { ApiPartnerService } from '@app/api/services/api-partner-service/api-partner.service';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Partner } from './partner.model';
import { PartnerTranslator } from './partner.translator';

@Injectable({ providedIn: 'root' })
export class PartnerService implements BaseInterface<Partner> {
    public resourceUrl: string = environment.apiUrl + '/partners';
    public headers: HttpHeaders;
    private translator = PartnerTranslator;

    constructor(
        private http: HttpClient,
        private apiPartnerService: ApiPartnerService
    ) {}

    getAll(param: CustomParams = null): Observable<Result<Partner[]>> {
        return this.apiPartnerService.getAll(param)
            .pipe(
                map((result: Result<PartnerContract[]>) =>
                    StatiaFunctions.buildResultArrayModel<PartnerContract, Partner>(result, this.translator)
                )
            );
    }

    getAllAsync(param: CustomParams = null): Promise<Result<Partner[]>> {
        return new Promise((resolve, reject) => {
            this.apiPartnerService.getAllAsync(param)
                .then((result: Result<PartnerContract[]>) =>
                    resolve(StatiaFunctions.buildResultArrayModel(result, this.translator))
                )
                .catch((err) => reject(err));
        });
    }

    getById(id: number|string): Observable<Result<Partner>> {
        return this.apiPartnerService.getById(id)
            .pipe(
                map((result: Result<PartnerContract>) => StatiaFunctions.buildResultModel(result, this.translator))
            );
    }

    create(param: Partner): Observable<Result<Partner>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: Partner): Promise<Result<Partner>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: Partner): Observable<Result<Partner>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: Partner): Promise<Result<Partner>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<Partner>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const Partners = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', Partners);
    }
}
