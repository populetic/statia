export class BillPreference {
    id: string;
    countryId: string;
    countryName: string;
    iva: number;
    expirationDaysClient: number;
    expirationDaysPopuletic: number;
    expirationDaysCancellation: number;
    amountCancellation: number;
    isActive: boolean; // TODO: translator
    createdAt: Date;

    constructor(model?: BillPreference) {
        this.id = (model && model.id) ? model.id : null;
        this.countryId = (model && model.countryId) ? model.countryId : null;
        this.countryName = (model && model.countryName) ? model.countryName : null;
        this.iva = (model && model.iva) ? model.iva : null;
        this.expirationDaysClient = (model && model.expirationDaysClient) ? model.expirationDaysClient : null;
        this.expirationDaysPopuletic = (model && model.expirationDaysPopuletic) ? model.expirationDaysPopuletic : null;
        this.expirationDaysCancellation = (model && model.expirationDaysCancellation) ?
            model.expirationDaysCancellation : null;
        this.amountCancellation = (model && model.amountCancellation) ? model.amountCancellation : null;
        this.isActive = (model && model.isActive) ? model.isActive : false;
        this.createdAt = (model && model.createdAt) ? model.createdAt : null;
    }

}
