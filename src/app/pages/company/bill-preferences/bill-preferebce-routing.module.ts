import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BillPreferenceEditorComponent } from './bill-preference-editor/bill-preference-editor.component';
import { BillPreferenceComponent } from './bill-preference.component';
const routes: Routes = [
        {
            path: '',
            component: BillPreferenceComponent
        },
        {
            path: 'create',
            component: BillPreferenceEditorComponent
        },
        {
            path: 'edit/:id',
            component: BillPreferenceEditorComponent
        }
    ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BillPreferenceRoutingModule { }
