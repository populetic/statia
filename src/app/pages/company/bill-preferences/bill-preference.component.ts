import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { BillPreferenceService } from './bill-preference.service';

@Component({
    selector: 'app-menu',
    template: `<app-main-table [actualMenuId]="menuId" [actualMenuService]="menuService"></app-main-table>`
})

export class BillPreferenceComponent implements OnInit {

    public menuId: string;

    constructor(
        private activeRoute: ActivatedRoute,
        public menuService: BillPreferenceService
    ) {}

    ngOnInit() {
        this.activeRoute.data.subscribe(
            data => this.menuId = data.menuId
        );
    }
}
