import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { Subscription } from 'rxjs';
import { BillPreference } from '../bill-preference.model';
import { BillPreferenceService } from '../bill-preference.service';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { FORM_OPERATIONS } from '@app/shared/constants/constants';
import { PendingInterceptorService } from '@fury/shared/loading-indicator/pending-interceptor.service';

@Component({
    selector: 'app-bill-editor',
    templateUrl: './bill-preference-editor.component.html',
    animations: [fadeInRightAnimation, fadeInUpAnimation]
})

export class BillPreferenceEditorComponent implements OnInit, OnDestroy {

    public parentRoute: string;
    public formAction: string;
    public currentEditor: string;
    public breadcrumbs: string[];

    public canUseFormOperation: boolean;
    public isLoading = false;
    private menuId: string;

    public editorFormData: FormGroup;
    subscriptions: Subscription[] = [];

    public bill: BillPreference;
    private id: any;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private billService: BillPreferenceService,
        private pendingInterceptorService: PendingInterceptorService
    ) {
        this.bill = new BillPreference();
    }

    ngOnInit() {
        this.route.paramMap.subscribe(paramMap => { this.id = paramMap.get('id'); });

        this.formAction = (this.id === null ? 'create' : 'edit');
        const routeToReplace = (this.id === null ? '/' + this.formAction : '/' + this.formAction + '/' + this.id);
        this.parentRoute = this.router.url.replace(routeToReplace, '');

        const routeItems = this.parentRoute.split('/').filter(item => item !== null && item !== '');
        this.breadcrumbs = routeItems;

        this.canUseFormOperation = StatiaFunctions.EnabledOption(this.menuId, FORM_OPERATIONS[this.formAction.toUpperCase()]);
        this.breadcrumbs.push(this.formAction);

        if (this.id !== null) {
            const sub: Subscription = this.pendingInterceptorService.status$
                .subscribe((value: boolean) => this.isLoading = value);

            this.createFormGroup();
            this.configureForm();
        } else {
            this.currentEditor = 'create';
        }
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
    }

    private createFormGroup() {
        const billForm: any = {
            name: ['', Validators.required],
            surnames: ['', Validators.required]
        };

        this.editorFormData = this.formBuilder.group(billForm);
    }

    private async configureForm() {
    }

    getData() {
        const billSubs: Subscription = this.billService.getById(this.id).subscribe(
            result => {
                this.bill = result.response;

                this.currentEditor = ''; // TODO: muestr campo identificador
            }
        );
        this.subscriptions.push(billSubs);
    }

    onSubmit() {
        if (this.editorFormData.invalid) {
            return;
        }

        let param: any;
        param = this.editorFormData.value;
    }

}
