import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormComponentsModule } from '@app/layout/form-components/form-components.module';
import { MainTableModule } from '@app/layout/main-table/main-table.module';
import { FurySharedModule } from '@fury/fury-shared.module';
import { BreadcrumbsModule } from '@fury/shared/breadcrumbs/breadcrumbs.module';
import { MaterialModule } from '@fury/shared/material-components.module';
import { BillPreferenceRoutingModule } from './bill-preferebce-routing.module';
import { BillPreferenceEditorComponent } from './bill-preference-editor/bill-preference-editor.component';
import { BillPreferenceComponent } from './bill-preference.component';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        BreadcrumbsModule,
        MaterialModule,
        FurySharedModule,

        BillPreferenceRoutingModule,
        FormComponentsModule,
        MainTableModule,
    ],
    declarations: [BillPreferenceComponent, BillPreferenceEditorComponent],
    exports: [BillPreferenceComponent]
})

export class BillPreferenceModule {
}
