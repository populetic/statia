export class Client {
    id: string;
    name: string;
    surnames: string;
    email: string;
    phone: string;
    city: string; // TODO: change to cityName => translation_system
    address: string;
    countryProvinceId: string;
    countryProvinceName: string;
    countryId: string;
    countryName: string;
    languageId: string;
    idCard: string;
    documentationTypeId: string;
    idCardExpiresAt: Date;
    legalGuardianId: string;
    gender: string;
    ip: string;
    userAgent: string;
    createdAt: Date;
    updatedAt: Date;
    completeName: string;
    languageName: string;
    documentationTypeName: string;
    legalGuardianName: string;
    legalGuardianSurnames: string;
    legalGuardianIdCard: string;
    legalGuardianDocumentationTypeId: string;
    legalGuardianIdCardExpiresAt: Date;

    constructor(model?: Client) {
        this.id = (model && model.id) ? model.id : null;
        this.name = (model && model.name) ? model.name : null;
        this.surnames = (model && model.surnames) ? model.surnames : null;
        this.email = (model && model.email) ? model.email : null;
        this.phone = (model && model.phone) ? model.phone : null;
        this.city = (model && model.city) ? model.city : null;
        this.address = (model && model.address) ? model.address : null;
        this.countryProvinceId = (model && model.countryProvinceId) ? model.countryProvinceId : null;
        this.countryId = (model && model.countryId) ? model.countryId : null;
        this.languageId = (model && model.languageId) ? model.languageId : null;
        this.idCard = (model && model.idCard) ? model.idCard : null;
        this.documentationTypeId = (model && model.documentationTypeId) ? model.documentationTypeId : null;
        this.idCardExpiresAt = (model && model.idCardExpiresAt) ? model.idCardExpiresAt : null;
        this.legalGuardianId = (model && model.legalGuardianId) ? model.legalGuardianId : null;
        this.gender = (model && model.gender) ? model.gender : null;
        this.ip = (model && model.ip) ? model.ip : null;
        this.userAgent = (model && model.userAgent) ? model.userAgent : null;
        this.createdAt = (model && model.createdAt) ? model.createdAt : null;
        this.updatedAt = (model && model.updatedAt) ? model.updatedAt : null;
        this.completeName = (model && model.completeName) ? model.completeName : null;
        this.countryProvinceName = (model && model.countryProvinceName) ? model.countryProvinceName : null;
        this.countryName = (model && model.countryName) ? model.countryName : null;
        this.languageName = (model && model.languageName) ? model.languageName : null;
        this.documentationTypeName = (model && model.documentationTypeName) ? model.documentationTypeName : null;
        this.legalGuardianName = (model && model.legalGuardianName) ? model.legalGuardianName : null;
        this.legalGuardianSurnames = (model && model.legalGuardianSurnames) ? model.legalGuardianSurnames : null;
        this.legalGuardianIdCard = (model && model.legalGuardianIdCard) ? model.legalGuardianIdCard : null;
        this.legalGuardianDocumentationTypeId = (model && model.legalGuardianDocumentationTypeId) ?
            model.legalGuardianDocumentationTypeId : null;
        this.legalGuardianIdCardExpiresAt = (model && model.legalGuardianIdCardExpiresAt) ?
            model.legalGuardianIdCardExpiresAt : null;
    }

}
