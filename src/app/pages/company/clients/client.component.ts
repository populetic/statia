import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Login } from '@app/pages/login/login.model';
import { SessionService } from '@app/shared/services/session.service';
import { ClientService } from './client.service';

@Component({
    selector: 'app-menu',
    template: `<app-main-table
        [actualMenuId]="menuId"
        [actualMenuService]="menuService"
        [actualMenuFilters]="menuFilters"
        [actualMenuSortActive]="sortActive"
        [actualMenuSortDirection]="sortDirection"
        >
        </app-main-table>`
})

export class ClientComponent implements OnInit {

    public menuId: string;
    public menuFilters: Array<any>;
    public sortActive: string;
    public sortDirection: string;

    // TODO: filtros en función del ID, revisar id de menu cliente,
    // este componente se reutilizara para otras entidades demandantes => claimComponent
    private availableMenuFilters: any = {
        13: { 'finished': 1, 'closed': 0 },
        30: { 'finished': 1, 'closed': 0 }
    };

    constructor(
        public menuService: ClientService,
        private activeRoute: ActivatedRoute,
        private sessionService: SessionService
    ) {

    }

    ngOnInit() {
        this.activeRoute.data.subscribe(
            data => this.menuId = data.menuId
        );

        const user: Login = this.sessionService.user;
        if (user !== null) {
            if (!user.isAdmin) {
                this.availableMenuFilters[this.menuId].userId = user.id;
            }
            // if (user.lawFirmId !== null) {
            //     this.availableMenuFilters[this.menuId].lawFirmId = user.lawFirmId;
            // }
        }
        this.menuFilters = this.availableMenuFilters[this.menuId];

        /** Default order */
        this.sortActive = 'createdAt';
        this.sortDirection = 'desc';
    }

}
