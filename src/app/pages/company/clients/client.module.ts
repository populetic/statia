import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MainTableModule } from '@app/layout/main-table/main-table.module';
import { SharedModule } from '@app/shared/modules/shared.module';
import { FurySharedModule } from '@fury/fury-shared.module';
import { BreadcrumbsModule } from '@fury/shared/breadcrumbs/breadcrumbs.module';
import { MaterialModule } from '@fury/shared/material-components.module';
import { ClientEditorComponent } from './client-editor/client-editor.component';
import { ClientComponent } from './client.component';
import { LayoutModule } from '@app/layout/layout.module';
import { FormComponentsModule } from '@app/layout/form-components/form-components.module';
import { TranslateModule } from '@ngx-translate/core';
import { ClientRoutingModule } from './client-routing.module';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        BreadcrumbsModule,
        MaterialModule,
        FurySharedModule,

        ClientRoutingModule,
        MainTableModule,
        SharedModule,
        LayoutModule,
        FormComponentsModule,
        TranslateModule
    ],
    declarations: [ClientComponent, ClientEditorComponent],
    exports: [ClientComponent]
})

export class ClientModule {
}
