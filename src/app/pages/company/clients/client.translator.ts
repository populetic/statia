import { Client } from './client.model';
import { ClientContract } from '../../../api/contracts/clientscontract/client.contract';

export class ClientTranslator {
    static translateContractToModel(contract: ClientContract): Client {
        const model: Client = {
            id: (contract && contract.id) ? contract.id : null,
            name: (contract && contract.name) ? contract.name.trim() : null,
            surnames: (contract && contract.surnames) ? contract.surnames.trim() : null,
            email: (contract && contract.email) ? contract.email.replace(/ /g, '') : null,
            phone: (contract && contract.phone) ? contract.phone : null,
            city: (contract && contract.city) ? contract.city : null,
            address: (contract && contract.address) ? contract.address : null,
            countryProvinceId: (contract && contract.countryProvinceId) ? contract.countryProvinceId : null,
            countryId: (contract && contract.countryId) ? contract.countryId : null,
            languageId: (contract && contract.languageId) ? contract.languageId : null,
            idCard: (contract && contract.idCard) ? contract.idCard.replace(/ /g, '') : null,
            documentationTypeId: (contract && contract.documentationTypeId) ?
                contract.documentationTypeId : null,
            idCardExpiresAt: (contract && contract.idCardExpiresAt) ? new Date(contract.idCardExpiresAt) : null,
            legalGuardianId: (contract && contract.legalGuardianId) ? contract.legalGuardianId : null,
            gender: (contract && contract.gender) ? contract.gender : null,
            ip: (contract && contract.ip) ? contract.ip : null,
            userAgent: (contract && contract.userAgent) ? contract.userAgent : null,
            createdAt: (contract && contract.createdAt) ? new Date(contract.createdAt) : null,
            updatedAt: (contract && contract.updatedAt) ? new Date(contract.updatedAt) : null,
            completeName: (contract && contract.completeName) ? contract.completeName : null,
            countryProvinceName: (contract && contract.countryProvinceName) ? contract.countryProvinceName : null,
            countryName: (contract && contract.countryName) ? contract.countryName : null,
            languageName: (contract && contract.languageName) ? contract.languageName : null,
            documentationTypeName: (contract && contract.documentationTypeName) ? contract.documentationTypeName : null,
            legalGuardianName: (contract && contract.legalGuardianName) ? contract.legalGuardianName.trim() : null,
            legalGuardianSurnames: (contract && contract.legalGuardianSurnames) ?
                contract.legalGuardianSurnames.trim() : null,
            legalGuardianIdCard: (contract && contract.legalGuardianIdCard) ?
                contract.legalGuardianIdCard.replace(/ /g, '') : null,
            legalGuardianDocumentationTypeId: (contract && contract.legalGuardianDocumentationTypeId) ?
                contract.legalGuardianDocumentationTypeId : null,
            legalGuardianIdCardExpiresAt: (contract && contract.legalGuardianIdCardExpiresAt) ?
                new Date(contract.legalGuardianIdCardExpiresAt) : null
        };
        return new Client(model);
    }
}
