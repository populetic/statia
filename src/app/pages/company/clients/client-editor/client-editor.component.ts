import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmActionDialogComponent } from '@app/layout/dialogs/confirm-action/confirm-action-dialog.component';
import { EMAIL_PATTERN, FORM_OPERATIONS, ID_CARD_PATTERN, RELATED_ENTITIES, SIZES } from '@app/shared/constants/constants';
import { MODEL_PROPERTY_NAME } from '@app/shared/enums/model-property-name.enum';
import { FilterValue } from '@app/shared/interfaces/filter-values.interface';
import { Result } from '@app/shared/models/result.model';
import { NotificationsService } from '@app/shared/services/notifications.service';
import { SessionService } from '@app/shared/services/session.service';
import { StatiaService } from '@app/shared/services/statia.service';
import { ValidationService } from '@app/shared/services/validation.service';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { PendingInterceptorService } from '@fury/shared/loading-indicator/pending-interceptor.service';
import { Observable, Subscription } from 'rxjs';
import { distinctUntilChanged, map, startWith, tap } from 'rxjs/operators';
import { RequiredCountry } from '../../auxiliaries/countries/required.country.enum';
import { DocumentationType } from '../../auxiliaries/document-types/documentation-type.model';
import { DocumentationTypeService } from '../../auxiliaries/document-types/documentation-type.service';
import { Language } from '../../auxiliaries/languages/language.model';
import { LegalGuardian } from '../../legal-guardians/legal-guardian.model';
import { LegalGuardianService } from '../../legal-guardians/legal-guardian.service';
import { Client } from '../client.model';
import { ClientService } from '../client.service';

@Component({
    selector: 'app-client-editor',
    templateUrl: './client-editor.component.html',
    animations: [
        fadeInRightAnimation,
        fadeInUpAnimation
    ]
})

export class ClientEditorComponent implements OnInit, OnDestroy {

    public SIZES = SIZES;
    public RELATED_ENTITIES = RELATED_ENTITIES;
    private emailPattern = EMAIL_PATTERN;
    private idCardPattern = ID_CARD_PATTERN;

    public client: Client;
    public id: string;
    private menuId: string;
    public parentRoute: string;
    public formAction: string;
    public currentEditor: string;
    public breadcrumbs: string[];
    public editorFormData: FormGroup;

    public availableLanguages: Language[];
    public availableCountries: any[];
    public allAvailableProvinces: any[];
    public availableProvinces: any[];
    public filteredCountries: Observable<any[]>;
    public filteredProvinces: Observable<any[]>;

    public isRequiredCountry = false;
    public canUseFormOperation: boolean;
    public isLoading = false;
    public datePipe = new DatePipe('en-GB');
    private subscriptions: Subscription[] = [];
    public documentationTypes: DocumentationType[] = this.documentationTypeService.userMainDocumentationTypes;
    public cliDocument: DocumentationType;
    public legGDocument: DocumentationType;

    constructor(
        private router: Router,
        private dialog: MatDialog,
        private route: ActivatedRoute,
        private snackbar: MatSnackBar,
        private formBuilder: FormBuilder,
        public clientService: ClientService,
        private statiaService: StatiaService,
        private sessionService: SessionService,
        private validationService: ValidationService,
        private legalGuardianService: LegalGuardianService,
        private notificationsService: NotificationsService,
        private documentationTypeService: DocumentationTypeService,
        private pendingInterceptorService: PendingInterceptorService
    ) {
        this.client = new Client();
        this.cleanDtaTabs();
    }

    ngOnInit() {
        this.route.paramMap.subscribe(paramMap => this.id = paramMap.get('id'));
        this.route.data.subscribe(data => this.menuId = data?.menuId);

        this.formAction = (this.id === null ? 'create' : 'edit');
        const routeToReplace = (this.id === null ? '/' + this.formAction : '/' + this.formAction + '/' + this.id);
        this.parentRoute = this.router.url.replace(routeToReplace, '');

        const routeItems = this.parentRoute.split('/').filter(item => item !== null && item !== '');
        this.breadcrumbs = routeItems;

        const sub: Subscription = this.pendingInterceptorService.status$.subscribe((value: boolean) => this.isLoading = value);
        this.subscriptions.push(sub);
        if (this.id !== null) {
            this.breadcrumbs.push('edit');
            this.canUseFormOperation = StatiaFunctions.EnabledOption(this.menuId, FORM_OPERATIONS.EDIT);

            this.createFormGroup();
            this.getData();
        }
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
        this.cleanDtaTabs();
    }

    private createFormGroup() {
        const clientForm: any = {
            name: ['', Validators.required],
            surnames: ['', Validators.required],
            documentationTypeId: ['', Validators.required],
            idCard: ['', [Validators.required, Validators.pattern(this.idCardPattern)]],
            idCardExpiresAt: [''],
            address: [''],
            city: ['', Validators.required],
            countryProvinceName: [''],
            countryName: ['', Validators.required],
            email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
            phone: ['', Validators.required],
            languageId: ['', Validators.required],
            gender: [''],
            isMinor: [''],
            legalGuardianName: ['', Validators.required],
            legalGuardianSurnames: ['', Validators.required],
            legalGuardianIdCard: ['', [Validators.required, Validators.pattern(this.idCardPattern)]],
            legalGuardianDocumentationTypeId: ['', Validators.required],
            legalGuardianIdCardExpiresAt: [''],
        };

        this.editorFormData = this.formBuilder.group(clientForm);
    }

    getData() {
        const clientSubs: Subscription = this.clientService.getById(this.id).subscribe(
            (result: Result<Client>) => {
                this.setDataToForm(result.response);
            }
        );
        this.subscriptions.push(clientSubs);
    }

    setDataToForm(client: Client): any {
        this.client = client;
        this.currentEditor = this.client.completeName;
        this.editorFormData.patchValue(client);
        this.editorFormData.patchValue({ isMinor: client.legalGuardianId ? true : false });
        this.cliDocument = client.documentationTypeId ? this.getDocument(client.documentationTypeId) : null;
        this.legGDocument = client.legalGuardianDocumentationTypeId ?
            this.getDocument(client.legalGuardianDocumentationTypeId) : null;

        this.configureForm();
    }

    private async configureForm() {
        this.availableLanguages =
            await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.LANGUAGE_SELECTOR, false, true);
        this.availableCountries = await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.COUNTRY);
        this.allAvailableProvinces = await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.PROVINCE, false);

        this.filteredCountries = this.editorFormData.controls['countryName'].valueChanges.pipe(
            startWith(''),
            map((value: string) => value ?
                this.statiaService._filter(value, this.availableCountries, ['name']) : this.availableCountries.slice()
            )
        );

        this.editorFormData.controls['countryName'].setValidators(
            [Validators.required, this.validationService.validateFieldValue(this.availableCountries, 'name')]
        );

        this.filteredProvinces = this.editorFormData.controls['countryProvinceName'].valueChanges.pipe(
            startWith(''),
            map((value: string) => {
                if (this.availableProvinces) {
                    return (value ? this.statiaService._filter(value, this.availableProvinces, ['name']) :
                        this.availableProvinces.slice());
                }
                return null;
            })
        );

        this.editorFormData.get('countryName').valueChanges.pipe(
            distinctUntilChanged(),
            tap((countryName: string) => {
                this.editorFormData.controls['countryProvinceName'].setValue('');
                this.isRequiredCountry = this.checksRequiredCountry(countryName);
                if (this.isRequiredCountry) {
                    this.processAvailableProvinces(countryName);
                } else {
                    this.availableProvinces = [];
                    this.editorFormData.controls['countryProvinceName'].setValidators([]);
                }
            })
        ).subscribe();

        this.editorFormData.get('isMinor').valueChanges.subscribe(
            (value: boolean) => {
                if (value) {
                    this.enableLegalGuardianControls();
                } else {
                    this.disableLegalGuardianControls();
                }
            }
        );

        this.editorFormData.get('documentationTypeId').valueChanges.pipe(
            distinctUntilChanged(),
            tap((id: string) => {
                this.cliDocument = this.getDocument(id);
            })
        ).subscribe();

        this.editorFormData.get('legalGuardianDocumentationTypeId').valueChanges.pipe(
            distinctUntilChanged(),
            tap((id: string) => {
                this.legGDocument = this.getDocument(id);
            })
        ).subscribe();

        if (this.client) {
            // disable (control & validation) legalGuardian controls
            if (!this.client.legalGuardianId) {
                this.disableLegalGuardianControls();
            }
            // Some clients only have countryName, no provinceName
            this.isRequiredCountry = this.checksRequiredCountry(this.client.countryName);
            if (this.isRequiredCountry) {
                this.processAvailableProvinces(this.client.countryName);
            }
        }
    }

    enableLegalGuardianControls(): any {
        this.editorFormData.controls['legalGuardianName'].enable();
        this.editorFormData.controls['legalGuardianSurnames'].enable();
        this.editorFormData.controls['legalGuardianIdCard'].enable();
        this.editorFormData.controls['legalGuardianDocumentationTypeId'].enable();
    }

    disableLegalGuardianControls(): any {
        this.editorFormData.controls['legalGuardianName'].disable();
        this.editorFormData.controls['legalGuardianSurnames'].disable();
        this.editorFormData.controls['legalGuardianIdCard'].disable();
        this.editorFormData.controls['legalGuardianDocumentationTypeId'].disable();
    }

    processAvailableProvinces(countryName: string) {
        this.statiaService.filterByField(this.availableCountries, 'name', countryName).map((res: any) => {
            if (res && res.id) {
                this.availableProvinces = this.statiaService._filter(res.id, this.allAvailableProvinces, ['countryId']);
                this.editorFormData.controls['countryProvinceName'].setValidators(
                    [
                        Validators.required,
                        this.validationService.validateFieldValue(this.availableProvinces, 'name')
                    ]
                );
            }
        });
    }

    getDocument(id: any): DocumentationType {
        const document: DocumentationType = this.documentationTypes.find((doc: DocumentationType) => doc.id === id);
        if (document?.name && document?.id) {
            document.name = document?.name && document?.id ? 'menuPropertydocumentationType' + document.id : null;
            return document;
        }
        return null;
    }

    onSubmit() {
        if (this.editorFormData.invalid || !this.canUseFormOperation) {
            return;
        }

        let param: any;
        param = this.editorFormData.value;
        param.idCardExpiresAt = (param && param.idCardExpiresAt) ?
            this.datePipe.transform(param.idCardExpiresAt, 'yyyy-MM-dd') : null;
        param.email = param.email.toLocaleLowerCase();

        const selCountry: FilterValue = this.filterByField(this.availableCountries, 'name', param.countryName);
        // if isRequiredCountry === true (Spain for the moment) get province ID & name
        const selProvince: FilterValue = (param && param.countryName && this.isRequiredCountry) ?
            this.filterByField(this.availableProvinces, 'name', param.countryProvinceName) : null;

        param.countryId = (selCountry && selCountry.id) ? selCountry.id : null;
        param.countryProvinceId = (selProvince && selProvince.id) ? selProvince.id : null;

        if (this.client.legalGuardianId && param.isMinor) {
            param.legalGuardianId = this.client.legalGuardianId;
            this.updateLegalGuardian(param);
        } else if (this.client.legalGuardianId && !param.isMinor) {
            this.openConfirmActionDialog(param);
        } else if (param.legalGuardianIdCard) {
            this.createLegalGuardian(param);
        } else {
            this.updateClient(param);
        }
    }

    filterByField(values: any[], field: string, value: string): any {
        const result = this.statiaService.filterByField(values, field, value);
        return (result) ? result[0] : null;
    }

    checksRequiredCountry(country: string): boolean {
        country = (country && country.toLocaleLowerCase());
        // country = country.replace(/ /g, '');
        return this.statiaService.validRequiredCountry(null, country, RequiredCountry);
    }

    getLegalGuardian(param: any): LegalGuardian {
        const model: LegalGuardian = {
            id: (param && param.legalGuardianId) ? param.legalGuardianId : null,
            name: (param && param.legalGuardianName) ? param.legalGuardianName : null,
            surnames: (param && param.legalGuardianSurnames) ? param.legalGuardianSurnames : null,
            idCard: (param && param.legalGuardianIdCard) ? param.legalGuardianIdCard : null,
            documentationTypeId: (param && param.legalGuardianDocumentationTypeId) ? param.legalGuardianDocumentationTypeId : null,
            idCardExpiresAt: (param && param.legalGuardianIdCardExpiresAt) ? param.legalGuardianIdCardExpiresAt : null
        };
        return new LegalGuardian(model);
    }

    updateClient(param: any) {
        param.name = param?.name?.trim();
        param.surnames = param?.surnames?.trim();
        param.email = param?.email?.replace(/ /g, '');
        param.idCard = param?.idCard?.replace(/ /g, '');
        const sub: Subscription = this.clientService.edit(this.id, param).subscribe(
            (resultCli: Result<Client>) => {
                if (resultCli.status === 1) {
                    this.setDataToForm(resultCli.response);
                }
                this.notificationsService.manageServiceResult(resultCli, null, this.router, this.snackbar, this.dialog);
            }
        );
        this.subscriptions.push(sub);
    }

    createLegalGuardian(param: any) {
        const model: LegalGuardian = this.getLegalGuardian(param);
        const sub = this.legalGuardianService.create(this.getLegalGuardian(param)).subscribe(
            (result: Result<LegalGuardian>) => {
                if (result.status === 1) {
                    param.legalGuardianId = (result && result.response && result.response.id) ? result.response.id : null;
                    this.updateClient(param);
                } else {
                    this.notificationsService.manageServiceResult(result, null, this.router, this.snackbar, this.dialog);
                }
            }
        );
        this.subscriptions.push(sub);
    }

    updateLegalGuardian(param: any) {
        const model: LegalGuardian = this.getLegalGuardian(param);
        const sub: Subscription = this.legalGuardianService.edit(model.id, model).subscribe(
            (result: Result<LegalGuardian>) => {
                if (result.status === 1) {
                    this.updateClient(param);
                } else {
                    this.notificationsService.manageServiceResult(result, null, this.router, this.snackbar, this.dialog);
                }
            }
        );
        this.subscriptions.push(sub);
    }

    deleteLegalGuardian(param: any) {
        const legalGuardianId: string = this.client.legalGuardianId;
        param.legalGuardianId = null;
        const sub: Subscription = this.clientService.edit(this.id, param).subscribe(
            (resultCli: Result<Client>) => {
                if (resultCli.status === 1) {
                    const subLegal: Subscription = this.legalGuardianService.deleteByParam([legalGuardianId]).subscribe(
                        (result: Result<LegalGuardian>) => {
                            if (result.status === 1) {
                                this.setDataToForm(resultCli.response);
                            }
                            this.notificationsService.manageServiceResult(resultCli, null, this.router, this.snackbar, this.dialog);
                        }
                    );
                    this.subscriptions.push(subLegal);
                } else {
                    this.notificationsService.manageServiceResult(resultCli, null, this.router, this.snackbar, this.dialog);
                }
            }
        );
        this.subscriptions.push(sub);
    }

    private openConfirmActionDialog(param: any) {
        const dialogData: any = {
            title: 'titleUpdateClient',
            messages: ['msgDeleteLegalGuardian'],
        };
        const propertyDialog = this.dialog.open(ConfirmActionDialogComponent, {
            width: '375px',
            data: dialogData
        });

        const sub: Subscription = propertyDialog.afterClosed().subscribe(action => {
            if (action) {
                this.deleteLegalGuardian(param);
            }
        });
        this.subscriptions.push(sub);
    }

    cleanDtaTabs(): void {
        this.sessionService.cleanDataTabs();
    }
}
