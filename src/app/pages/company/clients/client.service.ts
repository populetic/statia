import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ClaimContract } from '@app/api/contracts/claim-contract/claim.contract';
import { ClientContract } from '@app/api/contracts/clientscontract/client.contract';
import { DemandContract } from '@app/api/contracts/demand-contract/demand.contract';
import { ApiClientService } from '@app/api/services/api-client-service/api-client.service';
import { RELATED_ENTITIES } from '@app/shared/constants/constants';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { SessionService } from '@app/shared/services/session.service';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { Bill } from '../bills/bill.model';
import { Claim } from '../claims/claim.model';
import { ClaimTranslator } from '../claims/claim.translator';
import { Demand } from '../demands/demand.model';
import { DemandTranslator } from '../demands/demand.translator';
import { Client } from './client.model';
import { ClientTranslator } from './client.translator';
import { BillContract } from '@app/api/contracts/bill-contract/bill.contract';
import { BillTranslator } from '../bills/bill.translator';

@Injectable({ providedIn: 'root' })
export class ClientService implements BaseInterface<Client> {
    public resourceUrl: string = environment.apiUrl + '/clients';
    public headers: HttpHeaders;

    constructor(
        private http: HttpClient,
        private apiClientService: ApiClientService,
        private sessionService: SessionService
    ) {}

    getAll(customParams: CustomParams = null): Observable<Result<Client[]>> {
        return this.apiClientService.getAll(customParams)
            .pipe(
                map((result: Result<ClientContract[]>) => {
                    const newResult: Result<Client[]> = StatiaFunctions.getResultModel<ClientContract[], Client[]>(result);
                    newResult.response = result.response.map((client: ClientContract) => ClientTranslator.translateContractToModel(client));
                    return  newResult;
                })
            );
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<Client[]>> {
        return new Promise((resolve, reject) => {
            this.apiClientService.getAllAsync(customParams)
                .then((result: Result<ClientContract[]>) => {
                    const newResult: Result<Client[]> = StatiaFunctions.getResultModel<ClientContract[], Client[]>(result);
                    newResult.response = result.response.map((client: ClientContract) =>
                        ClientTranslator.translateContractToModel(client));
                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    getById(id: number|string): Observable<Result<Client>> {
        return this.apiClientService.getById(id).pipe(
            map((result: Result<ClientContract>) => {
                const newResult: Result<Client> = StatiaFunctions.getResultModel<ClientContract, Client>(result);
                const client: Client = ClientTranslator.translateContractToModel(result.response);
                newResult.response = client;

                return  newResult;
            })
        );
    }

    create(param: Client): Observable<Result<Client>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: Client): Promise<Result<Client>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: Client): Observable<Result<Client>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: Client): Promise<Result<Client>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    deleteByParam(param: any): Observable<Result<Client>> {
        const json = JSON.stringify(param);
        const stringParams = 'json=' + json;
        const params = new HttpParams({ fromString: stringParams });
        const Clients = { headers: this.headers, params };

        return this.http.delete(this.resourceUrl + '/delete', Clients);
    }


    getRelatedClaims(clientId: string): Observable<Result<Claim[]>> {
        return this.apiClientService.getRelatedClaims(clientId)
            .pipe(
                map((result: Result<ClaimContract[]>) => {
                    const newResult: Result<Claim[]> = StatiaFunctions.getResultModel<ClaimContract[], Claim[]>(result);
                    newResult.response = result.response.map((claim: ClaimContract) => ClaimTranslator.translateContractToModel(claim));
                    return  newResult;
                })
            );
    }

    getRelatedDemands(clientId: string): Observable<Result<Demand[]>> {
        return this.apiClientService.getRelatedDemands(clientId)
            .pipe(
                map((result: Result<DemandContract[]>) => {
                    const newResult: Result<Demand[]> = StatiaFunctions.getResultModel<DemandContract[], Demand[]>(result);
                    newResult.response = result.response.map((demand: DemandContract) => DemandTranslator.translateContractToModel(demand));
                    return  newResult;
                })
            );
    }

    getRelatedBills(clientId: string): Observable<Result<Bill[]>> {
        return this.apiClientService.getRelatedBills(clientId)
            .pipe(
                map((result: Result<BillContract[]>) => {
                    const newResult: Result<Bill[]> = StatiaFunctions.getResultModel<BillContract[], Bill[]>(result);
                    newResult.response = result.response.map((bill: BillContract) => BillTranslator.translateContractToModel(bill));
                    return  newResult;
                })
            );
    }

    getRelatedEntity(currentTab: string, clientId: string): Promise<Claim[] | Demand[] | Bill[]> {
        return new Promise((resolve) => {
            const dataByTab: any = this.sessionService.getDataByTab(currentTab);
            if (!dataByTab) {
                const sub: Subscription = this.getRelatedEntityFromAPI(currentTab, clientId)
                    .subscribe((result: Result<Claim[] | Demand[] | Bill[]>) => {
                        this.sessionService.setDataByTab(currentTab, result.response);
                        sub.unsubscribe();
                        resolve(result.response);
                    });
            } else {
                resolve(dataByTab);
            }
        });
    }

    getRelatedEntityFromAPI(currentTab: string, clientId: string): Observable<Result<Claim[] | Demand[] | Bill[]>> {
        switch (currentTab) {
            case RELATED_ENTITIES.CLIENT.CLAIMS:
                return this.getRelatedClaims(clientId);
            case RELATED_ENTITIES.CLIENT.DEMANDS:
                return this.getRelatedDemands(clientId);
            case RELATED_ENTITIES.CLIENT.BILLS:
                return this.getRelatedBills(clientId);
        }
    }
}
