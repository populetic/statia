import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientEditorComponent } from './client-editor/client-editor.component';
import { ClientComponent } from './client.component';

const routes: Routes = [
        {
            path: '',
            component: ClientComponent
        },
        {
            path: 'edit/:id',
            component: ClientEditorComponent
        }
    ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ClientRoutingModule { }
