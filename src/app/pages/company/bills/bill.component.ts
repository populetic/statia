import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { BillService } from './bill.service';

@Component({
    selector: 'app-menu',
    template: `<app-main-table [actualMenuId]="menuId" [actualMenuService]="menuService"></app-main-table>`
})

export class BillComponent implements OnInit {

    public menuId: string;

    constructor(
        private activeRoute: ActivatedRoute,
        public menuService: BillService
    ) {}

    ngOnInit() {
        this.activeRoute.data.subscribe(
            data => this.menuId = data.menuId
        );
    }
}
