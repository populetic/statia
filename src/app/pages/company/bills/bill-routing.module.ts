import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BillEditorComponent } from './bill-editor/bill-editor.component';
import { BillComponent } from './bill.component';

const routes: Routes = [
        {
            path: '',
            component: BillComponent
        },
        {
            path: 'create',
            component: BillEditorComponent
        },
        {
            path: 'edit/:id',
            component: BillEditorComponent
        }
    ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BillRoutingModule { }
