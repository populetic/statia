import { ID_MENU_BILL } from '@app/shared/constants/constants';
import { BillContract } from '../../../api/contracts/bill-contract/bill.contract';
import { Bill } from './bill.model';

export class BillTranslator {

    static translateContractToModel(contract?: BillContract) {
        const model: Bill = {
            id: contract.id ? contract.id : null,
            base: contract.base ? Number(contract.base) : null,
            billPreferenceId: contract.billPreferenceId ? contract.billPreferenceId : null,
            billTypeId: contract.billTypeId ? contract.billTypeId : null,
            billTypeName: contract.billTypeName ? contract.billTypeName : null,
            claimBillingDataId: contract.claimBillingDataId ? contract.claimBillingDataId : null,
            claimId: contract.claimId ? contract.claimId : null,
            clientId: contract.clientId ? contract.clientId : null,
            comments: contract.comments ? contract.comments : null,
            createdAt: contract.createdAt && contract?.createdAt !== '0000-00-00 00:00:00' ?
                new Date(contract.createdAt) : null,
            disruptionTypeId: contract.disruptionTypeId ? contract.disruptionTypeId : null,
            expiresAt: contract.expiresAt && contract.expiresAt !== '0000-00-00 00:00:00' ?
                new Date(contract.expiresAt) : null,
            flightId: contract.flightId ? contract.flightId : null,
            iva: contract.iva ? Number(contract.iva) : null,
            ivaTotal: contract.ivaTotal ? Number(contract.ivaTotal) : null,
            number: contract.number ? contract.number : null,
            parentBillId: contract.parentBillId ? contract.parentBillId : null,
            total: contract.total ? Number(contract.total) : null,
            series: contract?.series ? contract.series : null,
            billRef: contract?.series + '-' + contract?.number,
            menuId: ID_MENU_BILL
        };

        return new Bill(model);
    }
}
