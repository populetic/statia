import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormComponentsModule } from '@app/layout/form-components/form-components.module';
import { MainTableModule } from '@app/layout/main-table/main-table.module';
import { FurySharedModule } from '@fury/fury-shared.module';
import { BreadcrumbsModule } from '@fury/shared/breadcrumbs/breadcrumbs.module';
import { MaterialModule } from '@fury/shared/material-components.module';
import { BillEditorComponent } from './bill-editor/bill-editor.component';
import { BillComponent } from './bill.component';
import { BillRoutingModule } from './bill-routing.module';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        BreadcrumbsModule,
        MaterialModule,
        FurySharedModule,

        BillRoutingModule,
        FormComponentsModule,
        MainTableModule
    ],
    declarations: [BillComponent, BillEditorComponent],
    exports: [BillComponent]
})

export class BillModule {
}
