export class Bill {
    id: string;
    base?: number;
    billPreferenceId?: string;
    billTypeId?: string;
    billTypeName?: string;
    claimBillingDataId?: string;
    claimId?: string;
    clientId?: string;
    comments?: any; // TODO:
    createdAt?: Date;
    disruptionTypeId?: string;
    expiresAt?: Date;
    flightId?: string;
    iva?: number;
    ivaTotal?: number;
    number?: string;
    parentBillId?: any; // TODO:
    total?: number;
    series?: string;
    billRef?: string;
    menuId?: string;

    constructor(model?: Bill) {
        this.id = (model && model.id) ? model.id : null;
        this.base = (model && model.base) ? model.base : null;
        this.billPreferenceId = (model && model.billPreferenceId) ? model.billPreferenceId : null;
        this.billTypeId = (model && model.billTypeId) ? model.billTypeId : null;
        this.billTypeName = (model && model.billTypeName) ? model.billTypeName : null;
        this.claimBillingDataId = (model && model.claimBillingDataId) ? model.claimBillingDataId : null;
        this.claimId = (model && model.claimId) ? model.claimId : null;
        this.clientId = (model && model.clientId) ? model.clientId : null;
        this.comments = (model && model.comments) ? model.comments : null;
        this.createdAt = (model && model.createdAt) ? model.createdAt : null;
        this.disruptionTypeId = (model && model.disruptionTypeId) ? model.disruptionTypeId : null;
        this.expiresAt = (model && model.expiresAt) ? model.expiresAt : null;
        this.flightId = (model && model.flightId) ? model.flightId : null;
        this.iva = (model && model.iva) ? model.iva : null;
        this.ivaTotal = (model && model.ivaTotal) ? model.ivaTotal : null;
        this.number = (model && model.number) ? model.number : null;
        this.parentBillId = (model && model.parentBillId) ? model.parentBillId : null;
        this.total = (model && model.total) ? model.total : null;
        this.series = model?.series ? model.series : null ;
        this.billRef = model?.billRef ? model.billRef : null;
        this.menuId = model?.menuId ? model.menuId : null;
    }

}
