export class LegalGuardian {

    id?: string;
    name: string;
    surnames: string;
    idCard: string;
    documentationTypeId: string;
    documentationTypeName?: string;
    idCardExpiresAt: Date | string;

    constructor(model?: LegalGuardian) {
        this.id = (model && model.id) ? model.id : null ;
        this.name = (model && model.name) ? model.name : null ;
        this.surnames = (model && model.surnames) ? model.surnames : null ;
        this.idCard = (model && model.idCard) ? model.idCard : null ;
        this.documentationTypeId = (model && model.documentationTypeId) ? model.documentationTypeId : null ;
        this.documentationTypeName = (model && model.documentationTypeName) ? model.documentationTypeName : null ;
        this.idCardExpiresAt = (model && model.idCardExpiresAt) ? model.idCardExpiresAt : null ;
    }
}
