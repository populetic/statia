import { Injectable } from '@angular/core';
import { ApiLegalGuardianService } from '@app/api/services/api-legal-guardian-service/api-legal-guardian.service';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LegalGuardianContract } from '../../../api/contracts/legal-guadian-contract/legal-guardian.contract';
import { LegalGuardian } from './legal-guardian.model';
import { LegalGuardianTranslator } from './legal-guardian.translator';

@Injectable({ providedIn: 'root' })
export class LegalGuardianService implements BaseInterface<LegalGuardian> {
    private translator = LegalGuardianTranslator;

    constructor(
        private apiLegalGuardianService: ApiLegalGuardianService
    ) {}

    getAll(param: CustomParams = null): Observable<Result<LegalGuardian[]>> {
        return this.apiLegalGuardianService.getAll(param)
            .pipe(
                map((result: Result<LegalGuardianContract[]>) =>
                    StatiaFunctions.buildResultArrayModel<LegalGuardianContract, LegalGuardian>(result, this.translator)
                )
            );
    }

    getAllAsync(param: CustomParams = null): Promise<Result<LegalGuardian[]>> {
        return new Promise((resolve, reject) => {
            this.apiLegalGuardianService.getAllAsync(param)
                .then((result: Result<LegalGuardianContract[]>) =>
                    resolve(StatiaFunctions.buildResultArrayModel(result, this.translator))
                )
                .catch((err) => reject(err));
        });
    }

    getById(id: number|string): Observable<Result<LegalGuardian>> {
        return this.apiLegalGuardianService.getById(id)
            .pipe(
                map((result: Result<LegalGuardianContract>) => StatiaFunctions.buildResultModel(result, this.translator))
            );
    }

    create(model: LegalGuardian): Observable<Result<LegalGuardian>> {
        return this.apiLegalGuardianService.create(LegalGuardianTranslator.translateModelToContract(model))
            .pipe(
                map((result: Result<LegalGuardianContract>) => StatiaFunctions.buildResultModel(result, this.translator))
            );
    }

    createAsync(model: LegalGuardian): Promise<Result<LegalGuardian>> {
        return new Promise((resolve, reject) => {
            this.apiLegalGuardianService.createAsync(LegalGuardianTranslator.translateModelToContract(model))
                .then((result: Result<LegalGuardianContract>) =>
                    resolve(StatiaFunctions.buildResultModel(result, this.translator))
                )
                .catch((err) => reject(err));
        });
    }

    edit(id: number|string, model: LegalGuardian): Observable<Result<LegalGuardian>> {
        return this.apiLegalGuardianService.edit(id, LegalGuardianTranslator.translateModelToContract(model))
            .pipe(
                map((result: Result<LegalGuardianContract>) => StatiaFunctions.buildResultModel(result, this.translator))
            );
    }

    editAsync(id: number|string, model: LegalGuardian): Promise<Result<LegalGuardian>> {
        return new Promise((resolve, reject) => {
            this.apiLegalGuardianService.editAsync(id, LegalGuardianTranslator.translateModelToContract(model))
                .then((result: Result<LegalGuardianContract>) =>
                    resolve(StatiaFunctions.buildResultModel(result, this.translator))
                )
                .catch((err) => reject(err));
        });
    }

    deleteByParam(param: CustomParams): Observable<Result<LegalGuardian>> {
        return this.apiLegalGuardianService.deleteByParam(param);
    }
}
