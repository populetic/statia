import { LegalGuardian } from './legal-guardian.model';
import { LegalGuardianContract } from '../../../api/contracts/legal-guadian-contract/legal-guardian.contract';
import { DateFunctions } from '@app/utils/date.functions';

export class LegalGuardianTranslator {

    static translateContractToModel(contract: LegalGuardianContract): LegalGuardian {
        const model: LegalGuardian = {
            id: (contract && contract.id) ? contract.id : null,
            name: (contract && contract.name) ? contract.name.trim() : null,
            surnames: (contract && contract.surnames) ? contract.surnames.trim()  : null,
            idCard: (contract && contract.idCard) ? contract.idCard.replace(/ /g, '') : null,
            documentationTypeId: (contract && contract.documentationTypeId) ? contract.documentationTypeId : null,
            documentationTypeName: (contract && contract.documentationTypeName) ? contract.documentationTypeName : null,
            idCardExpiresAt: (contract && contract.idCardExpiresAt) ? new Date(contract.idCardExpiresAt) : null,
        };

        return new LegalGuardian(model);
    }

    static translateModelToContract(model: LegalGuardian): LegalGuardianContract {
        const contract: LegalGuardianContract = {
            id: model?.id ?? null,
            name: model?.name ? model.name.trim() : null,
            surnames: model?.surnames ? model?.surnames.trim() : null,
            idCard: model?.idCard ? model?.idCard.replace(/ /g, '') : null,
            documentationTypeId: model?.documentationTypeId ?? null,
            documentationTypeName: model?.documentationTypeName ?? null,
            idCardExpiresAt: model?.idCardExpiresAt ? DateFunctions.formatDate(model.idCardExpiresAt)  : null,
        };

        return contract;
    }
}
