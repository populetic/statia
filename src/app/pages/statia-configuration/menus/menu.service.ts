import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MenuContract } from '@app/api/contracts/menu-contract/menu.contract';
import { ApiMenuService } from '@app/api/services/api-menu-service/api-menu.service';
import { STATIA_AVAILABLE_MENUS } from '@app/shared/constants/constants';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { SessionService } from '@app/shared/services/session.service';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Menu } from './menu.model';
import { MenuTranslator } from './menu.translator';

@Injectable({ providedIn: 'root' })
export class MenuService implements BaseInterface<Menu> {
    public resourceUrl = environment.apiUrlStatia + '/menus';
    public headers: HttpHeaders;
    private translator = MenuTranslator;

    constructor (
        private http: HttpClient,
        private sessionService: SessionService,
        private apiMenuService: ApiMenuService
    ) { }

     getAll(customParams: CustomParams = null): Observable<Result<Menu[]>> {
        return this.apiMenuService.getAll(customParams)
            .pipe(
                map((result: Result<MenuContract[]>) =>
                    StatiaFunctions.buildResultArrayModel<MenuContract, Menu>(result, this.translator)
                )
            );
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<Menu[]>> {
        return new Promise((resolve, reject) => {
            this.apiMenuService.getAllAsync(customParams)
                .then((result: Result<MenuContract[]>) =>
                    resolve(StatiaFunctions.buildResultArrayModel<MenuContract, Menu>(result, this.translator))
                )
                .catch((err) => reject(err));
                });
    }

    getById(id: number|string): Observable<Result<Menu>> {
        return this.apiMenuService.getById(id)
            .pipe(
                map((result: Result<MenuContract>) => StatiaFunctions.buildResultModel<MenuContract, Menu>(result, this.translator))
            );
    }

    create(model: Menu): Observable<Result<Menu>> {
        return this.apiMenuService.create(this.translator.translateModelToContract(model))
            .pipe(
                map((result: Result<MenuContract>) => StatiaFunctions.buildResultModel<MenuContract, Menu>(result, this.translator))
            );
    }

    createAsync(model: Menu): Promise<Result<Menu>> {
        return new Promise((resolve, reject) => {
            this.apiMenuService.createAsync(this.translator.translateModelToContract(model))
                .then((result: Result<MenuContract>) =>
                    resolve(StatiaFunctions.buildResultModel<MenuContract, Menu>(result, this.translator))
                )
                .catch((err) => reject(err));
                });
    }

    edit(id: number|string, model: Menu): Observable<Result<Menu>> {
        return this.apiMenuService.edit(id, this.translator.translateModelToContract(model))
            .pipe(
                map((result: Result<MenuContract>) => StatiaFunctions.buildResultModel<MenuContract, Menu>(result, this.translator))
            );
    }

    editAsync(id: number|string, model: Menu): Promise<Result<Menu>> {
        return new Promise((resolve, reject) => {
            this.apiMenuService.editAsync(id, this.translator.translateModelToContract(model))
                .then((result: Result<MenuContract>) =>
                    resolve(StatiaFunctions.buildResultModel<MenuContract, Menu>(result, this.translator))
                )
                .catch((err) => reject(err));
                });
    }

    deleteByParam(param: CustomParams): Observable<Result<any>> {
        return this.apiMenuService.deleteByParam(param);
    }

    getByRoleId(id: string): Observable<Result<Menu[]>> {
        return this.apiMenuService.getByRoleId(id)
            .pipe(
                map((result: Result<MenuContract[]>) => {
                    const newResult: Result<Menu[]> = StatiaFunctions.getResultModel<MenuContract[], Menu[]>(result);
                    newResult.response = result.response.map((menu: MenuContract) => MenuTranslator.translateContractToModel(menu));
                    return  newResult;
                })
            );
    }

    getMenuByIdFromStorage(menuId: string): Menu {
        const menus: Menu[] = this.getMenusStorage();
        const menu: Menu = menus ? menus.find((m: Menu) => m.id === String(menuId)) : null;

        return menu;
    }

    getMenusStorage(): Menu[] {
        const menus: Menu[] = this.sessionService.getItem(STATIA_AVAILABLE_MENUS);

        return menus;
    }

    setMenusStorage(menus: Menu[]): void {
        this.sessionService.setItem(STATIA_AVAILABLE_MENUS, JSON.stringify(menus));
    }

    editRoleMenusOptions(param: CustomParams): Observable<any> {
        return this.apiMenuService.deletePropertyByParam(param);
    }

    deletePropertyByParam(param: CustomParams): Observable<any> {
        return this.apiMenuService.deletePropertyByParam(param);
    }

    editOptions(param: CustomParams): Observable<any> {
        return this.apiMenuService.editOptions(param);
    }

    createProperty(param: CustomParams): Observable<any> {
        return this.apiMenuService.createProperty(param);
    }

    editProperty(param: CustomParams): Observable<any> {
        return this.apiMenuService.editProperty(param);
    }

    getMenusRoleOption(menuId: string, roleId: string): Observable<Result<Menu[]>> {
        return this.apiMenuService.getMenusRoleOption(menuId, roleId)
            .pipe(
                map((result: Result<MenuContract[]>) => {
                    const newResult: Result<Menu[]> = StatiaFunctions.getResultModel<MenuContract[], Menu[]>(result);
                    newResult.response = result.response.map((menu: MenuContract) => MenuTranslator.translateContractToModel(menu));
                    return  newResult;
                })
            );
    }
}
