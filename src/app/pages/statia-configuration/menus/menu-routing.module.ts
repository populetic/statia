import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuEditorComponent } from './menu-editor/menu-editor.component';
import { MenuComponent } from './menu.component';

const routes: Routes = [
        {
            path: '',
            component: MenuComponent
        },
        {
            path: 'create',
            component: MenuEditorComponent
        },
        {
            path: 'edit/:id',
            component: MenuEditorComponent
        }
    ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MenuRoutingModule { }
