import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MenuService } from './menu.service';

@Component({
    selector: 'app-menu',
    template: `<app-main-table
                    [actualMenuId]="menuId"
                    [actualMenuService]="menuService"
                    [actualMenuSortActive]="sortActive"
                    [actualMenuSortDirection]="sortDirection">
                </app-main-table>`
})

export class MenuComponent implements OnInit {

    public menuId: string;
    public sortActive: string;
    public sortDirection: string;

    constructor(
        private activeRoute: ActivatedRoute,
        public menuService: MenuService
    ) { }

    ngOnInit() {
        this.activeRoute.data.subscribe(
            data => this.menuId = data.menuId
        );

        this.sortActive = 'position';
        this.sortDirection = 'asc';
    }
}
