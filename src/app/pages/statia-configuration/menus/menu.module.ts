import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MainTableModule } from '@app/layout/main-table/main-table.module';
import { FurySharedModule } from '@fury/fury-shared.module';
import { BreadcrumbsModule } from '@fury/shared/breadcrumbs/breadcrumbs.module';
import { MaterialModule } from '@fury/shared/material-components.module';
import { MenuEditorComponent } from './menu-editor/menu-editor.component';
import { MenuRoutingModule } from './menu-routing.module';
import { MenuComponent } from './menu.component';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        BreadcrumbsModule,
        MaterialModule,
        FurySharedModule,

        MenuRoutingModule,
        MainTableModule
    ],
    declarations: [MenuComponent, MenuEditorComponent],
    exports: [MenuComponent]
})

export class MenuModule {
}
