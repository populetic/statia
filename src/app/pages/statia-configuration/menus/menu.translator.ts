import { MenuContract } from '@app/api/contracts/menu-contract/menu.contract';
import { OptionContract } from '@app/api/contracts/option-contract/option.contract';
import { PropertyContract } from '@app/layout/dialogs/menu-property-dialog/property.contract';
import { PropertyTranslator } from '@app/layout/dialogs/menu-property-dialog/property.translator';
import { OptionTranslator } from '../options/option.translator';
import { Menu } from './menu.model';

export class MenuTranslator {
    static translateContractToModel(contract?: MenuContract): Menu {
        const model: Menu = {
            id: contract?.id || contract?.menu ? this.getId(contract.id, contract.menu) : null,
            name: contract?.name ?? null,
            parentId: contract?.parentId ?? null,
            position: contract?.position ? Number(contract.position) : null,
            icon: contract?.icon ?? null,
            hasCheckbox: contract?.hasCheckbox && contract?.hasCheckbox === '1' ? true : false,
            hasMultipleDelete: contract?.hasMultipleDelete && contract?.hasMultipleDelete === '1' ? true : false,
            hasTableMenu: contract?.hasTableMenu && contract?.hasTableMenu === '1' ? true : false,
            parentName: contract?.parentName ?? null,
            options: (contract && contract.options && contract.options.length) ?
                contract.options.map( (o: OptionContract) => OptionTranslator.translateContractToModel(o)) : [],
            properties: (contract && contract.properties && contract.properties.length) ?
                contract.properties.map((p: PropertyContract) => PropertyTranslator.translateContractToModel(p)) : [],
        };

        return new Menu(model);
    }

    static getId(id: string, menu: string): string {
        if (id) {
            return id;
        } else if (menu) {
            return menu;
        }
        return null;
    }

    static translateModelToContract(model: Menu): MenuContract {
        const contract: MenuContract = {
            id: model?.id ?? null,
            name: model?.name ?? null,
            parentId: model?.parentId ?? null,
            position: model?.position ? String(model?.position) : null,
            icon: model?.icon ?? null,
            hasCheckbox: model?.hasCheckbox ? '1' : '0',
            hasTableMenu: model?.hasTableMenu ? '1' : '0',
            hasMultipleDelete: model?.hasMultipleDelete ? '1' : '0'
        };

        return contract;
    }

}
