import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuOptionDialogComponent } from '@app/layout/dialogs/menu-option-dialog/menu-option-dialog.component';
import { MenuPropertyDialogComponent } from '@app/layout/dialogs/menu-property-dialog/menu-property-dialog.component';
import { Property } from '@app/layout/dialogs/menu-property-dialog/property.model';
import { AVAILABLE_ICONS } from '@app/shared/available-icons';
import { PAGINATOR } from '@app/shared/constants/constants';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { ListColumn } from '@fury/shared/list/list-column.model';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Menu } from '../menu.model';
import { MenuService } from '../menu.service';
import { NotificationsService } from '@app/shared/services/notifications.service';

@Component({
    selector: 'app-menu-editor',
    templateUrl: './menu-editor.component.html',
    styleUrls: ['./menu-editor.component.scss'],
    animations: [fadeInRightAnimation, fadeInUpAnimation]
})

export class MenuEditorComponent implements OnInit, OnDestroy {

    public parentRoute: string;
    public formAction: string;
    public currentEditor: string;
    public breadcrumbs: string[];

    public editorFormData: FormGroup;
    public menu: Menu;
    private id: any;

    private icons = AVAILABLE_ICONS;
    public iconCtrl: FormControl;
    public filteredIcons$: Observable<string[]>;
    public availableMenus: any;

    subscriptions: Subscription[] = [];

    public pageSizeOptions = PAGINATOR.LARGE.OPTIONS;
    public pageSize = PAGINATOR.LARGE.SIZE;

    public columnsPropertiesTable: any;
    public dataPropertiesTable: MatTableDataSource<any>;
    @ViewChild('PropertiesMatPaginator') propertiesTablePaginator: MatPaginator;
    @ViewChild('PropertiesMatSort') propertiesSort: MatSort;

    public columnsOptionsTable: any;
    public dataOptionsTable: MatTableDataSource<any>;
    @ViewChild('OptionsMatPaginator') optionsTablePaginator: MatPaginator;
    @ViewChild('OptionsMatSort') optionsSort: MatSort;

    constructor(
        private snackbar: MatSnackBar,
        private dialog: MatDialog,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private menuService: MenuService,
        private notificationsService: NotificationsService

    ) {
        this.iconCtrl = new FormControl();
        this.menu = new Menu();

        this.columnsPropertiesTable = [] as ListColumn[];
        this.dataPropertiesTable = new MatTableDataSource<any>();

        this.columnsOptionsTable = [] as ListColumn[];
        this.dataOptionsTable = new MatTableDataSource<any>();
    }

    ngOnInit() {
        this.route.paramMap.subscribe((paramMap: { get: (arg0: string) => any; }) => {
            this.id = paramMap.get('id');
        });

        this.formAction = this.id === null ? 'create' : 'edit';

        const actualPath = this.id === null ? ('/' + this.formAction) : ('/' + this.formAction + '/' + this.id);
        this.parentRoute = this.router.url.replace(actualPath, '');

        const routeItems = this.parentRoute.split('/').filter(item => item !== null && item !== '');
        this.breadcrumbs = routeItems;

        if (this.id !== null) {
            this.breadcrumbs.push('edit');
            this.getData();
        } else {
            this.currentEditor = 'create';
        }

        this.filteredIcons$ = this.iconCtrl.valueChanges.pipe(
            startWith(<string>null),
            map((search: string) => this.icons.filter(icon => icon.indexOf(search ? search.toLowerCase() : '') !== -1))
        );

        this.createFormGroup();
        this.configureForm();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
    }

    private createFormGroup() {
        const menuForm: any = {
            name: ['', Validators.required],
            parentId: [false],
            position: ['', Validators.required],
            hasCheckbox: [false],
            hasMultipleDelete: [false],
            hasTableMenu: [false],
            icon: ['']
        };

        this.editorFormData = this.formBuilder.group(menuForm);
    }

    private async configureForm() {
        await new Promise((resolve) => {
            this.menuService.getAllAsync(null).then((result: any) => {
                this.availableMenus = result.response.filter((menu: { parentId: any; }) => menu.parentId === null);
                resolve();
            });
        });
    }

    getData() {
        const sub: Subscription = this.menuService.getById(this.id).subscribe(
            (result: { response: Menu; }) => {
                let hasCheckboxVal = false;
                let hasMultipleDeleteVal = false;
                let hasTableMenuVal = false;

                this.menu = result.response;

                this.editorFormData.patchValue({ name: this.menu.name });
                this.editorFormData.patchValue({ parentId: this.menu.parentId });
                this.editorFormData.patchValue({ position: this.menu.position });
                if (this.menu.hasCheckbox) {
                    hasCheckboxVal = true;
                }
                if (this.menu.hasMultipleDelete) {
                    hasMultipleDeleteVal = true;
                }
                if (this.menu.hasTableMenu) {
                    hasTableMenuVal = true;
                }

                this.editorFormData.patchValue({ hasCheckbox: hasCheckboxVal });
                this.editorFormData.patchValue({ hasMultipleDelete: hasMultipleDeleteVal });
                this.editorFormData.patchValue({ hasTableMenu: hasTableMenuVal });

                if (this.menu.icon !== '') {
                    this.iconCtrl.setValue(this.menu.icon, { emitEvent: true });
                    this.editorFormData.patchValue({ icon: this.menu.icon });
                }

                /* Properties tab configuration */
                const propertiesColumns: Array<any> = [ // TODO: ListColumn @fury?
                    { name: 'field', isBoolean: false },
                    { name: 'sort', isBoolean: false },
                    { name: 'filter', isBoolean: true },
                    { name: 'isAdvancedFilter', isBoolean: true },
                    { name: 'isFilterSelected', isBoolean: true },
                    { name: 'onlyForAdmin', isBoolean: true },
                    { name: 'isBoolean', isBoolean: true },
                    { name: 'isNumber', isBoolean: true },
                    { name: 'isDate', isBoolean: true },
                    { name: 'isSimpleFilter', isBoolean: true },
                    { name: 'canBeTranslated', isBoolean: true },
                    { name: 'actions', isBoolean: false }
                ];

                this.columnsPropertiesTable = propertiesColumns as ListColumn[];

                this.dataPropertiesTable = new MatTableDataSource(this.menu.properties);
                this.dataPropertiesTable.paginator = this.propertiesTablePaginator;
                this.dataPropertiesTable.sort = this.propertiesSort;

                /* Options tab configuration */
                const optionsColumns: Array<any> = [
                    { name: 'name', isBoolean: false },
                    { name: 'icon', isBoolean: false },
                    { name: 'actions', isBoolean: false }
                ];

                this.columnsOptionsTable = optionsColumns as ListColumn[];

                this.dataOptionsTable = new MatTableDataSource(this.menu.options);
                this.dataOptionsTable.paginator = this.optionsTablePaginator;
                this.dataOptionsTable.sort = this.optionsSort;

                this.currentEditor = this.menu.name[0].toUpperCase() + this.menu.name.substr(1).toLowerCase();
            }
        );
        this.subscriptions.push(sub);
    }

    get visibleColumnsPropertiesTable() {
        return this.columnsPropertiesTable.map((column: { name: any; }) => column.name);
    }

    get visibleColumnsOptionsTable() {
        return this.columnsOptionsTable.map((column: { name: any; }) => column.name);
    }

    selectIcon(icon: string) {
        this.menu.icon = icon;
        this.editorFormData.patchValue({ icon: this.menu.icon });
        this.iconCtrl.setValue(this.menu.icon, { emitEvent: true });
    }

    checkIconValue(event: any) {
        if (event.target.value === '') {
            this.menu.icon = '';
            this.editorFormData.patchValue({ icon: '' });
            this.iconCtrl.setValue('', { emitEvent: true });
        }
    }

    propertiesTableFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataPropertiesTable.filter = filterValue.trim().toLowerCase();

        if (this.dataPropertiesTable.paginator) {
            this.dataPropertiesTable.paginator.firstPage();
        }
    }

    optionsTableFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataOptionsTable.filter = filterValue.trim().toLowerCase();

        if (this.dataOptionsTable.paginator) {
            this.dataOptionsTable.paginator.firstPage();
        }
    }

    onSubmit() {
        if (this.editorFormData.invalid) {
            return;
        }

        let pathToRedirect: string = null;
        const param: any = this.editorFormData.value;

        param.hasCheckbox = (param.hasCheckbox ? 1 : 0);
        param.hasMultipleDelete = (param.hasMultipleDelete ? 1 : 0);
        param.hasTableMenu = (param.hasTableMenu ? 1 : 0);

        param.parentId = (param.parentId === undefined ? null : param.parentId);
        let sub: Subscription;

        if (this.formAction === 'create') {
            sub = this.menuService.create(param).subscribe(
                result => {
                    pathToRedirect = this.parentRoute + '/edit/:id';
                    this.notificationsService.manageServiceResult(result, pathToRedirect, this.router, this.snackbar, this.dialog);
                }
            );
        } else {
            sub = this.menuService.edit(this.id, param).subscribe(
                result => {
                    this.notificationsService.manageServiceResult(result, pathToRedirect, this.router, this.snackbar, this.dialog);
                }
            );
        }
        this.subscriptions.push(sub);
    }

    openAddProperty(): void {
        const dialogData: Property = {
            menuId: this.id,
            action: 'create',
            title: 'New property',
            field: '', // false
            sort: 0, // false
            filter: false,
            isAdvancedFilter: false,
            isFilterSelected: false,
            onlyForAdmin: false,
            isBoolean: false,
            isNumber: false,
            isDate: false,
            isSimpleFilter: false,
            canBeTranslated: false
        };

        this.openPropertyDialog(MenuPropertyDialogComponent, dialogData);
    }

    openEditProperty(property: Property): void {
        const dialogData: Property = {
            menuId: this.id,
            action: 'edit',
            title: 'Edit property',
            field: property.field,
            sort: property.sort,
            filter: property.filter,
            isAdvancedFilter: property.isAdvancedFilter,
            isFilterSelected: property.isFilterSelected,
            onlyForAdmin: property.onlyForAdmin,
            isBoolean: property.isBoolean,
            isNumber: property.isNumber,
            isDate: property.isDate,
            isSimpleFilter: property.isSimpleFilter,
            canBeTranslated: property.canBeTranslated
        };

        this.openPropertyDialog(MenuPropertyDialogComponent, dialogData);
    }

    private openPropertyDialog(DialogComponent: any, dialogData: Property) {
        const propertyDialog = this.dialog.open(DialogComponent, {
            width: '670px',
            data: dialogData
        });

        propertyDialog.afterClosed().subscribe(saved => {
            if (saved) {
                this.getData();
            }
        });
    }

    deleteProperty(field: String) {
        const param = {
            menuId: this.id,
            field: field
        };

        const sub: Subscription = this.menuService.deletePropertyByParam(param).subscribe(
            result => {
                this.getData();
                this.notificationsService.manageServiceResult(result, null, null, this.snackbar, this.dialog);
            }
        );
        this.subscriptions.push(sub);
    }

    openAddOptions() {
        const selectedOptionIds = this.menu.options.map(({ id }) => id);
        const dialogData = {
            title: 'Select available options',
            menuId: this.id,
            optionsSelected: selectedOptionIds
        };

        this.openOptionDialog(MenuOptionDialogComponent, dialogData);
    }

    private openOptionDialog(DialogComponent: any, dialogData: any) {
        const optionDialog = this.dialog.open(DialogComponent, {
            width: '475px',
            data: dialogData
        });

        optionDialog.afterClosed().subscribe(saved => {
            if (saved) {
                this.getData();
            }
        });
    }

    deleteOption(selectedId: string) {
        const selectedOptionIds = this.menu.options.map(({ id }) => id);
        selectedOptionIds.splice(selectedOptionIds.indexOf(selectedId), 1);

        const param: any = {
            menuId: this.id,
            optionIds: selectedOptionIds
        };

        const sub: Subscription = this.menuService.editOptions(param).subscribe(
            result => {
                this.getData();
                this.notificationsService.manageServiceResult(result, null, null, this.snackbar, this.dialog);
            }
        );
        this.subscriptions.push(sub);
    }
}
