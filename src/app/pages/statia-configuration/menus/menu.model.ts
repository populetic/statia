import { Option } from '../options/option.model';
import { Property } from '@app/layout/dialogs/menu-property-dialog/property.model';

export class Menu {
    id: string;
    name?: string;
    parentId?: string;
    position?: number;
    icon?: string;
    hasCheckbox?: boolean;
    hasMultipleDelete?: boolean;
    hasTableMenu?: boolean;
    options?: Array<Option>;
    properties?: Array<Property>;
    parentName?: string;


    // tslint:disable: no-use-before-declare
    constructor(model?: Menu) {
        this.id = (model && model.id ) ? model.id : null;
        this.name = (model && model.name ) ? model.name : null;
        this.parentId = (model && model.parentId ) ? model.parentId : null;
        this.position = (model && model.position ) ? model.position : null;
        this.icon = (model && model.icon ) ? model.icon : null;
        this.hasCheckbox = (model && model.hasCheckbox ) ? model.hasCheckbox : false;
        this.hasMultipleDelete = (model && model.hasMultipleDelete ) ? model.hasMultipleDelete : false;
        this.hasTableMenu = (model && model.hasTableMenu ) ? model.hasTableMenu : false;
        this.parentName = (model && model.parentName ) ? model.parentName : null;
        this.options = (model && model.options && model.options.length) ?
            model.options.map( (o: Option) => new Option(o)) : [];
        this.properties = (model && model.properties && model.properties.length) ?
            model.properties.map((p: Property) => new Property(p)) : [];
    }
}
