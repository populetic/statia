import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { FORM_OPERATIONS } from '@app/shared/constants/constants';
import { NotificationsService } from '@app/shared/services/notifications.service';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { AVAILABLE_ICONS } from '@shared/available-icons';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Option } from '../option.model';
import { OptionService } from '../option.service';

@Component({
    selector: 'app-option-editor',
    templateUrl: './option-editor.component.html',
    styleUrls: ['./option-editor.component.scss'],
    animations: [fadeInRightAnimation, fadeInUpAnimation]
})

export class OptionEditorComponent implements OnInit, OnDestroy {

    public parentRoute: string;
    public formAction: string;
    public currentEditor: string;
    public breadcrumbs: string[];

    public editorFormData: FormGroup;
    public option: Option;
    private id: any;

    public canUseFormOperation: boolean;
    public isLoading = false;
    private menuId: string;

    private icons = AVAILABLE_ICONS;
    public iconCtrl: FormControl;
    public filteredIcons$: Observable<string[]>;

    subscriptions: Subscription[] = [];

    constructor(
        private snackbar: MatSnackBar,
        private dialog: MatDialog,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private optionService: OptionService,
        private notificationsService: NotificationsService
    ) {
        this.iconCtrl = new FormControl();
        this.option = new Option();
    }

    ngOnInit() {
        this.route.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id');
        });

        this.formAction = (this.id === null ? 'create' : 'edit');
        const routeToReplace = (this.id === null ? '/' + this.formAction : '/' + this.formAction + '/' + this.id);
        this.parentRoute = this.router.url.replace(routeToReplace, '');

        const routeItems = this.parentRoute.split('/').filter(item => item !== null && item !== '');
        this.breadcrumbs = routeItems;

        this.canUseFormOperation = StatiaFunctions.EnabledOption(this.menuId, FORM_OPERATIONS[this.formAction.toUpperCase()]);
        this.breadcrumbs.push(this.formAction);

        if (this.id !== null) {
            this.getData();
        } else {

        }

        this.filteredIcons$ = this.iconCtrl.valueChanges.pipe(
            startWith(<string>null),
            map(search => this.icons.filter(icon => icon.indexOf(search ? search.toLowerCase() : '') !== -1))
        );

        this.createFormGroup();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
    }

    getData() {
        const sub: Subscription = this.optionService.getById(this.id).subscribe(
            result => {
                this.option = result.response;

                this.editorFormData.patchValue({ name: this.option.name });

                this.editorFormData.patchValue({ onHeaderMenu: this.option.onHeaderMenu });
                this.editorFormData.patchValue({ onTableMenu: this.option.onTableMenu });

                if (this.option.icon !== '') {
                    this.iconCtrl.setValue(this.option.icon, { emitEvent: true });
                    this.editorFormData.patchValue({ icon: this.option.icon });
                }

                this.currentEditor = this.option.name[0].toUpperCase() + this.option.name.substr(1).toLowerCase();
            }
        );
        this.subscriptions.push(sub);
    }

    createFormGroup() {
        this.editorFormData = this.formBuilder.group({
            name: ['', Validators.required],
            icon: [''],
            onHeaderMenu: [false],
            onTableMenu: [false]
        });
    }

    onSubmit() {
        if (this.editorFormData.invalid) {
            return;
        }

        let param: any;
        param = this.editorFormData.value;
        param.onHeaderMenu = (param.onHeaderMenu ? 1 : 0);
        param.onTableMenu = (param.onTableMenu ? 1 : 0);
        let sub: Subscription;

        if (this.formAction === 'create') {
            sub = this.optionService.create(param).subscribe(
                result => {
                    this.notificationsService.manageServiceResult(result, this.parentRoute, this.router, this.snackbar, this.dialog);
                }
            );
        } else {
            sub = this.optionService.edit(this.id, param).subscribe(
                result => {
                    this.notificationsService.manageServiceResult(result, this.parentRoute, this.router, this.snackbar, this.dialog);
                }
            );
        }
        this.subscriptions.push(sub);
    }

    selectIcon(icon: string) {
        this.option.icon = icon;
        this.editorFormData.patchValue({ icon: this.option.icon });
        this.iconCtrl.setValue(this.option.icon, { emitEvent: true });
    }

    checkIconValue(event: any) {
        if (event.target.value === '') {
            this.option.icon = '';
            this.editorFormData.patchValue({ icon: '' });
            this.iconCtrl.setValue('', { emitEvent: true });
        }
    }
}
