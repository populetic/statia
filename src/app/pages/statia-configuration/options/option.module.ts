import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { FurySharedModule } from '@fury/fury-shared.module';
import { BreadcrumbsModule } from '@fury/shared/breadcrumbs/breadcrumbs.module';
import { MaterialModule } from '@fury/shared/material-components.module';
import { MainTableModule } from '@app/layout/main-table/main-table.module';
import { OptionComponent } from './option.component';
import { OptionEditorComponent } from './option-editor/option-editor.component';
import { FormComponentsModule } from '@app/layout/form-components/form-components.module';
import { OptionRoutingModule } from './option-routing.module';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        BreadcrumbsModule,
        MaterialModule,
        FurySharedModule,

        OptionRoutingModule,
        MainTableModule,
        FormComponentsModule
    ],
    declarations: [OptionComponent, OptionEditorComponent],
    exports: [OptionComponent]
})

export class OptionModule {
}
