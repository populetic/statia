import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { OptionService } from './option.service';

@Component({
    selector: 'app-option',
    template: `<app-main-table [actualMenuId]="menuId" [actualMenuService]="menuService"></app-main-table>`
})

export class OptionComponent implements OnInit {

    public menuId: string;

    constructor(
        private activeRoute: ActivatedRoute,
        public menuService: OptionService
    ) {}

    ngOnInit() {
        this.activeRoute.data.subscribe(
            data => this.menuId = data.menuId
        );
    }
}
