
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OptionComponent } from './option.component';
import { MENU_ID } from '@app/shared/enums/menu.enum';
import { OptionEditorComponent } from './option-editor/option-editor.component';

export const routes: Routes = [
        {
            path: '',
            component: OptionComponent
        },
        {
            path: 'create',
            component: OptionEditorComponent
        },
        {
            path: 'edit/:id',
            component: OptionEditorComponent
        }
    ];

    @NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
    export class OptionRoutingModule { }
