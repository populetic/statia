import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { OptionContract } from '@app/api/contracts/option-contract/option.contract';
import { ApiOptionService } from '@app/api/services/api-options-service/api-option.service';
import { BaseInterface } from '@app/shared/interfaces/base.interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { Result } from '@app/shared/models/result.model';
import { SessionService } from '@app/shared/services/session.service';
import { StatiaFunctions } from '@app/utils/statia.functions';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Option } from './option.model';
import { OptionTranslator } from './option.translator';

@Injectable({ providedIn: 'root' })
export class OptionService implements BaseInterface<Option> {
    public resourceUrl = environment.apiUrlStatia + '/options';
    public headers: HttpHeaders;

    constructor (
            public http: HttpClient,
            private apiOptionService: ApiOptionService,
            private sessionService: SessionService,
        ) { }

    getAll(customParams: CustomParams = null): Observable<Result<Option[]>> {
        return this.apiOptionService.getAll(customParams)
            .pipe(
                map((result: Result<OptionContract[]>) => {
                    const newResult: Result<Option[]> = StatiaFunctions.getResultModel<OptionContract[], Option[]>(result);
                    newResult.response = result.response.map((court: OptionContract) => OptionTranslator.translateContractToModel(court));
                    return  newResult;
                })
            );
    }

    getAllAsync(customParams: CustomParams = null): Promise<Result<Option[]>> {
        return new Promise((resolve, reject) => {
            this.apiOptionService.getAllAsync(customParams)
                .then((result: Result<OptionContract[]>) => {
                    const newResult: Result<Option[]> = StatiaFunctions.getResultModel<OptionContract[], Option[]>(result);
                    newResult.response = result.response.map((court: OptionContract) => OptionTranslator.translateContractToModel(court));
                    resolve(newResult);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    getById(id: number|string): Observable<Result<Option>> {
        return this.apiOptionService.getById(id).pipe(
            map((result: Result<OptionContract>) => {
                const newResult: Result<Option> = StatiaFunctions.getResultModel<OptionContract, Option>(result);
                const option: Option = OptionTranslator.translateContractToModel(result.response);
                newResult.response = option;

                return  newResult;
            })
        );
    }

    create(param: Option): Observable<Result<Option>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params);
    }

    createAsync(param: Option): Promise<Result<Option>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.post(this.resourceUrl + '/create', params).toPromise();
    }

    edit(id: number|string, param: Option): Observable<Result<Option>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params);
    }

    editAsync(id: number|string, param: Option): Promise<Result<Option>> {
        const json = JSON.stringify(param);
        const params = 'json=' + encodeURIComponent(json);
        return this.http.put(this.resourceUrl + '/edit/' + id, params).toPromise();
    }

    // stand by trello task 223
    deleteByParam(param: CustomParams): Observable<Result<Option>> {
        return this.apiOptionService.deleteByParam(param);
    }

    editRoleMenusOptions(option: Option): Observable<Result<Option>> {
        const json = JSON.stringify(option);
        const params = 'json=' + json;

        return this.http.post(environment.apiUrlStatia + '/menus-role-options/edit', params);
    }
}
