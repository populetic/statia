export class Option {
    id: string;
    name: string;
    icon: string;
    onHeaderMenu: boolean;
    onTableMenu: boolean;
    position: number;

    constructor(model?: Option) {
        this.id = (model && model.id ) ? model.id : null;
        this.name = (model && model.name ) ? model.name : null;
        this.icon = (model && model.icon ) ? model.icon : null;
        this.onHeaderMenu = (model && model.onHeaderMenu ) ? model.onHeaderMenu : false;
        this.onTableMenu = (model && model.onTableMenu ) ? model.onTableMenu : false;
        this.position = (model && model.position ) ? model.position : null;
    }
}
