import { Option } from '@app/pages/statia-configuration/options/option.model';
import { OptionContract } from '@app/api/contracts/option-contract/option.contract';

export class OptionTranslator {
    static translateContractToModel(contract: OptionContract): Option {
        // debugger;
        const model: Option = {
            id: (contract && contract.id ) ? contract.id : null,
            name: (contract && contract.name ) ? contract.name : null,
            icon: (contract && contract.icon ) ? contract.icon : null,
            onHeaderMenu: (contract && contract.onHeaderMenu &&
                (contract.onHeaderMenu === '1') ) ? true : false,
            onTableMenu: (contract && contract.onTableMenu &&
                (contract.onTableMenu === '1')) ? true : false,
            position: (contract && contract.position && contract.position !== '0') ? Number(contract.position) : 0,
        };

        return new Option(model);
    }

    static translateModelToContract(model: Option): OptionContract {
        const contract: OptionContract = {
            id: (model && model.id ) ? model.id.toString() : null,
            name: (model && model.name ) ? model.name : null,
            icon: (model && model.icon ) ? model.icon : null,
            onHeaderMenu: (model && model.onHeaderMenu ) ? '1' : '0',
            onTableMenu: (model && model.onTableMenu ) ? '1' : '0'
        };

        return new OptionContract(contract);
    }
}
