import { NgModule } from '@angular/core';
import { FurySharedModule } from '@fury/fury-shared.module';
import { BreadcrumbsModule } from '@fury/shared/breadcrumbs/breadcrumbs.module';
import { ListModule } from '@fury/shared/list/list.module';
import { MaterialModule } from '@fury/shared/material-components.module';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared/modules/shared.module';
import { FormComponentsModule } from '../form-components/form-components.module';
import { MainTableComponent } from './main-table.component';

@NgModule({
    imports: [
        MaterialModule,
        FurySharedModule,
        TranslateModule,
        SharedModule,

        ListModule,
        BreadcrumbsModule,
        FormComponentsModule
    ],
    declarations: [MainTableComponent],
    exports: [MainTableComponent]
})
export class MainTableModule {
}
