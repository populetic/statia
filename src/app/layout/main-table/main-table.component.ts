import { Component, Input, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Login } from '@app/pages/login/login.model';
import { Menu } from '@app/pages/statia-configuration/menus/menu.model';
import { MenuService } from '@app/pages/statia-configuration/menus/menu.service';
import { Option } from '@app/pages/statia-configuration/options/option.model';
import { OptionService } from '@app/pages/statia-configuration/options/option.service';
import { DATE_END, DATE_START, ID_MENU_ACTIVE_CLAIMS, PAGINATOR, TIMEOUTS } from '@app/shared/constants/constants';
import { MODEL_PROPERTY_NAME } from '@app/shared/enums/model-property-name.enum';
import { BooleanParam } from '@app/shared/interfaces/boolean-param-interface';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { SessionService } from '@app/shared/services/session.service';
import { StatiaService } from '@app/shared/services/statia.service';
import { ValidationService } from '@app/shared/services/validation.service';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { ListColumn } from '@fury/shared/list/list-column.model';
import { PendingInterceptorService } from '@fury/shared/loading-indicator/pending-interceptor.service';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject, Observable, ReplaySubject, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Property } from '../dialogs/menu-property-dialog/property.model';
import { DatePickerComponent } from '../form-components/date-picker/date-picker.component';
import { SelectorComponent } from '../form-components/selector/selector.component';

@Component({
    selector: 'app-main-table',
    templateUrl: './main-table.component.html',
    styleUrls: ['./main-table.component.scss'],
    animations: [fadeInRightAnimation, fadeInUpAnimation],
    providers: [MenuService, StatiaService]
})

export class MainTableComponent implements OnInit, OnDestroy {

    private completeConfiguration: BooleanParam = {
        loadData: false,
        configureOptions: false,
        configureMenu: false
    };
    private subject$: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
    private configurationTable: BehaviorSubject<BooleanParam> = new BehaviorSubject(this.completeConfiguration);
    private data$: Observable<any[]> = this.subject$.asObservable();
    private configTable$: Observable<BooleanParam> = this.configurationTable.asObservable();
    private parentRoute: string;

    public actualMenuName: string;
    public breadcrumbs: Array<string>;
    public listColumProperties: Array<any>;
    public headerOptions: Array<Option>;
    public tableOptions: Array<Option>;
    private hasMultipleDelete: Boolean;

    private rowsChecked: Array<Number>;

    private keyActualMenu: string;

    private subscriptions: Subscription[] = [];
    public booleanSelector: any;
    public statusLoading = false;
    public dateEnd = DATE_END;
    public dateStart = DATE_START;

    @Input() actualMenuId: string;
    @Input() actualMenuService: any;
    @Input() actualMenuFilters: any;
    @Input() actualMenuSortActive: string;
    @Input() actualMenuSortDirection: string;

    @Input() columns: ListColumn[];
    pageSize = PAGINATOR.LARGE.SIZE;
    pageSizeOptions = PAGINATOR.LARGE.OPTIONS;
    dataSource: MatTableDataSource<any> | null;

    @ViewChildren(DatePickerComponent) datePickers: QueryList<DatePickerComponent>;
    @ViewChildren(SelectorComponent) selectors: QueryList<SelectorComponent>;

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChildren('checkBoxRows') checkBoxRows: QueryList<any>;

    @Input() hideAdvancedFilter: Boolean = true;
    @Input() hasAdvancedFilter: Boolean;
    @Input() searchFilterActivated: Boolean;
    @Input() advancedFilterActivated: Boolean;

    constructor(
        private router: Router,
        private snackbar: MatSnackBar,
        private menuService: MenuService,
        private translate: TranslateService,
        private statiaService: StatiaService,
        private sessionService: SessionService,
        private validationService: ValidationService,
        private pendingService: PendingInterceptorService,
        private optionService: OptionService
    ) {
        this.dataSource = new MatTableDataSource();
        this.columns = [] as ListColumn[];
        this.rowsChecked = [];
        this.hasMultipleDelete = false;
        this.pendingService.status$.subscribe((status: boolean) => this.statusLoading = status);
    }

    ngOnInit() {
        this.parentRoute = this.router.url;
        this.actualMenuName = this.getActualMenuName();
        this.breadcrumbs = this.getBreadcrumbs();

        this.loadData();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
    }

    getBreadcrumbs() {
        const routeItems = this.parentRoute.split('/').filter(item => item !== null && item !== '');
        routeItems.splice(-1, 1);
        return routeItems;
    }

    getActualMenuName() {
        return this.parentRoute.split('/').filter(item => item !== null && item !== '').pop();
    }

    loadData() {
        const menuSubs: Subscription = this.actualMenuService.getAll(this.actualMenuFilters).subscribe(
            (result: { response: any[]; }) => {
                this.subject$.next(result.response);
            }
        );

        this.subscriptions.push(menuSubs);

        this.data$.pipe(filter(Boolean)).subscribe((rows: Array<Option>) => {
            this.dataSource.data = rows;
            this.completeConfiguration.loadData = true;
            this.configurationTable.next(this.completeConfiguration);
        });

        this.configTable$.subscribe((config: any) => {
            if (config?.loadData === true && config?.configureOptions === true && config?.configureMenu === true) {

                this.columns.forEach( async (c: ListColumn) => {
                    const filterValues: CustomParams =
                        c.isAdvancedFilter && !c.isSimpleFilter ? await this.getAdvancedFiltersInColumns(c) : null;

                    this.hasAdvancedFilter = c.isAdvancedFilter && (c.isSimpleFilter || filterValues) ? true : this.hasAdvancedFilter;
                    c.advancedFilterValues = filterValues;
                });
            }
        });

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

        this.configureTableByMenu(this.actualMenuId);
    }

    configureTableByMenu(actualMenuId: string): void {
        const menu: Menu = this.menuService.getMenuByIdFromStorage(actualMenuId);
        if (menu) {
            this.configureTableMenu(menu);
            this.configureOptionsMenu(menu?.options);
            return;
        }

        this.snackbar.open(this.translate.instant('snbErrorMenuByIdUndefined'), 'Close', { duration: TIMEOUTS['3'] });
        this.sessionService.logOut();
    }

    configureTableMenu(menu: Menu) {
        const responseMenuColumns: Property[] = menu.properties;
        const user: Login = this.sessionService.user;
        // TODO menuColumns = PropertyModel => Optional fields?
        //                    Other: ListColumn=> add to model canFilter,isFilterSelected..?
        let menuColumns: Array<any> = [];

        if (responseMenuColumns.length > 0) {

            if (menu.hasCheckbox) {
                menuColumns.push({ name: 'Checkbox', property: 'checkbox', visible: true });
            }

            responseMenuColumns.forEach(async (column: Property) => {
                let advancedFilterValues: any = {};

                if (column.isAdvancedFilter) {
                    if (!user.isAdmin && column.onlyForAdmin ||
                        (!user.isAdmin && !user.isLawFirmAdmin && column.field === MODEL_PROPERTY_NAME.USER &&
                        this.actualMenuId === ID_MENU_ACTIVE_CLAIMS)
                        ) {
                        return;
                    } else if (!column.isSimpleFilter && (column.onlyForAdmin && user.isAdmin || !column.onlyForAdmin)) {
                        if (this.hasAdvancedFilter === undefined) {
                            this.hasAdvancedFilter = true;
                        }
                        const customFilter: CustomParams = this.getCustomFilter(user, column, this.actualMenuId);
                        advancedFilterValues =
                            await this.statiaService.getPropertyValues(column.field, undefined, column.canBeTranslated, customFilter);
                    }
                }

                if (column.onlyForAdmin && user.isAdmin || !column.onlyForAdmin) {
                    menuColumns.push(this.buildColumn(column, advancedFilterValues));
                    menuColumns = this.statiaService.sortByField(menuColumns, 'sort');
                }
            }, this);

            if (menu.hasTableMenu) {
                menuColumns.push({ name: 'Actions', property: 'actions', visible: true });
            }
        }

        this.columns = menuColumns as ListColumn[];
        this.completeConfiguration.configureMenu = true;
        this.configurationTable.next(this.completeConfiguration);

    }
    async getAdvancedFiltersInColumns(column: ListColumn): Promise<Array<any>> {

        let advancedFilterValues: Array<any>;
        const user: Login = this.sessionService.user;
        if (column.isAdvancedFilter) {
            if (!user.isAdmin && column.onlyForAdmin ||
                (!user.isAdmin && !user.isLawFirmAdmin && column.property === MODEL_PROPERTY_NAME.USER &&
                this.actualMenuId === ID_MENU_ACTIVE_CLAIMS)
                ) {
                    advancedFilterValues = [];
            } else if (!column.isSimpleFilter && (column.onlyForAdmin && user.isAdmin || !column.onlyForAdmin)) {
                const customFilter: CustomParams = this.getCustomFilter(user, column, this.actualMenuId);
                advancedFilterValues =
                    await this.statiaService.getPropertyValues(column.property, undefined, column.canBeTranslated, customFilter);
            }
        }
        return advancedFilterValues;
    }

    buildColumn( column: Property, advancedFilterValues: any): ListColumn {
        return {
            name: column.field,
            property: column.field,
            visible: column.sort > 0 ? true : false,
            sort: column.sort,
            isModelProperty: true,
            canFilter: column.filter,
            isAdvancedFilter: column.isAdvancedFilter,
            advancedFilterValues: advancedFilterValues,
            isFilterSelected: column.isFilterSelected,
            onlyForAdmin: column.onlyForAdmin,
            isBoolean: column.isBoolean,
            isNumber: column.isNumber,
            isDate: column.isDate,
            isSimpleFilter: column.isSimpleFilter,
            canBeTranslated: column.canBeTranslated
        };
    }

    configureOptionsMenu(options: Option[]) {
        this.headerOptions = options.filter((option: Option) => option.onHeaderMenu);
        this.tableOptions = options.filter((option: Option) => option.onTableMenu);

        this.completeConfiguration.configureOptions = true;
        this.configurationTable.next(this.completeConfiguration);
    }

    get visibleColumns() {
        return this.columns.filter(column => column.visible).map(column => column.property);
    }

    onFilterChange(filterSelected: any, value: any) {

        if (!this.dataSource) {
            return;
        }

        const activeFilters: Array<any> = this.dataSource.filter ? JSON.parse(this.dataSource.filter) : {};
        let selectedValue: string;

        if (filterSelected === 'search' && this.columns.filter(column => column.isFilterSelected).length === 0) {
            this.snackbar.open(this.translate.instant('snbSelectAtLeastOneFieldToFilter.'), 'Close', { duration: TIMEOUTS['3'] });
            return;
        }

        if (this.validValue(value)) {
            selectedValue = String(value).trim().toLowerCase();
        }

        if (!this.validValue(selectedValue)) {
            delete activeFilters[filterSelected];
        } else {
            activeFilters[filterSelected] = selectedValue;
        }

        /** Ejecuta filtrado */
        this.dataSource.filterPredicate = (data) => {
            let filterResult: boolean;
            const allFilterResults: any = {};

            // Filtrado por búsqueda
            if (activeFilters.hasOwnProperty('search')) {
                this.searchFilterActivated = true;
                this.columns.filter(column => column.isFilterSelected).forEach(column => {
                    if (activeFilters.hasOwnProperty('search') && column.isFilterSelected) {

                        const propertyToFind = column.canBeTranslated && this.validValue(data[column.property]) ?
                            this.translate.instant(column.property + data[column.property]) : data[column.property];

                        if (allFilterResults['search'] === undefined || !allFilterResults['search']) {
                            if (propertyToFind !== '' && propertyToFind !== null &&
                                String(propertyToFind).toLowerCase().indexOf(activeFilters['search']) !== -1) {
                                allFilterResults['search'] = true;
                            } else {
                                allFilterResults['search'] = false;
                            }
                        }
                    }
                });
            } else {
                this.searchFilterActivated = false;
            }

            // Filtrado por selectores
            const advancedFiltersSelected: Array<any> = Object.keys(activeFilters).filter(
                advFilter => advFilter.indexOf('search') === -1
            );

            if (Object.keys(advancedFiltersSelected).length > 0) {
                this.advancedFilterActivated = true;

                advancedFiltersSelected.forEach((activeFilter: string) => {
                    this.columns.filter(column => activeFilter === column.name ||
                        activeFilter === column.name + this.dateStart ||
                        activeFilter === column.name + this.dateEnd).forEach(column => {
                            // Simple Filter
                            if (!column.isBoolean && !column.isDate && column.isSimpleFilter
                                && String(data[column.property]).toLowerCase().indexOf(activeFilters[column.property])
                                !== -1) {
                                    allFilterResults[activeFilter] = true;
                            // Selector filter
                            } else if (column.isBoolean === false && column.isDate === false
                                && column.isSimpleFilter === false &&
                                String(data[column.property]) === activeFilters[activeFilter]) {
                                    allFilterResults[activeFilter] = true;
                            // Boolean filter
                            } else if (column.isBoolean === true && column.isSimpleFilter === false &&
                                (activeFilters[activeFilter] === '1' && this.validValue(data[column.property])) ||
                                (activeFilters[activeFilter] === '0' && !this.validValue(data[column.property]))) {
                                    allFilterResults[activeFilter] = true;
                            // Date Filter
                            } else if (column.isDate === true &&  this.validValue(data[column.property] &&
                                this.validValue(activeFilters[activeFilter]))) {
                                    const startDate: Date = this.statiaService.formatDate(activeFilters[activeFilter]);
                                    const endDate: Date = this.statiaService.formatDate(data[column.property]);
                                    allFilterResults[activeFilter] =
                                    this.compareDates(startDate, endDate, activeFilter);
                            } else {
                                allFilterResults[activeFilter] = false;
                            }

                    });
                });
            } else {
                this.advancedFilterActivated = false;
            }

            if (!Object.values(allFilterResults).includes(false)) {
                filterResult = true;
            }

            return filterResult;
        };

        this.dataSource.filter = JSON.stringify(activeFilters);
    }

    compareDates( startDate: Date, endDate: Date, fieldToCompare: string): boolean {
        if (startDate <= endDate && fieldToCompare.endsWith(this.dateStart)) {
            return true;
        } else if (startDate >= endDate && fieldToCompare.endsWith(this.dateEnd)) {
            return true;
        } else {
            return false;
        }
    }

    clearAllFilters() {
        this.searchFilterActivated = false;
        this.advancedFilterActivated = false;
        this.datePickers.forEach(data => data.reset());
        this.selectors.forEach(data => data.reset());
        this.dataSource.filter = '';
        const menusByMenuId: Menu = this.sessionService.getItem(this.keyActualMenu);
        this.configureTableMenu(menusByMenuId);
    }

    updateCheckedRows(row: number, event: any) {
        if (event.checked) {
            this.rowsChecked.push(row);
        } else {
            this.rowsChecked.splice(this.rowsChecked.indexOf(row), 1);
        }
    }

    checkUncheckAllRows(event: any) {
        this.rowsChecked = [];

        this.checkBoxRows.forEach((checkBox) => {
            if (event.checked) {
                checkBox.checked = true;
                this.rowsChecked.push(checkBox.value);
            } else {
                checkBox.checked = false;
            }
        });
    }

    clickedOption(option: string, selectedId: any) {
        switch (option) {
            case 'create':
                this.router.navigate([this.parentRoute + '/' + option]);
                break;
            case 'edit':
                this.router.navigate([this.parentRoute + '/' + option + '/' + selectedId]);
                break;
            case 'delete':
                if (selectedId && this.rowsChecked.indexOf(selectedId) === -1) {
                    this.rowsChecked.push(selectedId);
                }

                if (this.rowsChecked.length > 0) {
                    if (!this.hasMultipleDelete && this.rowsChecked.length > 1) {
                        this.snackbar.open(this.translate.instant('snbSelectASingleRecord'), 'Close', { duration: TIMEOUTS['3'] });
                    } else {
                        this.deleteCheckedRows(this.rowsChecked);
                    }
                } else {
                    this.snackbar.open(this.translate.instant('snbSelectAnyRecordToDelete'), 'Close', { duration: TIMEOUTS['3'] });
                }
                break;
        }
    }

    deleteCheckedRows(checkedRows: Array<any>) {
        const menuSubs: Subscription = this.actualMenuService.deleteByParam(checkedRows).subscribe(
            (result: { response: any[]; }) => {
                this.snackbar.open(this.translate.instant('snbDeletedSuccessfully'), 'Close', { duration: TIMEOUTS['3'] });
                this.subject$.next(result.response);
                this.loadData();
                this.checkUncheckAllRows({checked: false});
            }
        );

        this.subscriptions.push(menuSubs);
    }

    validValue(value: any): boolean {
        return this.validationService.validValue(value);
    }

    getCustomFilter(user: Login, column: ListColumn, menuId: string): any {

        if (!user?.isAdmin && column?.property === MODEL_PROPERTY_NAME.USER && user?.isLawFirmAdmin && user?.lawFirmId &&
            menuId === ID_MENU_ACTIVE_CLAIMS) {

            return user?.lawFirmId ? {lawFirmId: user?.lawFirmId} : null;
        }
    }
}
