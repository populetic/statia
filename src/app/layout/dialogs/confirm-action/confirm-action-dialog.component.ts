import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-menu-option-dialog',
    templateUrl: './confirm-action-dialog.component.html'
})
export class ConfirmActionDialogComponent implements OnInit {

    public title: string;
    public messages: string[];
    public btnContinue: string;
    public btnCancel: string;

    constructor(
        @Inject(MAT_DIALOG_DATA) private options: any,
        private dialogRef: MatDialogRef<ConfirmActionDialogComponent>
    ) { }

    ngOnInit() {
        this.title = this.options?.title ?? 'msgConfirmAction';
        this.messages = this.options?.messages ?? null;
        this.btnContinue = this.options?.btnContinue ?? 'btnContinue';
        this.btnCancel = this.options?.btnContinue ?? 'btnCancel';
    }

    confirm() {
        this.dialogRef.close(true);
    }

    cancel() {
        this.dialogRef.close(false);
    }
}
