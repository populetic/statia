import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MenuService } from '@app/pages/statia-configuration/menus/menu.service';
import { OptionService } from '@app/pages/statia-configuration/options/option.service';
import { NotificationsService } from '@app/shared/services/notifications.service';

@Component({
    selector: 'app-menu-option-dialog',
    templateUrl: './menu-option-dialog.component.html'
})
export class MenuOptionDialogComponent implements OnInit, OnDestroy {

    public title: string;
    public menuId: string;
    public optionsAvailable: any;
    // TODO: subscriptions: Subscription[] = [];

    constructor(
        @Inject(MAT_DIALOG_DATA) private options: any,
        private dialogRef: MatDialogRef<MenuOptionDialogComponent>,
        private snackbar: MatSnackBar,
        private dialog: MatDialog,
        private optionService: OptionService,
        private notificationsService: NotificationsService,
        private menuService: MenuService
    ) { }

    ngOnInit() {
        this.title = this.options.title;
        this.menuId = this.options.menuId;

        this.configureAvailableOptions();
    }

    ngOnDestroy(): void {
        // this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
    }

    private configureAvailableOptions() {
        this.optionService.getAll(null).subscribe(
            (result: { response: any[]; }) => {
                const optionsSelected = this.options.optionsSelected;
                this.optionsAvailable = result.response;

                this.optionsAvailable.forEach((optionAvailable: { id: any; }, index: string | number) => {
                    if (optionsSelected.includes(optionAvailable.id)) {
                        this.optionsAvailable[index].selected = true;
                    } else {
                        this.optionsAvailable[index].selected = false;
                    }
                });
            }
        );
    }

    updateCheckedOption(option: number, event: any) {
        if (event.checked) {
            this.options.optionsSelected.push(option);
        } else {
            this.options.optionsSelected.splice(this.options.optionsSelected.indexOf(option), 1);
        }
    }

    save() {
        const param: any = {
            menuId: this.menuId,
            optionIds: this.options.optionsSelected
        };

        this.menuService.editOptions(param).subscribe(
            result => {
                this.notificationsService.manageServiceResult(result, null, null, this.snackbar, this.dialog);
            }
        );

        this.dialogRef.close(true);
    }

    close() {
        this.dialogRef.close(false);
    }
}
