import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'app-simple-dialog',
    templateUrl: './simple-dialog.component.html'
})
export class SimpleDialogComponent implements OnInit {

    title: string;
    content: string;

    constructor(
        @Inject(MAT_DIALOG_DATA) private options: any,
        private dialogRef: MatDialogRef<SimpleDialogComponent>
    ) {}

    ngOnInit() {
        this.title = this.options.title;
        this.content = this.options.content;
    }

    close() {
        this.dialogRef.close(true);
    }
}
