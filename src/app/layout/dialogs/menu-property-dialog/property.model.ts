export class Property {
    field: string;
    sort: number;
    filter: boolean;
    isAdvancedFilter: boolean;
    isFilterSelected: boolean;
    onlyForAdmin: boolean;
    isBoolean: boolean;
    isNumber: boolean;
    isDate: boolean;
    isSimpleFilter: boolean;
    action?: string;
    title?: string;
    menuId?: string;
    canBeTranslated: boolean;

    constructor(model?: Property) {
        this.field = (model && model.field) ? model.field : null;
        this.sort = (model && model.sort && model.sort !== 0 ) ? model.sort : 0;
        this.filter = (model && model.filter) ? model.filter : false;
        this.isAdvancedFilter = (model && model.isAdvancedFilter) ? model.isAdvancedFilter : false;
        this.isFilterSelected = (model && model.isFilterSelected) ? model.isFilterSelected : false;
        this.onlyForAdmin = (model && model.onlyForAdmin) ? model.onlyForAdmin : false;
        this.isBoolean = (model && model.isBoolean) ? model.isBoolean : false;
        this.isNumber = (model && model.isNumber) ? model.isNumber : false;
        this.isDate = (model && model.isDate) ? model.isDate : false;
        this.isSimpleFilter = (model && model.isSimpleFilter) ? model.isSimpleFilter : false;
        this.action = (model && model.action) ? model.action : null;
        this.title = (model && model.title) ? model.title : null;
        this.menuId = (model && model.menuId) ? model.menuId : null;
        this.canBeTranslated = (model && model.canBeTranslated) ? model.canBeTranslated : false;
    }

}
