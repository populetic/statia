import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MenuService } from '@app/pages/statia-configuration/menus/menu.service';
import { Property } from './property.model';
import { NotificationsService } from '@app/shared/services/notifications.service';
@Component({
    selector: 'app-menu-property-dialog',
    templateUrl: './menu-property-dialog.component.html',
    styleUrls: ['./menu-property-dialog.component.scss']
})
export class MenuPropertyDialogComponent implements OnInit, OnDestroy {

    private action: string;
    public title: string;

    public menuId: string;
    public field: string;
    public sort: number;
    public filter: boolean;
    public isAdvancedFilter: boolean;
    public isFilterSelected: boolean;
    public onlyForAdmin: boolean;
    public isBoolean: boolean;
    public isNumber: boolean;
    public isDate: boolean;
    public isSimpleFilter: boolean;
    public canBeTranslated: boolean;
    public editorFormProperty: FormGroup;
    // TODO: subscriptions: Subscription[] = [];

    constructor(
        private dialog: MatDialog,
        private snackbar: MatSnackBar,
        private formBuilder: FormBuilder,
        private menuService: MenuService,
        private notificationsService: NotificationsService,
        @Inject(MAT_DIALOG_DATA) private property: Property,
        private dialogRef: MatDialogRef<MenuPropertyDialogComponent>
    ) { }

    ngOnInit() {
        this.action = this.property.action;
        this.title = this.property.title;

        this.menuId = this.property.menuId;
        this.field = this.property.field;
        this.sort = this.property.sort;
        this.filter = this.property.filter;
        this.isAdvancedFilter = this.property.isAdvancedFilter;
        this.isFilterSelected = this.property.isFilterSelected;
        this.onlyForAdmin = this.property.onlyForAdmin;
        this.isBoolean = this.property.isBoolean;
        this.isNumber = this.property.isNumber;
        this.isDate = this.property.isDate;
        this.isSimpleFilter = this.property.isSimpleFilter;
        this.canBeTranslated = this.property.canBeTranslated;
        this.createFormGroup();

        if (this.action === 'edit') {
            this.setPropertyData();
        }
    }

    ngOnDestroy(): void {
        // this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
    }

    private createFormGroup() {
        const menuForm: any = {
            field: ['', Validators.required],
            sort: [0, Validators.min(0)],
            filter: [false, Validators.required],
            isAdvancedFilter: [false, Validators.required],
            isFilterSelected: [false, Validators.required],
            onlyForAdmin: [false, Validators.required],
            isBoolean: [false, Validators.required],
            isNumber: [false, Validators.required],
            isDate: [false, Validators.required],
            isSimpleFilter: [false, Validators.required],
            canBeTranslated: [false, Validators.required]
        };

        this.editorFormProperty = this.formBuilder.group(menuForm);
    }

    private setPropertyData() {
        this.editorFormProperty.patchValue(this.property);
    }

    save() {
        if (this.editorFormProperty.invalid) {
            return;
        }

        const pathToRedirect = null;
        const router = null;

        let param: any;
        param = this.editorFormProperty.value;
        param.menuId = this.menuId;

        param.sort = (param.sort === '' ? 0 : param.sort);
        param.filter = (param.filter ? 1 : 0);
        param.isAdvancedFilter = (param.isAdvancedFilter ? 1 : 0);
        param.isFilterSelected = (param.isFilterSelected ? 1 : 0);
        param.onlyForAdmin = (param.onlyForAdmin ? 1 : 0);
        param.isBoolean = (param.isBoolean ? 1 : 0);
        param.isNumber = (param.isNumber ? 1 : 0);
        param.isDate = (param.isDate ? 1 : 0);
        param.isSimpleFilter = (param.isSimpleFilter ? 1 : 0);
        param.canBeTranslated = (param.canBeTranslated ? 1 : 0);

        if (this.action === 'create') {
            this.menuService.createProperty(param).subscribe(
                (result: any) => {
                    this.notificationsService.manageServiceResult(result, pathToRedirect, router, this.snackbar, this.dialog);
                }
            );
        } else {
            param.fieldNewName = param.field;
            param.field = this.property.field;
            this.menuService.editProperty(param).subscribe(
                (result: any) => {
                    this.notificationsService.manageServiceResult(result, pathToRedirect, router, this.snackbar, this.dialog);
                }
            );
        }

        this.dialogRef.close(true);
    }

    close() {
        this.dialogRef.close(false);
    }
}
