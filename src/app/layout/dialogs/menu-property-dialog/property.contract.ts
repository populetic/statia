export class PropertyContract {
    field: string;
    sort: string;
    filter: string;
    isAdvancedFilter: string;
    isFilterSelected: string;
    onlyForAdmin: string;
    isBoolean: string;
    isNumber: string;
    isDate: string;
    isSimpleFilter: string;
    canBeTranslated: string;
}
