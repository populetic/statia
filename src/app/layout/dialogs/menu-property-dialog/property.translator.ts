import { Property } from './property.model';
import { PropertyContract } from './property.contract';

export class PropertyTranslator {
    static translateContractToModel(contract: PropertyContract): Property {
        const model: Property = {
            field: contract?.field ?? null,
            sort: contract?.sort && contract?.sort !== '0' ? Number(contract?.sort) : 0,
            filter: contract?.filter === '1' ? true : false,
            isAdvancedFilter: contract?.isAdvancedFilter === '1' ? true : false,
            isFilterSelected: contract?.isFilterSelected === '1' ? true : false,
            onlyForAdmin: contract?.onlyForAdmin === '1' ? true : false,
            isBoolean: contract?.isBoolean === '1' ? true : false,
            isNumber: contract?.isNumber === '1' ? true : false,
            isDate: contract?.isDate === '1' ? true : false,
            isSimpleFilter: contract?.isSimpleFilter === '1' ? true : false,
            canBeTranslated: contract?.canBeTranslated === '1' ? true : false
        };

        return new Property(model);
    }
}
