import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Airport } from '@app/pages/company/auxiliaries/airports/airport.model';
import { ClaimService } from '@app/pages/company/claims/claim.service';
import { ConnectionFlight } from '@app/pages/company/connection-flights/connection-flight.model';
import { ConnectionFlightService } from '@app/pages/company/connection-flights/connection-flight.service';
import { TIMEOUTS } from '@app/shared/constants/constants';
import { MODEL_PROPERTY_NAME } from '@app/shared/enums/model-property-name.enum';
import { Result } from '@app/shared/models/result.model';
import { NotificationsService } from '@app/shared/services/notifications.service';
import { StatiaService } from '@app/shared/services/statia.service';
import { ValidationService } from '@app/shared/services/validation.service';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
    selector: 'app-connection-flight-dialog',
    templateUrl: './connection-flight-dialog.component.html',
    styleUrls: ['./connection-flight-dialog.component.scss']
})
export class ConnectionFlightDialogComponent implements OnInit, OnDestroy {

    private action: string;
    public title: string;

    public claimId: string;
    public connectionFlight: ConnectionFlight;

    public editorForm: FormGroup;

    private availableAirports: any;
    public filteredDepartureAirports: Observable<any>;
    public filteredArrivalAirports: Observable<any>;

    constructor(
        private dialog: MatDialog,
        private snackbar: MatSnackBar,
        private formBuilder: FormBuilder,
        private claimService: ClaimService,
        private translate: TranslateService,
        private statiaService: StatiaService,
        private validationService: ValidationService,
        @Inject(MAT_DIALOG_DATA) private dialogData: any,
        private notificationsService: NotificationsService,
        private connectionFlightService: ConnectionFlightService,
        private dialogRef: MatDialogRef<ConnectionFlightDialogComponent>,
    ) { }

    ngOnInit() {
        this.action = this.dialogData.action;
        this.title = this.dialogData.title;
        this.claimId = this.dialogData.claimId;
        this.connectionFlight = this.dialogData.connectionFlight;

        this.createFormGroup();

        if (this.action === 'edit') {
            this.editorForm.patchValue(this.connectionFlight);
            this.editorForm.patchValue({
                arrivalScheduledTime: this.connectionFlight.arrivalScheduledTime.replace(' ', 'T')
            });
            if (this.connectionFlight.arrivalActualTime !== '' &&
                this.connectionFlight.arrivalActualTime !== undefined &&
                this.connectionFlight.arrivalActualTime !== null) {
                this.editorForm.patchValue({
                    arrivalActualTime: this.connectionFlight.arrivalActualTime
                });
            }
        }
    }

    ngOnDestroy(): void {
        // this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
    }

    private async createFormGroup() {
        const connectionFlightForm: any = {
            order: [1, [Validators.min(1), Validators.required]],
            airportDepartureNameAndIata: ['', Validators.required],
            airportArrivalNameAndIata: ['', Validators.required],
            arrivalScheduledTime: [null, Validators.required],
            arrivalActualTime: [null]
        };

        this.editorForm = this.formBuilder.group(connectionFlightForm);
        this.availableAirports = await this.statiaService.getPropertyValues(MODEL_PROPERTY_NAME.AIRPORT, false);

        this.filteredDepartureAirports =
            this.editorForm.controls['airportDepartureNameAndIata'].valueChanges.pipe(
                map(value => (value ?
                    this.statiaService._filter(value, this.availableAirports, ['nameAndIata']) :
                    this.availableAirports.slice()))
            );
        this.editorForm.controls['airportDepartureNameAndIata'].setValidators(
            [Validators.required, this.validationService.validateFieldValue(this.availableAirports, 'nameAndIata')]
        );

        this.filteredArrivalAirports = this.editorForm.controls['airportArrivalNameAndIata'].valueChanges.pipe(
            map(value => (value ?
                this.statiaService._filter(value, this.availableAirports, ['nameAndIata']) :
                this.availableAirports.slice()))
        );
        this.editorForm.controls['airportArrivalNameAndIata'].setValidators(
            [Validators.required, this.validationService.validateFieldValue(this.availableAirports, 'nameAndIata')]
        );
    }

    save() {
        if (this.editorForm.invalid) {
            this.snackbar.open(this.translate.instant('snbCompleteRequiredFields'), 'Close', { duration: TIMEOUTS['3'] });
            return;
        }

        let param: any;
        param = this.editorForm.value;

        /** Format values for DB */
        const selectedAirportDeparture = this.availableAirports.filter(
            (airport: Airport) => airport.nameAndIata === param.airportDepartureNameAndIata
        );
        param.airportDepartureId = selectedAirportDeparture[0].id;

        const selectedAirportArrival = this.availableAirports.filter(
            (airport: Airport) => airport.nameAndIata === param.airportArrivalNameAndIata
        );
        param.airportArrivalId = selectedAirportArrival[0].id;

        param.arrivalScheduledTime = param.arrivalScheduledTime.replace('T', ' ').concat(':00');
        if (param.arrivalActualTime !== null) {
            param.arrivalActualTime = param.arrivalActualTime.replace('T', ' ').concat(':00');
        }
        param.isDisrupted = 0;

        if (this.action === 'create') {
            this.createConnectionFlight(param);
        } else {
            param.fieldNewName = param.field;
            this.editConnectionFlight(param);
        }
    }

    close() {
        this.dialogRef.close(false);
    }

    async createConnectionFlight(param: any) {
        const pathToRedirect = null;
        const router = null;

        let connectionFlightId: any;
        let resultOk = true;

        await new Promise((resolve) => {
            this.connectionFlightService.createAsync(param).then(
                (result: Result<ConnectionFlight>) => {
                    if (result.status) {
                        connectionFlightId = result.response.id;
                    } else {
                        resultOk = false;
                    }
                    this.notificationsService.manageServiceResult(result, pathToRedirect, router, this.snackbar, this.dialog);
                    resolve();
                }
            );
        });

        if (connectionFlightId !== undefined) {
            const parameters = {
                connectionFlightId: connectionFlightId,
                order: param.order
            };

            await new Promise((resolve) => {
                this.claimService.addClaimConnectionFlightsAsync(this.claimId, parameters).then(
                    (result: Result<any[]>) => {
                        if (!result.status) {
                            resultOk = false;
                        }
                        this.notificationsService.manageServiceResult(result, pathToRedirect, router, this.snackbar,
                            this.dialog);
                        resolve();
                    }
                );
            });
        }

        if (resultOk) {
            this.dialogRef.close(true);
        } else {
            if (connectionFlightId !== undefined) {
                this.deleteConnectionFlight(connectionFlightId);
            }
        }
    }

    editConnectionFlight(param: any) {
        const pathToRedirect = null;
        const router = null;

        this.connectionFlightService.edit(this.connectionFlight.connectionFlightId, param).subscribe(
            (result: Result<ConnectionFlight>) => {
                if (result.status) {
                    this.dialogRef.close(true);
                }
                this.notificationsService.manageServiceResult(result, pathToRedirect, router, this.snackbar, this.dialog);
            }
        );
    }

    deleteConnectionFlight(connectionFlightId: string | number) {
        this.connectionFlightService.deleteByParam({ connectionFlightId }).subscribe(() => { });
    }

    clearInput(input: string) {
        this.editorForm.controls[input].reset();
    }
}
