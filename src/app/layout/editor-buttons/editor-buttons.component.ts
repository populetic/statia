import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-editor-buttons',
  templateUrl: './editor-buttons.component.html',
  styleUrls: ['./editor-buttons.component.scss']
})
export class EditorButtonsComponent implements OnInit {

    @Input() parentRoute: string;
    @Input() id: string;
    @Input() textId = 'id';
    @Input() date: Date;
    @Input() textDate = 'UpdateAt';
    @Input() isInvalid: boolean;
    @Input() isDisabled: boolean;
    @Input() canUseFormOperation = true;
    @Input() messageOption: string;
    @Input() textButtonSubmit = 'save';
    @Input() showSaveButton = true;
    @Input() showBackButton = true;

    @Output() saveButtonClickedEvent: EventEmitter<any> = new EventEmitter<any>();

    public buttons: {[key: string]: string} = {
        generate: 'add', // 'vertical_align_bottom'
        save: 'save',
        edit: 'edit',
        create: 'add',
        add: 'add',
    };

    constructor(
    ) { }

    ngOnInit(): void {
    }

    get message() {
        return this.isInvalid ? 'toolTipInvalidForm' : 'toolTip' + this.textButtonSubmit;
    }
    saveClicked(e: Event) {
        this.saveButtonClickedEvent.emit(e);
    }
}
