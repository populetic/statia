import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubordinateTableComponentComponent } from './subordinate-table-component.component';

describe('SubordinateTableComponentComponent', () => {
  let component: SubordinateTableComponentComponent;
  let fixture: ComponentFixture<SubordinateTableComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubordinateTableComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubordinateTableComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
