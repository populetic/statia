import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { PAGINATOR } from '@app/shared/constants/constants';
import { AccessMenu } from '@app/shared/interfaces/access.menu.interface';
import { SessionService } from '@app/shared/services/session.service';
import { StatiaService } from '@app/shared/services/statia.service';
import { ValidationService } from '@app/shared/services/validation.service';
import { environment } from '@environments/environment';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { ListColumn } from '@fury/shared/list/list-column.model';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { RelatedDataType, TableRelatedConfiguration } from '../table-related-data/table-related-configuration.interface';
import { TableRelatedData } from '../table-related-data/table-related-data.interface';
import { TableRelatedDataService } from '../table-related-data/table-related-data.service';

@Component({
  selector: 'app-subordinate-table-component',
  templateUrl: './subordinate-table-component.component.html',
  animations: [
      fadeInRightAnimation,
      fadeInUpAnimation
  ]
})
export class SubordinateTableComponentComponent implements OnInit, OnDestroy, OnChanges {

 /*
    Important !,
    In the main component, from its onInit & onDestroy function, call
    TableRelatedDataService.cleanDataTabs () to clean up sessionStorage of current tabs
    */

   public PAGINATOR = PAGINATOR;

   @Input() parentRoute: string;
   @Input() currentTab: string;
   @Input() idRelatedEntity: string | any;
   @Input() isInvalid = false;
   @Input() sizePaginator = 'SMALL';
   @Input() paramsForFilter: {filterSelected: any; filterValue: any};
   @Input() forceToReloadData = false;


   @Output() allDataFromTable: EventEmitter<TableRelatedData<any>> = new EventEmitter<TableRelatedData<any>>();
   @Output() rowData: EventEmitter<TableRelatedData<any>> = new EventEmitter<TableRelatedData<any>>();

   @ViewChild(MatPaginator, { static: true }) tablePaginator: MatPaginator;
   @ViewChild(MatSort, { static: true }) tableSort: MatSort;

   public dataSource: MatTableDataSource<any>;
   public columnsTable: ListColumn[];
   public accessMenu: AccessMenu[];
   public sortActive: string;
   public sortDirection: string;
   public idField: string;
   public enableNavigation: boolean;
   public checkBoxField = 'isSelected';
   private subsPendingService: Subscription;

   constructor(
       private translate: TranslateService,
       private statiaService: StatiaService,
       private validationService: ValidationService,
       private tableRelatedDataService: TableRelatedDataService,
       private sessionService: SessionService
   ) {
       this.dataSource = new MatTableDataSource();
    }
    ngOnInit(): void {
        this.configTab();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (this.paramsForFilter?.filterSelected) {
            this.tableFilter(this.paramsForFilter?.filterSelected, this.paramsForFilter?.filterValue);
        }

        if (this.forceToReloadData) {
            this.configTab();
        }

    }

   ngOnDestroy(): void {
       this.subsPendingService?.unsubscribe();
   }

   async configTab() {
       if (this.currentTab) {
           this.tableRelatedDataService.getRelatedData(this.currentTab, this.idRelatedEntity)
               .then((data: TableRelatedConfiguration<RelatedDataType[]>) => this.setData(data));
       }
   }

   setData(data: TableRelatedConfiguration<any>): void { // TODO: TableRelatedConfiguration<any>
       this.dataSource.data = data?.dataTable;
       this.dataSource.paginator = this.tablePaginator;
       this.dataSource.sort = this.tableSort;
       this.columnsTable = data?.columnsTable;
       this.idField = data?.settings?.idField;
       this.enableNavigation = data?.settings?.enableNavigation;
       this.checkBoxField = data?.settings?.checkBoxField ?? this.checkBoxField;

       this.sortActive = data?.settings?.sortActive ?? '';
       this.sortDirection = data?.settings?.sortDirection ?? '';

       this.accessMenu = this.enableNavigation ? this.statiaService.buildMenuAccess(data?.menuIds, 'edit') : [];
       this.emitDataFromTable();
   }

   tableFilter(filterSelected: any, filterValue: any): void {
       if (!this.dataSource) {
           return;
       }

       const activeFilters: Array<any> = this.dataSource.filter ? JSON.parse(this.dataSource.filter) : {};

       let selectedValue: string;
       if (this.validationService.validValue(filterValue)) {
           selectedValue = String(filterValue).trim().toLowerCase();
       }

       if (!this.validationService.validValue(selectedValue)) {
           delete activeFilters[filterSelected];
       } else {
           activeFilters[filterSelected] = selectedValue;
       }

       /** exec Filter */
       this.dataSource.filterPredicate = (data) => {
           let filterResult: boolean;
           const allFilterResults: any = {};
           // Filter by Search
           if (activeFilters.hasOwnProperty('search')) {
               this.columnsTable.forEach(column => {
                   const propertyToFind = column?.canBeTranslated ?
                       this.translate.instant(column.name + data[column.name]) : data[column.name];
                   if (allFilterResults['search'] === undefined || !allFilterResults['search']) {
                       if (propertyToFind !== '' && propertyToFind !== null &&
                           String(propertyToFind).toLowerCase().indexOf(selectedValue) !== -1) {
                           allFilterResults['search'] = true;
                       } else {
                           allFilterResults['search'] = false;
                       }
                   }
               });
           }

           if (!Object.values(allFilterResults).includes(false)) {
               filterResult = true;
           }

           return filterResult;
       };

       this.dataSource.filter = JSON.stringify(activeFilters);
   }

   get visibleColumnsTable() {
       return this.columnsTable?.map((column: { name: any; }) => column.name);
   }

   rowClickEvent(dataFromRow: string, menu: AccessMenu): void {
       if (menu?.canAccess && menu?.pathMenu && this.enableNavigation) {
           window.open(environment.siteUrl + menu?.pathMenu + dataFromRow[this.idField]);
       } else {
           this.rowData.emit({currentTab: this.currentTab, dataFromTable: dataFromRow});
       }
   }

   updateCheckedRows(rowId: string, event: MatCheckboxChange) {
       this.dataSource.data.forEach((data: any) => {
           if (data[this.idField] === rowId) {
               data[this.checkBoxField] = event.checked;
           }
       });

       this.emitDataFromTable();
   }

   checkUnCheckAllRows(event: MatCheckboxChange) {
       this.dataSource.data.forEach((data: any) => {
            data[this.checkBoxField] = event.checked;
       });

       this.emitDataFromTable();
   }

   emitDataFromTable(): void {
       this.allDataFromTable.emit({currentTab: this.currentTab, dataFromTable: this.dataSource?.data});
   }

   getDataFromTable(): any[] {
       return this.dataSource.data;
   }

   cleanDataTabs() {
       this.sessionService.cleanDataTabs();
   }

}
