import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { FILTER_SELECTOR_LIMIT } from '@app/shared/constants/constants';
import { StatiaService } from '@app/shared/services/statia.service';
import { ValidationService } from '@app/shared/services/validation.service';
import { Observable, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, startWith, tap } from 'rxjs/operators';

@Component({
    selector: 'app-selector',
    templateUrl: './selector.component.html',
    styleUrls: ['./selector.component.scss']
})
export class SelectorComponent implements OnDestroy, OnInit {

    public limit: number = FILTER_SELECTOR_LIMIT;
    private subscriptions: Subscription[] = [];
    public filteredItems: Observable<any[]>;

    @Input() control = new FormControl();
    @Input() nameField: string;
    @Input() placeHolder: string;
    @Input() items: any[];
    @Input() idField = 'id';
    @Input() idName = 'name';
    @Input() validate = false;
    @Input() required = false;
    @Input() translateOptions = false;
    @Input() selectorType: string;
    @Output() controlEvent: EventEmitter<any> = new EventEmitter<any>();

    constructor(
        private statiaService: StatiaService,
        private validationService: ValidationService
    ) { }

    ngOnInit(): void {
        if (this.selectorType === 'select' || !this.selectorType && this.items?.length <= this.limit) {
            this.selectorField();
        } else if (this.selectorType === 'autocomplete' || !this.selectorType && this.items?.length > this.limit) {
            this.autocompleteField();
        }
    }

    selectorField(): void {
        const sub: Subscription = this.control.valueChanges.pipe(
            distinctUntilChanged(),
            tap((select: any) => this.controlEvent.emit(select))
        ).subscribe();

        this.subscriptions.push(sub);
    }

    autocompleteField(): void {
        this.filteredItems = this.control.valueChanges
            .pipe(
                debounceTime(500),
                tap(val => !val ? this.controlEvent.emit(val) : val),
                startWith(''),
                map((value: string) =>
                    (value ? this.statiaService._filter(value, this.items, [this.idName]) : this.items.slice())
                )
            );

        if (this.validate) {
            this.control.setValidators(
                [Validators.required, this.validationService.validateFieldValue(this.items, this.idName)]
            );
        }
    }

    ngOnDestroy(): void {
        this.subscriptions.map((sub: Subscription) => sub.unsubscribe());
    }

    optionSelected(value: any): void {
        if (value) {
            const filter: string[] = this.statiaService._filter(value, this.items, [this.idName]);
            return this.controlEvent.emit(filter[0][this.idField]);
        }

        return this.controlEvent.emit(value);
    }

    reset(): void {
        this.control.reset();
    }

    get message(): string {
        return this.placeHolder ? 'msg' + this.placeHolder : this.nameField;
    }

}
