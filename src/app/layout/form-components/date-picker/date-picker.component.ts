import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { distinctUntilChanged, tap } from 'rxjs/operators';

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss']
})
export class DatePickerComponent implements OnInit, OnDestroy {

    private subscriptions: Subscription[] = [];
    @Input() control = new FormControl('');
    @Input() name = 'msgSelectDate';
    @Input() textExtra: string;
    @Input() placeHolder = 'msgddMMyyyy';
    @Input() toolTip = 'msgSelectDate';
    @Input() startDate = true;
    @Input() disabled = false;

    @Output() controlEvent: EventEmitter<any> = new EventEmitter<any>();
    constructor() { }

    ngOnDestroy(): void {
        this.subscriptions.map((sub: Subscription) => sub.unsubscribe());
    }

    ngOnInit(): void {
       const sub: Subscription = this.control.valueChanges.pipe(
           distinctUntilChanged(),
           tap( (date: any) => this.controlEvent.emit(date))
       ).subscribe();
       this.subscriptions.push(sub);

    }

    reset() { // call from mainTable.clearAllFilters()
        this.control.setValue('');
    }

    get textTooltip() {
        return this.control?.disabled ? null : this.toolTip;
    }
}
