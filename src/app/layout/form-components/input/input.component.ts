import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, OnChanges } from '@angular/core';
import { FormControl } from '@angular/forms';
import { tap, debounceTime } from 'rxjs/operators';
import { Subscription } from 'rxjs';
@Component({
    selector: 'app-input',
    templateUrl: './input.component.html'
})
export class InputComponent implements OnInit, OnDestroy, OnChanges {

    private inputSearchSubs: Subscription;
    @Input() inputFilter = new FormControl();
    @Input() parentRoute: string;
    @Input() isLoading: boolean;
    @Input() msgInput = 'msgInput';
    @Output() controlEvent: EventEmitter<string> = new EventEmitter<string>();

    constructor() {
     }

    ngOnInit(): void {
        this.filter();
    }

    ngOnChanges(): void {
        if (this.isLoading) {
            this.inputFilter.disable();
        } else {
            this.inputFilter.enable();
        }
    }

    filter(): void {
        this.inputSearchSubs = this.inputFilter.valueChanges
        .pipe(
            debounceTime(500),
            tap(val => this.controlEvent.emit(val)),
        ).subscribe();
    }
    reset(): void {
        this.inputFilter.reset();
    }
    ngOnDestroy(): void {
        this.inputSearchSubs.unsubscribe();
    }
}
