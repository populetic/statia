import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared/modules/shared.module';
import { MaterialModule } from '@fury/shared/material-components.module';
import { TranslateModule } from '@ngx-translate/core';
import { EditorButtonsComponent } from '../editor-buttons/editor-buttons.component';
import { DatePickerComponent } from './date-picker/date-picker.component';
import { SelectorComponent } from './selector/selector.component';
import { InputComponent } from './input/input.component';

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        ReactiveFormsModule,
        RouterModule,
        TranslateModule,
        SharedModule
    ],
    declarations: [
        DatePickerComponent,
        EditorButtonsComponent,
        SelectorComponent,
        InputComponent
    ],
    exports: [
        DatePickerComponent,
        EditorButtonsComponent,
        SelectorComponent,
        InputComponent
    ]
})
export class FormComponentsModule { }
