import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MenuOptionDialogComponent } from '@app/layout/dialogs/menu-option-dialog/menu-option-dialog.component';
import { MenuPropertyDialogComponent } from '@app/layout/dialogs/menu-property-dialog/menu-property-dialog.component';
import { SimpleDialogComponent } from '@app/layout/dialogs/simple-dialog/simple-dialog.component';
import { MainTableModule } from '@app/layout/main-table/main-table.module';
import { FurySharedModule } from '@fury/fury-shared.module';
import { BackdropModule } from '@fury/shared/backdrop/backdrop.module';
import { LoadingIndicatorModule } from '@fury/shared/loading-indicator/loading-indicator.module';
import { MaterialModule } from '@fury/shared/material-components.module';
import { TranslateModule } from '@ngx-translate/core';
import { ConfirmActionDialogComponent } from './dialogs/confirm-action/confirm-action-dialog.component';
import { ConnectionFlightDialogComponent } from './dialogs/connection-flight-dialog/connection-flight-dialog.component';
import { FooterModule } from './footer/footer.module';
import { FormComponentsModule } from './form-components/form-components.module';
import { LayoutComponent } from './layout.component';
import { NavigationModule } from './navigation/navigation.module';
import { SidenavModule } from './sidenav/sidenav.module';
import { SubordinateTableComponentComponent } from './subordinate-table-component/subordinate-table-component.component';
import { TableRelatedDataComponent } from './table-related-data/table-related-data.component';
import { ToolbarModule } from './toolbar/toolbar.module';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        MaterialModule,
        LoadingIndicatorModule,
        FurySharedModule,
        ReactiveFormsModule,
        TranslateModule,

        // Core
        ToolbarModule,
        SidenavModule,
        FooterModule,
        BackdropModule,
        NavigationModule,

        // Custom
        MainTableModule,
        FormComponentsModule
    ],
    declarations: [
        LayoutComponent,
        SimpleDialogComponent,
        MenuPropertyDialogComponent,
        MenuOptionDialogComponent,
        ConnectionFlightDialogComponent,
        ConfirmActionDialogComponent,
        TableRelatedDataComponent,
        SubordinateTableComponentComponent
    ],
    entryComponents: [
        SimpleDialogComponent,
        MenuPropertyDialogComponent,
        MenuOptionDialogComponent,
        ConnectionFlightDialogComponent,
        ConfirmActionDialogComponent,
    ],
    exports: [
        TableRelatedDataComponent
    ]
})
export class LayoutModule {
}
