import { Component, OnInit } from '@angular/core';
import { Login } from '@app/pages/login/login.model';
import { UserLoggedData } from '@app/shared/interfaces/user-logged-data.interface';
import { SessionService } from '@app/shared/services/session.service';

@Component({
    selector: 'app-toolbar-user',
    templateUrl: './toolbar-user.component.html',
    styleUrls: ['./toolbar-user.component.scss']
})
export class ToolbarUserComponent implements OnInit {

    isOpen: boolean;

    public userLoggedData: UserLoggedData;

    constructor(
        private sessionService: SessionService
    ) { }

    ngOnInit() {
        const {name, username}: Login = this.sessionService.user;
        this.userLoggedData = { name, username };
    }

    toggleDropdown() {
        this.isOpen = !this.isOpen;
    }

    onClickOutside() {
        this.isOpen = false;
    }

    logOut() {
        this.sessionService.logOut();
    }

}
