import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { STATIA_LANGUAGES } from '@app/shared/constants/constants';
import { LANGUAGE } from '@app/shared/enums/language.enum';
import { LanguageApp } from '@app/shared/interfaces/language.app.interface';
import { Result } from '@app/shared/models/result.model';
import { SessionService } from '@app/shared/services/session.service';
import { LIST_FADE_ANIMATION } from '@fury/shared/list.animation';
import { TranslateService } from '@ngx-translate/core';
import { LanguagesService } from '@service/languages.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-toolbar-languages',
    templateUrl: './toolbar-languages.component.html',
    styleUrls: ['./toolbar-languages.component.scss'],
    animations: [...LIST_FADE_ANIMATION],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToolbarLanguagesComponent implements OnInit, OnDestroy {

    languages: any[];
    actualLanguage: string;
    isOpen: boolean;
    subscriptions: Subscription[] = [];

    constructor(
        private sessionService: SessionService,
        public translateService: TranslateService,
        public languagesService: LanguagesService,
    ) {}

    ngOnInit() {
        this.actualLanguage = this.sessionService.actualLanguage;
        if (!this.actualLanguage) {
            this.actualLanguage = LANGUAGE.EN;
        }
        this.sessionService.actualLanguage = this.actualLanguage;

        this.languages = this.sessionService.getItem(STATIA_LANGUAGES);
        if (!this.languages) {
            const langSubs: Subscription = this.languagesService.getAllLanguagesByApp().subscribe(
                (result: Result<LanguageApp[]>) => { // TODO: translator
                    if (result.status === 1) {
                        this.sessionService.setItem(STATIA_LANGUAGES, JSON.stringify(result.response));
                        this.languages = result.response;
                    }
                }
            );
            this.subscriptions.push(langSubs);
        }
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
    }

    changeLanguage(language: any) {
        this.sessionService.actualLanguage = language.languageCode;
        this.actualLanguage = language.languageCode;
        this.toggleDropdown();
    }

    toggleDropdown() {
        this.isOpen = !this.isOpen;
    }

    onClickOutside() {
        this.isOpen = false;
    }
}
