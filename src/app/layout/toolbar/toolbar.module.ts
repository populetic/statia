import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { TranslateModule } from '@ngx-translate/core';

import { FuryCardModule } from '@fury/shared/card/card.module';
import { ClickOutsideModule } from '@fury/shared/click-outside/click-outside.module';
import { MaterialModule } from '@fury/shared/material-components.module';
import { ScrollbarModule } from '@fury/shared/scrollbar/scrollbar.module';

import { ToolbarSidenavMobileToggleComponent } from './toolbar-sidenav-mobile-toggle/toolbar-sidenav-mobile-toggle.component';
import { ToolbarUserComponent } from './toolbar-user/toolbar-user.component';
import { ToolbarLanguagesComponent } from './toolbar-languages/toolbar-languages.component';
import { ToolbarNotificationsComponent } from './toolbar-notifications/toolbar-notifications.component';
import { ToolbarComponent } from './toolbar.component';

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        RouterModule,
        ScrollbarModule,
        FormsModule,
        ClickOutsideModule,
        FuryCardModule,
        TranslateModule
    ],
    declarations: [
        ToolbarComponent,
        ToolbarUserComponent,
        ToolbarLanguagesComponent,
        ToolbarNotificationsComponent,
        ToolbarSidenavMobileToggleComponent
    ],
    exports: [ToolbarComponent]
})
export class ToolbarModule {
}
