import { ListColumn } from '@fury/shared/list/list-column.model';
import { Claim } from '@app/pages/company/claims/claim.model';
import { Bill } from '@app/pages/company/bills/bill.model';
import { Demand } from '@app/pages/company/demands/demand.model';
import { Companion } from '@app/pages/company/companions/companion.model';
import { LawFirm } from '@app/pages/company/law-firms/law-firm.model';


export interface TableRelatedConfiguration<T> {
    dataTable?: T;
    columnsTable?: ListColumn[];
    menuIds?: string[];
    settings?: {[key: string]: any};
}

export type  RelatedDataType = Claim | Bill | Demand | Companion | LawFirm;

