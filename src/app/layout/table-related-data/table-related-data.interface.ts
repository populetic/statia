export interface TableRelatedData<T> {
    currentTab: string;
    dataFromTable: T;
}
