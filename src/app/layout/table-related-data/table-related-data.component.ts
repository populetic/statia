import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { PAGINATOR, SIZES } from '@app/shared/constants/constants';
import { CustomParams } from '@app/shared/interfaces/custom-params.interface';
import { SessionService } from '@app/shared/services/session.service';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { TableRelatedData } from './table-related-data.interface';

@Component({
    selector: 'app-table-related-data',
    templateUrl: './table-related-data.component.html',
    animations: [
        fadeInRightAnimation,
        fadeInUpAnimation
    ]
})
export class TableRelatedDataComponent {

    /*
    Important !,
    In the main component, from its onInit & onDestroy function, call
    TableRelatedDataService.cleanDataTabs () to clean up sessionStorage of current tabs
    */

    public PAGINATOR = PAGINATOR;


    @Input() showSearch = true;
    @Input() parentRoute: string;
    @Input() currentTab: string;
    @Input() idRelatedEntity: string | any;
    @Input() title: string;
    @Input() sizePaginator = SIZES.SMALL;
    @Input() isInvalid = false;
    @Input() isLoading: boolean;
    @Input() messageOption: string;
    @Input() textButtonSubmit = 'save';
    @Input() canUseFormOperation = true;
    @Input() showBack = true;
    @Input() showSave = true;
   @Input() forceToReloadData = false;

    @Output() allDataFromTable: EventEmitter<TableRelatedData<any>> = new EventEmitter<TableRelatedData<any>>();
    @Output() rowData: EventEmitter<TableRelatedData<any>> = new EventEmitter<TableRelatedData<any>>();
    @Output() saveButtonClickedEvent: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild(MatPaginator, { static: true }) tablePaginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) tableSort: MatSort;

    public buttons: CustomParams = {
        generate: 'add', // 'vertical_align_bottom'
        save: 'save',
        edit: 'edit',
        create: 'add',
        add: 'add',
    };
    public paramsForFilter: { filterSelected: any; filterValue: any; };

    constructor(
        private sessionService: SessionService
    ) { }

    get message() {
        return this.isInvalid ? 'toolTipInvalidForm' : 'toolTip' + this.textButtonSubmit;
    }

    setTableFilter(filterSelected: any, filterValue: any): void {
        this.paramsForFilter = {filterSelected, filterValue};
    }

    emitDataTable(event: any): void {
        this.allDataFromTable.emit(event);
    }

    emitRowData(data: any): void {
        this.rowData.emit({currentTab: this.currentTab, dataFromTable: data});
    }

    cleanDataTabs() {
        this.sessionService.cleanDataTabs();
    }

    emitSaveButton(event: any): void {
        this.saveButtonClickedEvent.emit(event);
    }

}
