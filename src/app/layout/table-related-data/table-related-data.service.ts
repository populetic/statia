import { Injectable } from '@angular/core';
import { Bill } from '@app/pages/company/bills/bill.model';
import { Claim } from '@app/pages/company/claims/claim.model';
import { ClaimService } from '@app/pages/company/claims/claim.service';
import { ClientService } from '@app/pages/company/clients/client.service';
import { Companion } from '@app/pages/company/companions/companion.model';
import { Demand } from '@app/pages/company/demands/demand.model';
import { LawFirmAdministratorService } from '@app/pages/company/law-firms/law-firm-administrators.service';
import { User } from '@app/pages/management/users/user.model';
import {
    ID_MENU_ACTIVE_CLAIMS,
    ID_MENU_BILL,
    ID_MENU_CLOSED_CLAIMS,
    ID_MENU_DEMAND,
    ID_MENU_LAWFIRM,
    RELATED_ENTITIES
} from '@app/shared/constants/constants';
import { ListColumn } from '@fury/shared/list/list-column.model';
import { RelatedDataType, TableRelatedConfiguration } from './table-related-configuration.interface';

@Injectable({ providedIn: 'root' })
export class TableRelatedDataService {

    /*
    Important !,
    In the main component, from its onInit & onDestroy function, call
    TableRelatedDataService/SessionService.cleanDataTabs () to clean up sessionStorage of current tabs
    */

    constructor(
        private claimService: ClaimService,
        private clientService: ClientService,
        private lawFirmAdministratorService: LawFirmAdministratorService,
    ) { }

    async getRelatedData(currentTab: string, id: string | any):
        Promise<TableRelatedConfiguration<RelatedDataType[]>> {
            switch (currentTab) {
                case RELATED_ENTITIES.CLIENT.CLAIMS:
                    return await this.getRelatedClaims(currentTab, id);
                case RELATED_ENTITIES.CLIENT.DEMANDS:
                    return await this.getRelatedDemands(currentTab, id);
                case RELATED_ENTITIES.CLIENT.BILLS:
                    return await this.getRelatedBills(currentTab, id);
                case RELATED_ENTITIES.DEMAND.COMPANIONS:
                    return await this.getRelatedCompanions(currentTab, id);
                case RELATED_ENTITIES.DEMAND.POSSIBLE_COMPANIONS:
                    return await this.getRelatedPossibleCompanions(currentTab, id);
                case RELATED_ENTITIES.LAWFIRM.ADMINISTRATORS:
                    return await this.getLawFirmsAdministrators(currentTab, id);
            }
    }

    // Build Config
    async getRelatedClaims(currentTab: string, clientId: string): Promise<TableRelatedConfiguration<Claim[]>> {
        const data: TableRelatedConfiguration<Claim[]> = {
            dataTable: await this.clientService.getRelatedEntity(currentTab, clientId),
            columnsTable: this.getColumnsClaim(),
            menuIds: [ID_MENU_ACTIVE_CLAIMS, ID_MENU_CLOSED_CLAIMS],
            settings: this.getDefaultSettings()
        };
        return data;
    }

    async getRelatedDemands(currentTab: string, clientId: string): Promise<TableRelatedConfiguration<Demand[]>> {
        return {
            dataTable: await this.clientService.getRelatedEntity(currentTab, clientId),
            columnsTable: this.getColumnsDemands(),
            menuIds: [ID_MENU_DEMAND],
            settings: this.getDefaultSettings()
        };
    }

    async getRelatedBills(currentTab: string, clientId: string): Promise<TableRelatedConfiguration<Bill[]>> {
        return {
            dataTable: await this.clientService.getRelatedEntity(currentTab, clientId),
            columnsTable: this.getColumnsBills(),
            menuIds: [ID_MENU_BILL],
            settings: this.getDefaultSettings()
        };
    }

    async getRelatedCompanions(currentTab: string, claimId: string): Promise<TableRelatedConfiguration<Companion[]>> {
        return {
            dataTable: await this.claimService.getRelatedEntity(currentTab, claimId),
            columnsTable: this.getColumnsCompanions(),
            menuIds: [ID_MENU_DEMAND],
            settings: this.getSettingCompanion()
        };
    }

    async getRelatedPossibleCompanions(currentTab: string, claimId: string):
        Promise<TableRelatedConfiguration<Companion[]>> {
            return {
                dataTable: await this.claimService.getRelatedEntity(currentTab, claimId),
                columnsTable: this.getColumnsCompanions(),
                menuIds: [ID_MENU_DEMAND],
                settings: this.getSettingCompanion()
            };
    }

    async getLawFirmsAdministrators(currentTab: string, lawFirmId: string):
        Promise<TableRelatedConfiguration<User[]>> {
            return {
                dataTable: await this.lawFirmAdministratorService.getRelatedEntity(currentTab, lawFirmId),
                columnsTable: this.getColumnsLawFirmsUsers(),
                menuIds: [ID_MENU_LAWFIRM],
                settings: this.getSettingLawAFirmdmin()
            };
    }

    // Columns
    private getColumnsClaim(): ListColumn[] {
        const propertiesColumns: Array<ListColumn> = [
            { name: 'id', isBoolean: false },
            { name: 'disruptionTypeId', canBeTranslated: true },
            { name: 'flightDate', isDate: true },
            { name: 'flightNumber', isBoolean: false },
            { name: 'claimIsFinished', isBoolean: true },
            { name: 'isCompanion', isBoolean: true },
            { name: 'hasCompanion', isBoolean: true },
            { name: 'isActive', isBoolean: true }
        ];
        return propertiesColumns;
    }

    private getColumnsDemands(): ListColumn[] {
        const propertiesColumns: Array<ListColumn> = [
            { name: 'id', isBoolean: false },
            { name: 'presentedAt', isDate: true },
            { name: 'courtCompleteName', isBoolean: false },
            { name: 'amountRequested', isNumber: true },
            { name: 'cancelledAt', isDate: true }
        ];
        return propertiesColumns;
    }

    private getColumnsBills(): ListColumn[] {
        const propertiesColumns: Array<ListColumn> = [
            { name: 'billRef', isBoolean: false },
            { name: 'createdAt', isDate: true },
            { name: 'total', isNumber: true }
        ];
        return propertiesColumns;
    }

    private getColumnsCompanions(): ListColumn[] {
        const propertiesColumns: Array<ListColumn> = [
            { name: 'isSelected', isCheckBox: true },
            { name: 'completeName', isBoolean: false },
            { name: 'claimStatusId', canBeTranslated: true },
            { name: 'claimDocumentationStatusId', canBeTranslated: true },
            { name: 'finishedAt', isDate: true },
            { name: 'amountEstimated', isCurrency: true },
            { name: 'amountTickets', isCurrency: true },
        ];
        return propertiesColumns;
    }

    private getColumnsLawFirmsUsers(): ListColumn[] {
        const propertiesColumns: Array<ListColumn> = [
            { name: 'isLawFirmAdmin', isCheckBox: true },
            { name: 'name'},
            { name: 'surnames'},
            { name: 'lawFirmId', isBoolean: true},
        ];
        return propertiesColumns;
    }

    // Configuration
    private getDefaultSettings() {
        return {
            idField: 'id',
            enableNavigation: true,
        };
    }

    private getSettingCompanion() {
        return {
            idField: 'claimId',
            enableNavigation: false,
            sortActive: 'claimStatusId',
            sortDirection: 'desc'
        };
    }
    private getSettingLawAFirmdmin() {
        return {
            idField: 'id',
            enableNavigation: false,
            checkBoxField: 'isLawFirmAdmin'
        };
    }
}
