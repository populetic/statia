import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NavigationEnd, Router } from '@angular/router';
import { SidenavService } from '@app/layout/sidenav/sidenav.service';
import { Menu } from '@app/pages/statia-configuration/menus/menu.model';
import { MenuService } from '@app/pages/statia-configuration/menus/menu.service';
import { SessionService } from '@app/shared/services/session.service';
import { ThemeService } from '@fury/services/theme.service';
import { SidebarDirective } from '@fury/shared/sidebar/sidebar.directive';
import { checkRouterChildsData } from '@fury/utils/check-router-childs-data';
import { Subscription } from 'rxjs';
import { filter, map, startWith } from 'rxjs/operators';
import { TIMEOUTS } from '@app/shared/constants/constants';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})

export class LayoutComponent implements OnInit, OnDestroy {

    @ViewChild('configPanel', { static: true }) configPanel: SidebarDirective;

    sidenavOpen$ = this.sidenavService.open$;
    sidenavMode$ = this.sidenavService.mode$;
    sidenavCollapsed$ = this.sidenavService.collapsed$;
    sidenavExpanded$ = this.sidenavService.expanded$;
    quickPanelOpen: boolean;

    sideNavigation$ = this.themeService.config$.pipe(map(config => config.navigation === 'side'));
    topNavigation$ = this.themeService.config$.pipe(map(config => config.navigation === 'top'));
    toolbarVisible$ = this.themeService.config$.pipe(map(config => config.toolbarVisible));
    toolbarPosition$ = this.themeService.config$.pipe(map(config => config.toolbarPosition));
    footerPosition$ = this.themeService.config$.pipe(map(config => config.footerPosition));

    subscriptions: Subscription[] = [];

    scrollDisabled$ = this.router.events.pipe(
        filter<NavigationEnd>(event => event instanceof NavigationEnd),
        // tslint:disable-next-line: deprecation
        startWith(null),
        map(() => checkRouterChildsData(this.router.routerState.root.snapshot, data => data.scrollDisabled))
    );

    constructor(
        private router: Router,
        private snackbar: MatSnackBar,
        private menuService: MenuService,
        private themeService: ThemeService,
        private translate: TranslateService,
        private sidenavService: SidenavService,
        private sessionService: SessionService,
    ) { }

    ngOnInit() {
        this.loadSidebarMenus();
    }

    openQuickPanel() {
        this.quickPanelOpen = true;
    }

    openConfigPanel() {
        this.configPanel.open();
    }

    closeSidenav() {
        this.sidenavService.close();
    }

    openSidenav() {
        this.sidenavService.open();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
    }

    loadSidebarMenus() {
        let sideNavItems: Array<any>;
        const menusByRoleId: Menu[] = this.menuService.getMenusStorage();
        if (menusByRoleId) {
            sideNavItems = this.buildNavItems(menusByRoleId);
            this.sidenavService.addItems(sideNavItems);
            return;
        }

        this.snackbar.open(this.translate.instant('snbErrorMenusByRoleUndefined'), 'Close', { duration: TIMEOUTS['3'] });
        this.sessionService.logOut();
    }

    buildNavItems(menus: Menu[]): Array<any> {
        const sideNavItems = new Array();

        menus.forEach((navItem: Menu) => {
            let newNavItem = null;
            if (navItem.parentId === null) {
                newNavItem = {
                    id: navItem.id,
                    name: navItem.name,
                    position: navItem.position,
                    type: 'subheading',
                    customClass: 'first-subheading'
                };
            } else {
                newNavItem = {
                    id: navItem.id,
                    name: navItem.name,
                    position: navItem.position,
                    routeOrFunction: '/' + navItem.parentName + '/' + navItem.name,
                    icon: navItem.icon
                };
            }
            sideNavItems.push(newNavItem);
        });

        return sideNavItems;
    }
}

