import { Component, HostBinding, HostListener, Input, OnDestroy, OnInit } from '@angular/core';
import { Login } from '@app/pages/login/login.model';
import { UserLoggedData } from '@app/shared/interfaces/user-logged-data.interface';
import { SessionService } from '@app/shared/services/session.service';
import { ThemeService } from '@fury/services/theme.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SidenavItem } from './sidenav-item/sidenav-item.interface';
import { SidenavService } from './sidenav.service';

@Component({
    selector: 'app-sidenav',
    templateUrl: './sidenav.component.html',
    styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit, OnDestroy {

    sidenavUserVisible$ = this.themeService.config$.pipe(map(config => config.sidenavUserVisible));

    @Input()
    @HostBinding('class.collapsed')
    collapsed: boolean;

    @Input()
    @HostBinding('class.expanded')
    expanded: boolean;

    items$: Observable<SidenavItem[]>;

    public userLoggedData: UserLoggedData;

    constructor(
        private themeService: ThemeService,
        private sidenavService: SidenavService,
        private sessionService: SessionService
    ) { }

    ngOnInit() {
        this.items$ = this.sidenavService.items$.pipe(
            map((items: SidenavItem[]) => this.sidenavService.sortRecursive(items, 'position'))
        );

        const user: Login = this.sessionService.user;
        this.userLoggedData = {
            completeName: user.completeName,
            roleName: user.roleName,
            username: user.username
        };
    }

    toggleCollapsed() {
        this.sidenavService.toggleCollapsed();
    }

    @HostListener('mouseenter')
    @HostListener('touchenter')
    onMouseEnter() {
        this.sidenavService.setExpanded(true);
    }

    @HostListener('mouseleave')
    @HostListener('touchleave')
    onMouseLeave() {
        this.sidenavService.setExpanded(false);
    }

    ngOnDestroy() {
    }
}
