import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatFormFieldDefaultOptions, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
// Needed for Touch functionality of Material Components
import { MatSnackBarConfig, MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from '@app/app-routing.module';
import { AppComponent } from '@app/app.component';
import { LayoutModule } from '@app/layout/layout.module';
import { PendingInterceptorModule } from '@fury/shared/loading-indicator/pending-interceptor.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { ApiModule } from './api/api.module';
import { APP_DATE_FORMATS, CustomDateAdapter } from './helpers/custom-date-adapter';
import { ErrorInterceptor } from './helpers/error.interceptor';
import { JwtInterceptor } from './helpers/jwt.interceptor';
import { TranslatorInterceptor } from './helpers/translator.interceptor';
import { TIMEOUTS } from './shared/constants/constants';
import { HttpLoaderFactory } from './utils/http-loader-factory';

@NgModule({
    imports: [
        // Angular Core Module
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),

        // App
        AppRoutingModule,

        // Layout Module (Sidenav, Toolbar, Quickpanel, Content)
        LayoutModule,

         // API module (Translate Contract To Model)
         ApiModule,

        // Displays Loading Bar when a Route Request or HTTP Request is pending
        PendingInterceptorModule
    ],
    declarations: [AppComponent],
    bootstrap: [AppComponent],
    providers: [
        {
            provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
            useValue: {
                appearance: 'fill'
            } as MatFormFieldDefaultOptions
        },
        {
            provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
            useValue: {
                duration: TIMEOUTS['5'],
                horizontalPosition: 'end',
                verticalPosition: 'bottom'
            } as MatSnackBarConfig
        },
        {
            provide: DateAdapter,
            useClass: CustomDateAdapter
        },
        {
            provide: MAT_DATE_FORMATS,
            useValue: APP_DATE_FORMATS
        },
        {
            provide: MAT_DATE_LOCALE,
            useValue: 'es-ES'
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TranslatorInterceptor,
            multi: true
        }
    ]
})

export class AppModule {
}

