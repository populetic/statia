import { Platform } from '@angular/cdk/platform';
import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { ActivatedRoute, Router } from '@angular/router';
import { ThemeService } from '@fury/services/theme.service';
import { TranslateService } from '@ngx-translate/core';
import { filter } from 'rxjs/operators';
import { LANGUAGE } from './shared/enums/language.enum';
import { SessionService } from './shared/services/session.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})

export class AppComponent implements OnInit {
    constructor(
        private router: Router,
        private platform: Platform,
        private renderer: Renderer2,
        private route: ActivatedRoute,
        private themeService: ThemeService,
        public translate: TranslateService,
        private iconRegistry: MatIconRegistry,
        private sessionService: SessionService,
        @Inject(DOCUMENT) private document: Document
    ) {
        /* Translations */
        translate.addLangs([LANGUAGE.ES, LANGUAGE.EN]);
        translate.setDefaultLang(LANGUAGE.EN);

        let actualLanguage: string = this.sessionService.actualLanguage;
        if (!actualLanguage) {
            actualLanguage = LANGUAGE.EN;
        }
        this.sessionService.actualLanguage = actualLanguage;
        /* Default icons */
        this.iconRegistry.setDefaultFontSetClass('material-icons');
        if (this.platform.BLINK) {
            this.renderer.addClass(this.document.body, 'is-blink');
        }

        /* Template configuration */
        this.themeService.setSidenavUserVisible(true);
        this.themeService.setFooterVisible(false);

        this.route.queryParamMap.pipe(
            filter(queryParamMap => queryParamMap.has('style'))
        ).subscribe(queryParamMap => this.themeService.setStyle(queryParamMap.get('style')));

        this.themeService.theme$.subscribe(theme => {
            if (theme[0]) {
                this.renderer.removeClass(this.document.body, theme[0]);
            }
            this.renderer.addClass(this.document.body, theme[1]);
        });
    }

    ngOnInit() {
        if (!this.sessionService.isUserLogged) {
            this.sessionService.logOut();
        }
    }
}
