import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class LoadResolver implements Resolve<any> {

    constructor(
        private translate: TranslateService
    ) { }

    resolve(): Observable<any> | Promise<any> | any {
        return this.translate.getTranslation(this.translate.currentLang);
    }
}
