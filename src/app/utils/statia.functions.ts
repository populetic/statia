import { Menu } from '@app/pages/statia-configuration/menus/menu.model';
import { Option } from '@app/pages/statia-configuration/options/option.model';
import { STATIA_AVAILABLE_MENUS } from '@app/shared/constants/constants';
import { Result } from '@app/shared/models/result.model';
import { SessionService } from '@app/shared/services/session.service';

export class StatiaFunctions {

    static sessionService: SessionService = new SessionService();

    static EnabledOption(menuId: string, optionId: string): boolean {
        const menus: Menu[] = this.sessionService.getItem(STATIA_AVAILABLE_MENUS);
        const menu: Menu = menus.find((m: Menu) => m.id === menuId);
        return menu?.options ? menu?.options.some((option: Option) => option.id === optionId) : false;
    }

    static getQueryString(param: any) {
        let queryString = '';

        if (param && param !== null) {
            queryString = '?' + Object.keys(param).map(key => key + '=' + param[key]).join('&');
        }

        return queryString;
    }

    static getResultModel<Contract, Model>(result: Result<Contract>): Result<Model> {
        const newResult: Result<Model> = new Result<Model>();
        newResult.errors = result?.errors;
        newResult.status = result?.status;
        newResult.message = result?.message;
        return  newResult;
    }

    static buildResultArrayModel<Contract, Model>(result: Result<Contract[]>, translator: any): Result<Model[]> {
        const newResult: Result<Model[]> = this.getResultModel<Contract[], Model[]>(result);
        newResult.response = result?.response.map((contract: Contract) => translator.translateContractToModel(contract));
        return newResult;
    }

    static buildResultModel<Contract, Model>(result: Result<Contract>, translator: any): Result<Model> {
        const newResult: Result<Model> = this.getResultModel<Contract, Model>(result);
        newResult.response = translator.translateContractToModel(result?.response);
        return newResult;
    }
}
