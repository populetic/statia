import { DatePipe } from '@angular/common';

export class DateFunctions {

    private static  datePipe = new DatePipe('en-GB');

    static CalculateDaysDiff(firstDate: Date, secondDate: Date): number {
        return Math.floor((Date.UTC(secondDate.getFullYear(), secondDate.getMonth(), secondDate.getDate()) -
            Date.UTC(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate())) / (1000 * 60 * 60 * 24));
    }

    static formatDate(date: Date | string): string {
        return this.datePipe.transform(date, 'yyyy-MM-dd');
    }

}
