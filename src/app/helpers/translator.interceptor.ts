import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AirlineReasonContract } from '@app/api/contracts/airline-reason-contract/airline-reason.contract';
import { AirlineContract } from '@app/api/contracts/airlines-contract/airline.contract';
import { AirportContract } from '@app/api/contracts/airport-contract/airport.contract';
import { BillContract } from '@app/api/contracts/bill-contract/bill.contract';
import { CityContract } from '@app/api/contracts/city-contract/city.contract';
import { ClaimContract } from '@app/api/contracts/claim-contract/claim.contract';
import { ClientContract } from '@app/api/contracts/clientscontract/client.contract';
import { CompanionContract } from '@app/api/contracts/companion-contract/companion.contract';
import { CountryContract } from '@app/api/contracts/country-contract/country.contract';
import { CourtNumberContract } from '@app/api/contracts/court-contract/court-number.contract';
import { CourtContract } from '@app/api/contracts/court-contract/court.contract';
import { DemandContract } from '@app/api/contracts/demand-contract/demand.contract';
import { DepartmentContract } from '@app/api/contracts/department-contract/department.contract';
import { FlightContract } from '@app/api/contracts/flight-contract/flight.contract';
import { LanguageContract } from '@app/api/contracts/language-contract/language.contract';
import { LawsuiteTypeContract } from '@app/api/contracts/lawsuite-type-contract/lawsuite-type.contract';
import { LegalGuardianContract } from '@app/api/contracts/legal-guadian-contract/legal-guardian.contract';
import { LoginContract } from '@app/api/contracts/login-contract/login.contract';
import { LuggageIssueMomentContract } from '@app/api/contracts/luggage-issue-moment-contract/luggage-issue-moment.contract';
import { MenuContract } from '@app/api/contracts/menu-contract/menu.contract';
import { OptionContract } from '@app/api/contracts/option-contract/option.contract';
import { ProvinceContract } from '@app/api/contracts/province-contract/province.contract';
import { RoleContract } from '@app/api/contracts/role-contract/role.contract';
import { UserContract } from '@app/api/contracts/user-contract/user.contract';
import { AirlineAlternativeContract } from '@app/pages/company/auxiliaries/airline-alternatives/airline-alternative.contract';
import { AirlineAlternative } from '@app/pages/company/auxiliaries/airline-alternatives/airline-alternative.model';
import { AirlineAlternativeTranslator } from '@app/pages/company/auxiliaries/airline-alternatives/airline-alternative.translator';
import { AirlineReason } from '@app/pages/company/auxiliaries/airline-reasons/airline-reason.model';
import { AirlineReasonTranslator } from '@app/pages/company/auxiliaries/airline-reasons/airline-reason.translator';
import { Airline } from '@app/pages/company/auxiliaries/airlines/airline.model';
import { AirlineTranslator } from '@app/pages/company/auxiliaries/airlines/airline.translator';
import { Airport } from '@app/pages/company/auxiliaries/airports/airport.model';
import { AirportTranslator } from '@app/pages/company/auxiliaries/airports/airport.translator';
import { City } from '@app/pages/company/auxiliaries/cities/city.model';
import { CityTranslator } from '@app/pages/company/auxiliaries/cities/city.translator';
import { Country } from '@app/pages/company/auxiliaries/countries/country.model';
import { CountryTranslator } from '@app/pages/company/auxiliaries/countries/country.translator';
import { DocumentationTypeContract } from '@app/pages/company/auxiliaries/document-types/documentation-type.contract';
import { DocumentationType } from '@app/pages/company/auxiliaries/document-types/documentation-type.model';
import { DocumentationTypeTranslator } from '@app/pages/company/auxiliaries/document-types/documentation-type.translator';
import { Language } from '@app/pages/company/auxiliaries/languages/language.model';
import { LanguageTranslator } from '@app/pages/company/auxiliaries/languages/language.translator';
import { LawsuiteType } from '@app/pages/company/auxiliaries/lawsuite-types/lawsuite-type.model';
import { LawsuiteTypeTranslator } from '@app/pages/company/auxiliaries/lawsuite-types/lawsuite-type.translator';
import { LuggageIssueMoment } from '@app/pages/company/auxiliaries/luggage-issue-moment/luggage-issue-moment.model';
import { LuggageIssueMomentTranslator } from '@app/pages/company/auxiliaries/luggage-issue-moment/luggage-issue-moment.translator';
import { Province } from '@app/pages/company/auxiliaries/provinces/province.model';
import { ProvinceTranslator } from '@app/pages/company/auxiliaries/provinces/province.translator';
import { Bill } from '@app/pages/company/bills/bill.model';
import { BillTranslator } from '@app/pages/company/bills/bill.translator';
import { Claim } from '@app/pages/company/claims/claim.model';
import { ClaimTranslator } from '@app/pages/company/claims/claim.translator';
import { Client } from '@app/pages/company/clients/client.model';
import { ClientTranslator } from '@app/pages/company/clients/client.translator';
import { Companion } from '@app/pages/company/companions/companion.model';
import { CompanionTranslator } from '@app/pages/company/companions/companion.translator';
import { ConnectionFlightContract } from '@app/pages/company/connection-flights/connection-flight.contract';
import { ConnectionFlight } from '@app/pages/company/connection-flights/connection-flight.model';
import { ConnectionFlightTranslator } from '@app/pages/company/connection-flights/connection-flight.translator';
import { CourtNumber } from '@app/pages/company/courts/court-number/court-number.model';
import { CourtNumberTranslator } from '@app/pages/company/courts/court-number/court-number.translator';
import { Court } from '@app/pages/company/courts/court.model';
import { CourtTranslator } from '@app/pages/company/courts/court.translator';
import { Demand } from '@app/pages/company/demands/demand.model';
import { DemandTranslator } from '@app/pages/company/demands/demand.translator';
import { Flight } from '@app/pages/company/flights/flight.model';
import { FlightTranslator } from '@app/pages/company/flights/flight.translator';
import { LegalGuardian } from '@app/pages/company/legal-guardians/legal-guardian.model';
import { LegalGuardianTranslator } from '@app/pages/company/legal-guardians/legal-guardian.translator';
import { Login } from '@app/pages/login/login.model';
import { LoginTranslator } from '@app/pages/login/login.translator';
import { Department } from '@app/pages/management/departments/department.model';
import { DepartmentTranslator } from '@app/pages/management/departments/department.translator';
import { Role } from '@app/pages/management/roles/role.model';
import { RoleTranslator } from '@app/pages/management/roles/role.translator';
import { User } from '@app/pages/management/users/user.model';
import { UserTranslator } from '@app/pages/management/users/user.translator';
import { Menu } from '@app/pages/statia-configuration/menus/menu.model';
import { MenuTranslator } from '@app/pages/statia-configuration/menus/menu.translator';
import { Option } from '@app/pages/statia-configuration/options/option.model';
import { OptionTranslator } from '@app/pages/statia-configuration/options/option.translator';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class TranslatorInterceptor implements HttpInterceptor {

    constructor(
        private translate: TranslateService
    ) { }

    intercept(request: HttpRequest<any> , next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {

                    const message: string = (event && event.body && event.body.message) ? event.body.message : null;
                    if (message) {
                        switch (message) {

                            case 'getAllMenusOk':
                                break;
                            case 'getMenusByRoleOk':
                                // event.body.response = this.translateMenuContracts(event.body.response);
                                break;
                            case 'getMenuByIdOk':
                                // event.body.response = this.translateMenuContract(event.body.response);
                                break;
                            case 'getAllOptionsOk':
                                // event.body.response = this.translateOptionContracts(event.body.response);
                                break;
                            case 'getOptionByIdOk':
                                // event.body.response = this.translateOptionContract(event.body.response);
                                break;
                            case 'getMenuRoleOptionsOk':
                                // event.body.response = this.translateOptionContractsByRole(request, event.body.response);
                                break;
                            case 'getAllRolesOk':
                                // event.body.response = this.translateRoleContracts(event.body.response);
                                break;
                            case 'getRoleByIdOk':
                                // event.body.response = this.translateRoleContract(event.body.response);
                                break;
                            // Department
                            case 'getAllDepartmentsOk':
                                // event.body.response = this.translateDepartmentContracts(event.body.response);
                                break;
                            case 'getDepartmentByIdOk':
                                // event.body.response = this.translateDepartmentContract(event.body.response);
                                break;
                            // User
                            case 'getUsersOk':
                                // event.body.response = this.translateUserContracts(event.body.response);
                                break;
                            case 'getUserByIdOk':
                                // event.body.response = this.translateUserContract(event.body.response);
                                break;
                            // Client
                            case 'getAllClientsOk':
                                // event.body.response = this.translateClientContracts(event.body.response);
                                break;
                            // case 'getClientByIdOk':
                            case 'editClientOk':
                                // event.body.response = this.translateClientContract(event.body.response);
                                break;
                            // Claim
                            case 'getAllClaimsOk':
                            case 'getClientRelatedClaimsOk':
                                // event.body.response = this.translateClaimContracts(event.body.response);
                                break;
                            case 'getClaimByIdOk':
                            case 'getAllByEmailAndStatusOk':
                                 // event.body.response = this.translateClaimContract(event.body.response);
                                break;
                            // Session
                            case 'loginUserOk':
                                // event.body.response = this.translateSessionContract(event.body.response);
                                break;
                            // Language
                            case 'getAllLanguagesOk':
                                // event.body.response = this.translateLanguageContracts(event.body.response);
                                break;
                            case 'getLanguageByIdOk':
                                // event.body.response = this.translateLanguageContract(event.body.response);
                                break;
                            // Country
                            case 'getAllCountriesOk':
                                // event.body.response = this.translateCountryContracts(event.body.response);
                                break;
                            case 'getCountryByIdOk':
                                // event.body.response = this.translateCountryContract(event.body.response);
                                break;
                            // Province
                            case 'getAllCountryProvincesOk':
                                // event.body.response = this.translateProvinceContracts(event.body.response);
                                break;
                            case 'getCountryProvinceByIdOk':
                                // event.body.response = this.translateProvinceContract(event.body.response);
                                break;
                            // DocumentationType
                            case 'getAllDocumentationTypesOk':
                                // event.body.response = this.translateDocumentationTypeContracts(event.body.response);
                                break;
                            case 'getDocumentationTypeByIdOk':
                                // event.body.response = this.translateDocumentationTypeContract(event.body.response);
                                break;
                            // LegalGuardian
                            case 'getAllLegalGuardiansOk':
                                // event.body.response = this.translateLegalGuardianContracts(event.body.response);
                                break;
                            case 'getLegalGuardianByIdOk':
                            case 'createLegalGuardianOk':
                            case 'editLegalGuardianOk':
                                // event.body.response = this.translateLegalGuardianContract(event.body.response);
                                break;
                            // Airlines
                            case 'getAllAirlinesOk':
                                // event .body.response = this.translateAirlineContracts(event.body.response);
                                break;
                            case 'getAirlineByIdOk':
                                // event.body.response = this.translateAirlineContract(event.body.response);
                                break;
                            case 'getAllAirportsOk':
                                // event.body.response = this.translateAirportContracts(event.body.response);
                                break;
                            case 'getAirportByIdOk':
                                // event.body.response = this.translateAirportContract(event.body.response);
                                break;
                            // Lawsuite type
                            case 'getAllLawsuiteTypesOk':
                                // event.body.response = this.translateLawsuiteTypeContracts(event.body.response);
                                break;
                            case 'getLawsuiteTypeByIdOk':
                                // event.body.response = this.translateLawsuiteTypeContract(event.body.response);
                                break;
                            // Airline reason
                            case 'getAllAirlineReasonsOk':
                                // event.body.response = this.translateAirlineReasonContracts(event.body.response);
                                break;
                            case 'getAirlineReasonByIdOk':
                                // event.body.response = this.translateAirlineReasonContract(event.body.response);
                                break;
                            // Airline alternative
                            case 'getAllAirlineAlternativesOk':
                                // event.body.response = this.translateAirlineAlternativeContracts(event.body.response);
                                break;
                            case 'getAirlineAlternativeByIdOk':
                                // event.body.response = this.translateAirlineAlternativeContract(event.body.response);
                                break;
                            // Flights
                            case 'getAllFlightsOk':
                                // event.body.response = this.translateFlightContracts(event.body.response);
                                break;
                            case 'getFlightByIdOk':
                            case 'editFlightOk':
                                // event.body.response = this.translateFlightContract(event.body.response);
                                break;
                            // Connection Flights
                            case 'getAllConnectionFlightsOk':
                                // event.body.response = this.translateConnectionFlightContracts(event.body.response);
                                break;
                            case 'getConnectionFlightByIdOk':
                                // event.body.response = this.translateConnectionFlightContract(event.body.response);
                                break;
                            // Luggage Issue Moment
                            case 'getAllLuggageIssueMomentsOk':
                                // event.body.response = this.translateLuggageIssueMomentContracts(event.body.response);
                                break;
                            case 'getLuggageIssueMomentByIdOk':
                                // event.body.response = this.translateLuggageIssueMomentContract(event.body.response);
                                break;
                            // Demands
                            case 'getClientRelatedDemandsOk':
                                // event.body.response = this.translateDemandContracts(event.body.response);
                                break;
                            case 'getAllDemandsOk':
                                // event.body.response = this.translateDemandContracts(event.body.response);
                                break;
                            case 'getDemandByIdOk':
                            case 'editDemandOk':
                                // event.body.response = this.translateDemandContract(event.body.response);
                                break;
                            // Bills
                            case 'getClientRelatedBillsOk':
                                // event.body.response = this.translateBillContract(event.body.response);
                                break;
                            // Courts
                            case 'getAllCourtsOk':
                                // event.body.response = this.translateCourtContracts(event.body.response);
                                break;
                            case 'getAllCourtNumbersOk':
                                // event.body.response = this.translateCourtNumberContracts(event.body.response);
                                break;
                            case 'getCourtNumberByIdOk':
                            case 'editCourtNumberOk':
                            case 'createCourtNumberOk':
                                // event.body.response = this.translateCourtNumberContract(event.body.response);
                                break;
                            // Companions
                            case 'getAllCompanionsByMainClaimIdOk':
                                // event.body.response = this.translateCompanionContracts(event.body.response);
                                break;
                            case 'getAllPossibleSameFlightTravelersByClaimIdOk':
                                // event.body.response = this.translatePossibleCompanionContracts(event.body.response);
                                break;
                            // Cities
                            case 'getAllCitiesOk':
                                // event.body.response = this.translateCityContracts(event.body.response);
                                break;
                            case 'getCityByIdOk':
                                // event.body.response = this.translateCityContract(event.body.response);
                                break;

                            default:
                                break;
                        }
                    }
                }
                return event;
            }));
        }

    private translateMenuContracts(contract: MenuContract[]): Menu[] {
        return contract.map((c: MenuContract) => MenuTranslator.translateContractToModel(c));
    }

    private translateMenuContract(contract: MenuContract): Menu {
        return MenuTranslator.translateContractToModel(contract);
    }

    private translateOptionContracts(contract: OptionContract[]): Option[] {
        return contract.map((c: OptionContract) => OptionTranslator.translateContractToModel(c));
    }

    translateOptionContractsByRole(request: HttpRequest<any>, contract: any): Option[] | Menu {
        if (request && request.body) {
            const body: string = request.body.replace('json=', '');
            const param: any = JSON.parse(body);
            if (param && !param.menuId) { // menuId is optional
                return contract.map((menu: MenuContract) => MenuTranslator.translateContractToModel(menu));
            } else {
                return contract.map((c: OptionContract) => OptionTranslator.translateContractToModel(c));
            }
        }
        return [];
    }

    private translateOptionContract(contract: OptionContract): Option {
        return OptionTranslator.translateContractToModel(contract);
    }

    private translateRoleContracts(contract: RoleContract[]): Role[] {
        return contract.map((c: RoleContract) => RoleTranslator.translateContractToModel(c));
    }

    private translateRoleContract(contract: RoleContract): Role {
        return RoleTranslator.translateContractToModel(contract);
    }

    private translateDepartmentContracts(contract: DepartmentContract[]): Department[] {
        return contract.map((c: DepartmentContract) => DepartmentTranslator.translateContractToModel(c));
    }

    private translateDepartmentContract(contract: DepartmentContract): Department {
        return DepartmentTranslator.translateContractToModel(contract);
    }

    private translateUserContracts(contract: UserContract[]): User[] {
        return contract.map((c: UserContract) => UserTranslator.translateContractToModel(c));
    }

    private translateUserContract(contract: UserContract): User {
        return UserTranslator.translateContractToModel(contract);
    }

    private translateClientContracts(contract: ClientContract[]): Client[] {
        return contract.map((c: ClientContract) => ClientTranslator.translateContractToModel(c));
    }

    private translateClientContract(contract: ClientContract): Client {
        return ClientTranslator.translateContractToModel(contract);
    }

    private translateClaimContracts(contract: ClaimContract[]): Claim[] {
        return contract.map((c: ClaimContract) => ClaimTranslator.translateContractToModel(c));
    }

    private translateClaimContract(contract: ClaimContract): Claim {
        return ClaimTranslator.translateContractToModel(contract);
    }

    private translateSessionContract(contract: LoginContract): Login {
        return LoginTranslator.translateContractToModel(contract);
    }

    private translateLanguageContracts(contract: LanguageContract[]): Language[] {
        return contract.map((c: LanguageContract) => LanguageTranslator.translateContractToModel(c));
    }

    private translateLanguageContract(contract: LanguageContract): Language {
        return LanguageTranslator.translateContractToModel(contract);
    }

    private translateCountryContracts(contract: CountryContract[]): Country[] {
        return contract.map((c: CountryContract) => CountryTranslator.translateContractToModel(c));
    }

    private translateCountryContract(contract: CountryContract): Country {
        return CountryTranslator.translateContractToModel(contract);
    }

    private translateProvinceContracts(contract: ProvinceContract[]): Province[] {
        return contract.map((c: ProvinceContract) => ProvinceTranslator.translateContractToModel(c));
    }

    private translateProvinceContract(contract: ProvinceContract): Province {
        return ProvinceTranslator.translateContractToModel(contract);
    }

    private translateDocumentationTypeContracts(contract: DocumentationTypeContract[]): DocumentationType[] {
        return contract.map((c: DocumentationTypeContract) => DocumentationTypeTranslator.translateContractToModel(c));
    }

    private translateDocumentationTypeContract(contract: DocumentationTypeContract): DocumentationType {
        return DocumentationTypeTranslator.translateContractToModel(contract);
    }

    private translateLegalGuardianContracts(contract: LegalGuardianContract[]): LegalGuardian[] {
        return contract.map((c: LegalGuardianContract) => LegalGuardianTranslator.translateContractToModel(c));
    }

    private translateLegalGuardianContract(contract: LegalGuardianContract): LegalGuardian {
        return LegalGuardianTranslator.translateContractToModel(contract);
    }

    private translateAirlineContracts(contract: AirlineContract[]): Airline[] {
        return contract.map((c: AirlineContract) => AirlineTranslator.translateContractToModel(c));
    }

    private translateAirlineContract(contract: AirlineContract): Airline {
        return AirlineTranslator.translateContractToModel(contract);
    }

    private translateAirportContracts(contract: AirportContract[]): Airport[] {
        return contract.map((c: AirportContract) => AirportTranslator.translateContractToModel(c));
    }

    private translateAirportContract(contract: AirportContract): Airport {
        return AirportTranslator.translateContractToModel(contract);
    }

    private translateLawsuiteTypeContracts(contract: LawsuiteTypeContract[]): LawsuiteType[] {
        return contract.map((c: LawsuiteTypeContract) => LawsuiteTypeTranslator.translateContractToModel(c));
    }

    private translateLawsuiteTypeContract(contract: LawsuiteTypeContract): LawsuiteType {
        return LawsuiteTypeTranslator.translateContractToModel(contract);
    }

    private translateAirlineReasonContracts(contract: AirlineReasonContract[]): AirlineReason[] {
        return contract.map((c: AirlineReasonContract) => AirlineReasonTranslator.translateContractToModel(c));
    }

    private translateAirlineReasonContract(contract: AirlineReasonContract): AirlineReason {
        return AirlineReasonTranslator.translateContractToModel(contract);
    }

    private translateAirlineAlternativeContracts(contract: AirlineAlternativeContract[]): AirlineAlternative[] {
        return contract.map(
            (c: AirlineAlternativeContract) => AirlineAlternativeTranslator.translateContractToModel(c));
    }

    private translateAirlineAlternativeContract(contract: AirlineAlternativeContract): AirlineAlternative {
        return AirlineAlternativeTranslator.translateContractToModel(contract);
    }

    private translateFlightContracts(contract: FlightContract[]): Flight[] {
        return contract.map(
            (c: FlightContract) => FlightTranslator.translateContractToModel(c));
    }

    private translateFlightContract(contract: FlightContract): Flight {
        return FlightTranslator.translateContractToModel(contract);
    }

    private translateConnectionFlightContracts(contract: ConnectionFlightContract[]): ConnectionFlight[] {
        return contract.map(
            (c: ConnectionFlightContract) => ConnectionFlightTranslator.translateContractToModel(c));
    }

    private translateConnectionFlightContract(contract: ConnectionFlightContract): ConnectionFlight {
        return ConnectionFlightTranslator.translateContractToModel(contract);
    }

    private translateLuggageIssueMomentContracts(contract: LuggageIssueMomentContract[]): LuggageIssueMoment[] {
        return contract.map(
            (c: LuggageIssueMomentContract) => LuggageIssueMomentTranslator.translateContractToModel(c));
    }

    private translateLuggageIssueMomentContract(contract: LuggageIssueMomentContract): LuggageIssueMoment {
        return LuggageIssueMomentTranslator.translateContractToModel(contract);
    }

    translateBillContracts(contract: BillContract[]): Bill[] {
        return contract.map((c: BillContract) => BillTranslator.translateContractToModel(c));
    }

    private translateDemandContracts(contract: DemandContract[]): Demand[] {
        return contract.map((c: DemandContract) => DemandTranslator.translateContractToModel(c, this.translate));
    }

    private translateDemandContract(contract: DemandContract): Demand {
        return DemandTranslator.translateContractToModel(contract, this.translate);
    }

    private translateBillContract(contract: BillContract[]): Bill[] {
        return contract.map((c: BillContract) => BillTranslator.translateContractToModel(c));
    }

    translateCourtContract(contract: CourtContract): Court {
        return CourtTranslator.translateContractToModel(contract, this.translate);
    }

    translateCourtNumberContract(contract: CourtNumberContract): Court {
        return CourtNumberTranslator.translateContractToModel(contract);
    }
    private translateCompanionContracts(contract: CompanionContract[]): Companion[] {
        return contract.map((c: CompanionContract) => CompanionTranslator.translateContractCompanionToModel(c));
    }

    private translatePossibleCompanionContracts(contract: CompanionContract[]): Companion[] {
        return contract.map((c: CompanionContract) => CompanionTranslator.translateContractPossibleCompanionToModel(c));
    }

    private translateCityContracts(contract: CityContract[]): City[] {
        return contract.map((c: CityContract) => CityTranslator.translateContractToModel(c));
    }

    private translateCityContract(contract: CityContract): City {
        return CityTranslator.translateContractToModel(contract);
    }

    private translateCourtContracts(contract: CourtContract[]): Court[] {
        return contract.map((c: CourtContract) => CourtTranslator.translateContractToModel(c, this.translate));
    }

    private translateCourtNumberContracts(contract: CourtNumberContract[]): CourtNumber[] {
        return contract.map((c: CourtNumberContract) => CourtNumberTranslator.translateContractToModel(c));
    }

}
