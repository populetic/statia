import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    constructor() { }

    intercept(request: HttpRequest<any> , next: HttpHandler): Observable<HttpEvent<any>> {

        return next.handle(request).pipe(
            // TODO:
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    if (event && event.body && event.body.status === 0) {
                    }
                }
                if (event instanceof HttpErrorResponse) {
                }
                return event;
            }));
        }
}
