import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Login } from '@app/pages/login/login.model';
import { SessionService } from '@app/shared/services/session.service';
import { Observable } from 'rxjs';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(private sessionService: SessionService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (this.sessionService.isUserLogged) {
            const user: Login = this.sessionService.user;
            request = request.clone({
                setHeaders: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': (user && user.token) ? user.token : null
                }
            });
        } else { // Login
            request = request.clone({
                setHeaders: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        }
        return next.handle(request);
    }
}
